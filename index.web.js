import React from 'react';
import { render } from 'react-dom';
import { store } from './app/runtime';
import { Provider } from 'react-redux';
import './antd.css';
import './app/modules/toastWhenNetInfoChange';
// import { AppContainer as HotReloader } from 'react-hot-loader';
import routes from './routes';
const rootStore = store({});
const rootElement = document.getElementById('root');
const content = (
    <Provider store={rootStore}>
        {routes}
    </Provider>
);
render(content, rootElement);

// render(<HotReloader>{content}</HotReloader>, rootElement);

// if (module.hot) module.hot.accept('./routes', rootElement);
