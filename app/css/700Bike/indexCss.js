const css = {
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    backgroundColor:'#F6F6F6',
    //backgroundColor:'#fff'
  },
  topNav:{
        height: 54,
        backgroundColor: '#fff',
        position: 'relative',
        borderBottomColor: '#7d7d7d',
        borderBottomWidth: 1,
        borderBottomStyle: 'solid',
        flexShrink: 0,
    },
    vcbox :{
        alignItems:'center',
        height:54,
        flexGrow:1,
        flexBasis:0,
        justifyContent: 'space-between',
    },
    backBtn:{
        paddingLeft:10,
        paddingRight:15,
        height:54,
        alignItems:'center',
        justifyContent: 'center',
    },
    arrow:{
        width:10,
        height:16
    },
  mapIcon:{
      width:95,
      height:54,
      justifyContent:'center',
      alignItems:'flex-end',
      paddingRight:5
  },
  listBox:{
      flexGrow:1,
  },
  innerBox:{
      height:44,
      backgroundColor:'#fff',
      marginTop:10,
      borderStyle:'solid',
      borderColor:'#d6d6d6',
      borderBottomWidth:1,
      borderTopWidth:1,
      borderLeftWidth:0,
      borderRightWidth:0,
      flexShrink:0
  },
  li:{
      flexGrow:1,
      height:44,
    //   flexBasis:0,
      justifyContent:'center',
      alignItems:'center'
  },
  searchBox:{
      height:54,
      alignItems: 'center',
      flexGrow:1,
      flexBasis: 0,
  },
  leftBox:{
      flexGrow:1,
      flexBasis: 0,
      position:'relative',
      justifyContent:'space-between',
      borderRadius:4,
      backgroundColor:"#ebebeb",
      height:44
  },
  serachInput:{
      height:44,
      fontSize:16,
      backgroundColor:"#ebebeb",
      flexGrow:1,
      flexBasis: 0,
      borderStyle:'solid',
      borderWidth:1,
      borderColor:"#ebebeb",
      paddingLeft:5
  },
  searchBtn:{
      height:44,
      width:40,
      position:"relative"
  },
  search:{
      width:20,
      height:20, 
      position:"absolute",
      left:10,
      top:12,
  },
  distanceBox:{
      height:44,
      backgroundColor:'#fff',
      borderStyle:'solid',
      borderColor:'#d6d6d6',
      borderBottomWidth:1,
      borderTopWidth:0,
      borderLeftWidth:0,
      borderRightWidth:0,
      flexShrink:0
  },
  distance:{
      flexGrow:1,
      flexBasis:0,
      height:44,
      justifyContent:'center',
      position:'relative',
  },
  line:{
      width:10,
      height:13,
      borderStyle:'solid',
      borderColor:'#d6d6d6',
      borderBottomWidth:0,
      borderTopWidth:0,
      borderLeftWidth:0,
      borderRightWidth:1,
      position:'absolute',
      right:0,
      top:15.5
  },
  item:{
      borderStyle:'solid',
      borderColor:'#d6d6d6',
      borderBottomWidth:1,
      borderTopWidth:1,
      borderLeftWidth:0,
      borderRightWidth:0,
      backgroundColor:'#fff',
      padding:13,
      marginTop:10
  },
  battery:{
      width:50,
      height:50,
      justifyContent:'center',
      alignItems:'center',
      borderRadius:25,
  },
  carno:{
      color:'#333',
      fontSize:18,
      marginBottom:8,
      marginTop:2
  },
  filterBtn:{
      width:90,
      height:26,
      borderRadius:8,
      justifyContent:'center',
      alignItems:'center'
  }
  
};

export default css;
