const css = {
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    backgroundColor:'#fff',
    //backgroundColor:'#fff'
  },
  item:{
      borderStyle:'solid',
      borderColor:'#d6d6d6',
      borderBottomWidth:1,
      borderTopWidth:1,
      borderLeftWidth:0,
      borderRightWidth:0,
      backgroundColor:'#fff',
      padding:13,
  },
  battery:{
      width:50,
      height:50,
      justifyContent:'center',
      alignItems:'center',
      borderRadius:25,
  },
  carno:{
      color:'#333',
      fontSize:18,
      marginBottom:8,
      marginTop:2
  },
  itemBot:{
      paddingLeft:20,
      paddingRight:20,
      paddingTop:10,
      paddingBottom:10,
      borderStyle:"solid",
      borderBottomWidth:1,
      borderColor:"#d6d6d6",
      borderTopWidth:0,
      borderLeftWidth:0,
      borderRightWidth:0,
      flex:1,
      alignItems:'flex-start'
  },
  icon:{
      width:11,
      height:15,
  },
  
};

export default css;
