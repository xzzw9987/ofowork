const css = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor:'#F6F6F6',
    },
    nav: {
        backgroundColor: '#fff',
        borderStyle: 'solid',
        borderBottomColor: '#7d7d7d',
        borderBottomWidth:1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
    },
    inner: {
        alignItems: 'center',
        justifyContent: 'center',
        flexGrow: 1,
        height: 44,
    },
    item1: {
        paddingLeft: 20,
        paddingRight: 20,
        borderBottomColor:'#FAD500',
        borderBottomWidth: 4,
        borderStyle: 'solid',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        height: 40,
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    item2: {
        height: 44,
        paddingBottom: 4,
        flexGrow: 1,
        paddingLeft: 20,
        paddingRight: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text1: {
        alignSelf: 'center',
        flexGrow: 1,

    },
    text2: {
        alignSelf: 'center',
        flexGrow: 1,
        color: '#999',
    },
    listView:{
        flexGrow: 1,
        flexBasis: 0,
        backgroundColor: '#fff',
    },
    list: {
        borderBottomColor: '#d6d6d6',
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        marginLeft: 10,
        paddingTop: 20,
        paddingRight: 15,
        paddingBottom: 8,
    },
    firstHorizontal: {
        justifyContent: 'space-between',
        position: 'relative',
        marginBottom: 8,
    },
    firstHorizontalText1: {
        alignSelf: 'flex-start',
        fontSize: 18,
    },
    reasons: {
        paddingLeft: 4,
        paddingRight: 4,
        paddingTop: 2,
        paddingBottom: 2,
        marginBottom: 8,
        marginRight: 10,
        borderColor:"#ff7000",
        borderStyle: 'solid',
        borderWidth:1,
        borderRadius: 2,
    },
    reasonItem: {
        color: '#ff7000',
        fontSize: 12,
    },
    address: {
        fontSize: 14,
        flexWrap: 'wrap',
        marginBottom: 8,
    },
    time: {
        color: '#999',
        fontSize: 14,
        position: 'absolute',
        right: 0,
        top: 3,
    },
    reminder1: {
        width: 8,
        height: 8,
        borderRadius: 8,
        backgroundColor: '#ff7000',
        position: 'absolute',
        top: -1,
        right: -12,
    },
    resultTab: {
        borderRadius: 2,
    },
    pending: {
        fontSize: 14,
        backgroundColor: '#ffd900',
        paddingLeft: 3,
        paddingRight: 3,
    },
    name: {
        fontSize: 14,
        marginLeft: 5,
    },
};
export default css;