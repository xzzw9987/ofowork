const css = {
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    backgroundColor:'#fff'
  },
  bottomInfo:{
      position:'relative',
      alignItems: 'center',
  },   

  infoBox:{
      backgroundColor:'#fff',
      borderStyle:"solid",
      borderColor:'#d6d6d6',
      borderBottomWidth:0,
      borderTopWidth:1,
      borderLeftWidth:0,
      borderRightWidth:0,
  },
  yuan:{
      width:70,
      height:70,
      borderRadius:35,
      borderStyle:"solid",
      borderColor:'#FFD900',
      borderBottomWidth:1,
      borderTopWidth:1,
      borderLeftWidth:1,
      borderRightWidth:1,
      position: 'relative',
      background: '#fff',
      zIndex: 9999,
      marginTop:-30,
      justifyContent:'center',
      alignItems:'center'
  },
  center:{
      paddingTop:10,
      alignItems:'center'
  },
  h1:{
      fontSize:24
  },
  color2:{
      color:"#666",
      fontSize:14
  },
  lineBox:{
      height:14,
      borderStyle:"solid",
      borderBottomWidth:0,
      borderColor:"#666",
      borderTopWidth:0,
      borderLeftWidth:1,
      borderRightWidth:0,
      marginLeft:10,
      paddingLeft:10,
      alignItems:'center'
  },
  //原因
  reasonBox:{
      marginTop:10,
      marginBottom:5,
      flex:1,
      flexWrap:"wrap",
      justifyContent:"center",
  },
  reason:{
      borderStyle:"solid",
      borderRadius:2,
      borderColor:"#ff7000",
      borderWidth:1,
      justifyContent:"center",
      alignItems:"center",
      paddingLeft:8,
      paddingRight:8,
      height:17,
      marginRight:10,
      marginBottom:5,
      flexShrink:0
  },
  reasonItem:{
      fontSize:14,
      color:"#ff7000"
  },
  
  itemBot:{
      paddingLeft:20,
      paddingRight:20,
      paddingTop:10,
      borderStyle:"solid",
      borderBottomWidth:0,
      borderColor:"#d6d6d6",
      borderTopWidth:1,
      borderLeftWidth:0,
      borderRightWidth:0,
      marginTop:10,
      flex:1,
      alignItems:'flex-start'
  },
  arrow:{
      width:10,
      height:16
  },
  icon:{
      width:11,
      height:15
  },
  address:{
      flex:1,
      paddingLeft:10,
      paddingRight:10,
      paddingBottom:10,
  },
  //footer
  
  btnBox:{
      backgroundColor:'#fff',
      paddingLeft:20,
      paddingRight:20,
      borderStyle:"solid",
      borderBottomWidth:0,
      borderColor:"#d6d6d6",
      borderTopWidth:1,
      borderLeftWidth:0,
      borderRightWidth:0,
      justifyContent:"center",
  },
  btn:{
      height:48,
      borderRadius:6,
      backgroundColor:'#333',
      flexGrow:1,
      justifyContent:"center",
      alignItems:"center",
      marginTop:16,
      marginBottom:16
  }
};

export default css;
