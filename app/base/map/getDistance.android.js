import {NativeModules} from 'react-native';
const RCTLocationManager = NativeModules.LocationManager;
export default (from, to)=> new Promise(resolve=> RCTLocationManager.distance(from, to, d=>resolve(d)));
