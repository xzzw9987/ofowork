import React, {Component} from 'react';
import ReactDOM, {render} from 'react-dom';

export default class Map extends Component {

    constructor(props) {
        super(props);
        this.heatMap = null;
    }

    moveToTarget(oldProps) {
        if (!this.amap) return;

        const region = this.props.region;
        if (region) {
            const southWest = new AMap.LngLat(region.longitude - region.longitudeDelta, region.latitude - region.latitudeDelta);
            const northEast = new AMap.LngLat(region.longitude + region.longitudeDelta, region.latitude + region.latitudeDelta);
            this.amap.setBounds(new AMap.Bounds(southWest, northEast));
        }

        this.amap.clearMap();

        const annotations = this.props.annotations;
        if (annotations) {
            var infoWindow = new AMap.InfoWindow({offset: new AMap.Pixel(0, -30)});
            annotations.forEach((item) => {
                const marker = new AMap.Marker({
                    position: [item.longitude, item.latitude],
                    icon: item.icon ? new AMap.Icon({
                        image: item.icon,
                        size: new AMap.Size(item.size ? item.size.width : 24, item.size ? item.size.height : 24)
                    }) : null,
                    title: item.title,
                    animation: item.animation ? item.animation : null,
                    clickable: !!this.props.onAnnotationClicked
                });
                marker.setMap(this.amap);
                marker.emit('click', {target: marker});
                AMap.event.addListener(marker, 'click', (e) => {
                    if (this.props.onAnnotationClicked) {
                        infoWindow.setContent(e.target.getTitle());
                        infoWindow.open(this.amap, e.target.getPosition());
                        this.props.onAnnotationClicked(item.key);
                    }
                });

                if(item.number){
                    setTimeout(()=>{
                        const dom = marker.getContentDom();
                        dom.setAttribute('data-label', item.number);
                    }, 100);
                }

            });
        }

        const hotSpots = this.props.hotSpots;
        if (hotSpots) {
            this.amap.plugin(['AMap.Heatmap'], () => {
                if (!this.heatMap)
                    this.heatMap = new AMap.Heatmap(this.amap, {
                        radius: 25,
                        opacity: [0, .8],
                        gradient: {
                            0.5: 'blue',
                            0.65: 'rgb(117,211,248)',
                            0.7: 'rgb(0, 255, 0)',
                            0.9: '#ffea00',
                            1.0: 'red'
                        }
                    });
                this.heatMap.setDataSet({
                    max: Math.max.apply(null, hotSpots.map(v=>v.count)) || 10,
                    data: hotSpots.map(v => ({...v, lat: v.latitude, lng: v.longitude}))
                });
            });
        }

        // 拖拽地图
        // const positionPicker = this.props.positionPicker;
        // if (positionPicker) {
        //     AMapUI.loadUI(['misc/PositionPicker'], function(PositionPicker) {
        //         this.amap = new AMap.Map(this.amap, {
        //             zoom: 16
        //         });
        //         var positionPicker = new PositionPicker({
        //             mode: 'dragMap', //设定为拖拽地图模式，可选'dragMap'、'dragMarker'，默认为'dragMap'
        //             map: this.amap//依赖地图对象
        //         });
        //         //TODO:事件绑定、结果处理等
        //     });
        // }

        if (this.props.showsUserLocation || this.props.onLocationChange) {
            if (oldProps && ((!!oldProps.showsUserLocation) !== (!!this.props.showsUserLocation)) && this.positionWatcher) { // old positionWatcher needs to be deleted. We have to create another one with different options
                this.removePositionWatcher();
            }
            if (!this.positionWatcher) {
                this.positionWatcher = new AMap.Geolocation({
                    enableHighAccuracy: true,
                    convert: true,
                    showButton: !!this.props.showsUserLocation,
                    showMarker: !!this.props.showsUserLocation,
                    showCircle: !!this.props.showsUserLocation,
                    panToLocation: !!this.props.showsUserLocation
                });
                AMap.event.addListener(this.positionWatcher, 'complete', (res) => {
                    if (this.props.onLocationChange) {
                        this.props.onLocationChange({
                            latitude: res.position.getLat(),
                            longitude: res.position.getLng(),
                            accuracy: res.accuracy,
                        });
                    }
                });
                this.amap.addControl(this.positionWatcher);
                this.positionWatcherHandle = this.positionWatcher.watchPosition();
            }
        } else {
            if (this.positionWatcher) {
                this.removePositionWatcher();
            }
        }

        this.amap.setStatus({zoomEnable: this.props.zoomEnabled !== false});

    }

    removePositionWatcher() {
        this.amap.removeControl(this.positionWatcher);
        this.positionWatcher.clearWatch(this.positionWatcherHandle);
        this.positionWatcher = null;
    }

    componentDidUpdate(oldProps) {
        this.moveToTarget(oldProps);
    }

    componentDidMount() {
        const dom = ReactDOM.findDOMNode(this.refs.webmap);
        this.amap = new window.AMap.Map(dom);
        this.moveToTarget(null);
    }

    componentWillUnmount() {
        if (this.positionWatcher) {
            this.removePositionWatcher();
            //this.amap.removeControl(this.positionWatcher);
        }
    }

    render() {
        // maybe we should check amap script is already there
        return <div ref='webmap' style={this.props.style}/>;
    }

}
