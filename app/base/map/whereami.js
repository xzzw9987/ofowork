let loc;
export default function () {
    return new Promise(resolve => {

        if (loc) {
            resolve({ payload: loc });
            return;
        }

        const div = createEl('div', { width: 0, height: 0, position: 'absolute' }),
            map = new AMap.Map(div);

        map.plugin('AMap.Geolocation', () => {
            const geolocation = new AMap.Geolocation({
                enableHighAccuracy: true,//是否使用高精度定位，默认:true
                maximumAge: 0,           //定位结果缓存0毫秒，默认：0
                convert: true,           //自动偏移坐标，偏移后的坐标为高德坐标，默认：true
                showButton: false,        //显示定位按钮，默认：true
                showMarker: false,        //定位成功后在定位到的位置显示点标记，默认：true
                showCircle: false,        //定位成功后用圆圈表示定位精度范围，默认：true
                panToLocation: false,     //定位成功后将定位到的位置作为地图中心点，默认：true
                zoomToAccuracy: false      //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
            });
            geolocation.getCurrentPosition((status, result) => {
                if (status === 'complete') {
                    resolve({
                        el: div,
                        payload: {
                            error: false,
                            latitude: result.position.getLat(),
                            longitude: result.position.getLng(),
                            accuracy: result.accuracy,
                        }
                    });
                }
                else {
                    resolve({
                        el: div,
                        payload: {
                            error: true,
                            latitude: 0,
                            longitude: 0,
                            accuracy: '请下载APP',
                        }
                    });
                }
            });
        });
    }).then(d => {
        remove(d.el);
        loc = d.payload;
        return d.payload;
    });
}

function createEl(name, style) {
    const el = document.createElement(name);
    Object.assign(el.style, style);
    return el;
}

function append(el) {
    return document.body.appendChild(el);
}
function remove(el) {
    return el && el.remove();
}
