import {NativeModules} from 'react-native';
const RCTLocationManager = NativeModules.LocationManager;

export default location => {
    return new Promise(resolve => {
        RCTLocationManager.geoMessage(
            parseFloat(location.latitude),
            parseFloat(location.longitude),
            (error, address = '') => {
                resolve({error, address});
            });
    })
}
