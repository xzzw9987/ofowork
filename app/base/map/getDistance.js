export default (from, to)=>Promise.resolve(makeLngLat(from).distance(makeLngLat(to)));

function makeLngLat(p) {
    return new AMap.LngLat(p.longitude, p.latitude);
}
