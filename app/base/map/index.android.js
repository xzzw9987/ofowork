import {requireNativeComponent, View} from 'react-native';
import React from 'react';
import {Image} from "react-native/Libraries/Image/Image.android";
const resolveAssetSource = require('resolveAssetSource');

const PropTypes = React.PropTypes;

const ReactAMapView = requireNativeComponent('ReactAMapView', Map);

class Map extends React.Component {

    _onChange(event) {
        [
            this._onLocationChange.bind(this),
            this._onRegionChange.bind(this)
        ].forEach(handler => handler(event));
    }

    _onLocationChange(e) {
        if (this.props.onLocationChange && e.nativeEvent.type === 'locationChange') {
            this.props.onLocationChange(e.nativeEvent);
        }
    }
    _onRegionChange(e) {
        if (this.props.onRegionChange && e.nativeEvent.type === 'cameraChange') {
            this.props.onRegionChange(e.nativeEvent);
        }
    }

    _onSelect(event) {
        if (this.props.onAnnotationClicked) {

            // const m = this.props.annotations.filter(v=> event.nativeEvent.key == v.key)[0];
            this.props.onAnnotationClicked(event.nativeEvent.key);
        }
    }

    render() {
        const processedAnnotations = !this.props.annotations ? [] : this.props.annotations.map(v => Object.assign({}, v, {image: resolveAssetSource(v.icon)['uri']}));
        return <ReactAMapView  {...this.props} onChange={this._onChange.bind(this)} annotations={processedAnnotations}
                               onSelect={this._onSelect.bind(this)}/>;
    };

}

Map.propTypes = {
    region: PropTypes.shape({
        latitude: PropTypes.number.isRequired,
        longitude: PropTypes.number.isRequired,
        latitudeDelta: PropTypes.number.isRequired,
        longitudeDelta: PropTypes.number.isRequired,
    }),
    defaultRegion: PropTypes.shape({
        latitude: PropTypes.number.isRequired,
        longitude: PropTypes.number.isRequired,
        latitudeDelta: PropTypes.number.isRequired,
        longitudeDelta: PropTypes.number.isRequired,
    }),
    showsUserLocation: PropTypes.bool,
    zoomEnabled: PropTypes.bool,
    clusterMode: PropTypes.bool,
    annotations: PropTypes.arrayOf(PropTypes.shape({
        latitude: PropTypes.number.isRequired,
        key: PropTypes.string,
        longitude: PropTypes.number.isRequired,
        title: PropTypes.string,
        image: PropTypes.oneOfType([
            PropTypes.shape({
                uri: PropTypes.string,
            }),
            // Opaque type returned by require('./image.jpg')
            PropTypes.number
        ])
    })),
    lines: React.PropTypes.arrayOf(React.PropTypes.shape({
        latitude: React.PropTypes.number.isRequired,
        longitude: React.PropTypes.number.isRequired
    })),
    hotSpots: PropTypes.arrayOf(PropTypes.shape({
        key: PropTypes.string,
        radius: PropTypes.number,
        latitude: PropTypes.number,
        longitude: PropTypes.number,
        fillColor: PropTypes.string,
        strokeColor: PropTypes.string,
        count: React.PropTypes.number.isRequired
    })),
    onLocationChange: PropTypes.func,
    onAnnotationClicked: PropTypes.func,
    onRegionChange: PropTypes.func,
    ...View.propTypes
};

module.exports = {Map};
