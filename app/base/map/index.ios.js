import React from 'react';
import {requireNativeComponent} from 'react-native';
const resolveAssetSource = require('resolveAssetSource');
class Map extends React.Component {
    render() {
        const annotations = !this.props.annotations ? [] : this.props.annotations.map(v => (
            {
                ...v,
                image: resolveAssetSource(v.icon)
            }
        ));
        let onLocationChange = e => {
            this.props.onLocationChange && this.props.onLocationChange(e.nativeEvent);
        };
        let onAnnotationClicked = e => {
            // const m = annotations.filter(v=> e.nativeEvent.key == v.key)[0];
            this.props.onAnnotationClicked && this.props.onAnnotationClicked(e.nativeEvent.key);
        };
        let onRegionChange = e => {
            this.props.onRegionChange && this.props.onRegionChange(e.nativeEvent);
        };
        console.log('------annotations-------');
        return <RCTAMap
            {...this.props}
            annotations={annotations}
            onLocationChange={onLocationChange}
            onRegionChange={onRegionChange}
            onAnnotationClicked={onAnnotationClicked}/>;
    }
}
Map.propTypes = {
    region: React.PropTypes.shape({
        latitude: React.PropTypes.number.isRequired,
        longitude: React.PropTypes.number.isRequired,
        latitudeDelta: React.PropTypes.number.isRequired,
        longitudeDelta: React.PropTypes.number.isRequired,
    }),
    defaultRegion: React.PropTypes.shape({
        latitude: React.PropTypes.number.isRequired,
        longitude: React.PropTypes.number.isRequired,
        latitudeDelta: React.PropTypes.number.isRequired,
        longitudeDelta: React.PropTypes.number.isRequired,
    }),
    onLocationChange: React.PropTypes.func,
    onAnnotationClicked: React.PropTypes.func,
    onRegionChange: React.PropTypes.func,
    userTrackingMode: React.PropTypes.bool,
    showsUserLocation: React.PropTypes.bool,
    zoomEnabled: React.PropTypes.bool,
    clusterMode: React.PropTypes.bool,
    annotations: React.PropTypes.arrayOf(React.PropTypes.shape({
        latitude: React.PropTypes.number.isRequired,
        longitude: React.PropTypes.number.isRequired,
        image: React.PropTypes.image
    })),
    lines: React.PropTypes.arrayOf(React.PropTypes.shape({
        latitude: React.PropTypes.number.isRequired,
        longitude: React.PropTypes.number.isRequired
    })),
    hotSpots: React.PropTypes.arrayOf(React.PropTypes.shape({
        latitude: React.PropTypes.number.isRequired,
        longitude: React.PropTypes.number.isRequired,
        count: React.PropTypes.number.isRequired
    }))
};

var RCTAMap = requireNativeComponent('RCTAMap', Map);

module.exports = {Map};
