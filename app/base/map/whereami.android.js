import {NativeModules} from 'react-native';
const RCTLocationManager = NativeModules.LocationManager;
import Permissions from 'react-native-permissions';
import {Toast} from '../index';
let loc;
export default function () {
    return new Promise(resolve=> {
        Permissions.requestPermission('location')
            .then(response => {
                // response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
                // this.setState({ photoPermission: response })
                if (response == 'denied') {
                    resolve({error: true, latitude: 0, longitude: 0, accuracy: 0});
                    Toast.fail('无法获取定位，请开启定位服务');
                    return;
                }

                const retryCount = 10,
                    result = {error: true, latitude: 0, longitude: 0, accuracy: 0};

                function retry(count) {

                    if (count > retryCount) {
                        resolve(result);
                        Toast.fail('无法获取定位，请开启定位服务');
                        return;
                    }

                    RCTLocationManager.whereami((error, latitude, longitude, accuracy)=> {
                        if (error === false) {
                            loc = {error, latitude, longitude, accuracy};
                            resolve({error, latitude, longitude, accuracy});
                            return;
                        }
                        retry(++count);
                    });
                }

                retry(0);

            });
    });
}
