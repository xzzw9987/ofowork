import geoMessage from './geoImplement';
let lock = false;
const queue = [];
// export default geoMessage;

export default location => {
    return new Promise(resolve => {
        const r = resolve ;
        resolve = (...args)=>{
            // alert('resolve');
            r(...args);
        };
        queue.push(() => geoMessage(location).then((...args) => resolve(...args)));
        if (!lock) {
            lock = true;
            (function f() {
                if (queue.length) {
                    const cb = queue.shift();
                    cb().then(f);
                } else {
                    lock = false;
                }
            })();
        }
    });
}
