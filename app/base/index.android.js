import React, { Component } from 'react';
import {
  Text,
  ScrollView,
  View,
  TextInput,
  Image,
  TouchableHighlight,
  TouchableNativeFeedback,
  ActivityIndicator,
  Keyboard
} from 'react-native';
import { Map } from './map';
import ActionSheet from 'antd-mobile/lib/action-sheet';
import List from 'antd-mobile/lib/list';
import ListView from 'antd-mobile/lib/list-view';

import WingBlank from 'antd-mobile/lib/wing-blank';
// import Modal from 'antd-mobile/lib/modal';
import Modal from 'react-native-modalbox';
import Tabs from 'antd-mobile/lib/tabs';
import Toast from 'antd-mobile/lib/toast';
import 'antd-mobile/lib/toast/style';

import Checkbox from 'antd-mobile/lib/checkbox';

var StaticImage = require('./staticImages');

class Br extends Component {
  render() {
    return <Text>{'\n'}</Text>;
  }
}
class WrapText extends Component {
  render() {
    var props = Object.assign({}, this.props);
    var style = Object.assign(
      {},
      { color: '#000', fontSize: 16 },
      this.props.style || {}
    );

    return <Text style={style}>{this.props.children}</Text>;
  }
}
class TabPane extends Component {
  render() {
    return <View {...this.props} style={{ flexGrow: 1 }} />;
  }
}

class WrapList extends List {
  render() {
    var props = Object.assign({}, this.props);
    if (props.style) {
      delete props.style.borderBottomStyle;
      delete props.style.borderTopStyle;
    }
    return <List {...props} />;
  }
}

class WrapView extends View {
  render() {
    var props = Object.assign({}, this.props);
    if (props.style) {
      delete props.style.borderBottomStyle;
      delete props.style.borderTopStyle;
    }
    return <View {...props} />;
  }
}

class HorizontalLayout extends Component {
  setNativeProps(nativeProps) {
    this._root.setNativeProps(nativeProps);
  }

  render() {
    var style = Object.assign({}, this.props.style || {}, {
      flexDirection: 'row'
    });
    delete style.borderBottomStyle;
    delete style.borderTopStyle;
    return (
      <View
        ref={component => this._root = component}
        {...this.props}
        style={style}
      />
    );
  }
}

class VerticalLayout extends Component {
  setNativeProps(nativeProps) {
    this._root.setNativeProps(nativeProps);
  }

  render() {
    var style = Object.assign({}, this.props.style || {}, {
      flexDirection: 'column'
    });
    delete style.borderBottomStyle;
    delete style.borderTopStyle;
    return (
      <View
        ref={component => this._root = component}
        {...this.props}
        style={style}
      />
    );
  }
}

class WrapImage extends Component {
  render() {
    var props = Object.assign({}, this.props);
    var source;
    if (props.src) {
      if (props.src.startsWith('http') || props.src.startsWith('data:')) {
        source = { uri: props.src };
      } else {
        source = StaticImage[props.src];
      }
    }
    delete props.src;
    return <Image resizeMode="stretch" source={source} {...props} />;
  }
}
class WrapTouchableHighlight extends Component {
  handleClick() {
    if (!this.props.onPress) return;
    const lastClick = this.lastClick || 0;
    const clickDuration = this.props.clickDuration == 0 ? 0 : 3000;
    if (new Date().getTime() - lastClick > clickDuration) {
      this.props.onPress();
      this.lastClick = new Date().getTime();
    }
  }
  render() {
    var props = Object.assign({}, this.props);
    return (
      <TouchableHighlight {...props} onPress={this.handleClick.bind(this)} />
    );
  }
}
class Button extends Component {
  handleClick() {
    if (this.props.disabled) return;
    if (!this.props.onPress) return;
    const lastClick = this.lastClick || 0;
    const clickDuration = this.props.clickDuration == 0 ? 0 : 3000;
    if (new Date().getTime() - lastClick > clickDuration) {
      this.props.onPress();
      this.lastClick = new Date().getTime();
    }
  }
  render() {
    var props = Object.assign({}, this.props);
    var onPress;
    if (!props.disabled) {
      onPress = props.onPress;
    }
    delete props.onPress;
    delete props.disabled;
    let color, fontSize;
    let style = {};

    if (props.style) {
      style = Object.assign({}, props.style);
      color = style.color;
      fontSize = style.fontSize;
      delete props.style;
      delete style.color;
      delete style.fontSize;
      delete style.borderBottomStyle;
    }

    return (
      <TouchableNativeFeedback
        style={style}
        onPress={this.handleClick.bind(this)}
        {...props}
      >
        <View style={style}>
          <Text style={{ color, fontSize }}>{this.props.children}</Text>
        </View>
      </TouchableNativeFeedback>
    );
  }
}

class WrapTextInput extends Component {
  render() {
    return (
      <TextInput
        underlineColorAndroid={'transparent'}
        ref={c => this._textInput = c}
        {...this.props}
      />
    );
  }
  getFocus() {
    this._textInput.focus();
  }
}

class UselessTextInput extends Component {
  render() {
    return (
      <TextInput
        {...this.props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
        editable={true}
      />
    );
  }
}
const Color = { orange: '#ff8300', yellow: '#ffd900', green: '#2ebf38' };

module.exports = {
  Tabs,
  TabPane,
  ScrollView,
  Modal,
  WingBlank,
  List: WrapList,
  ListView,
  ActivityIndicator,
  ActionSheet,
  Text: WrapText,
  Br,
  Map,
  Image: WrapImage,
  Button,
  TextInput: WrapTextInput,
  HorizontalLayout,
  VerticalLayout,
  View,
  Toast,
  TouchableHighlight: WrapTouchableHighlight,
  Color,
  UselessTextInput,
  Checkbox
};
