import React, { Component } from 'react';
import Map from './map';
import 'antd-mobile/lib/wing-blank/style';
import 'antd-mobile/lib/modal/style';
import 'antd-mobile/lib/tabs/style';
import 'antd-mobile/lib/toast/style';
import 'antd-mobile/lib/checkbox/style';
import './custom.less';
export ActionSheet from 'antd-mobile/lib/action-sheet';
export ListView from 'antd-mobile/lib/list-view';
export List from 'antd-mobile/lib/list';
export WingBlank from 'antd-mobile/lib/wing-blank';
export Modal from 'antd-mobile/lib/modal';
export Toast from 'antd-mobile/lib/toast';
export Checkbox from 'antd-mobile/lib/checkbox';
import Tabs from 'antd-mobile/lib/tabs';
const TabPane = Tabs.TabPane;
require('es6-object-assign').polyfill();
require('string.prototype.startswith');

import * as StaticImage from './staticImages';

class Text extends Component {
    render() {
        return <div {...this.props} />;
    }
}

class View extends Component {
    render() {
        var props = Object.assign({}, this.props);
        var { style } = props;
        delete props.style;
        style = style || {};
        style.display = 'flex';
        return <div style={style} {...props} />;
    }
}

class TextInput extends Component {

    handleChange(event) {
        if (this.props.onChangeText) {
            this.props.onChangeText(event.target.value);
        }
    }

    render() {
        var props = Object.assign({}, this.props);
        delete props.onChangeText;
        delete props.onSubmitEditing;

        if (props.keyboardType === 'numeric') {
            props.type = 'number';
            props.pattern = '[0-9]*';
            props.inputMode = 'numeric';
            delete props.keyboardType;
        }
        if (props.editable) {
            return (
                <textarea {...props} onChange={this.handleChange.bind(this)} onKeyDown={e => {
                    if (e.keyCode === 9 || e.keyCode === 13) { // tab or enter
                        this.handleSubmit();
                    }
                }}></textarea>
            );
        } else {
            return (
                <input {...props} onChange={this.handleChange.bind(this)} onKeyDown={e => {
                    if (e.keyCode === 9 || e.keyCode === 13) { // tab or enter
                        this.handleSubmit();
                    }
                }} />
            );
        }
    }

    handleSubmit() {
        if (this.props.onSubmitEditing) {
            this.props.onSubmitEditing();
        }
    }

}

class UselessTextInput extends Component {
    handleChange(event) {
        if (this.props.onChangeText) {
            this.props.onChangeText(event.target.value);
        }
    }

    render() {
        var props = Object.assign({}, this.props);
        delete props.onChangeText;
        delete props.onSubmitEditing;
        delete props.multiline;
        delete props.numberOfLines;

        return (
            <textarea {...props} onChange={this.handleChange.bind(this)}></textarea>
        );

    }

}

class TouchableHighlight extends Component {
    handleClick() {
        if (!this.props.onPress) return;
        //this.props.onPress();
        const lastClick = this.lastClick || 0;
        const clickDuration = this.props.clickDuration == 0 ? 0 : 3000;
        if (new Date().getTime() - lastClick > clickDuration) {
            this.props.onPress();
            this.lastClick = new Date().getTime();
        }
    }

    render() {
        var props = Object.assign({}, this.props);
        var onclick = props.onPress;
        var style = Object.assign({}, this.props.style || {}, {
            flexDirection: 'column',
            display: 'flex',
            flexShrink: 0
        });
        delete props.onPress;
        delete props.underlayColor;
        return (

            <div {...props} onClick={this.handleClick.bind(this)} style={style}>
            </div>
        );
    }
}
class Button extends Component {

    handleClick() {
        if (this.props.disabled) return;
        if (!this.props.onPress) return;
        const lastClick = this.lastClick || 0;
        const clickDuration = this.props.clickDuration == 0 ? 0 : 3000;
        if (new Date().getTime() - lastClick > clickDuration) {
            this.props.onPress();
            this.lastClick = new Date().getTime();
        }
    }

    render() {
        var props = Object.assign({}, this.props);
        var onclick = props.onPress;
        delete props.onPress;
        delete props.clickDuration;
        delete props.disabled;
        return (
            <View {...props} onClick={this.handleClick.bind(this)}>
                {this.props.children}
            </View>
        );
    }
}

class Image extends Component {
    render() {
        var props = Object.assign({}, this.props);
        delete props.resizeMode;
        var source;
        if (props.src) {
            if (props.src.startsWith('http') || props.src.startsWith('data:')) {
                source = props.src;
            }
            else {
                source = StaticImage[props.src];
            }
        }
        delete props.src;
        return (
            <img {...props} src={source} />
        );
    }
}


class HorizontalLayout extends Component {
    render() {
        var style = Object.assign({}, this.props.style || {}, { flexDirection: 'row', display: 'flex' });
        return <div {...this.props} style={style} />;
    }
}

class VerticalLayout extends Component {
    render() {
        var props = Object.assign({}, this.props);
        delete props.horizontal;
        var style = Object.assign({}, this.props.style || {}, { flexDirection: 'column', display: 'flex' });
        return <div {...props} style={style} />;
    }
}

class Br extends Component {
    render() {
        return <br />;
    }
}
class Span extends Component {
    render() {
        return <span {...this.props} />;
    }
}
class ScrollView extends Component {
    render() {
        return <div style={{
            ...this.props.style,
            overflow: 'auto',
            WebkitOverflowScrolling: 'touch',
            flex: '1 1 0'
        }}>{this.props.children}</div>;
    }
}
export const Color = { orange: '#ff8300', yellow: '#ffd900', green: '#2ebf38' };

export { Map, Text, View, TextInput, UselessTextInput, TouchableHighlight, Button, Image, HorizontalLayout, VerticalLayout, Br, Span, ScrollView, Image as ActivityIndicator };

