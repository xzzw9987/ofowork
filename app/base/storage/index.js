var Promise = require('bluebird');

export default class Storage {
    static setItem(key, value) {
        return new Promise((resolve) => {
            resolve(localStorage.setItem(key, value));
        });
    }

    static getItem(key) {
        return new Promise((resolve) => {
            resolve(localStorage.getItem(key));
        });
    }

    static clear() {
        return new Promise((resolve) => {
            resolve(localStorage.clear());
        });
    }

}
