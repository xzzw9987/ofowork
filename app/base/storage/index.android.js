import { AsyncStorage } from 'react-native';

export default class Storage {
  static setItem(key, value) {
    return AsyncStorage.setItem(key, value);
  }
  
  static getItem(key) {
    return AsyncStorage.getItem(key);
  }

  static clear() {
    return AsyncStorage.clear();
  }
}