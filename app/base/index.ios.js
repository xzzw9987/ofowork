import React, { Component } from 'react';
import {
  Text,
  View,
  TextInput,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  ActivityIndicator,
  ScrollView
} from 'react-native';
import { Map } from './map';
import { HorizontalLayout, VerticalLayout } from './index.android';
import ActionSheet from 'antd-mobile/lib/action-sheet';
import ListView from 'antd-mobile/lib/list-view';
import WingBlank from 'antd-mobile/lib/wing-blank';
// import Modal from 'antd-mobile/lib/modal';
import Modal from 'react-native-modalbox';
import Toast from 'antd-mobile/lib/toast';
import Tabs from 'antd-mobile/lib/tabs';
import 'antd-mobile/lib/toast/style';
import Checkbox from 'antd-mobile/lib/checkbox';

var StaticImage = require('./staticImages');

class WrapView extends View {
  render() {
    var props = Object.assign({}, this.props);
    if (props.style) {
      console.log('WrapView style', props.style);
      delete props.style.borderBottomStyle;
      delete props.style.borderTopStyle;
    }
    return <View {...props} />;
  }
}

class Br extends Component {
  render() {
    return <Text>{'\n'}</Text>;
  }
}

class WrapTextInput extends Component {
  render() {
    var style = Object.assign({}, this.props.style || {}, {
      height: this.props.style.height || 26
    });
    return (
      <TextInput ref={c => this._textInput = c} {...this.props} style={style} />
    );
  }

  getFocus() {
    this._textInput.focus();
  }
}

class WrapImage extends Component {
  render() {
    var props = Object.assign({}, this.props);
    var source;
    if (props.src) {
      if (props.src.startsWith('http') || props.src.startsWith('data:')) {
        source = { uri: props.src };
      } else {
        source = StaticImage[props.src];
      }
    }
    delete props.src;
    return <Image resizeMode="stretch" source={source} {...props} />;
  }
}

class Button extends Component {
  render() {
    var props = Object.assign({}, this.props);
    var onPress;
    if (!props.disabled) {
      onPress = props.onPress;
    }
    delete props.onPress;
    delete props.disabled;
    let color, fontSize;
    let style = {};

    if (props.style) {
      style = Object.assign({}, props.style);
      color = style.color;
      fontSize = style.fontSize;
      delete props.style;
      delete style.color;
      delete style.fontSize;
      delete style.borderBottomStyle;
    }

    return (
      <TouchableOpacity onPress={onPress} {...props} style={style}>
        <View>
          <Text style={{ color, fontSize }}>{this.props.children}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

class TabPane extends Component {
  render() {
    return <View {...this.props} style={{ flexGrow: 1 }} />;
  }
}
class UselessTextInput extends Component {
  render() {
    return (
      <TextInput
        onStartShouldSetResponder={() => true}
        onResponderGrant={() => console.log('inside')}
        {...this.props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
        editable={true}
      />
    );
  }
}
const Color = { orange: '#ff8300', yellow: '#ffd900', green: '#2ebf38' };

module.exports = {
  Modal,
  WingBlank,
  ListView,
  ActionSheet,
  Text,
  View,
  Br,
  Map,
  Image: WrapImage,
  Button,
  TextInput: WrapTextInput,
  HorizontalLayout,
  VerticalLayout,
  ActivityIndicator,
  Toast,
  ScrollView,
  TouchableHighlight,
  Tabs,
  TabPane,
  Color,
  UselessTextInput,
  Checkbox
};
