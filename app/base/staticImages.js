import logo from '../public/images/logo.png';
import arrow from '../public/images/arrow.png';
import arrow2 from '../public/images/arrow2.png';
import setting from '../public/images/setting.png';
import kaoqin from '../public/images/kaoqin.png';
import deleteIcon from '../public/images/delete.png';
import search2 from '../public/images/search2.png';
import search3 from '../public/images/search3.png';
import icon from '../public/images/icon.png';
import icon1 from '../public/images/icon1.png';
import icon2 from '../public/images/icon2.png';
import checkout from '../public/images/checkout.png';
import checkin from '../public/images/checkin.png';
import notcheck from '../public/images/notcheck.png';
import hot from '../public/images/huo.png';
import history from '../public/images/history.png';
import success from '../public/images/success.png';
import fail from '../public/images/fail.png';
import detail from '../public/images/detail.png';
import loading from '../public/images/loading.gif';
import up from '../public/images/up.png';
import down from '../public/images/down.png';
import flag from '../public/images/flag.png';
import lookhistory from '../public/images/lookhistory.png';
import gold from '../public/images/gold.png';
import silver from '../public/images/silver.png';
import bronze from '../public/images/bronze.png';
import unreport from '../public/images/wei.png';
import reported from '../public/images/yi.png';
import upload from '../public/images/upload.png';
import position from '../public/images/position.png';
import bicycle from '../public/images/bicycle.png';
import sign from '../public/images/sign.png';
import geo from '../public/images/geo.png';
import active from '../public/images/active.png';
import unlockCode from '../public/images/unlockCode.png';
import record from '../public/images/record.png';
import hand from '../public/images/hand.png';
import photo from '../public/images/photo.png';
import map from '../public/images/map.png';
import cam from '../public/images/cam.png';
import repairIcon from '../public/images/repairIcon.png';
import reportIcon from '../public/images/reportIcon.png';
import back from '../public/images/back.png';
import tester from '../public/images/tester.jpg';
import defaultImage from '../public/images/defaultImage.png';
import defaultImage2 from '../public/images/defaultImage2.png';
import noData from '../public/images/noData.png';
import sevenbike from '../public/images/700bike.png';
import sevenicon from '../public/images/700icon.png';
import triangle from '../public/images/triangle.png';
import recycle from '../public/images/recycle.png';
import release from '../public/images/release.png';
import release2 from '../public/images/release2.png';
import newUsers from '../public/images/newUsers.png';
import deliveryList from '../public/images/deliveryList.png';
import recordList from '../public/images/recordList.png';
import createRecord from '../public/images/createRecord.png';
import hxBanner2 from '../public/images/hxBanner2.png';
import breakingHot from '../public/images/breakingHot.png';
import checkBroken from '../public/images/checkBroken.png';
import repairerRecord from '../public/images/repairerRecord.png';
import changeBrand from '../public/images/changeBrand.png';
import badMark from '../public/images/badMark.png';
import badCarIcon from '../public/images/badCarIcon.png';
import badCarDone from '../public/images/badCarDone.png';
import spot from '../public/images/spot.png';
import dataView from '../public/images/dataView.png';
import changeBrandSN from '../public/images/changeBrandSN.png';
import unbindBrand from '../public/images/unbindBrand.png';
import lowElectricIcon from '../public/images/lowElectricIcon.png';
import lowElectric1 from '../public/images/lowElectric1.png';
import lowElectric2 from '../public/images/lowElectric2.png';
import lowElectric3 from '../public/images/lowElectric3.png';
import snUnlock from '../public/images/snUnlock.png';
import mechRecord from '../public/images/mechRecord.png';
import smartRecord from '../public/images/smartRecord.png';
import snGetCode from '../public/images/snGetCode.png';
import more from '../public/images/more.png';
import collect from '../public/images/collect.png';
import show from '../public/images/show.png';
import trajectory from '../public/images/trajectory.png';
import tooltip from '../public/images/tooltip.png';
import movePathStart from '../public/images/movePathStart.png';
import movePathEnd from '../public/images/movePathEnd.png';
import ofo from '../public/images/ofo.png';
import other from '../public/images/other.png';
import repairRate from '../public/images/repairRate.png';
import smartRecordNew from '../public/images/smartRecordNew.png';
import lockCode from '../public/images/lockCode.png';
import stockIn from '../public/images/stockIn.png';
import stockOut from '../public/images/stockOut.png';
import stockRecycle from '../public/images/stockRecycle.png';
import unbindNew from '../public/images/unbindNew.png';

export {
  logo,
  arrow,
  arrow2,
  setting,
  kaoqin,
  deleteIcon,
  search2,
  search3,
  icon,
  icon1,
  icon2,
  checkout,
  checkin,
  notcheck,
  hot,
  history,
  success,
  fail,
  detail,
  loading,
  up,
  down,
  flag,
  lookhistory,
  gold,
  silver,
  bronze,
  unreport,
  reported,
  upload,
  position,
  bicycle,
  sign,
  geo,
  active,
  record,
  hand,
  photo,
  map,
  cam,
  repairIcon,
  reportIcon,
  back,
  tester,
  defaultImage,
  defaultImage2,
  noData,
  sevenbike,
  sevenicon,
  triangle,
  recycle,
  release,
  release2,
  newUsers,
  deliveryList,
  recordList,
  createRecord,
  hxBanner2,
  breakingHot,
  checkBroken,
  repairerRecord,
  changeBrand,
  badMark,
  badCarIcon,
  unlockCode,
  badCarDone,
  spot,
  changeBrandSN,
  unbindBrand,
  dataView,
  lowElectricIcon,
  lowElectric1,
  lowElectric2,
  lowElectric3,
  snUnlock,
  mechRecord,
  smartRecord,
  snGetCode,
  more,
  collect,
  show,
  trajectory,
  tooltip,
  movePathStart,
  movePathEnd,
  ofo,
  other,
  repairRate,
  smartRecordNew,
  lockCode,
  stockIn,
  stockOut,
  stockRecycle,
  unbindNew
};
