const wgs2gcj = (function () {
  var transformLat = function (x, y) {
    let ret = -100.0 + 2.0 * x + 3.0 * y + 0.2 * y * y + 0.1 * x * y + 0.2 * Math.sqrt(Math.abs(x));
    ret += (20.0 * Math.sin(6.0 * x * Math.PI) + 20.0 * Math.sin(2.0 * x * Math.PI)) * 2.0 / 3.0;
    ret += (20.0 * Math.sin(y * Math.PI) + 40.0 * Math.sin(y / 3.0 * Math.PI)) * 2.0 / 3.0;
    ret += (160.0 * Math.sin(y / 12.0 * Math.PI) + 320 * Math.sin(y * Math.PI / 30.0)) * 2.0 / 3.0;
    return ret;
  };
  var transformLon = function (x, y) {
    let ret = 300.0 + x + 2.0 * y + 0.1 * x * x + 0.1 * x * y + 0.1 * Math.sqrt(Math.abs(x));
    ret += (20.0 * Math.sin(6.0 * x * Math.PI) + 20.0 * Math.sin(2.0 * x * Math.PI)) * 2.0 / 3.0;
    ret += (20.0 * Math.sin(x * Math.PI) + 40.0 * Math.sin(x / 3.0 * Math.PI)) * 2.0 / 3.0;
    ret += (150.0 * Math.sin(x / 12.0 * Math.PI) + 300.0 * Math.sin(x / 30.0 * Math.PI)) * 2.0 / 3.0;
    return ret;
  };
  var delta = function (lat, lng) {
    let a = 6378245.0;
    let ee = 0.00669342162296594323;
    let dLat = transformLat(lng - 105.0, lat - 35.0);
    let dLng = transformLon(lng - 105.0, lat - 35.0);
    let radLat = lat / 180.0 * Math.PI;
    let magic = Math.sin(radLat);
    magic = 1 - ee * magic * magic;
    let sqrtMagic = Math.sqrt(magic);
    dLat = (dLat * 180.0) / ((a * (1 - ee)) / (magic * sqrtMagic) * Math.PI);
    dLng = (dLng * 180.0) / (a / sqrtMagic * Math.cos(radLat) * Math.PI);
    return { lat: dLat, lng: dLng };
  };
  return function (wgsLat, wgsLng) {
    let {lat, lng} = delta(wgsLat, wgsLng);
    return { lat: wgsLat + lat, lng: wgsLng + lng };
  };
}());

const wrapWith = successCallback =>
  res => {
    let gcj = wgs2gcj(res.coords.latitude, res.coords.longitude);
    res.coords.latitude = gcj.lat;
    res.coords.longitude = gcj.lng;
    successCallback(res);
  };

export const getCurrentPosition = (successCallback, errorCallback, options) => navigator.geolocation.getCurrentPosition(wrapWith(successCallback), errorCallback, options);
export const watchPosition = (successCallback, errorCallback, options) => navigator.geolocation.watchPosition(wrapWith(successCallback), errorCallback, options);
export const clearWatch = navigator.geolocation.clearWatch.bind(navigator.geolocation);