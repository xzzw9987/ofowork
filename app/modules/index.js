import memoryHistory from '../runtime/memoryHistory';
require('whatwg-fetch');

export function JumpTo(page, allowBack = false, state = null) {
    if (allowBack) {
        memoryHistory.push({ pathname: page, state });
    }
    else {
        memoryHistory.replace({ pathname: page, state });
    }
}

export function JumpBack() {
    memoryHistory.goBack();
}

export function ResetTo(...args) {
    JumpTo(...args);
}

function WebFetch(url, options) {
    let form = null;
    if (options.method.toUpperCase() === 'POST') {
        form = new FormData();
        Object.keys(options.body).forEach(key => form.append(key, options.body[key]));
    }
    options.body = form;
    return fetch(url, options).then((response) => {
        return response.json();
    });
}

function createHistoryStack() {
    let state = null;
    memoryHistory.listen(location => state = location.state);
    return {
        get() {
            return state;
        }
    };
}
const historyStack = createHistoryStack();
export { WebFetch as fetch };
export { historyStack };
