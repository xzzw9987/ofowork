import {AppState} from 'react-native';
import {getNetInfo, onNetInfoChange} from './netInfo';
import {Toast} from '../base';

const onGetConnection = netState=>netState == 'offline' && Toast.fail('无网络连接!', 2);
AppState.addEventListener('change', appState=> {
    if (appState == 'active') {
        setTimeout(()=>getNetInfo().then(onGetConnection), 3000);
    }
});

onNetInfoChange(onGetConnection);