import React, {Component} from 'react';
import {Keyboard} from 'react-native'
import androidBackCallback from './androidBackCallback';
import {JumpBack} from './';

export default function (baseCls) {
    return class extends baseCls {
        constructor() {
            super();

            this.keyboardShow = false;
            this.dismissKeyboard = (e)=> {
                Keyboard.dismiss();
            };

            this.getKeyboardState = ()=> {
                return this.keyboardShow;
            };
        }

        componentDidMount() {
            super.componentDidMount && super.componentDidMount();
            this.isInTab();
            this.isInNavigator();

            this.showListener = Keyboard.addListener('keyboardDidShow', e=> {this.onKeyboardShow && this.onKeyboardShow(e); this.keyboardShow = true;});
            this.hideListener = Keyboard.addListener('keyboardDidHide', e=> {this.onKeyboardHide && this.onKeyboardHide(e); this.keyboardShow = false;});
        }

        componentWillUnmount() {
            super.componentWillUnmount && super.componentWillUnmount();
            this.showListener.remove();
            this.hideListener.remove();

            setTimeout(()=> {
                this.offTab();
                this.offNavigator();
            }, 0);
        }

        viewWillAppear() {
            // setTimeout(()=>this.setState({activeTab: true}), 20);
            super.viewWillAppear && super.viewWillAppear();
            androidBackCallback.callback = (this.handleBack || this.handleGoBack || JumpBack).bind(this);
        }

        viewWillDisappear() {
            // setTimeout(()=>this.setState({activeTab: false}), 20);
            super.viewWillDisappear && super.viewWillDisappear();
            androidBackCallback.callback = null;
        }

        //
        // render() {
        //     if (!this.props.tab)
        //         return super.render();
        //     if (this.state.activeTab)
        //         return super.render();
        //     return null;
        // }

        isInTab() {
            if (!this.props.tab) return;
            this.removeTab = this.props.tab.subscribe((tabID, currentID)=> {
                tabID === currentID ? this.viewWillAppear() : this.viewWillDisappear();
            })
        }

        offTab() {
            if (!this.props.tab) return;
            this.removeTab && this.removeTab();
        }


        isInNavigator() {
            if (!this.props.navigator)return;
            const currentRoute = this.props.navigator.navigationContext.currentRoute;
            this.navSubscription = this.props.navigator.navigationContext.addListener('didfocus', e=> {
                currentRoute === e.data.route ? this.viewWillAppear() : this.viewWillDisappear();
            })
        }

        offNavigator() {
            if (!this.props.navigator)return;
            this.navSubscription && this.navSubscription.remove();
        }

    }
}
