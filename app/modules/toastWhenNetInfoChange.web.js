import {onNetInfoChange} from './netInfo';
import {Toast} from '../base';

const onGetConnection = netState=>netState == 'offline' && Toast.fail('无网络连接!', 2);

onNetInfoChange(onGetConnection);