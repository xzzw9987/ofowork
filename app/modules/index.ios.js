import { NativeModules, AppState } from 'react-native';
import keyboard from './keyboard';
import { getNetInfo } from './netInfo';
import { Toast } from '../base';
import { getDeviceID } from './device';
import platform from './platform';
import appVersion from './version';

var Promise = require('bluebird');
const jumpConf = {};

function JumpTo(page, allowBack = true, state = null) {
    console.log("ios jump to ", page);
    jumpConf.JumpTo(page, allowBack, state);
}

function JumpBack(page, allowBack = true) {
    console.log("Jump Back");
    keyboard.dismiss();
    jumpConf.JumpBack();
}

function ResetTo(page, state = null) {
    jumpConf.ResetTo(page, state);
}
let isToasting = false;
function _fetch(url, options) {
    let body = new FormData();
    for (let key in options.body) {
        body.append(key, options.body[key]);
    }
    console.log(`----------start--------${url}`);

    getNetInfo()
        .then(status => {
            if (!isToasting && status === 'offline' && AppState.currentState === 'active') {
                isToasting = true;
                Toast.fail('无网络连接', 2);
                return new Promise(resolve => setTimeout(resolve, 2));
            }
        })
        .then(() => isToasting = false);
    return getDeviceID()
        .then(address => {
            body.append('device_id', address);
            body.append('platform', platform.OS);
            body.append('version', appVersion);
            let opt = {
                ...options,
                headers: {...options.headers, "Content-Type": "multipart/form-data; boundary=" + body.boundary},
                body: body.toString()
            };
            return fetch(url, opt);
        })
        .then(response => {
            console.log(`---------get response---------${url}`);
            return response.json()
        })
        .then(json => {
            console.log(`---------get response---------${JSON.stringify(json)}`);
            return json
        });
}

function FormData() {
    this.fake = true;
    this.boundary = "--------FormData" + parseInt(Math.random() * 100000);
    this._fields = [];
}
FormData.prototype.append = function (key, value) {
    this._fields.push([key, value]);
};
FormData.prototype.toString = function () {
    var boundary = this.boundary;
    var body = "";
    this._fields.forEach(function (field) {
        body += "--" + boundary + "\r\n";
        // file upload
        body += "Content-Disposition: form-data; name=\"" + field[0] + "\";\r\n\r\n";
        body += field[1] + "\r\n";
    });
    body += "--" + boundary + "--";
    return body;
};
const historyStack = createHistoryStack();
function createHistoryStack() {
    let stack = [];
    return {
        get(){
            return jumpConf.getRoute() ? jumpConf.getRoute().state : null;
        },
        length(){
            return stack.length
        }
    }
}

module.exports = {
    JumpTo, fetch: _fetch, JumpBack, ResetTo, historyStack, jumpConf
};
