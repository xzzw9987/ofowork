import {NetInfo} from 'react-native';
export const getNetInfo = ()=> {
    return NetInfo
        .isConnected
        .fetch()
        .then(isConnected=>isConnected ? 'online' : 'offline')
};

export const onNetInfoChange = handler=> {
    NetInfo.isConnected.addEventListener('change', isConnected=> handler(isConnected ? 'online' : 'offline'));
};