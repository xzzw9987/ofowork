import config, {transformPathname, transformQuery} from './externalApp.config';

const isIOS = /iphone|ipad/i.test(navigator.userAgent),
    isAndroid = /android/i.test(navigator.userAgent),
    platform = isIOS ? 'ios' : 'android';

export default (appKey, pathname, query)=> {
    if (!config[appKey]) return;
    if (navigator.userAgent.indexOf('MicroMessenger') >= 0) {
        alert("请安装work APP或在浏览器中使用此功能");
        return;
    }
    const platformProtocol = isIOS ? config[appKey]['ios'] : config[appKey]['android'];
    location.href = `${platformProtocol}${transformPathname(pathname)[platform]}?${
        stringifyQuery(
            transformQuery(
                query
            )[platform]
        )}`;
    
}

function query(str) {
    var arr = str.split('&');
    return arr.reduce(function (prev, now) {
        var key = now.split('=')[0],
            value = now.split('=')[1];
        prev[key] = value;
        return prev;
    }, {});
}


function stringifyQuery(q) {
    return Object.keys(q).map(function (objKey) {
        return objKey + '=' + q[objKey];
    }).join('&');
}