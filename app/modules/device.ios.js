import {NativeModules} from 'react-native';
const RCTDeviceManager = NativeModules.DeviceManager;
let deviceID;
export const getDeviceID = () => new Promise(resolve => {
    if (deviceID) {
        resolve(deviceID);
        return;
    }
    RCTDeviceManager.getDeviceID(d => {
        deviceID = d.deviceID;
        resolve(deviceID);
    });
});

let hasStartUploadLocation = false;
export const startUploadLocation = (token, url) => {
    !hasStartUploadLocation && RCTDeviceManager.startUploadLocation(token, url);
    hasStartUploadLocation = true;
};
