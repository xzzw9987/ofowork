export const getNetInfo = ()=> {
    return new Promise(resolve=>resolve(window.navigator.onLine ? 'online' : 'offline'));
};

export const onNetInfoChange = handler=> {
    window.addEventListener('online', ()=>handler('online'));
    window.addEventListener('offline', ()=>handler('offline'));
};