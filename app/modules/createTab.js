import pageWrapper from './pageWrapper';
import {View} from '../base';
import React, {Component} from 'react';
import TabBar from 'antd-mobile/lib/tab-bar';
import platform from '../modules/platform';

const Item = TabBar.Item;

export default (tabs = [], conf = {})=> {
    return pageWrapper(class extends Component {
        constructor(...args) {
            super(...args);
            this.state = {selected: 0};
            this.listeners = [];
        }

        componentDidMount() {

        }

        add(cb) {
            let f = function (...args) {
                cb(...args);
            };
            this.listeners.push(f);
            return ()=> {
                /* remove */
                let idx = this.listeners.indexOf(f);
                this.listeners.splice(idx, 1);
            };
        }

        render() {
            return (
                <TabBar tintColor="#333">
                    { this.walk() }
                </TabBar>
            );
        }

        walk() {
            return tabs.map((v, i)=> {
                const {view, icon, selectedIcon, title} = v,
                    cloneProps = {...this.props};

                delete cloneProps['navigator'];

                return (
                    <Item key={i} onPress={()=>this.onSelected(i)} selected={this.state.selected == i} icon={icon}
                          selectedIcon={selectedIcon} title={title}>
                        <View style={{flexDirection: 'column', position: 'absolute', top: 0, left: 0, right: 0, bottom: platform.OS == 'android'? 0: 50}}>
                            {
                                React.cloneElement(view, {
                                    tab: {
                                        subscribe: cb=> {
                                            return this.add(selected=> {
                                                cb(i, selected);
                                            });
                                        }
                                    },
                                    ...cloneProps
                                })
                            }
                        </View>

                    </Item>
                );
            });
        }

        onSelected(selected) {
            this.setState({selected}, ()=> {
                this.runListeners(this.state.selected);
            });
        }

        runListeners(selected) {
            this.listeners.forEach(f=>f(selected));
        }

        viewWillAppear() {
            setTimeout(()=> {
                this.runListeners(this.state.selected);
            }, 0);
        }

        viewWillDisappear(){
            this.runListeners(99);
        }
    });
};