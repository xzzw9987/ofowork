import {Linking} from 'react-native'
import config, {transformQuery, transformPathname} from './externalApp.config';

const isIOS = false,
    isAndroid = true,
    platform = isIOS ? 'ios' : 'android';

export default (appKey, pathname, query)=> {
    if (!config[appKey]) return;
    let platformProtocol = isIOS ? config[appKey]['ios'] : config[appKey]['android'],
        url = `${platformProtocol}${transformPathname(pathname)[platform]}?${
            stringifyQuery(
                transformQuery(
                    query
                )[platform]
            )}`;

    Linking.canOpenURL(url).then(supported=> {
        supported ? Linking.openURL(url) : alert('请安装高德地图');
    })
}

function parseQuery(str) {
    var arr = str.split('&');
    return arr.reduce(function (prev, now) {
        var key = now.split('=')[0],
            value = now.split('=')[1];
        prev[key] = value;
        return prev;
    }, {});
}


function stringifyQuery(q) {
    return Object.keys(q).map(function (objKey) {
        return objKey + '=' + q[objKey];
    }).join('&');
}