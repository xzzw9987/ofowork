export default {
    get(ref){
        if (ref == 'window') {
            return {
                width: window.innerWidth,
                height: window.innerHeight
            }
        }
    }
}
