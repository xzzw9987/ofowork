import React, { Component } from 'react';
import { View, ActivityIndicator, HorizontalLayout, Text } from '../base';

export default () => (
    <View style={styles.container}>
        <View style={styles.outer}>
            <View style={styles.inner}>
                <ActivityIndicator size='large' src='loading' />
                <HorizontalLayout style={styles.top10}><Text>正在加载 ...</Text></HorizontalLayout>
            </View>
        </View>
    </View>
);

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff',
    },
    outer: {
        position: 'relative',
        flexGrow: 1,
    },
    inner: {
        position: 'absolute',
        flexDirection: 'column',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    top10: {
        marginTop: 10,
    }
};
