const duration = 5000,
    queue = [];

let timeout = setTimeout(r, duration);

function r() {
    queue.forEach(c=>c());
}

export default  {
    resume() {
        clearTimeout(timeout);
        timeout = setTimeout(r, duration);
    },

    pause() {
        clearTimeout(timeout);
    },

    subscribe(cb) {
        queue.push(cb);
        return ()=> {
            /* unsubscribe */
            const idx = queue.indexOf(cb);
            if (idx > -1) {
                queue.splice(idx, 1);
            }
        }
    }

}