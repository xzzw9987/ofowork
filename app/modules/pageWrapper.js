import React, {Component} from 'react';
export default function (baseCls) {
    return class extends baseCls {
        componentWillMount(){
            super.componentWillMount && super.componentWillMount();
            this.viewWillAppear();
        }

        componentWillUnmount(){
            super.componentWillUnmount && super.componentWillUnmount();
            this.viewWillDisappear();
        }

        viewWillAppear(){
            super.viewWillAppear && super.viewWillAppear() ;
        }

        viewWillDisappear(){
            super.viewWillDisappear && super.viewWillDisappear() ;
        }
    }
}