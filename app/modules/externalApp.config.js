export default {
    amap: {
        ios: 'iosamap://',
        android: 'androidamap://',
    }
};

export const transformPathname = pathname=> {
    return {
        ios: pathname,
        android: (pathname=>{
            if(pathname === 'path')
                return 'route';
            return pathname;
        })(pathname)
    }
};

export const transformQuery = query=> {
    return {
        ios: (query=>{
            const m = {} ;

            if(query.t) {
                switch (query.t) {
                    case 'car':
                        m['t'] = 0;
                        break;
                    case 'bus':
                        m['t'] = 1;
                        break;
                    case 'walk':
                        m['t'] = 2 ;
                        break ;
                    default: break;
                }
            }

            return {...query, ...m} ;
        })(query),
        android: (query=>{
            const m = {} ;
            if(query.t) {
                switch (query.t) {
                    case 'car':
                        m['t'] = 2;
                        break;
                    case 'bus':
                        m['t'] = 1;
                        break;
                    case 'walk':
                        m['t'] = 4 ;
                        break ;
                    default: break;
                }
            }

            return {...query, ...m} ;
        })(query)
    }
};

