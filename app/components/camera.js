import React, {Component} from 'react';


export default class extends Component {

    static propTypes = {
        radar: React.PropTypes.bool,
        torch: React.PropTypes.bool,
        buttons: React.PropTypes.array,
        hint: React.PropTypes.string,
        onBarCodeRead: React.PropTypes.func
    };

    constructor(...args) {
        super(...args);
        this.state = {
        };
        this.active = true;
    }
    componentWillUnmount(){
        this.active = false;
    }

    componentDidMount() {
    }


    render() {
        return (
            <div style={styles.container}>

                <div style={{...styles.absolute, top: 0, right: 0, bottom: 0, left: 0}}>
                    <div style={{color: '#fff', textAlign: 'center', marginTop: 180, fontSize: 20}}>网页端暂不支持扫描二维码</div>

                    { this.renderButtons() }

                </div>
            </div>
        )
    }

    renderButtons() {
        let buttons = Array.from(this.props.buttons || []);
        return (
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-around',
                position: 'absolute',
                bottom: 30,
                left: 30,
                right: 30
            }}>{buttons}</div>);
    }
}

const styles = {
    container: {
        flexGrow: 1,
        position: 'relative',
        background: '#333'
    },
    relative: {
        position: 'relative'
    },
    absolute: {
        position: 'absolute'
    },
    mask: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        overflow: 'hidden'
    }
};