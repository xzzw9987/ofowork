import React, {Component} from 'react';

export default class extends Component {
    static propTypes = {
        containerStyle: React.PropTypes.object
    };

    render(){
        return (
            <div style={{...styles.container, ...this.props.style}} >
                <div style={{display: 'flex', flexDirection: 'column', ...this.props.containerStyle}}>
                    { this.props.children }
                </div>
            </div>
        )
    }
}

const styles = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        position: 'fixed',
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: 999
    }
};