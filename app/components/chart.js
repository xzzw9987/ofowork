import React from 'react';
import ECharts from 'react-echarts';
import {Color} from '../base';

const stringOrNumber = React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.number]);

const maxRatio = 1.1;

class Chart extends React.Component {
    constructor(...args) {
        super(...args);
        this.state = {
            dataIndex: null
        };
    }

    componentDidMount() {
        let instance = this.refs.chart.getInstance();
        instance.on('click', params=> {
            console.log(params);
            this.props.onPress && this.props.onPress(params.dataIndex);
            instance.dispatchAction({
                type: 'highlight',
                seriesIndex: params.seriesIndex,
                dataIndex: params.dataIndex
            });
            this.setState({dataIndex: params.dataIndex})
        });

        instance.on('mouseup', params=> {
            instance.dispatchAction({
                type: 'downplay',
                seriesIndex: params.seriesIndex,
                dataIndex: params.dataIndex
            });
        });
        this.setState({
            dataIndex: this.props.defaultDataIndex
        });
    }

    componentDidUpdate() {
        let instance = this.refs.chart.getInstance();
        if (this.state.dataIndex !== null) {
            instance.dispatchAction({
                type: 'downplay',
                seriesIndex: 1,
                dataIndex: this.state.dataIndex
            });

            instance.dispatchAction({
                type: 'highlight',
                seriesIndex: 1,
                dataIndex: this.state.dataIndex
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.defaultDataIndex === undefined
            && nextProps.defaultDataIndex !== undefined) {
            this.setState({
                dataIndex: nextProps.defaultDataIndex
            })
        }
    }

    render() {
        return <ECharts ref="chart" option={this.makeOption(this.props)} style={this.props.style}/>
    }

    makeOption() {
        return {};
    }
}

export class VerticalBarChart extends Chart {
    static propTypes = {
        onPress: React.PropTypes.func,
        defaultDataIndex: React.PropTypes.number,
        xAxis: React.PropTypes.arrayOf(stringOrNumber),
        yAxis: React.PropTypes.bool,
        data: React.PropTypes.arrayOf(stringOrNumber),
        markPoint: React.PropTypes.bool
    };

    makeOption(props) {
        // console.log(props.xAxis);
        return {
            tooltip: {
                show: false,
                trigger: 'axis',
                axisPointer: {
                    lineStyle: {
                        opacity: 0
                    }
                }
            },
            xAxis: props.xAxis ? [{
                type: 'category',
                data: props.xAxis,
                axisLine: {
                    show: false
                },
                axisTick: {
                    show: false
                }
            }] : [{
                show: false,
                type: 'category'
            }],
            yAxis: {
                show: false,
                /*axisLine: {
                 show: false
                 },
                 axisTick: {
                 show: false
                 },
                 axisLabel: {
                 margin: 32,
                 textStyle: {
                 color: '#999',
                 align: 'left'
                 },
                 formatter: function (value, index) {
                 //@todo format value
                 return value;
                 }
                 },*/
                max: parseInt(Math.max(...props.data) * maxRatio, 10)
            },
            series: [
                { // For shadow
                    type: 'bar',
                    itemStyle: {
                        normal: {color: 'rgba(0,0,0,0.05)'}
                    },
                    barGap: '-100%',
                    barCategoryGap: '40%',
                    data: props.data.map(v=> {
                        return parseInt(Math.max(...props.data) * maxRatio, 10);
                    })
                },
                {
                    type: 'bar',
                    data: props.data || [],
                    markPoint: {
                        symbol: 'path://M0 0L100 0,100 100,0 100Z',
                        symbolOffset: [0, -10],
                        label: {
                            normal: {
                                textStyle: {color: '#000'},
                                formatter: v=> {
                                    //console.log(v.data.yAxis);
                                    return formatterNumber(v.data.yAxis)
                                }
                            }
                        },
                        itemStyle: {
                            normal: {
                              color: 'rgba(0,0,0,0)'
                            }
                        },
                        data: props.markPoint ? props.xAxis.map((v, index)=> {
                            // console.log(index, props.data[index]);
                            return {
                                xAxis: index,
                                yAxis: props.data[index]
                            }
                        }) : []
                    },
                    markLine: {silent: true},
                    itemStyle: {
                        normal: {color: '#929292'},
                        emphasis: {color: Color.orange}
                    }

                }],
            grid: {
                x: 15,
                x2: 15,
                y: 10,
                y2: 32
            }
        }
    }

}

export class HorizontalBarChart extends Chart {
    static propTypes = {
        onPress: React.PropTypes.func,
        defaultDataIndex: React.PropTypes.number,
        xAxis: React.PropTypes.arrayOf(stringOrNumber),
        yAxis: React.PropTypes.bool,
        data: React.PropTypes.arrayOf(stringOrNumber),
        markPoint: React.PropTypes.bool
    };

    makeOption(props) {
        return {
            tooltip: {
                show: false,
                trigger: 'axis',
                axisPointer: {
                    lineStyle: {
                        opacity: 0
                    }
                }
            },
            xAxis: {
                show: false,
                type: 'value',
                max: parseInt(Math.max(...props.data) * maxRatio, 10)
            },
            yAxis: props.xAxis ? [{
                type: 'category',
                data: props.xAxis,
                axisLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },
                axisLabel: {
                    margin: 24,
                    textStyle: {
                        color: '#000',
                        align: 'center'
                    },
                    formatter: function (value, index) {
                        //@todo format value
                        return value;
                    }
                }
            }] : [{
                show: false,
                type: 'category'
            }],
            series: [
                { // For shadow
                    type: 'bar',
                    itemStyle: {
                        normal: {color: 'rgba(0,0,0,0.05)'}
                    },
                    barGap: '-100%',
                    barCategoryGap: '40%',
                    data: props.data.map(v=> {
                        return parseInt(Math.max(...props.data) * maxRatio, 10);
                    }),
                    label: {
                        normal: {
                            position: 'right',
                            show: true,
                            textStyle: {fontSize: 16, color: '#000'},
                            formatter: v=>props.data[v.dataIndex]
                        },
                        emphasis: {
                            position: 'right',
                            show: true,
                            textStyle: {fontSize: 16, color: '#000'},
                            formatter: function (v) {
                                return props.data[v.dataIndex];
                            }
                        }
                    }
                },
                {
                    type: 'bar',
                    data: props.data || [],
                    markPoint: {
                        data: props.markPoint || []
                    },
                    markLine: {silent: true},
                    itemStyle: {
                        normal: {color: '#929292'},
                        emphasis: {color: Color.orange}
                    }
                }],
            grid: {
                x: 50,
                x2: 85,
                y: 10,
                y2: 20
            }
        }
    }
}

export class LineChart extends Chart {
    static propTypes = {
        onPress: React.PropTypes.func,
        xAxis: React.PropTypes.arrayOf(stringOrNumber),
        yAxis: React.PropTypes.bool,
        data: React.PropTypes.arrayOf(stringOrNumber),
        markPoint: React.PropTypes.bool
    };

    makeOption(props) {
        return {
            tooltip: {
                show: false,
                trigger: 'axis',
                axisPointer: {
                    lineStyle: {
                        opacity: 0
                    }
                }
            },
            legend: {
                data:['上周', '今天']
            },
            xAxis: props.xAxis ? [{
                type: 'category',
                boundaryGap: false,
                data: props.xAxis
            }] : [{
                type: 'category',
                boundaryGap: false,
                data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
            }],
            yAxis: {
                type: 'value',
                axisLabel: {
                    formatter: function (value, index) {
                        //@todo format value
                        return value;
                    }
                }
            },
            series: [
                {
                    name: '上周',
                    type: 'line',
                    data: props.data[0] || []
                },
                {
                    name: '今天',
                    type: 'line',
                    data: props.data[1] || []
                }
            ],
            grid: {
                y: 10,
                y2: 30
            }
        }
    }
}


function formatterNumber(val) {
    val = parseInt(val);
    if (val > 10000) {
        val = parseInt(val / 1000, 10);
        return `${val}k`;
    }
    return val;
}