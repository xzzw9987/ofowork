import React, {Component} from 'react';
import base from '../base';

import {
    Text,
    View,
    ScrollView,
    Dimensions,
    TouchableOpacity,
    ViewPagerAndroid,
    Platform,
    ActivityIndicator
} from 'react-native'
import Swiper from 'react-native-swiper';

export default class extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            width: null,
            height: null
        }
    }

    render() {
        return (
            <View onLayout={this.onLayout} style={{flexGrow: 1}}>
                {
                    this.state.width && this.state.height ? (
                        <Swiper
                            loop={false}
                            showsButtons={false}
                            width={this.state.width}
                            height={this.state.height}
                            dot={<View style={{
                                backgroundColor: '#ddd',
                                width: 8,
                                height: 8,
                                borderRadius: 4,
                                marginLeft: 3,
                                marginRight: 3,
                                marginTop: 13
                            }}/>}
                            activeDot={<View style={{
                                backgroundColor: '#999',
                                width: 8,
                                height: 8,
                                borderRadius: 4,
                                marginLeft: 3,
                                marginRight: 3,
                                marginTop: 13,
                                ...styles.borderTop,
                                ...styles.borderBottom,
                                ...styles.borderLeft,
                                ...styles.borderRight
                            }}/>}
                        >
                            {this.props.children}
                        </Swiper>) : null
                }
            </View>
        );
    }

    onLayout = e=> {
        this.setState({
            width: parseInt(e.nativeEvent.layout.width, 10),
            height: parseInt(e.nativeEvent.layout.height, 10)
        })
    }
}

export class Item extends Component {
    render() {
        return (
            <base.VerticalLayout style={{flexGrow: 1}}>
                {this.props.children}
            </base.VerticalLayout>
        );
    }
}


let styles = {
    borderBottom: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    borderTop: {
        borderTopWidth: 1,
        borderTopColor: '#ddd'
    },
    borderLeft: {
        borderLeftWidth: 1,
        borderLeftColor: '#ddd'
    },
    borderRight: {
        borderRightWidth: 1,
        borderRightColor: '#ddd'
    }

};