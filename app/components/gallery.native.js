import React, {Component} from 'react';
import {View, Text, TouchableHighlight, Image as RImage} from 'react-native' ;
import {Image} from '../base';
import ImagePicker from 'react-native-image-picker';

export default class Cam extends Component {

    constructor(...args) {
        super(...args);
        this.state = {
            src: null
        };
    }

    static propTypes = {
        onSelectPhoto: React.PropTypes.func,
        desc: React.PropTypes.string,
        options: React.PropTypes.object
    };


    render() {
        return (
            <View style={{...styles.container, ...this.props.style}}>
                { this.state.src ? this.renderImage() : this.renderCamera() }
            </View>
        );
    }

    renderImage() {
        return (
            <View style={styles.c1}>
                <TouchableHighlight onPress={this.onSelectPhoto}>
                    <View style={{backgroundColor: '#fff', alignItems: 'center'}}>
                        <RImage source={this.state.src} style={styles.img}/>
                        <Text style={{color: '#666'}}>点击图片重新拍摄</Text>
                    </View>
                </TouchableHighlight>
            </View>
        );
    }

    renderCamera() {
        return (
            <View style={styles.c1}>
                <TouchableHighlight onPress={this.onSelectPhoto}>
                    <View style={{backgroundColor: '#fff', alignItems: 'center'}}>
                        <Image src={this.props.desc == '再次拍摄' ? 'more' : 'cam'} style={styles.img}/>
                        <Text style={{color: '#666'}}>{this.props.desc}</Text>
                    </View>
                </TouchableHighlight>
            </View>
        );
    }

    onSelectPhoto = () => {
        const options = {
            title: '选择一张照片',
            takePhotoButtonTitle: '拍照',
            chooseFromLibraryButtonTitle: '从相册中选择',
            cancelButtonTitle: '取消',
            quality: .5,
            maxWidth: 500,
            maxHeight: 500,
            ...this.props.options
        };
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) return;
            if (!response.data) return;
            this.props.onChangeImage && this.props.onChangeImage(`data:image/jpeg;base64,${response.data}`);
            this.setState({
                src: {uri: `data:image/jpeg;base64,${response.data}`, isStatic: true}
            });
            this.props.onSelectPhoto && this.props.onSelectPhoto(`data:image/jpeg;base64,${response.data}`);
        });
    }
}

Cam.defaultProps = {
    desc: ''
};

const styles = {
    container: {
        position: 'relative',
        flexGrow: 1
    },
    c1: {
        flexGrow: 1,
        alignItems: 'center',
        position: 'relative'
    },
    img: {
        width: 70,
        height: 70,
        marginBottom: 10
    }
};
