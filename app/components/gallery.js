import React, {Component} from 'react';
import {Image} from '../base';
export default class extends Component {

    constructor(...args) {
        super(...args);
        this.state = {
            src: null
        };
    }

    static propTypes = {
        onSelectPhoto: React.PropTypes.func,
        desc: React.PropTypes.string
    };

    render() {
        return (
            <div style={{...styles.container, ...this.props.style}}>
                { this.state.src ? this.renderImage() : this.renderCamera() }
            </div>
        );
    }

    onSelectPhoto(e) {
        if (!e.target.files.length)return;
        let file = e.target.files[0],
            reader = new FileReader();
        reader.addEventListener('load', e=> {
            this.setState({src: e.target.result});
            this.props.onSelectPhoto && this.props.onSelectPhoto(e.target.result);
        });
        reader.readAsDataURL(file);
    }

    renderImage() {
        return (
            <div style={styles.c1}>
                <div style={{position: 'relative'}}>
                    <div style={{
                        display: 'flex',
                        flexDirection: 'column',
                        backgroundColor: '#fff',
                        alignItems: 'center'
                    }}>
                        <img src={this.state.src} style={styles.img}/>
                        <div style={{color: '#666'}}>点击图片重新选择</div>
                    </div>
                    <input accept="image/*" style={{
                        position: 'absolute',
                        height: '100%',
                        left: '50%',
                        top: 0,
                        opacity: 0,
                        marginLeft: -35,
                        width: 70
                    }} type="file" onChange={e=>this.onSelectPhoto(e)}/>
                </div>
            </div>
        );
    }

    renderCamera() {
        return (
            <div style={styles.c1}>
                <div style={{position: 'relative'}}>
                    <div style={{
                        display: 'flex',
                        flexDirection: 'column',
                        backgroundColor: '#fff',
                        alignItems: 'center'
                    }}>
                        <Image src="cam" style={styles.img}/>
                        <div style={{color: '#666'}}>{this.props.desc}</div>
                    </div>
                    <input accept="image/*" style={{
                        position: 'absolute',
                        height: '100%',
                        left: '50%',
                        top: 0,
                        opacity: 0,
                        marginLeft: -35,
                        width: 70
                    }} type="file" onChange={e=>this.onSelectPhoto(e)}/>
                </div>
            </div>
        );
    }
}

const styles = {
    container: {
        position: 'relative',
        flexGrow: 1
    },
    c1: {
        flexGrow: 1,
        alignItems: 'center',
        position: 'relative'
    },
    img: {
        width: 70,
        height: 70,
        marginBottom: 10
    }
};
