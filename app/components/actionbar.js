import React, { Component } from 'react';
import * as base from '../base';
import { Text, Input, Button } from '../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../modules';

export class ActionBar extends Component {

    constructor(props) { super(props); }

    render() {
        return (
            <base.HorizontalLayout style={styles.title}>
                <base.HorizontalLayout style={styles.vcbox} >
                    <Button className="back" style={styles.text} onPress={ this.props.handleBack }>{this.props.children}</Button>
                </base.HorizontalLayout>
                <base.Text style={styles.titleText}>{ this.props.title }</base.Text>
                <base.HorizontalLayout style={styles.vcboxRight} >
                    <Button className="back" style={styles.rightBtn} onPress={ this.props.rightAction }>{this.props.rightItem}</Button>
                </base.HorizontalLayout>
            </base.HorizontalLayout>
        );
    }

}

const styles = {
    title: {
        height: 44,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        borderBottomColor: '#ddd',
        borderBottomWidth: 1,
        borderBottomStyle: 'solid',
        flexShrink: 0
    },
    titleText: {
        fontSize: 17,
        color: '#000'
    },
    vcbox: {
        alignItems: 'center',
        height: 44,
        position: 'absolute',
        left: 0,
        justifyContent: 'center',
    },
    vcboxRight: {
        alignItems: 'center',
        height: 44,
        position: 'absolute',
        right: 0,
        justifyContent: 'center',
    },
    text: {
        paddingLeft: 10,
        paddingRight: 20,
        fontSize: 15,
        height: 44,
        color: '#000',
        alignItems: 'center',
        justifyContent: 'center',
    },
    rightBtn: {
        paddingLeft: 20,
        paddingRight: 10,
        fontSize: 15,
        height: 44,
        color: '#000',
        alignItems: 'center',
        justifyContent: 'center',
    }
};
