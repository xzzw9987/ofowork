import React, {Component} from 'react';
import {KeyboardAvoidingView, View, Keyboard} from 'react-native';

export default class extends Component {
    static propTypes = {
        containerStyle: React.PropTypes.object,
        onDismissKeyboard: React.PropTypes.func
    };

    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', e=>this._keyboardDidShow(e));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', e=>this._keyboardDidHide(e));
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow(e) {
        console.log(e);
    }

    _keyboardDidHide(e) {
        console.log(e);
        this.props.onDismissKeyboard && this.props.onDismissKeyboard();
    }

    render() {
        return (
            <KeyboardAvoidingView contentContainerStyle={this.props.containerStyle} behavior="position"
                                  style={{...styles.container, ...this.props.style}}>
                { this.props.children }
            </KeyboardAvoidingView>
        )
    }
}

const styles = {
    container: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    }
};