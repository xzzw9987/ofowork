import React, {Component} from 'react';
import {KeyboardAvoidingView, View} from 'react-native';

export default class extends Component {
    static propTypes = {
        containerStyle: React.PropTypes.object
    };

    render(){
        return (
            <KeyboardAvoidingView contentContainerStyle={this.props.containerStyle} behavior="position" style={{...styles.container, ...this.props.style}} >
                { this.props.children }
            </KeyboardAvoidingView>
        )
    }
}

const styles = {
    container: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    }
};