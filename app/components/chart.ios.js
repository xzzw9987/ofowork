import React, {Component} from 'react';
import {requireNativeComponent, WebView} from 'react-native';
import base from '../base';
import {Map, Text, Button, ListView, HorizontalLayout} from '../base';
import WebViewBridge from 'react-native-webview-bridge';
import {Color} from '../base';

const maxRatio = 1.1;
// const htmlURL = 'http://192.168.121.137:3001';
const htmlURL = 'https://work.ofo.so/assets/index.html';
export default class extends Component {
    render() {
        return <base.View/>;
    }
};

/*var RCTHChart = requireNativeComponent('RCTHChart', HorizontalBarChart);
var RCTVChart = requireNativeComponent('RCTVChart', VerticalBarChart);
var RCTLineChart = requireNativeComponent('RCTLineChart', LineChart);*/

/*
 export class VerticalBarChart extends Component {

 render() {
 let onPress = e=> {
 this.props.onPress && this.props.onPress(e.nativeEvent.idx);
 };

 return <RCTHChart {...this.props} baseColor={'#929292'} activeColor={base.Color.orange} onPress={onPress}/>;
 }
 }
 */

export class VerticalBarChart extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            ready: false,
            width: 0,
            height: 0,
            dataIndex: null
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.defaultDataIndex === undefined
            && nextProps.defaultDataIndex !== undefined) {
            this.setState({
                dataIndex: nextProps.defaultDataIndex
            })
        }
    }

    componentDidMount() {
        this.setState({
            dataIndex: this.props.defaultDataIndex
        });
    }

    componentDidUpdate() {
        this.refs.bridge.sendToBridge(JSON.stringify({
            type: 'wh',
            width: this.state.width,
            height: this.state.height
        }));
        if (this.state.ready) {
            let options = {
                type: 'chart',
                chartType: 'vertical',
                xAxis: this.props.xAxis.map(v=>v.replace(/\n/g, '\\n')) || [],
                data: this.props.data || [],
                markPoint: this.props.markPoint,
                dataIndex: this.state.dataIndex
            };
            this.refs.bridge.sendToBridge(JSON.stringify(options));
        }
    }

    onBridgeMessage(msg) {
        if (msg == 'ready') {
            this.setState({
                ready: true
            });
        }
        else {
            msg = JSON.parse(msg);
            this.onPress(msg);
        }
    }

    onPress(msg) {
        if (msg.type !== 'onPress')
            return;
        this.setState({dataIndex: msg.payload});
        this.props.onPress && this.props.onPress(msg.payload);
    }

    setWh(props) {
        // alert(props.width);
        this.refs.bridge.sendToBridge(JSON.stringify({
            type: 'wh',
            width: props.width,
            height: props.height
        }));
    }

    render() {
        return <WebViewBridge
            onLayout={e=>this.setState({width: e.nativeEvent.layout.width, height: e.nativeEvent.layout.height})}
            bounces={false} ref="bridge" style={this.props.style}
            onBridgeMessage={msg=>this.onBridgeMessage(msg)}
            source={{uri: htmlURL}}/>
    }
}

export class HorizontalBarChart extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            ready: false,
            width: 0,
            height: 0,
            dataIndex: null
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.defaultDataIndex === undefined
            && nextProps.defaultDataIndex !== undefined) {
            this.setState({
                dataIndex: nextProps.defaultDataIndex
            })
        }
    }

    componentDidMount() {
        this.setState({
            dataIndex: this.props.defaultDataIndex
        });
    }

    componentDidUpdate() {
        this.refs.bridge.sendToBridge(JSON.stringify({
            type: 'wh',
            width: this.state.width,
            height: this.state.height
        }));
        if (this.state.ready) {
            let options = {
                type: 'chart',
                chartType: 'horizontal',
                xAxis: this.props.xAxis.map(v=>v.replace(/\n/g, '\\n')) || [],
                data: this.props.data || [],
                markPoint: this.props.markPoint,
                dataIndex: this.state.dataIndex
            };
            this.refs.bridge.sendToBridge(JSON.stringify(options));
        }
    }

    onBridgeMessage(msg) {
        if (msg == 'ready') {
            this.setState({
                ready: true
            });
        }
        else {
            msg = JSON.parse(msg);
            this.onPress(msg);
        }
    }

    onPress(msg) {
        if (msg.type !== 'onPress')
            return;
        this.setState({dataIndex: msg.payload});
        this.props.onPress && this.props.onPress(msg.payload);
    }

    setWh(props) {
        // alert(props.width);
        this.refs.bridge.sendToBridge(JSON.stringify({
            type: 'wh',
            width: props.width,
            height: props.height
        }));
    }

    render() {
        return <WebViewBridge
            onLayout={e=>this.setState({width: e.nativeEvent.layout.width, height: e.nativeEvent.layout.height})}
            bounces={false} ref="bridge" style={this.props.style}
            onBridgeMessage={msg=>this.onBridgeMessage(msg)}
            source={{uri: htmlURL}}/>
    }
}

export class LineChart extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            ready: false,
            width: 0,
            height: 0
        }
    }

    componentDidMount() {

    }

    componentDidUpdate() {
        this.refs.bridge.sendToBridge(JSON.stringify({
            type: 'wh',
            width: this.state.width,
            height: this.state.height
        }));

        if (this.state.ready) {
            let options = {
                type: 'chart',
                chartType: 'line',
                xAxis: null,
                data: this.props.data || [],
                markPoint: this.props.markPoint
            };
            this.refs.bridge.sendToBridge(JSON.stringify(options));
        }
    }

    onBridgeMessage(msg) {
        if (msg == 'ready') {
            this.setState({
                ready: true
            });
        }
        else {
            msg = JSON.parse(msg);
            this.onPress(msg);
        }
    }

    onPress(msg) {
        if (msg.type !== 'onPress')
            return;
        this.props.onPress && this.props.onPress(msg.payload);
    }

    setWh(props) {
        // alert(props.width);
        this.refs.bridge.sendToBridge(JSON.stringify({
            type: 'wh',
            width: props.width,
            height: props.height
        }));
    }

    render() {
        return <WebViewBridge
            onLayout={e=>this.setState({width: e.nativeEvent.layout.width, height: e.nativeEvent.layout.height})}
            bounces={false} ref="bridge" style={this.props.style}
            onBridgeMessage={msg=>this.onBridgeMessage(msg)}
            source={{uri: htmlURL}}/>
    }
}
