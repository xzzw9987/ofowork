import React, {Component} from 'react';

const styles = {
    container: {
        flexGrow: 1,
        display: 'flex',
    },
    item: {
        flexGrow: 1,
        flexShrink: 0,
        flexBasis: 'auto',
        width: '100%',
        display: 'flex',
        flexDirection: 'column'
    }
};

export default class extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            offset: 0,
            index: 0,
            transition: ''
        }
    }

    componentDidMount() {
        this.setState({
            width: this.refs.container.getBoundingClientRect().width
        })
    }

    getWidth() {
        return this.state.width;
    }

    onClickCursor(index) {
        this.setState({
            offset: -index * this.getWidth(),
            index,
            transition: 'transform .5s',
        })
    }

    onTouchStart(e) {
        let touch = e.changedTouches[0];
        this.__lastOffset = touch.clientX;
    }

    onTouchMove(e) {
        let touch = e.changedTouches[0],
            offset = touch.clientX,
            delta = offset - this.__lastOffset,
            target = this.state.offset + delta;

        if (target < -(this.props.children.length - 1) * this.getWidth())
            target = -(this.props.children.length - 1) * this.getWidth();
        else if (target > 0)
            target = 0;

        this.setState({
            offset: target,
            transition: ''
        });

        this.__lastOffset = offset;
        this.__speed = delta;
    }

    onTouchEnd() {
        if (undefined === this.__speed)
            return;
        let index = parseInt((-this.state.offset + .5 * this.getWidth()) / this.getWidth());
        if (this.__speed > 5)
            index -= 1;
        // index = parseInt((-this.state.offset - this.getWidth()) / this.getWidth());
        else if (this.__speed < -5)
            index += 1;
        // index = parseInt((-this.state.offset + this.getWidth()) / this.getWidth());

        if (index < 0)
            index = 0;
        else if (index >= this.props.children.length)
            index = this.props.children.length - 1;

        this.setState({
            index,
            offset: -index * this.getWidth(),
            transition: 'transform .3s'
        });

        this.__speed = undefined;
    }

    render() {
        let transitionStyle = {
            transition: this.state.transition
        };
        return (
            <div ref="container"
                 onTouchStart={(e)=>this.onTouchStart(e)}
                 onTouchMove={(e)=>this.onTouchMove(e)}
                 onTouchEnd={(e)=>this.onTouchEnd(e)}
                 style={{...styles.container, ...this.props.style, overflow: 'hidden', position: 'relative'}}>
                <div style={{
                    display: 'flex',
                    flexWrap: 'nowrap',
                    width: '100%',
                    transform: `translate(${this.state.offset}px)`,
                    ...transitionStyle
                }}>
                    {this.props.children}
                </div>

                <div style={{
                    textAlign: 'center',
                    bottom: 0,
                    left: '50%',
                    position: 'absolute',
                    height: 24,
                    transform: 'translate(-50%)'
                }}>
                    {
                        this.props.children.map((child, index)=> {
                            let style = {
                                display: 'inline-block',
                                width: 10,
                                height: 10,
                                borderRadius: '50%',
                                backgroundColor: '#fff',
                                border: '1px solid #8d8d8d'
                            };
                            if (index === this.state.index)
                                style = Object.assign({}, style, {
                                    backgroundColor: '#8d8d8d'
                                });

                            if (index > 0)
                                style = Object.assign({}, style, {marginLeft: 10});

                            return <span key={index} style={style} onClick={()=>this.onClickCursor(index)}/>
                        })
                    }
                </div>
            </div>
        );
    }
}

export class Item extends Component {
    render() {
        return (
            <div style={styles.item}>
                {this.props.children}
            </div>
        );
    }
}