import React, {Component} from 'react';
import {KeyboardAvoidingView, View, Keyboard} from 'react-native';

export default class extends Component {
    static propTypes = {
        containerStyle: React.PropTypes.object,
        onDismissKeyboard: React.PropTypes.func
    };

    constructor() {
        super();
        this.defaultBottom = 40 ;
        this.state = {
            bottom: this.defaultBottom
        };
    }

    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', e=>this._keyboardDidShow(e));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', e=>this._keyboardDidHide(e));
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow(e) {
        this.setState({
            bottom: e.endCoordinates.height
        })
    }

    _keyboardDidHide(e) {
        console.log(e);
        this.setState({
            bottom: this.defaultBottom
        });

        this.props.onDismissKeyboard && this.props.onDismissKeyboard();

    }

    render() {

        return (
            <View contentContainerStyle={this.props.containerStyle}
                                  style={{...styles.container}}>
                <View style={this.props.containerStyle}>
                    { this.props.children }
                </View>
            </View>
        )
    }
}

const styles = {
    container: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    }
};