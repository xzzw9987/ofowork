import React, {Component} from 'react' ;
export default class extends Component {
    static propTyps = {
        onEndReachedThreshold: React.PropTypes.number,
        onEndReached: React.PropTypes.func,
        dataSource: React.PropTypes.array
    };

    constructor() {
        super();
        this.__lastScrollTop = 0;
    }

    render() {
        return (
            <div style={{...styles.container, ...this.props.style}} data-role="list-container">
                <div ref="scroll" onScroll={e=>this.onScroll(e)} style={{flex: 1, overflow: 'auto'}}>
                    { this.props.dataSource.map((v, index)=>this.props.renderRow(v, index)) }
                </div>
            </div>
        )
    }

    onScroll(e) {
        const {scrollHeight, scrollTop, clientHeight} = e.target;
        if (!this.__haveReached) {
            if (scrollTop > this.__lastScrollTop
                && (scrollTop + clientHeight + (this.props.onEndReachedThreshold || 10) > scrollHeight)) {
                this.props.onEndReached && this.props.onEndReached();
                this.__haveReached = true;
                setTimeout(()=>this.__haveReached = false, 2000)
            }
        }
        this.__lastScrollTop = scrollTop;

    }

    scrollTo(...args) {
        this.refs['scroll'].scrollTop = args[0].y;
        this.refs['scroll'].scrollLeft = args[0].x;
    }
}

const styles = {
    container: {
        display: 'flex',
        WebkitOverflowScrolling: 'touch'
    }
};