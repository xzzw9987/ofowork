import { Alert, Text, TouchableHighlight, View, WebView } from 'react-native';

export default function (options) {
    const buttons = [];
    options.buttons && options.buttons.map((item, idx) => {
        buttons[idx] = item;
    });
    return Alert.alert(options.title || '', options.content, buttons, { cancelable: false });
}

