import React, {Component} from 'react';
import {View, TouchableOpacity, TouchableHighlight ,Image, Animated, Text} from 'react-native';
import Camera from 'react-native-camera';

// frame border
const from = 110, to = 294;

export default class extends Component {

    static propTypes = {
        radar: React.PropTypes.bool,
        torch: React.PropTypes.bool,
        buttons: React.PropTypes.array,
        hint: React.PropTypes.string,
        onBarCodeRead: React.PropTypes.func
    };

    constructor(...args) {
        super(...args);
        this.state = {
            camWidth: 274,
            camHeight: 274,
            containerWidth: 0,
            containerHeight: 0,
            torchMode: false,
            top: from
        };
        this.active = true;
    }
    componentWillUnmount(){
        this.active = false;
    }
    componentDidMount() {

    }


    render() {
        return (
            <View style={styles.container}>

                <View onLayout={(e)=> {
                    const {layout} = e.nativeEvent;
                    this.setState({
                        containerWidth: layout.width,
                        containerHeight: layout.height
                    })
                }} style={{...styles.absolute, top: 0, right: 0, bottom: 0, left: 0}}>

                    <Camera
                        torchMode={this.state.torchMode ? Camera.constants.TorchMode.on : Camera.constants.TorchMode.off}
                        onBarCodeRead={e=>this.onBarCodeRead(e)}
                        style={{...styles.absolute, top: 0, right: 0, bottom: 0, left: 0}} ref="backgroundCam"/>

                    {
                        (()=> {
                            const coord = getCoord(this.state.camWidth,
                                this.state.camHeight,
                                this.state.containerWidth,
                                this.state.containerHeight);

                            return !coord ? null : this.renderMask(coord);
                        })()
                    }

                    { this.renderButtons() }

                </View>
            </View>
        )
    }

    renderMask(coord) {
        const {x, y, width, height} = coord;
        // from 110
        // end 294
        const maskWidth = 375,
            maskHeight = 1500,
            radarWidth = 241,
            radarHeight = 60,
            left = 67,
            scale = this.state.containerWidth / maskWidth;

        return (
            <View style={styles.mask}>
                <Image source={require('./mask.png')} style={{
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    width: maskWidth * scale,
                    height: maskHeight * scale
                }}/>
                <View style={{alignItems: 'center', position: 'absolute', top: to + 80, left: 0,right: 0}}>
                    <Text style={{fontSize: 18, backgroundColor: 'rgba(0,0,0,0)', color: '#fff'}}>{this.props.hint}</Text>
                </View>
            </View>
        );
    }

    renderButtons() {
        let buttons = Array.from(this.props.buttons || []);
        if (this.props.torch)
            buttons.push(
                <View key={99}>
                    <TouchableHighlight onPress={()=>this.setState({torchMode: !this.state.torchMode})}>
                        <View style={{alignItems: 'center'}}>
                            {
                                !this.state.torchMode
                                    ? <Image style={{width: 64, height: 64}} source={require('./torchoff.png')}/>
                                    : <Image style={{width: 64, height: 64}} source={require('./torchon.png')}/>
                            }
                            <Text style={{marginTop: 10, fontSize: 14, backgroundColor: 'rgba(0,0,0,0)', color: '#fff'}}>手电筒</Text>
                        </View>

                    </TouchableHighlight>
                </View>);
        return (
            <View style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                position: 'absolute',
                bottom: 30,
                left: 30,
                right: 30
            }}>{buttons}</View>);
    }

    onBarCodeRead(e) {
        this.props.onBarCodeRead && this.props.onBarCodeRead(e.data);
    }
}


function getCoord(targetWidth, targetHeight, width, height) {
    if (!width || !height) {
        return;
    }
    return {
        x: .5 * (width - targetWidth),
        y: .5 * (height - targetHeight),
        width: targetWidth,
        height: targetHeight
    }
}

const styles = {
    container: {
        flexGrow: 1,
        position: 'relative'
    },
    relative: {
        position: 'relative'
    },
    absolute: {
        position: 'absolute'
    },
    mask: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        overflow: 'hidden'
    }
};