import React, {Component} from 'react';
import {ListView} from 'react-native' ;
export default class extends Component {
    constructor() {
        super();
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this._canReachedEnd = true;
    }

    componentWillReceiveProps() {
        this._canReachedEnd = false;
        setTimeout(() => this._canReachedEnd = true, 50);
    }

    render() {
        const ds = this.ds.cloneWithRows(this.props.dataSource);
        const props = {...this.props};
        if (props.onEndReached) {
            const onEndReached = props.onEndReached;
            props.onEndReached = () => {
                if (this._canReachedEnd) onEndReached();
            };
        }
        return (
            <ListView
                ref="listView"
                {...props}
                onEndReachedThreshold={10}
                dataSource={ds}
                renderRow={(...args) => this.props.renderRow(...args)}
                style={this.props.style}/>
        );
    }

    scrollTo(...args) {
        this.refs['listView'].scrollTo(...args);
    }
}
