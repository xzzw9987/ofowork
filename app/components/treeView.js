import React, { Component } from 'react';
import './tree.css';

export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            collapsed: this.props.defaultCollapsed,
        };
        this.handleClick = this.handleClick.bind(this);
        this.itemClick = this.itemClick.bind(this);
    }

    componentWillMount() {
    }

    handleClick(e) {
        e.stopPropagation();
        this.setState({ collapsed: !this.state.collapsed });
        
    }

    itemClick(...args) {
        if (this.props.onClick) {
            this.props.onClick(...args);
        }
    }

    render() {
        const {
            collapsed = this.state.collapsed,
            className = '',
            itemClassName = '',
            nodeLabel,
            children,
            defaultCollapsed,
            ...rest,
        } = this.props;

        let arrowClassName = 'tree-view_arrow';
        let containerClassName = 'tree-view_children';
        if (collapsed) {
            arrowClassName += ' tree-view_arrow-collapsed';
            containerClassName += ' tree-view_children-collapsed';
        }

        const arrow =
            <div
                {...rest}
                className={className + ' ' + arrowClassName}
                onClick={this.handleClick}
                 />;

        return (
            <div className="tree-view">
                <div className={'tree-view_item ' + itemClassName}
                    onClick={this.itemClick}
                >
                    {arrow}
                    {nodeLabel}
                </div>
                <div className={containerClassName}>
                    {collapsed ? null : children}
                </div>
            </div>
        );
    }
}