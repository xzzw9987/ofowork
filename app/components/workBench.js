import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Text,
    View,
    Br,
    Button,
    ListView,
    List,
    Toast,
    ScrollView,
    TouchableHighlight,
    ActivityIndicator,
    HorizontalLayout,
    VerticalLayout,
    Image
} from '../base';
import { fetchBench } from '../runtime/action/fetchBench';
import { JumpTo } from '../modules';
import pageWrapper from '../modules/pageWrapper';
import Loading from '../modules/loading';
import * as staticImage from '../base/staticImages';

const BenchContainer = class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        };
    }

    async componentDidMount() {
        await this.props.dispatch(fetchBench({}));
        this.setState({ loading: false });
    }

    handlePress(url) {
        switch (url) {
        case 'repairer/checkIn':
            this.props.dispatch({ type: 'zoneList', data: this.props.zone_names });
            JumpTo('repairer/checkIn', true, {});
            break;
        case 'repairer/carRecycle':
            JumpTo('repairer/carSchelduling', true, { from: 'recycle' });
            break;
        case 'repairer/carRelease':
            JumpTo('repairer/carSchelduling', true, { from: 'release' });
            break;
        case 'operation/changeBrand':
            JumpTo('operation/changeBrand', true, { from: 'mech' });
            break;
        case 'operation/changeSmartBrand':
            JumpTo('operation/changeBrand', true, { from: 'smart' });
            break;
        case 'supply/recordMech':
            JumpTo('chainIndex', true, { from: 'mech' });
            break;
        case 'supply/recordSmart':
            JumpTo('chainIndex', true, { from: 'smart' });
            break;
        case 'supply/upgrade':
            JumpTo('chainIndex', true, { from: 'upgrade' });
            break;
        case 'supply/upgradeNew':
            JumpTo('chainInputNew', true, { from: 'upgrade' });
            break;
        case 'supply/recordMechNew':
            JumpTo('chainInputNew', true, { from: 'mech' });
            break;
        case 'supply/recordSmartNew':
            JumpTo('chainInputNew', true, { from: 'smart' });
            break;
        case 'factory/brandCheck':
            JumpTo('factory/scan', true, { fromWhere: 'brand' });
            break;
        case 'factory/fenderCheck':
            JumpTo('factory/scan', true, { fromWhere: 'fender' });
            break;
        case 'scm/stockIn':
            JumpTo('scm/inform', true, { fromWhere: 'stock-in' });
            break;
        case 'scm/stockOut':
            JumpTo('scm/inform', true, { fromWhere: 'stock-out' });
            break;
        case 'scm/stockRecycle':
            JumpTo('scm/inform', true, { fromWhere: 'stock-back' });
            break;
        default:
            JumpTo(url, true, {});
        }
    }

    renderWorkBench() {
        const { benchList, buttonNum, showTitle, isVertical } = this.props;
        const normal = [],
            data = [],
            listNormal = [],
            listData = [];

        benchList.list.map((item, idx) => {
            if (item['img_src'] in staticImage) {
                if (item.classification === 'normal') {
                    normal.push(item);
                }
                if (item.classification === 'data') {
                    data.push(item);
                }
            }
        });

        handleList(normal, listNormal);
        handleList(data, listData);

        function handleList(arr, target) {
            let length = arr.length;
            let indicate = buttonNum - length % buttonNum;
            while (indicate && indicate !== buttonNum) {
                arr.push({});
                indicate--;
            }
            arr.map((item, idx, self) => {
                if ((idx + 1) % buttonNum === 0 && idx !== 0) {
                    target.push(self.slice(idx - buttonNum + 1, idx + 1));
                }
                return target;
            });
        }

        return (
            <View style={styles.container}>
                <ScrollView className="overflowY" style={styles.listView}>

                    {listNormal.length
                        ? showTitle
                            ? <HorizontalLayout style={styles.title}>
                                <Text>常用应用</Text>
                            </HorizontalLayout>
                            : null
                        : null}

                    {listNormal.map((item, index) => {
                        return (
                            <HorizontalLayout
                                key={index}
                                style={Object.assign({}, styles.row, styles.rowBorder)}
                            >
                                {item.map((item, idx) => {
                                    return item.sort
                                        ? <TouchableHighlight
                                            key={idx}
                                            style={Object.assign(
                                                {},
                                                styles.rowItem,
                                                styles.rightBorder
                                            )}
                                            underlayColor={'#f3f3f3'}
                                            onPress={() => this.handlePress(item.url)}
                                        >
                                            {isVertical
                                                ? <VerticalLayout style={styles.rowItem}>
                                                    <Image src={item.img_src} style={styles.img} />
                                                    <Text style={styles.top10}>{item.name}</Text>
                                                </VerticalLayout>
                                                : <HorizontalLayout style={styles.rowItem}>
                                                    <Image src={item.img_src} style={styles.img} />
                                                    <Text style={styles.left10}>{item.name}</Text>
                                                </HorizontalLayout>}
                                        </TouchableHighlight>
                                        : <VerticalLayout key={idx} style={styles.rowItem} />;
                                })}
                            </HorizontalLayout>
                        );
                    })}

                    {listData.length
                        ? showTitle
                            ? <HorizontalLayout style={styles.title}>
                                <Text>数据中心</Text>
                            </HorizontalLayout>
                            : null
                        : null}

                    {listData.map((item, index) => {
                        return (
                            <HorizontalLayout
                                key={index}
                                style={Object.assign({}, styles.row, styles.rowBorder)}
                            >
                                {item.map((item, idx) => {
                                    return item.sort
                                        ? <TouchableHighlight
                                            key={idx}
                                            style={Object.assign(
                                                {},
                                                styles.rowItem,
                                                styles.rightBorder
                                            )}
                                            underlayColor={'#f3f3f3'}
                                            onPress={() => this.handlePress(item.url)}
                                        >
                                            {isVertical
                                                ? <VerticalLayout style={styles.rowItem}>
                                                    <Image src={item.img_src} style={styles.img} />
                                                    <Text style={styles.top10}>{item.name}</Text>
                                                </VerticalLayout>
                                                : <HorizontalLayout style={styles.rowItem}>
                                                    <Image src={item.img_src} style={styles.img} />
                                                    <Text style={styles.left10}>{item.name}</Text>
                                                </HorizontalLayout>}
                                        </TouchableHighlight>
                                        : <VerticalLayout key={idx} style={styles.rowItem} />;
                                })}
                            </HorizontalLayout>
                        );
                    })}

                </ScrollView>
            </View>
        );
    }

    render() {
        if (this.state.loading) return <Loading />;
        return this.renderWorkBench();
    }
};

const mapStateToProps = (state, ownProps) => {
    return {
        benchList: state.api.benchList,
        zone_names: state.api.zone_names
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff'
    },
    listView: {
        flex: 1
    },
    sign: {
        height: 107,
        backgroundColor: '#fff',
        paddingLeft: 10,
        paddingRight: 10,
        borderBottomStyle: 'solid',
        borderBottomColor: '#d8d8d8',
        borderBottomWidth: 1
    },
    title: {
        height: 40,
        alignItems: 'center',
        borderBottomStyle: 'solid',
        borderBottomColor: '#d8d8d8',
        borderBottomWidth: 1,
        paddingLeft: 10
    },
    signIn: {
        height: 66,
        justifyContent: 'center'
    },
    signItem: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexShrink: 1,
        flexBasis: 0
    },
    rowItem: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        flexShrink: 1,
        flexBasis: 0
    },
    vLine: {
        width: 1,
        height: 40,
        backgroundColor: '#d8d8d8'
    },
    jump: {
        borderTopStyle: 'solid',
        borderTopColor: '#d8d8d8',
        borderTopWidth: 1,
        position: 'relative'
    },
    row: {
        height: 88,
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    rowBorder: {
        borderBottomStyle: 'solid',
        borderBottomColor: '#d8d8d8',
        borderBottomWidth: 1,
        borderTopStyle: 'solid',
        borderTopColor: '#d8d8d8',
        borderTopWidth: 1
    },
    rightBorder: {
        borderRightStyle: 'solid',
        borderRightColor: '#d8d8d8',
        borderRightWidth: 1
    },
    f24: {
        fontSize: 24
    },
    left10: {
        marginLeft: 10
    },
    top10: {
        marginTop: 10
    },
    img: {
        width: 27,
        height: 27
    },
    bannerContainer: {
        alignItems: 'center'
    },
    banner: {
        flexGrow: 1,
        flexBasis: 0
    },
    wrap: {
        flexGrow: 1,
        backgroundColor: '#fff'
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BenchContainer);
