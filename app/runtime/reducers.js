import {keys} from './api';
import {JumpTo} from '../modules';
import newReducers from './reducer';

function originalReducers(state = {}, action) {
    switch (action.type) {
        case keys.login:
            if (action.response.code != 0 ) {
                return Object.assign({}, state, {code: action.response.code, alert: action.response.message});
            }
            return Object.assign({}, state, {
                token: action.response.data.token,
                defaultPassword: action.response.data.default_password
            });

        case keys.getVerifyCode:
            if (action.response.code != 0 ) {
                return Object.assign({}, state, {code: action.response.code, alert: action.response.message});
            }

            return Object.assign({}, state, {code: action.response.code, alert: undefined});

        // 用户信息
        case keys.userInfo:
            if (action.response.code != 0 ) {

                return Object.assign({}, state, {code:action.response.code,alert: action.response.message});
            }
            return Object.assign({}, state, {
                    userInfo: action.response.data,
                    zone_ids: (action.response.data.zone_ids || '').trim().split(','),
                    zone_names: (action.response.data.zone_names || '').trim().split(','),
                    zoneIndex: 0,
                    code:action.response.code
                }
            );

        // 修改密码
        case keys.userModifyPassword:
            if (action.response.code != 0) {
                return Object.assign({}, state, {code: action.response.code, alert: action.response.message});
            }
            return Object.assign({}, state, {code: action.response.code, alert: undefined});

        // 待维修车辆数
    case keys.carsWaitForRepair:

        if (action.response.code === 0 || action.response.code === 2002){
            const carsWaitForRepair = state.carsWaitForRepair || {};
            const zone_id = action.request.zone_id;
            carsWaitForRepair[zone_id] = action.response.data.num;

            return Object.assign({},state,{carsWaitForRepair: carsWaitForRepair });
        }
        return state;

        //今日维修率
    case "carsRepairRateToady":
        if (action.response.code === 0){
            const carsRepairRateToady = state.carsRepairRateToady || {};
            const zone_id = action.request.zone_id;
            carsRepairRateToady[zone_id] = action.response.data;
            return Object.assign({},state,{carsRepairRateToady: carsRepairRateToady });
        }
        return state;
        //是否有权限上报
     case "repairerShowReport":
       if (action.response.code === 0){
            const repairerShowReport = state.repairerShowReport || {};
            const zone_id = action.request.zone_id;
            repairerShowReport[zone_id] = action.response.data.show;
            return Object.assign({},state,{repairerShowReport: repairerShowReport });
       }
        return state;
        //维修率列表
    case "repairRateList":
        if (action.response.code != 0){
            return Object.assign({},state,{alert:action.response.message,[action.type]:undefined});
        }

        return Object.assign({},state,{repairRate:action.response.data.average_rate,repairRateList:action.response.data.list});

        // 待维修车辆列表
        case keys.carsWaitForRepairList:
            if (action.response.code != 0 && action.response.code != 2002) {
                return Object.assign({}, state, {alert: action.response.message, carsWaitForRepairList: undefined});
            }

        return Object.assign({},state,{carsWaitForRepairList:action.response.data, repairDone: false});
        // 附近虚拟车桩
    case keys.selectVirtualPosition:
        if (action.response.code != 0){
            // return Object.assign({},state,{code: action.response.code, alert:action.response.message});
            return Object.assign({},state,{code: action.response.code});
        }

        return Object.assign({},state,{nearVirtualPile:action.response.data.list});

        // 上报虚拟车桩
    case keys.reportiVirtualPile:
        if (action.response.code != 0){
            return Object.assign({},state,{alert:action.response.message, reportStatus: {ret : false}});
        }

        return Object.assign({},state,{reportStatus: {ret : true} });

        // 搜索车牌号
    case "carSearchInfo":
        if (action.response.code != 0){
            return Object.assign({},state,{alert:action.response.message,[action.type]:undefined});
        }
        return Object.assign({},state,{[action.type]:action.response.data,alert: undefined});
        //清除搜索信息
    case "clearSearchInfo":
            return Object.assign({}, state, { carSearchInfo: null });
        //修改解锁码
    case "updateUnlockCode":
        if (action.response.code != 0) {
            return Object.assign({}, state, {ret:{code: action.response.code}, retAlert:{alert: action.response.message}});
        }
        return Object.assign({}, state, {ret:{code: action.response.code}, alert: undefined});
        // 待维修车辆详情
        case keys.carWaitForRepairDetail:
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message});
            }
        return Object.assign({},state,{carWaitForRepairDetail:action.response.data});
    case "clearWaitForRepairDetail":
        return Object.assign({}, state, { carWaitForRepairDetail: null });
        // 维修完成
        case keys.repairerRepairDone:
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message, repairDone: false});
            }

            return Object.assign({}, state, {repairDone: true});

        // 师傅签到/签到
        case keys.repairerCheckInOut:
            if (action.response.code != 0) {
                return Object.assign({}, state, {ret:{code: action.response.code},  alert: action.response.message});
            }

            return Object.assign({}, state, {ret:{code: 0}});

        // 待修车辆位置
        case keys.repairerCoordinate:
            if (action.response.code != 0) {
                // return Object.assign({},state,{code: action.response.code, alert:action.response.message});
                return Object.assign({}, state, {code: action.response.code});
            }

            return Object.assign({}, state, {repairerCoordinate: action.response.data.coordinate});

        // 师傅签到记录
        case keys.repairerCheckRecords:
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message});
            }

            return Object.assign({}, state, {checkRecords: action.response.data});

        // 某区域下所有子区域
        case "childZoneListCountry":
        case "childZoneListCregion":
        case "zoneChildZoneListDetailSchool":
        case "zoneChildZoneListRegion":
        case "zoneChildZoneListCity":
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message});
            }

            return Object.assign({}, state, {[action.type]: action.response.data.list});

        case keys.zoneChildZoneList:
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message});
            }

            return Object.assign({}, state, {childZoneList: action.response.data.list});

        // 全国所有城市

        case "zoneAllCityZoneListCountry":
        case "zoneAllCityZoneListRegion":
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message});
            }

            return Object.assign({}, state, {[action.type]: action.response.data.list});
        case keys.zoneAllCityZoneList:
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message});
            }

            return Object.assign({}, state, {zoneAllCityZoneList: action.response.data.list});

        // 全国所有战区
        case "zoneAllRegionZoneListCountry":
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message});
            }

            return Object.assign({}, state, {[action.type]: action.response.data.list});

        case keys.zoneAllRegionZoneList:
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message});
            }

            return Object.assign({}, state, {zoneAllRegionZoneList: action.response.data.list});

        // 特定区域内今日订单量
        case "zoneTodayOrderListCountry":
        case "zoneTodayOrderListCregion":
        case "zoneTodayOrderListDetailSchool":
        case "zoneTodayOrderListRegion":
        case "zoneTodayOrderListCity":
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message});
            }

            return Object.assign({}, state, {[action.type]: action.response.data.list});
        case keys.zoneTodayOrderList:
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message});
            }

            return Object.assign({}, state, {zoneTodayOrderList: action.response.data.list});

        // 城市区域内今日订单量
        case "zoneCityTodayOrderListDetailSchool":
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message});
            }

            return Object.assign({}, state, {[action.type]: action.response.data.list});

        case keys.zoneCityTodayOrderList:
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message});
            }

            return Object.assign({}, state, {zoneCityTodayOrderList: action.response.data.list});

        // 全国所有城市订单
        case "zoneCityAllTodayOrderListCountry":
        case "zoneCityAllTodayOrderListRegion":
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message});
            }

            return Object.assign({}, state, {[action.type]: action.response.data.list});
        case keys.zoneCityAllTodayOrderList:
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message});
            }

            return Object.assign({}, state, {zoneCityAllTodayOrderList: action.response.data.list});

        // 全国所有战区订单
        case "zoneRegionAllTodayOrderListCountry":
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message});
            }

            return Object.assign({}, state, {[action.type]: action.response.data.list});
        case keys.zoneRegionAllTodayOrderList:
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message});
            }

            return Object.assign({}, state, {zoneRegionAllTodayOrderList: action.response.data.list});

        // 特定区域内历史订单量
        case keys.zoneOrderHistor:
            if (action.response.code != 0) {
                return Object.assign({}, state, {alert: action.response.message});
            }

            return Object.assign({}, state, {zoneOrderHistor: action.response.data.list});

        case 'clear':
            return Object.assign({}, state, action.data);
        case 'token':
            return Object.assign({}, state, {token: action.data});

        case 'loading':
            return Object.assign({}, state, {loading: action.data});

        default:
            if (action.type.substr(0, 18) === 'carsWaitForRepair_') {
                const zoneid = action.type.substr(18);
                let value = state.carsWaitForRepair || {};
                value[zoneid] = action.response.data.num;
                return Object.assign({}, state, {carsWaitForRepair: value});
            }
            return state;
    }
}

export var api = (state = {}, action)=> {
    return [originalReducers, newReducers].reduce((prevState, reducer)=> reducer(prevState, action), state);
};

export var user = (state = {}, action) => {
    switch (action.type) {
        case 'oldPassword':
            return Object.assign({}, state, {oldPassword: action.data});
        case 'zoneSwitch':
            return Object.assign({}, state, {zoneIndex: action.data});
        case 'zoneList':
            return Object.assign({}, state, {zoneList: action.data});
        case 'zoneOrderSummaryCountry':
        case 'zoneOrderSummaryRegion':
        case 'zoneOrderSummaryRegion2':
            return Object.assign({}, state, {[action.type]: action.data});
        case 'zoneOrderSummary':
            return Object.assign({}, state, {zoneOrderSummary: action.data});
        case 'zoneHistoryLevel':
            return Object.assign({}, state, {zoneHistoryLevel: action.data});
        case 'userInfo':
            return Object.assign({}, state, {userInfo: action.data});
        case 'carLocation':
            return Object.assign({}, state, { carLocation: action.data });
        case 'carDetail':
            return Object.assign({}, state, { carDetail: action.data });
        case "clearCarDetail":
            return Object.assign({}, state, { carDetail: null });
        case 'virtualPileInfo':
            return Object.assign({}, state, { virtualPileInfo: action.data });
         case 'clearVirtualPileInfo':
            return Object.assign({}, state, { virtualPileInfo: null });

        default:
            return state;
    }
};
