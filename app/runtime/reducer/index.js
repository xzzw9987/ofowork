const orderTotal = (state = {}, action) => {
    if (action.type !== 'setOrderTotal') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}OrderTotal`]: action.payload });
};

const orderDelta = (state = {}, action) => {
    if (action.type !== 'setOrderDelta') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}OrderDelta`]: action.payload });
};
const weekOrderDelta = (state = {}, action) => {
    if (action.type !== 'setWeekOrderDelta') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}WeekOrderDelta`]: action.payload });
};
const yesterdayOrder = (state = {}, action) => {
    if (action.type !== 'setYesterday') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}YesterdayOrder`]: action.payload });
};
const lastWeekOrder = (state = {}, action) => {
    if (action.type !== 'setLastWeek') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}LastWeekOrder`]: action.payload });
};

const orderDeltaAmount = (state = {}, action) => {
    if (action.type !== 'setOrderDeltaAmount') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}OrderDeltaAmount`]: action.payload });
};

const orderWeekAmount = (state = {}, action) => {
    if (action.type !== 'setOrderWeekAmount') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}OrderWeekAmount`]: action.payload });
};

const chart = (state = {}, action) => {
    if (action.type !== 'setChart') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}Chart`]: action.payload });
};

const chartDefault = (state = {}, action) => {
    if (action.type !== 'setChartDefault') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}ChartDefault`]: action.payload });
};

const zoneList = (state = {}, action) => {
    if (action.type !== 'setZoneList') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}ZoneList`]: action.payload });
};

const cityList = (state = {}, action) => {
    if (action.type !== 'setCityList') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}CityList`]: action.payload });
};

/* 学校负责人 */
const schoolNames = (state = {}, action) => {
    if (action.type !== 'setSchoolNames') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}schoolNames`]: action.data });
};
/* 学校默认id */
const schoolId = (state = {}, action) => {
    if (action.type !== 'setSchoolId') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}schoolId`]: action.data });
};
/* 学校详情 */
const schoolDetail = (state = {}, action) => {
    if (action.type !== 'setSchoolDetail') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}schoolDetail`]: action.data });
};
/* 学校图表 */
const schoolChart = (state = {}, action) => {
    if (action.type !== 'setSchoolChart') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}schoolChart`]: action.data });
};
const districtList = (state = {}, action) => {
    if (action.type !== 'setDistrictList') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}DistrictList`]: action.payload });
};

const schoolList = (state = {}, action) => {
    if (action.type !== 'setSchoolList') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}SchoolList`]: action.payload });
};

const pageTitle = (state = {}, action) => {
    if (action.type !== 'setPageTitle') return state;
    return Object.assign({}, state, { [`${action.prefix || ''}PageTitle`]: action.payload });
};

const userInfo = (state = {}, action) => {
    if (action.type !== 'setUserInfo') return state;
    return Object.assign({}, state, { userInfo: action.payload });
};

const signInData = (state = {}, action) => {
    if (action.type !== 'getSignInData') return state;
    return Object.assign({}, state, { signInData: action.payload });
};
/*师傅报修率 */
const schoolInfo = (state = {}, action) => {
    if (action.type !== 'setSchoolInfo') return state;
    return Object.assign({}, state, { 'schoolInfo': action.data });
};
/*师傅首页订单量 */
const orderInfo = (state = {}, action) => {
    if (action.type !== 'setOrderInfo') return state;
    return Object.assign({}, state, { 'orderInfo': action.data });
};
/*师傅举报报修列表 */
const repairerList = (state = {}, action) => {
    if (action.type !== 'setRepairerList') return state;
    let data = action.data;
    if (action.concat && action.name == 'Report')
        data = [...state['repairerReportList'], ...data];
    return Object.assign({}, state, { ['repairer' + action.name + 'List']: data });
};

/*举报详情 */
const reportDetail = (state = {}, action) => {
    if (action.type !== 'setReportDetail') return state;
    return Object.assign({}, state, { 'reportDetail': action.data });
};

/*运营举报列表 */
const operationReportManagement = (state = {}, action) => {
    if (action.type !== 'setOperationReportManagement') return state;
    let data = action.data;
    if (action.concat)
        data = [...state['operationReportManagement'], ...data];
    return Object.assign({}, state, { 'operationReportManagement': data });
};

/*运营举报列表详情 */
const operationReportDetail = (state = {}, action) => {
    if (action.type !== 'setOperationReportDetail') return state;
    return Object.assign({}, state, { 'operationReportDetail': action.data });
};

/*运营举报列表详情type */
const reportType = (state = {}, action) => {
    if (action.type !== 'setReportType') return state;
    return Object.assign({}, state, { 'reportType': action.data });
};

/*运营举报处理结果 */
const reportResult = (state = {}, action) => {
    if (action.type !== 'setReportResult') return state;
    return Object.assign({}, state, { 'reportResult': action.data });
};

const unlockCode = (state = {}, action) => {
    if (action.type !== 'getUnlockCode') return state;
    return Object.assign({}, state, { unlockCode: action.payload });
};

const resetUnlockCode = (state = {}, action) => {
    if (action.type !== 'resetUnlockCode') return state;
    return Object.assign({}, state, { resetResult: action.payload });
};

const activeNewCar = (state = {}, action) => {
    if (action.type !== 'activeNewCar') return state;
    return Object.assign({}, state, { activeResult: action.payload });
};

const getZone = (state = {}, action) => {
    if (action.type !== 'getZone') return state;
    return { ...state, zoneInfo: action.payload };
};

const locationStatus = (state = {}, action) => {
    if (action.type !== 'catchYou' && action.type !== 'missYou') return state;
    return { ...state, locationInfo: action.payload };
};

const transportStatus = (state = {}, action) => {
    if (action.type !== 'carTransportSucc' && action.type !== 'carTransportFail') return state;
    return { ...state, transportRes: action.payload };
};

const getSocietyList = (state = {}, action) => {
    if (action.type !== 'getSocietyList') return state;
    return Object.assign({}, state, { societyList: action.payload });
};

const getSchoolList = (state = {}, action) => {
    if (action.type !== 'getSchoolList') return state;
    return Object.assign({}, state, { schoolList: action.payload });
};

const getReportButton = (state = {}, action) => {
    if (action.type !== 'getReportButton') return state;
    return Object.assign({}, state, { showButton: action.payload });
};

// const comparedList = {};
const listToCompare = (state = {}, action) => {
    if (action.type !== 'setListToCompare') return state;
    const { data } = action;
    return Object.assign({}, state, { listToCompare: data });
};

const combineList = (state = {}, action) => {
    if (action.type !== 'setCombineList') return state;
    return Object.assign({}, state, action.payload);
};


const clear = (state = {}, action) => {
    if (action.type !== '@@clear') return state;
    return { ...state, ...action.payload };
};

/* 700bike list */
const bikeList = (state = {}, action) => {
    if (action.type !== 'setBikeList') return state;
    let data = action.data;
    if (action.concat) {
        data = [...state['bikeList'], ...data];
    }
    return Object.assign({}, state, { bikeList: data });
};
const bikeDetail = (state = {}, action) => {
    if (action.type !== 'setBikeDetail') return state;
    return Object.assign({}, state, { bikeDetail: action.data });
};

const clearAll = (state = {}, action) => {
    if (action.type !== '@@clearAll') return state;
    return {};
};

/*运营车牌查询 */
const operationCarSearch = (state = {}, action) => {
    if (action.type !== 'setOperationCarSearch') return state;
    return Object.assign({}, state, { 'operationCarSearch': action.data });
};
const operationCarCode = (state = {}, action) => {
    if (action.type !== 'setOperationCarCode') return state;
    return Object.assign({}, state, { 'operationCarCode': action.data });
};
const operationCarMessage = (state = {}, action) => {
    if (action.type !== 'setOperationCarMessage') return state;
    return Object.assign({}, state, { 'operationCarMessage': action.data });
};
/* 运营负责区域 */
const operationZoneId = (state = {}, action) => {
    if (action.type !== 'setOperationZoneId') return state;
    return Object.assign({}, state, { 'operationZoneId': action.data });
};
/* 运营负责区域id */
const operationZoneNames = (state = {}, action) => {
    if (action.type !== 'setOperationZoneNames') return state;
    return Object.assign({}, state, { 'operationZoneNames': action.data });
};
/* 运营负责区域信息 */
const operationZoneInfo = (state = {}, action) => {
    if (action.type !== 'setOperationZoneInfo') return state;
    return Object.assign({}, state, { 'operationZoneInfo': action.data });
};
/**
 * 运营换牌
 */

const opChangeBrand = (state = {}, action) => {
    if (action.type !== 'opChangeBrand') return state;
    return { ...state, changeBrandStatus: action.payload };
};
/**
 * 修车记录
 */
const subRepairerRecord = (state = {}, action) => {
    if (action.type !== 'subRepairerRecord') return state;
    return { ...state, subRecordStatus: action.payload };
};

/**
 * 坏车标记
 */
const subBadCarMark = (state = {}, action) => {
    if (action.type !== 'subBadCarMark') return state;
    return { ...state, subBadCarMarkStatus: action.payload };
};

/**
 * 地图选点
 */
const subBadCarMarkPos = (state = {}, action) => {
    if (action.type !== 'subBadCarMarkPos') return state;
    return Object.assign({}, state, { 'subBadCarMarkPos': action.payload });
};

/**
 * 车厂
 */
const bicycleStatus = (state = {}, action) => {
    if (action.type !== 'getBicycleStatus') return state;
    return { ...state, bicycleStatus: action.payload };
};

/* 合作工作记录 */
const workRecord = (state = {}, action) => {
    if (action.type !== 'setWorkRecord') return state;
    return Object.assign({}, state, { 'workRecord': action.data });
};

const workRecordDetail = (state = {}, action) => {
    if (action.type !== 'setWorkRecordDetail') return state;
    let data = action.data;
    if (action.concat)
        data = [...state['workRecordDetail'], ...data];
    return Object.assign({}, state, { 'workRecordDetail': data });
};


/*坏车列表 */
const hxList = (state = {}, action) => {
    if (action.type !== 'setHxList') return state;
    let data = action.data;
    if (action.concat)
        data = [...state['hxList'], ...data];
    return Object.assign({}, state, { 'hxList': data });
};

/**
 * 工作台
 */
const benchList = (state = {}, action) => {
    if (action.type !== 'fetchBench') return state;
    return { ...state, benchList: action.payload };
};

/* 运营坏车标记数据概览 */
const badCarData = (state = {}, action) => {
    if (action.type !== 'setBadCarData') return state;
    return Object.assign({}, state, { 'badCarData': action.data });
};
const badCarMessage = (state = {}, action) => {
    if (action.type !== 'setBadCarMessage') return state;
    let data = action.data;
    if (action.concat)
        data = [...state['badCarMessage'], ...data];
    return Object.assign({}, state, { 'badCarMessage': data });
};

/**
 * 报修率
 */
const repairRateList = (state = {}, action) => {
    if (action.type !== 'getRepairRate') return state;
    return { ...state, [`${action.prefix || ''}RepairRateList`]: action.payload };
};
/* 路面信息采集 */
const collectCompetitorImg = (state = {}, action) => {
    if (action.type !== 'setCollectCompetitorImg') return state;
    return Object.assign({}, state, { 'collectCompetitorImg': action.data });
};
const collectCompetitorInformation = (state = {}, action) => {
    if (action.type !== 'setCollectCompetitorInformation') return state;
    return Object.assign({}, state, { 'collectCompetitorInformation': action.data });
};

const scmPrefix = (state = {}, action) => {
    if (action.type !== 'setScmPre') return state;
    return { ...state, scmPrefix: action.payload };
};
const scmDetail = (state = {}, action) => {
    if (action.type !== 'fetchScmDetail') return state;
    return { ...state, scmDetail: action.payload };
};
const scmAddBsn = (state = {}, action) => {
    if (action.type !== 'addBsn') return state;
    return { ...state, bsnList: [...state.bsnList || [], action.payload] };
};
const scmBsnAuth = (state = {}, action) => {
    if (action.type !== 'fetchBsnAuth') return state;
    return { ...state, bsnAuth: action.payload };
};
const scmUpload = (state = {}, action) => {
    if (action.type !== 'uploadScm') return state;
    return { ...state, uploadScm: action.payload };
};



const processReducers = [
    orderTotal,
    orderDelta,
    weekOrderDelta,
    chart,
    chartDefault,
    orderDeltaAmount,
    orderWeekAmount,
    zoneList,
    cityList,
    schoolNames,
    schoolId,
    schoolDetail,
    schoolList,
    schoolChart,
    districtList,
    pageTitle,
    userInfo,
    signInData,
    schoolInfo,
    orderInfo,
    repairerList,
    reportDetail,
    unlockCode,
    operationReportManagement,
    operationReportDetail,
    reportType,
    reportResult,
    reportDetail,
    resetUnlockCode,
    activeNewCar,
    getZone,
    clear,
    getSchoolList,
    getSocietyList,
    listToCompare,
    combineList,
    getReportButton,
    bikeList,
    bikeDetail,
    operationCarSearch,
    operationCarCode,
    operationCarMessage,
    operationZoneNames,
    operationZoneId,
    operationZoneInfo,
    clearAll,
    locationStatus,
    transportStatus,
    benchList,
    bicycleStatus,
    workRecord,
    hxList,
    workRecordDetail,
    opChangeBrand,
    subRepairerRecord,
    subBadCarMark,
    subBadCarMarkPos,
    badCarData,
    badCarMessage,
    collectCompetitorImg,
    collectCompetitorInformation,
    repairRateList,
    yesterdayOrder,
    lastWeekOrder,
    scmPrefix,
    scmDetail,
    scmAddBsn,
    scmBsnAuth,
    scmUpload
];


export default function (state = {}, action) {
    return processReducers.reduce((prev, now) => now(prev, action), state);
}

export { repairerList };
