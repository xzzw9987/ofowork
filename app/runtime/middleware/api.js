import _ from 'lodash';
import { fetch } from '../../modules';

var Symbol = require('es6-symbol');
// require('fetch-polyfill');
// require('whatwg-fetch');

export const CALL_API = Symbol('CALL_API');

var pendingRequest = {};

export default store => next => action => {
    if (!action[CALL_API]) {
        return next(action);
    }

    let request = action[CALL_API];
    let { getState, dispatch } = store;
    dispatch({ type: 'loading', data: true });
    let { method, path, body, successType, errorType, afterSuccess, afterError } = request;

	/*let form = null;
	if (method.toUpperCase() === 'POST') {
		form = new FormData();
		Object.keys(body).forEach(key => form.append(key, body[key]));
	}*/
    let url = `${path}`;
    // if (url in pendingRequest){
    // 	console.log("skip dup request:", url);
    // 	return Promise.resolve("skip dup request: " + url);
    // }
    pendingRequest[url] = 1;
    return fetch(url, {
        method: method,
        body: body
    }).then(json => {
        setTimeout(() => {
            dispatch({ type: 'loading', data: false });
        }, 1000);
        delete pendingRequest[url];
        next({
            type: successType,
            response: json,
            request: body,
        });
        if (_.isFunction(afterSuccess)) {
            afterSuccess({ getState });
        }
        return null;
    }).catch(error => {
        setTimeout(() => {
            dispatch({ type: 'loading', data: false });
        }, 1000);
        delete pendingRequest[url];
        if (errorType) {
            next({
                type: errorType,
                error: err
            });
        }
        if (_.isFunction(afterError)) {
            afterError({ getState });
        }
        return null;
    });
};
