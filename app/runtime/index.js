import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import apiMiddleware from './middleware/api';
import createLogger from 'redux-logger';
import React from 'react';

var reducers = require('./reducers');

const logger = createLogger({
    level: 'info',
    collapsed: false,
    logger: console,
    predicate: (getState, action) => true
});

const composeEnhancers =
    process.env.NODE_ENV !== 'production' &&
        typeof window === 'object' &&
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            // Specify here name, actionsBlacklist, actionsCreators and other options
        }) : compose;

const createStoreWithMiddleware = composeEnhancers(
    applyMiddleware(
        thunkMiddleware,
        apiMiddleware,
        // logger,
    )
)(createStore);

export var store = function (initState) {
    return createStoreWithMiddleware(combineReducers(reducers), initState);
};

export var wrap = function (renderCnt) {
    return React.createClass({
        render: function() {
            return React.cloneElement(renderCnt, {...renderCnt.props, ...this.props});
        }
    });
};

