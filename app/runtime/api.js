import { CALL_API } from './middleware/api';
import Storage from '../base/storage';
import { fetch } from '../modules';
import apibase from './apiBase';
import { startUploadLocation } from '../modules/device';
/*eslint-env node*/
// const wsURL = 'ws://47.94.113.52/ws';
const wsURL = 'ws://xinzhongzhu.tech:6501';

const pathcfg = {
    captcha: '/v2/getCaptcha',
    verifyCode: '/v2/getVerifyCode',
    login: '/user/login',
    getVerifyCode: '/user/get-verify-code',
    userInfo: '/user/get-info',
    userModifyPassword: '/user/modify-password',
    carsWaitForRepair: '/repairer/get-bicycle',
    carsWaitForRepairList: '/repairer/get-list',
    carWaitForRepairDetail: '/repairer/get-detail',
    repairerCheckRecords: '/repairer/attendance-history',
    repaireRecord: '/repairer/repaireRecord',
    repairerRepairDone: '/repairer/done',
    repairerCheckInOut: '/repairer/attendance',
    repairerCoordinate: '/repairer/get-bicycle-coordinate',
    zoneChildZoneList: '/zone/get-child-zone-list',
    zoneAllCityZoneList: '/zone/get-child-zone-list',
    zoneAllRegionZoneList: '/zone/get-child-zone-list',
    zoneTodayOrderList: '/order/get-zone-today-order',
    zoneCityTodayOrderList: '/order/get-zone-today-order',
    zoneCityAllTodayOrderList: '/order/get-zone-today-order',
    zoneRegionAllTodayOrderList: '/order/get-zone-today-order',
    zoneOrderHistor: '/order/get-zone-history-order',
    carsRepairRate: '/repairer/get-repair-rate', //报修率
    carSearchNum: '/repairer/search-bicycle-num', //车牌号查询
    updateUnlockCode: '/repairer/update-unlock-code', //修改解锁码
    selectVirtualPosition: '/repairer/get-virtual-pile',
    reportiVirtualPile: '/repairer/report-virtual-bicycle-number',
    repairerShowReport: '/repairer/show-report',
    reportManagement: '/report/operation-get-list', //举报管理
};

export var keys = {};
Object.keys(pathcfg).forEach((key) => {
    keys[[key]] = key;
});

export function api(pathkey, body, succtype) {
    if (pathkey in keys) {
        return {
            [CALL_API]: {
                method: 'post',
                path: apibase + pathcfg[pathkey],
                body,
                successType: succtype || pathkey
            }
        };
    } else if (pathkey === 'clear') {
        return { type: 'clear', data: body };
    } else if (pathkey === 'token') {
        return { type: 'token', data: body };
    }
    else {
        // TODO should return an error action
    }
}

export function loadToken(call) {
    if (!this.props.token) {
        Storage.getItem('token').then((token) => {
            if (token) {
                this.props.dispatch({ type: 'token', data: token });
                this.props.dispatch(api(keys.userInfo, { token }));
            } else {
                // Warn
                JumpTo('login', false);
            }
        });
    } else {
        call();
    }
}
let _token;
export function _loadToken() {
    return new Promise(resolve => {
        if (!_token) {
            Storage.getItem('token').then((token) => {
                if (token) {
                    _token = token;
                    resolve(token);
                } else {
                    // Warn
                    JumpTo('login', false);
                }
            });
        }
        else resolve(_token);
    });
}

let userInfo;
export function getUserInfo() {
    return _loadToken()
        .then(token => {
            // alert(1);
            if (userInfo)
                return userInfo;
            return fetch(`${apibase}${pathcfg.userInfo}`, { method: 'POST', body: { token } })
                .then(d => {
                    userInfo = Object.assign({}, d.data, { token });
                    if(userInfo.identity == 'repairer' || (userInfo.identity == 'operation' && (userInfo.operation_level === 'society' || userInfo.operation_level === 'school'))){
                        startUploadLocation(token, `${wsURL}?token=${token}`);
                    }
                    return userInfo;
                });
        });
}

export function clear() {
    _token = null;
    userInfo = null;
}


export function parseNum(num) {
    var reg = /(?=(?!\b)(\d{3})+$)/g;
    return String(num).replace(reg, ',');
}

export function getNowFormatDate() {
    var date = new Date();
    var date2 = new Date(date.getTime() - 7 * 24 * 3600 * 1000);

    function FormatDate(time) {
        var year = time.getFullYear();
        var month = time.getMonth() + 1;
        var strDate = time.getDate();
        if (month >= 1 && month <= 9) {
            month = '0' + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = '0' + strDate;
        }
        var formatDate = year + '-' + month + '-' + strDate;
        return formatDate;
    }

    var dateObj = {
        'todayDate': FormatDate(date),
        'prevDate': FormatDate(date2)
    };
    return dateObj;
}
export function showWeekCN(dayOfEN) {
    return {
        'Monday': '周一',
        'Tuesday': '周二',
        'Wednesday': '周三',
        'Thursday': '周四',
        'Friday': '周五',
        'Saturday': '周六',
        'Sunday': '周日',
    }[dayOfEN] || '未知';
}
export function numberSplit(a) {
    a = '' + a;
    let x = parseInt(a.length / 3, 10), arr = [];
    for (let i = 1; i <= x + 1; i++) {
        let start = a.length - i * 3, end = a.length - i * 3 + 3;
        if (start < 0) start = 0;
        a.substring(start, end) && arr.unshift(a.substring(start, end));
    }
    return arr.join(',');
}
export function getDateDiff(date) {
    var dateTimeStamp = Date.parse(date.replace(/-/gi, '/'));
    var minute = 1000 * 60;
    var result;
    var now = new Date().getTime();
    var diffValue = now - dateTimeStamp;
    if(diffValue < 0){return;}
    var minC = diffValue/minute;
    if(minC <= 1){
        result = '刚刚';
    }else if(minC>=1 && minC <= 15){
        result = ''+ parseInt(minC) +'分钟前';
    }else{
        if(new Date(dateTimeStamp).toDateString() === new Date().toDateString()) {
            result = '今天' + date.substring(11, 16);
        }else{
            result = date.substring(5, date.length-3);
        }

    }

    return result;
}
export function getTodayDate(date) {
    var dateTimeStamp = Date.parse(date.replace(/-/gi, '/'));
    if (new Date(dateTimeStamp).toDateString() === new Date().toDateString()) {
        return '今天' + date.substring(11, 16);
    }else{
        return date.substring(5, date.length-3);
    }

}
export function timeStamp( second_time ){

    var time = parseInt(second_time) + '秒';
    if( parseInt(second_time )> 60){

        var second = parseInt(second_time) % 60;
        var min = parseInt(second_time / 60);
        time = min + '分钟';

        if( min > 60 ){
            min = parseInt(second_time / 60) % 60;
            var hour = parseInt( parseInt(second_time / 60) /60 );
            time = hour + '小时' + min + '分';

            if( hour > 24 ){
                hour = parseInt( parseInt(second_time / 60) /60 ) % 24;
                var day = parseInt( parseInt( parseInt(second_time / 60) /60 ) / 24 );
                time = day + '天' + hour + '小时';
            }
        }


    }
    return time;
}

export function timeSilence( second_time ){

    var time = parseInt(second_time) + '秒';
    if( parseInt(second_time )> 60){

        var second = parseInt(second_time) % 60;
        var min = parseInt(second_time / 60);
        time = min + '分钟' + second + '秒';

        if( min > 60 ){
            min = parseInt(second_time / 60) % 60;
            var hour = parseInt( parseInt(second_time / 60) /60 );
            time = hour + '小时' + min + '分' + second + '秒';

            if( hour > 24 ){
                hour = parseInt( parseInt(second_time / 60) /60 ) % 24;
                var day = parseInt( parseInt( parseInt(second_time / 60) /60 ) / 24 );
                time = day + '天' + hour + '小时' + min + '分' + second + '秒';
            }
        }
    }
    return time;
}

export { apibase };
