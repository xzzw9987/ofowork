import { fetch } from '../../modules';
import { _loadToken, getUserInfo, apibase, getNowFormatDate, showWeekCN } from '../api';
var moment = require('moment');
import { historyStack } from '../../modules';
import whereami from '../../base/map/whereami';
/**
 * 师傅首页
 */
export function fetchRepairerIndex(props) {

    return (dispatch, getState) => {
        // @mock
        // getToken.then(...)

        return getUserInfo()
            .then((d) => {
                let userInfo = d;
                //let {userInfo} = d ;
                let promiseList = [];
                let {token} = userInfo;
                let {repairer_level} = userInfo;
                let zone_ids, zone_names;
                zone_ids = userInfo['zone_ids'];
                zone_names = userInfo['zone_names'];
                let zone_school = zone_ids.split(',').map((id, index) => {
                    return {
                        id: id,
                        name: zone_names.split(',')[index]
                    };
                });
                let zoneId = props && props.schoolId;
                let page = props && props.page;
                let concat = props && props.concat;
                let currentId;
                if (zoneId) {
                    currentId = zoneId;
                } else {
                    currentId = zone_ids.split(',')[0];
                }
                dispatch({ type: 'setSchoolId', data: currentId });
                dispatch({ type: 'setSchoolNames', data: zone_school });

                //获取今日订单量
                let orderInfo = {};
                promiseList.push(fetch(`${apibase}/order/get-zone-today-detail`, {
                    method: 'POST',
                    body: { token, 'zone_id': currentId, 'zone_ids': currentId, 'level': repairer_level }
                }).then(respones => {
                    let data = respones['data']['list'][0];
                    let sign = data['increase_proportion'] >= 0 ? '+' : '-';
                    let textColor = sign == '+' ? '#FF7000' : '#2EBF38';
                    orderInfo['increase'] = Math.abs(data['num'] - data['yesterday_num']);
                    orderInfo['num'] = data['num'];
                    orderInfo['sign'] = sign;
                    orderInfo['textColor'] = textColor;
                    dispatch({ type: 'setOrderInfo', data: orderInfo });
                }));
                // 获取报修率
                let schoolInfo = {};
                promiseList.push(fetch(`${apibase}/repairer/get-repair-rate`, {
                    method: 'POST',
                    body: { token, 'zone_id': currentId, 'type': 'today' }
                }).then(respones => {
                    if (respones['code'] === 0) {
                        schoolInfo['rate'] = respones['data']['rate'];
                        let rateText = '未知';
                        let bgColor = '#E23000';
                        if (schoolInfo['rate'] < 5) {
                            rateText = '正常';
                            bgColor = '#2EBF38';
                        } else if (schoolInfo['rate'] >= 5 && schoolInfo['rate'] < 10) {
                            rateText = '较高';
                            bgColor = '#FF8300';
                        } else if (schoolInfo['rate'] > 10) {
                            rateText = '很高';
                            bgColor = '#E23000';
                        }
                        schoolInfo['color'] = bgColor;
                        schoolInfo['rateText'] = rateText;
                        dispatch({ type: 'setSchoolInfo', data: schoolInfo });
                    }
                }));
                whereami().then(d => {
                    let {latitude, longitude} = d;
                    //维修列表
                    fetch(`${apibase}/maintenance/list`, {
                        method: 'POST',
                        body: { token, 'zone_id': currentId, 'lat': latitude, 'lng': longitude }
                    }).then(respones => {
                        var data = [];
                        if (respones['data']['list']) {
                            data = respones['data']['list'].map(v => ({ ...v, distance: d.error ? null : v.distance }));
                        }
                        dispatch({ type: 'setRepairerList', concat: concat, data: data, name: 'Report' });
                    });

                });

                return Promise.all(promiseList);
            });
    };
}
