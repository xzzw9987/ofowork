import {fetch} from '../../../modules';
import {_loadToken, getUserInfo, apibase, getNowFormatDate, showWeekCN} from '../../api';
var moment = require('moment');
import {historyStack} from '../../../modules';
import whereami from '../../../base/map/whereami';

/**
 * 坏车详情
 */
export function fetchHxDetail(zoneId) {

    return (dispatch, getState)=> {
        // @mock
        // getToken.then(...)

        return getUserInfo()
            .then((userInfo)=>{
              // let {userInfo, latitude, longitude} = d ;
                let id = historyStack.get().id;
                let promiseList = [];
                let {token} = userInfo;
               //获取坏车详情
                if(id){
                    promiseList.push(fetch(`${apibase}/hx/detail`, {
                        method: 'POST',
                        body: {token, 'id': id}
                    }).then(respones=> {
                        dispatch({type: 'setReportDetail', data: respones['data']});

                    }));
                }

                return Promise.all(promiseList);

            });
    };
}
