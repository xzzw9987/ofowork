import {fetch} from '../../../modules';
import {_loadToken, getUserInfo, apibase, getNowFormatDate, showWeekCN} from '../../api';
var moment = require('moment');
import {historyStack} from '../../../modules';

/**
 * 运营工作记录
 */
export function fetchWorkRecord(props) {
    return (dispatch, getState)=> {
        // @mock
        // getToken.then(...)
        return getUserInfo()
            .then(userInfo=> {
                let promiseList = [];
                let argument = {};
                argument['token'] = userInfo['token'];
                if(props.phone && props.phone != ''){
                    argument['phone'] = props.phone;
                }
                //获取工作记录
                promiseList.push(fetch(`${apibase}/hx/record-list`, {
                    method: 'POST',
                    body: argument,
                }).then(respones=>{
                    dispatch({type: 'setWorkRecord', data: respones['data']});
                }));
                return Promise.all(promiseList);
            });
    };
}

