import {fetch} from '../../../modules';
import {_loadToken, getUserInfo, apibase, getNowFormatDate, showWeekCN} from '../../api';
var moment = require('moment');
import {historyStack} from '../../../modules';

/**
 * 工作记录详情
 */
export function fetchWorkRecordDetail(props) {
    return (dispatch, getState)=> {
        // @mock
        // getToken.then(...)
        return getUserInfo()
            .then(userInfo=> {
                let promiseList = [];
                let argument = {};
                argument['token'] = userInfo['token'];
                argument['type'] = props.type;
                argument['date'] = props.date;
                argument['page'] = props.page || 0;
                if(props.phone && props.phone != ''){
                    argument['phone'] = props.phone;
                }
                //获取举报列表
                promiseList.push(fetch(`${apibase}/hx/record-day-list`, {
                    method: 'POST',
                    body: argument,
                }).then(respones=> {
                    dispatch({type: 'setWorkRecordDetail', concat: props.concat, data: respones['data']['list']});
                }));
                return Promise.all(promiseList);
            });
    };
}
