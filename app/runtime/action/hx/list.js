import { fetch } from '../../../modules';
import { _loadToken, getUserInfo, apibase, getNowFormatDate, showWeekCN } from '../../api';
var moment = require('moment');
import { historyStack } from '../../../modules';
import whereami from '../../../base/map/whereami';
/**
 * 坏车列表
 */
export function fetchHxList(props) {

    return (dispatch, getState) => {
        // @mock
        // getToken.then(...)

        return getUserInfo()
            .then((d) => {
                let userInfo = d;
                //let {userInfo} = d ;
                let promiseList = [];
                let {token} = userInfo;
                let area_code = userInfo['zone_ids'];
                let concat = props && props.concat;
                whereami().then(d => {
                    let {latitude, longitude} = d;
                    //维修列表
                    promiseList.push(fetch(`${apibase}/hx/list`, {
                        method: 'POST',
                        body: { token, 'lat': latitude, 'lng': longitude, area_code}
                    }).then(respones => {
                        var data = [];
                        if (respones['data']['list']) {
                            data = respones['data']['list'];
                        }
                        dispatch({ type: 'setHxList', concat: concat, data: data });
                    }));

                });

                return Promise.all(promiseList);
            });
    };
}
