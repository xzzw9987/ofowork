export { fetchOperationReportManagement } from './operation/reportManagement';
export { fetchOperationReportDetail } from './operation/reportDetail';
/**
 * assemble all actions
 * here
 */
export { fetchCountryIndex, fetchCountryDetail } from './operation/country';
export { fetchCityIndex, fetchCityDetail } from './operation/city';
export { fetchSchoolIndex, fetchSchoolDetail } from './operation/school';
export { fetchRegionDetail, fetchRegionIndex } from './operation/region';
export {
    fetchSocietyDetail,
    fetchSocietyIndex,
    fetchDistrictIndex
} from './operation/society';
export { fetchRepairerBench } from './repairer/bench';
export { fetchUnlockCode } from './repairer/searchCode';
export { resetUnlockCode } from './repairer/resetCode';
export { activeNewCar, activeSmartCar } from './repairer/carActive';
export { getZone } from './repairer/getZone';
export { getZoneList } from './repairer/getZoneList';
export { getLocation } from './repairer/getLocation';
export { bicycleRecycle } from './repairer/transportation';

/**
 * 师傅
 */
export { fetchRepairerIndex } from './repairerIndex';
export { fetchRepairerList } from './repairerIndex';
export { subRepairerRecord } from './repairer/repairRecord';
export {
    subBadCarMark,
    subBadCarMarkPos,
    clearBadCarPos
} from './repairer/badCarMark';

/**
 * 举报详情
 */
export { fetchReportDetail } from './repairReportDetail';
/**
 * 700bike
 */
export { fetchBikeList } from './700Bike/index';
export { fetchBikeDetail } from './700Bike/bikeDetail';

/**
 * 运营车牌查询,解锁码更改，换车牌，城市区域审核坏车
 */
export { fetchOperationCarSearch } from './operation/carSearch';
export { opChangeBrand } from './operation/changeBrand.js';

/**
 * 运营报修率
 */
export { fetchOperationRepairRate } from './operation/repairRate';
export { fetchOperationNewUsers } from './operation/newUsers';

/**
 * 工作记录
 */
export { fetchWorkRecord } from './hx/workRecord';
export { fetchWorkRecordDetail } from './hx/workRecordDetail';

/**
 * 车厂
 */
export { fetchStatus } from './factory/status';
export { factoryCodeSearch } from './factory/codeSearch.js';

/**
 * 合作
 */
export { fetchHxList } from './hx/list';
export { fetchHxDetail } from './hx/detail';

/**
 * 公共
 */

export { fetchBench } from './fetchBench';

/**
 * 运营坏车标记数据概览
 */
export { fetchBadCarData, fetchBadCarMessage } from './operation/badCarData';

/**
 * 报修率
 */
export { fetchRepairRate } from './operation/fetchRepairRate';

/**
 * 路面信息
 */
export { fetchCompetitorInformation } from './operation/competitorInformation';

/**
 * scm
 */
export { setPrefix } from './scm/setPre';
export { addBsn } from './scm/addBsn';
export { fetchScmDetail } from './scm/detail';
export { fetchBsnAuth } from './scm/fetchBsnAuth';
export { uploadScmOrder } from './scm/uploadScmOrder';
