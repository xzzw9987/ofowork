import { fetch } from '../../../modules';
import { getUserInfo, apibase } from '../../api';

export const bicycleRecycle = options => async dispatch => {
    const { token } = await getUserInfo();
    const url = `${apibase}/dispatch/bicycle`;
    const res = await fetch(url, {
        method: 'POST',
        body: {
            token,
            bicycle_no: options.carno,
            lat: options.latitude,
            lng: options.longitude,
            'accuracy': options.accuracy === null ? -1 : options.accuracy,
            type: options.type,
        }
    });
    res.code === 0 ?
        dispatch({ type: 'carTransportSucc', payload: res }) :
        dispatch({ type: 'carTransportFail', payload: res });
};
