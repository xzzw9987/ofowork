import { fetch } from '../../../modules';
import { getUserInfo, apibase } from '../../api';
import Storage from '../../../base/storage';
import whereami from '../../../base/map/whereami';

// 获取区域
export const getZone = options => async dispatch => {
    const token = await Storage.getItem('token');
    const location = await whereami();
    !location.error && dispatch({ type: 'catchYou', payload: location });
    location.error && dispatch({ type: 'missYou', payload: location });
    const url = `${apibase}/zone/get-zone-by-location`;
    const res = await fetch(url, {
        method: 'POST',
        body: {
            token,
            lat: location.latitude,
            lng: location.longitude,
        }
    });
    res.code === 0 &&
        dispatch({ type: 'getZone', payload: res.data });
};
