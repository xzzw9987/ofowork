import Storage from '../../../base/storage';
import whereami from '../../../base/map/whereami';

export const getLocation = options => async dispatch => {
    const token = await Storage.getItem('token');
    const res = await whereami();
    !res.error && dispatch({ type: 'catchYou', payload: res });
    res.error && dispatch({ type: 'missYou', payload: res });
};
