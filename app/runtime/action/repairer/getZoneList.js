import { fetch } from '../../../modules';
import { getUserInfo, apibase } from '../../api';

// 获取区域
export const getZoneList = options => async dispatch => {
    const { token } = await getUserInfo();
    const promiseList = [];

    const getList = async levelType => {
        const res = await fetch(`${apibase}/zone/get-child-zone-list`, {
            method: 'POST',
            body: {
                token,
                'zone_id': options.zone_id,
                'current_level': options.current_level,
                'child_level': levelType,
            }
        });
        const list = handleList(res.data.list, levelType);
        if (levelType === 'society') return dispatch({ type: 'getSocietyList', payload: list });
        return dispatch({ type: 'getSchoolList', payload: list });
    };

    promiseList.push(
        getList('society'),
        getList('school')
    );

    return Promise.all(promiseList);
};

function handleList(list, zoneType) {
    list.map((zone) => zone.zone_level = zoneType);
    return list;
}
