import { fetch } from '../../../modules';
import { getUserInfo, apibase } from '../../api';
import whereami from '../../../base/map/whereami';

export const fetchRepairerBench = options => async (dispatch, getState) => {
    const { token } = await getUserInfo();
    const promiseList = [];

    // 签到
    promiseList.push(request(`${apibase}/repairer/get-attendance-overview`, {}, 'getSignInData'));
    // 上报虚拟装
    promiseList.push(request(`${apibase}/repairer/show-report`, { zone_id: options.schoolId }, 'getReportButton'));

    return Promise.all(promiseList);

    async function request(url, options, type) {
        const res = await fetch(url, {
            method: 'POST',
            body: {
                token,
                ...options,
            }
        });
        res.code === 0 &&
            dispatch({ type: `${type}`, payload: res.data });
    }
};
