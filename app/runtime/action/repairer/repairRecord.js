import { fetch } from '../../../modules';
import { getUserInfo, apibase } from '../../api';
import whereami from '../../../base/map/whereami';

// 修车记录
export const subRepairerRecord = options => async dispatch => {
    const { token } = await getUserInfo();
    const location = await whereami();
    const url = `${apibase}/repairer/m-bicycle-log`;
    const res = await fetch(url, {
        method: 'POST',
        body: {
            token,
            bicycle_no: options.carno,
            lat: location.latitude,
            lng: location.longitude,
            img_path: options.img_path,
            reason: options.reason,
        }
    });
    dispatch({ type: 'subRepairerRecord', payload: res });
};
