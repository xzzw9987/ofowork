import {fetch} from '../../../modules';
import {getUserInfo, apibase} from '../../api';
import whereami from '../../../base/map/whereami';

// 坏车上报
export const subBadCarMark = options => async dispatch => {
    const {token} = await getUserInfo();
    const location = await whereami();
    const url = `${apibase}/repairer/record-broken-bicycle`;
    const res = await fetch(url, {
        method: 'POST',
        body: {
            token,
            img_path: options.img_path,
            num: options.num,
            lat: location.latitude,
            lng: location.longitude,
            // 'accuracy': 15,
        }
    });
    dispatch({type: 'subBadCarMark', payload: res});
};

export const subBadCarMarkPos = pos => ({
    type: 'subBadCarMarkPos',
    payload: pos
});

export const clearBadCarPos = () => ({
    type: 'subBadCarMarkPos',
    payload: {}
});
