import { fetch } from '../../../modules';
import { getUserInfo, apibase } from '../../api';

// 激活新车
export const activeNewCar = options => async dispatch => {
    const { token } = await getUserInfo();
    const url = `${apibase}/supply/active-bicycle`;
    const res = await request(token, url, options);
    res.code === 0 && dispatch({ type: 'activeNewCar', payload: res.data });
    return res;
};

// 激活智能锁
export const activeSmartCar = options => async dispatch => {
    const { token } = await getUserInfo();
    const url = `${apibase}/supply/active-intelligent-bicycle`;
    const res = await request(token, url, options);
    res.code === 0 && dispatch({ type: 'activeNewCar', payload: res.data });
    return res;
};

function request(token, url, options) {
    return fetch(url, {
        method: 'POST',
        body: {
            token,
            bicycle_no: options.carno,
            zone_id: options.zone_id,
            zone_level: options.zone_level,
            new_unlock_code: options.newCode || '',
            //   lat: options.latitude || '',
            //   lng: options.longitude || '',
            //   accuracy: options.accuracy === null ? -1 : options.accuracy || -1
        }
    });
}
