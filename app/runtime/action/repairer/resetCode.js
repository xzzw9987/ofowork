import { fetch } from '../../../modules';
import { getUserInfo, apibase } from '../../api';

// 修改解锁码
export const resetUnlockCode = options => async dispatch => {
    const { token } = await getUserInfo();
    const url = `${apibase}/repairer/update-unlock-code`;
    const res = await fetch(url, {
        method: 'POST',
        body: {
            token,
            'bicycle_num': options.carno,
            'new_unlock_code': options.newCode,
        }
    });
    dispatch({ type: 'resetUnlockCode', payload: res.data });
};
