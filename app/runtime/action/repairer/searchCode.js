import { fetch } from '../../../modules';
import { getUserInfo, apibase } from '../../api';

// 解锁码
export const fetchUnlockCode = options => async dispatch => {
    const { token } = await getUserInfo();
    const url = `${apibase}/supply/get-bicycle`;
    const res = await fetch(url, {
        method: 'POST',
        body: {
            token,
            'bicycle_no': options.carno,
        }
    });
    res.code === 0 &&
        dispatch({ type: 'getUnlockCode', payload: res.data });
    return res;
};
