import {fetch} from '../../../modules';
import {_loadToken, getUserInfo, apibase, getNowFormatDate, showWeekCN} from '../../api';
var moment = require('moment');
import {historyStack} from '../../../modules';



/**
 * @action
 * 学校负责人--首页
 */
export function fetchSchoolIndex(props) {
    console.log("args===================", props);
    return (dispatch, getState)=> {
        // @mock
        // getToken.then(...)

        return getUserInfo()
            .then(userInfo=> {
                console.log(props);
                dispatch({type: 'setUserInfo', payload: userInfo});

                let h = historyStack.get() || {};
                let promiseList = [];
                let {token} = userInfo;
                let zone_ids, zone_names;
                let isStack = false;
                if (historyStack.get() && historyStack.get().id) {
                    zone_ids = historyStack.get().id;
                    zone_names = historyStack.get().title;
                    isStack = true;
                } else {
                    zone_ids = userInfo['zone_ids'];
                    zone_names = userInfo['zone_names'];
                    isStack = false;
                }
                console.log("zone_idszone_idszone_idszone_idszone_ids", zone_ids)
                console.log("historyStack.get()=====================", historyStack.get())
                let zone_school = zone_ids.split(',').map((id, index)=> {
                    return {
                        id: id,
                        name: zone_names.split(',')[index]
                    }
                });
                if((!props.schoolId && !isStack)  || isStack){
                   dispatch({type: "setSchoolId", prefix: props.random, data: zone_ids.split(',')[0]});
                }
                dispatch({type: "setSchoolNames", prefix: props.random, data: zone_school});
                promiseList.push(fetch(`${apibase}/order/get-zone-today-detail`, {
                    method: 'POST',
                    body: {token, 'zone_ids': zone_ids, level: h.level || 'school'}
                }).then(respones=> {
                    let schoolDetail = respones['data']['list'].reduce((prev, now)=> {
                        return {...prev, [now['zone_id']]: now};
                    }, {});
                    console.log("schoolDetail.............", schoolDetail)
                    dispatch({type: "setSchoolDetail", prefix: props.random, data: schoolDetail});

                    //getHourTrend();
                }));

                /**
                 * 获取所有学校的图表
                 */
                let allSchoolChart = [];
                let hourChartArr = {};
                let date = getNowFormatDate();
                promiseList.push(Promise.all(zone_ids.split(',').map((id, index)=> {
                    return Promise.all([getHourTrend(id), geeDayTrend(id), getWeekTrend(id)])
                        .then(list=> {
                            return {id, list};
                        });
                })).then(d=> {
                    let ret = {};
                    d.forEach(v=>ret[v.id] = v.list);
                    return ret;
                }).then(result=> {
                    dispatch({type: "setSchoolChart", prefix: props.random, data: result});
                    dispatch(chartDefault([0/* dataIndex */, 7, 4], `${props.random || ''}school`));
                    console.log("所有学校的图表啊＝＝＝＝＝＝＝＝＝＝＝＝", result)
                }));
                /*获取订单小时级分布 */
                function getHourTrend(currentId) {
                    return fetch(`${apibase}/order/get-hourly-order-num`, {
                        method: 'POST',
                        body: {
                            token,
                            'date': '' + date.prevDate + ',' + date.todayDate,
                            //'date':'2016-11-28,2016-11-29',
                            'zone_level': h.level || 'school',
                            'zone_id': currentId
                        }
                        //body: {token,'date':'2016-11-24,2016-11-25', 'zone_level': 'school', 'zone_id': currentId}
                    }).then(respones=> {
                        //console.log("小时级趋势接口返回================",respones)
                        let hourList = respones['data']['list'].reduce((prev, now)=> {
                            prev['y'].push(now['num_list'].split(","));
                            return prev;
                        }, {y: []});
                        let extraData = [];
                        hourList.y[0].map((item, idx) => {
                            let idx2 = idx + 1;
                            extraData.push({
                                "d1": idx + '-' + idx2 + "时订单量",
                                "d2": "上周此时段:" + item,
                                "d3": "今天此时段:" + hourList.y[1][idx]
                            })
                        })
                        hourList.extraData = extraData;
                        hourList.type = 'lineChart';
                        hourChartArr[currentId] = hourList;
                        //console.log("小时级订单hourChartArr=======",hourChartArr);
                        return hourList;
                    });
                }

                /*获取日订单趋势 */
                let dayChartArr = {};

                function geeDayTrend(currentId) {

                    return fetch(`${apibase}/order/get-day-trend`, {
                        method: 'POST',
                        body: {
                            token,
                            'level': h.level || 'school',
                            'zone_id': currentId,
                            'start_time': date.prevDate,
                            'end_time': date.todayDate
                        }
                    }).then(respones=> {
                        //console.log("日订单趋势接口返回=======",respones)
                        let dayTrend = respones['data']['list'].reduce((prev, now, i)=> {
                            prev['x'].push(whichDay(now['date']));
                            prev['y'].push(now['num']);
                            let text = now['increase_proportion'] >= 0 ? "上涨" : "下跌";
                            prev['extraData'].push(
                                {"d1": i === respones['data']['list'].length - 1 ? '' : monthAndDay(now['date']) + "订单量比前一天" + text + Math.abs(now['increase_proportion']) + "%"}
                            );
                            prev['type'] = 'verticalChart'
                            return prev;

                        }, {x: [], y: [], extraData: []});
                        dayChartArr[currentId] = dayTrend;
                        //dispatch(chart(chartArr,'school'));
                        //console.log("日订单趋势dayChartArr================",dayChartArr);
                        return dayTrend;
                    });
                }

                /*获取周订单趋势 */
                let weekChartArr = {};

                function getWeekTrend(currentId) {

                    return fetch(`${apibase}/order/get-week-trend`, {
                        method: 'POST',
                        body: {token, 'level': h.level || 'school', 'zone_id': currentId}
                    }).then(respones=> {
                        //console.log("周订单趋势接口返回================",respones);
                        let weekTrend = respones['data']['list'].reduce((prev, now)=> {
                            prev['x'].push(now['week_name']);
                            prev['y'].push(now['num']);
                            let text = now['increase_proportion'] >= 0 ? "上涨" : "下跌";
                            prev['extraData'].push({
                                "d1": now['week_name'] + "订单量比前一周" + text + Math.abs(now['increase_proportion']) + "%"
                            });
                            prev['type'] = 'horizontalChart';
                            return prev;
                        }, {x: [], y: [], extraData: []});
                        weekChartArr[currentId] = weekTrend;
                        //dispatch(chart(chartArr,'school'));

                        //console.log("周订单趋势weekChartArr================",weekChartArr);
                        return weekTrend;
                    });
                }

                return Promise.all(promiseList);
            });
    }
}

/**
 * @action
 * 运营学校负责人--列表
 */
export function fetchSchoolDetail(props) {
    return (dispatch, getState)=> {
        console.log("props.schoolId=========", props.schoolId);

        return getUserInfo()
            .then(userInfo=> {
                let promiseList = [];
                let {token} = userInfo;
                //console.log("schoolIdschoolId",this.api.schoolId)
                promiseList.push(
                    fetch(`${apibase}/zone/get-child-zone-list`, {
                        method: 'POST',
                        body: {token, 'zone_id': props.schoolId, 'current_level': 'school', 'child_level': 'society'}
                    })
                        .then(d=> {
                            let ids = d['data']['list'].map(v=>v['zone_id']).join(',');
                            return fetch(`${apibase}/order/get-zone-today-detail`, {
                                method: 'POST',
                                body: {token, 'zone_ids': ids, level: 'society'}
                            }).then(d2=> d2['data']['list'].map((v, idx)=> {
                                let m = d['data']['list'].filter(x=>x['zone_id'] == v['zone_id'])[0];
                                return {
                                    left: m['zone_name'],
                                    center: m['parent_zone_name'],
                                    value: parseInt(v['num'], 10),
                                    cal: parseFloat(v['increase_proportion'])
                                }
                            }).sort((a, b)=>b.value - a.value));
                        }).then(list=>dispatch(cityList(list, `${props.random || ''}school`)))
                );


                promiseList.push(fetch(`${apibase}/zone/get-child-zone-list`, {
                    method: 'POST',
                    body: {token, 'zone_id': props.schoolId, 'current_level': 'school', 'child_level': 'school'}
                })
                    .then(d=> {
                        let ids = d['data']['list'].map(v=>v['zone_id']).join(',');
                        return fetch(`${apibase}/order/get-zone-today-detail`, {
                            method: 'POST',
                            body: {token, 'zone_ids': ids, level: 'school'}
                        }).then(d2=> d2['data']['list'].map((v, idx)=> {
                            let m = d['data']['list'].filter(x=>x['zone_id'] == v['zone_id'])[0];
                            return {
                                //left: d['data']['list'][idx]['zone_name'],
                                left: m['zone_name'],
                                value: parseInt(v['num'], 10),
                                /* extra */
                                id: v['zone_id'],
                                title: m['zone_name'],
                                name: 'school',
                                cal: parseFloat(v['increase_proportion'])
                            }
                        }));
                    }).then(list=>dispatch(zoneList(list, `${props.random || ''}school`))));

                return Promise.all(promiseList);
            });
    }
}




function zoneList(payload, prefix) {
    return {type: 'setZoneList', prefix, payload};
}

function cityList(payload, prefix) {
    return {type: 'setCityList', prefix, payload};
}

function schoolList(payload, prefix) {
    return {type: 'setSchoolList', prefix, payload};
}

function districtList(payload, prefix) {
    return {type: 'setDistrictList', prefix, payload};
}

function daysAgo(num) {
    let d = new Date;
    d.setTime(d.getTime() - num * 24 * 3600 * 1000);
    function b(n) {
        if (n < 10) {
            return `0${n}`;
        }
        return `${n}`;
    }

    return `${d.getFullYear()}-${b(d.getMonth() + 1)}-${b(d.getDate())}`;
}

function whichDay(d) {
    let date = new Date(d),
        hanDay;
    switch (date.getDay()) {
        case 0:
            hanDay = '日';
            break;
        case 1:
            hanDay = '一';
            break;
        case 2:
            hanDay = '二';
            break;
        case 3:
            hanDay = '三';
            break;
        case 4:
            hanDay = '四';
            break;
        case 5:
            hanDay = '五';
            break;
        case 6:
            hanDay = '六';
            break;
    }
    return `${d.substring(5)}\n${hanDay}`;
}

function monthAndDay(d) {
    let r = d.split('-');
    r.shift();
    return r.join('-');
}

function isUndefined(val) {
    return val === undefined;
}


/**
 * @action
 * 运营全国负责人--首页今日订单量
 */
function orderTotal(payload, prefix) {
    return {type: 'setOrderTotal', prefix, payload};
}

/**
 * @action
 * 运营全国负责人--首页今日订单变化百分比
 */
function orderDelta(payload, prefix) {
    return {type: 'setOrderDelta', prefix, payload};
}

/**
 * @action
 * 运营全国负责人--首页今日订单变数量
 */
function orderDeltaAmount(payload, prefix) {
    return {type: 'setOrderDeltaAmount', prefix, payload};
}

/**
 * @action
 * 运营全国负责人--首页全部图表
 */
function chart(payload, prefix) {
    return {type: 'setChart', prefix, payload};
}

/**
 * @action
 * 运营全国负责人--首页全部图表--默认选中
 */

function chartDefault(payload, prefix) {
    return {type: 'setChartDefault', prefix, payload};
}
