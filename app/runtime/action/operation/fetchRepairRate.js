import { fetch } from '../../../modules';
import { getUserInfo, apibase } from '../../api';

// 报修率
export const fetchRepairRate = options => async dispatch => {
    const { token } = await getUserInfo();
    const url = `${apibase}/broken-bicycle/get-repair-rate`;
    const promiseList = [];
    const gRate = {};

    if (options.all) {
        const getList = async (type, ids) => {
            const res = await fetch(url, {
                method: 'POST',
                body: {
                    token,
                    zone_level: type,
                    zone_ids: ids,
                    start_date: options.startDate,
                    end_date: options.endDate
                }
            });
            if (res.code === 0) {
                return handleList(res.data.list[options.startDate], type);
            }
        };
        promiseList.push(
            getList('society', options.societyIds),
            getList('school', options.schoolIds)
        );
        const combineRate = await Promise.all(promiseList);
        const tmp = combineRate.reduce((acc, cur) => acc.concat(cur), []);
        gRate[options.startDate] = tmp;
        dispatch({ type: 'getRepairRate', prefix: 'city', payload: gRate });
    } else {
        const res = await fetch(url, {
            method: 'POST',
            body: {
                token,
                zone_level: options.zoneLevel,
                zone_ids: Array.isArray(options.zoneIds)
                    ? options.zoneIds.join(',')
                    : options.zoneIds,
                start_date: options.startDate,
                end_date: options.endDate
            }
        });

        if (res.code === 0) {
            Object.keys(res.data.list).map(item => {
                return handleList(res.data.list[item], options.zoneLevel);
            });
            dispatch({ type: 'getRepairRate', prefix: options.prefix || '', payload: res.data.list });
            return res;
        }
    }

    function handleList(list, zoneType) {
        list.map(zone => {
            zone.zoneLevel = zoneType;
            zone.repair_rate = zone.repair_rate === '-.-' ? '暂无数据' : (100 * zone.repair_rate).toFixed(2) + '%';
        });
        return list;
    }
};
