import { fetch } from '../../../modules';
import { _loadToken, getUserInfo, apibase, getNowFormatDate, showWeekCN } from '../../api';
var moment = require('moment');
import { historyStack } from '../../../modules';


/**
 * @action
 * 运营全国负责人--首页
 */
export function fetchCountryIndex(props) {
    return (dispatch, getState) => {
        // @mock
        // getToken.then(...)

        return getUserInfo()
            .then(userInfo => {
                dispatch({ type: 'setUserInfo', payload: userInfo });
                let { token } = userInfo,
                    promiseList = [];

                /**
                 * detail
                 */

                promiseList.push(
                    fetch(`${apibase}/order/get-zone-today-detail`, {
                        method: 'POST',
                        body: { token, 'zone_ids': userInfo['zone_ids'], level: 'country' }
                    }).then(d => {
                        dispatch(
                            orderTotal(
                                parseInt(d['data']['list'][0]['num'], 10), `${props.random || ''}country`));
                        dispatch(orderDelta(parseFloat(d['data']['list'][0]['increase_proportion']), `${props.random || ''}country`));
                        dispatch(weekOrderDelta(parseFloat(d['data']['list'][0]['week_increase_proportion']), `${props.random || ''}country`));
                        dispatch(yesterdayOrder(parseInt(d['data']['list'][0]['yesterday_num']), `${props.random || ''}country`));
                        dispatch(lastWeekOrder(parseInt(d['data']['list'][0]['week_num']), `${props.random || ''}country`));
                        dispatch(orderDeltaAmount(
                            parseInt(d['data']['list'][0]['num'])
                            - parseInt(d['data']['list'][0]['yesterday_num']), `${props.random || ''}country`));
                        dispatch(orderWeekAmount(
                            parseInt(d['data']['list'][0]['num'])
                            - parseInt(d['data']['list'][0]['week_num']), `${props.random || ''}country`));
                    }));

                promiseList.push(
                    Promise.all(
                        [
                            /**
                             * 日趋势
                             */

                            fetch(`${apibase}/order/get-day-trend`, {
                                method: 'POST',
                                body: {
                                    token,
                                    'zone_id': userInfo['zone_ids'],
                                    'start_time': daysAgo(6),
                                    'end_time': daysAgo(0),
                                    level: 'country'
                                }
                            }),
                            /**
                             * 周趋势
                             */

                            fetch(`${apibase}/order/get-week-trend`, {
                                method: 'POST',
                                body: {
                                    token,
                                    'zone_id': userInfo['zone_ids'],
                                    level: 'country'
                                }
                            })
                        ]
                    ).then(d => {
                        dispatch(chart([
                            {
                                x: d[0]['data']['list'].map(v => whichDay(v.date)),
                                y: d[0]['data']['list'].map(v => v.num),
                                extraData: d[0]['data']['list'].map((v, i) => ({
                                    d1: i == d[0]['data']['list'].length - 1 ? '' : `${monthAndDay(v['date'])}订单量比前一天${parseFloat(v['increase_proportion']) > 0 ? '上涨' : '下跌'}${Math.abs(parseFloat(v['increase_proportion']))}%`,
                                    d2: `${v['society_proportion']}%城市${v['society_num']}`,
                                    d3: `${v['school_proportion']}%学校${v['school_num']}`
                                })),
                                type: 'verticalChart'
                            },
                            {
                                x: d[1]['data']['list'].map(v => v['week_name']),
                                y: d[1]['data']['list'].map(v => v.num),
                                extraData: d[1]['data']['list'].map(v => ({
                                    d1: `${v['week_name']}订单量比前一周${parseFloat(v['increase_proportion']) > 0 ? '上涨' : '下跌'}${Math.abs(parseFloat(v['increase_proportion']))}%`,
                                    d2: `${v['society_proportion']}%城市${v['society_num']}`,
                                    d3: `${v['school_proportion']}%学校${v['school_num']}`
                                })),
                                type: 'horizontalChart'
                            }
                        ], `${props.random || ''}country`));

                        dispatch(chartDefault([6, 0], `${props.random || ''}country`));
                    }));
                return Promise.all(promiseList);
            });
    };
}

/**
 * @action
 * 运营全国负责人--列表
 */
export function fetchCountryDetail(props) {
    return (dispatch, getState) => {
        // @mock

        return getUserInfo()
            .then(userInfo => {
                dispatch({ type: 'setUserInfo', payload: userInfo });

                let { token } = userInfo,
                    promiseList = [];

                /**
                 * 城市列表数据
                 */

                promiseList.push(
                    fetch(`${apibase}/zone/get-child-zone-list`, {
                        method: 'POST',
                        body: {
                            token,
                            'zone_id': userInfo['zone_ids'],
                            'current_level': 'country',
                            'child_level': 'city'
                        }
                    })
                        .then(d => {

                            let ids = d['data']['list'].map(v => v['zone_id']).join(',');
                            return fetch(`${apibase}/order/get-zone-today-detail`, {
                                method: 'POST',
                                body: { token, 'zone_ids': ids, level: 'city' }
                            }).then(d2 => d2['data']['list'].map((v, idx) => {
                                let m = d['data']['list'].filter(x => x['zone_id'] == v['zone_id'])[0];
                                return {
                                    left: m['zone_name'],
                                    center: m['parent_zone_name'],
                                    value: parseInt(v['num'], 10),
                                    bottom: v['weather'],

                                    /* extra */
                                    id: v['zone_id'],
                                    title: m['zone_name'],
                                    name: 'city',
                                    cal: parseFloat(v['increase_proportion'])
                                };
                            }).sort((a, b) => b.value - a.value));
                        }).then(list => dispatch(cityList(list, 'country')))
                );

                /**
                 * 战区列表数据
                 */

                promiseList.push(
                    fetch(`${apibase}/zone/get-child-zone-list`, {
                        method: 'POST',
                        body: {
                            token,
                            'zone_id': userInfo['zone_ids'],
                            'current_level': 'country',
                            'child_level': 'region'
                        }
                    })
                        .then(d => {
                            let ids = d['data']['list'].map(v => v['zone_id']).join(',');
                            return fetch(`${apibase}/order/get-zone-today-detail`, {
                                method: 'POST',
                                body: { token, 'zone_ids': ids, level: 'region' }
                            }).then(d2 => d2['data']['list'].map((v, idx) => {
                                let m = d['data']['list'].filter(x => x['zone_id'] == v['zone_id'])[0];

                                return {
                                    left: m['zone_name'],
                                    value: parseInt(v['num'], 10),

                                    /* extra */
                                    id: v['zone_id'],
                                    title: m['zone_name'],
                                    name: 'region',
                                    cal: parseFloat(v['increase_proportion'])
                                };
                            }).sort((a, b) => b.value - a.value));
                        }).then(list => dispatch(zoneList(list, 'country')))
                );
                return Promise.all(promiseList);
            });
    };
}



function zoneList(payload, prefix) {
    return { type: 'setZoneList', prefix, payload };
}

function cityList(payload, prefix) {
    return { type: 'setCityList', prefix, payload };
}

function schoolList(payload, prefix) {
    return { type: 'setSchoolList', prefix, payload };
}

function districtList(payload, prefix) {
    return { type: 'setDistrictList', prefix, payload };
}

function daysAgo(num) {
    let d = new Date();
    d.setTime(d.getTime() - num * 24 * 3600 * 1000);
    function b(n) {
        if (n < 10) {
            return `0${n}`;
        }
        return `${n}`;
    }

    return `${d.getFullYear()}-${b(d.getMonth() + 1)}-${b(d.getDate())}`;
}

function whichDay(d) {
    let date = new Date(d),
        hanDay;
    switch (date.getDay()) {
        case 0:
            hanDay = '日';
            break;
        case 1:
            hanDay = '一';
            break;
        case 2:
            hanDay = '二';
            break;
        case 3:
            hanDay = '三';
            break;
        case 4:
            hanDay = '四';
            break;
        case 5:
            hanDay = '五';
            break;
        case 6:
            hanDay = '六';
            break;
    }
    return `${d.substring(5)}\n${hanDay}`;
}

function monthAndDay(d) {
    let r = d.split('-');
    r.shift();
    return r.join('-');
}

function isUndefined(val) {
    return val === undefined;
}


/**
 * @action
 * 运营全国负责人--首页今日订单量
 */
function orderTotal(payload, prefix) {
    return { type: 'setOrderTotal', prefix, payload };
}

/**
 * @action
 * 运营全国负责人--首页今日订单变化百分比
 */
function orderDelta(payload, prefix) {
    return { type: 'setOrderDelta', prefix, payload };
}
/**
 * @action
 * 运营全国负责人--首页周订单变化百分比
 */
function weekOrderDelta(payload, prefix) {
    return { type: 'setWeekOrderDelta', prefix, payload };
}
/**
 * @action
 * 运营全国负责人--首页昨日订单
 */
function yesterdayOrder(payload, prefix) {
    return { type: 'setYesterday', prefix, payload };
}
/**
 * @action
 * 运营全国负责人--首页上周订单
 */
function lastWeekOrder(payload, prefix) {
    return { type: 'setLastWeek', prefix, payload };
}

/**
 * @action
 * 运营全国负责人--首页今日订单变数量
 */
function orderDeltaAmount(payload, prefix) {
    return { type: 'setOrderDeltaAmount', prefix, payload };
}

function orderWeekAmount(payload, prefix) {
    return { type: 'setOrderWeekAmount', prefix, payload };
}

/**
 * @action
 * 运营全国负责人--首页全部图表
 */
function chart(payload, prefix) {
    return { type: 'setChart', prefix, payload };
}

/**
 * @action
 * 运营全国负责人--首页全部图表--默认选中
 */

function chartDefault(payload, prefix) {
    return { type: 'setChartDefault', prefix, payload };
}
