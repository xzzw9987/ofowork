import {fetch, JumpBack} from '../../../modules';
import {_loadToken, getUserInfo, apibase, getNowFormatDate, showWeekCN} from '../../api';
import { Toast } from '../../../base';
var moment = require('moment');
import {historyStack} from '../../../modules';

/**
 * 路面信息
 */
export function fetchCompetitorInformation(props) {
    return (dispatch, getState)=> {
        return getUserInfo()
            .then(userInfo=> {
                let promiseList = [];
                let img = [];
                let {token} = userInfo;
                if(props.img && props.img.length != 0){
                    props.img.map((item)=>{
                        promiseList.push(fetch(`${apibase}/user/upload-img-with-base64`, {
                            method: 'POST',
                            body: {token, 'img': item}
                        }).then(respones=> {
                            if(respones.code == 0){
                                img.push(respones.data.img_path);
                                dispatch({type: 'setCollectCompetitorImg', data: img});
                            }
                        }));
                    });
                }

                if(props.info){
                    promiseList.push(fetch(`${apibase}/competitor/record`, {
                        method: 'POST',
                        body: {token, 'img_path': props.img_path, 'lat': props.lat, 'lng': props.lng, 'info': JSON.stringify(props.info)}
                    }).then(respones=> {
                        dispatch({type: 'setCollectCompetitorInformation', data: respones});
                    }));
                }

                return Promise.all(promiseList);
            });
    };
}

