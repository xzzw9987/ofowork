import { fetch } from '../../../modules';
import { getUserInfo, apibase } from '../../api';

// 运营换牌
export const opChangeBrand = options => async dispatch => {
    const { token } = await getUserInfo();
    const url = `${apibase}/operation/replace-bicycle-no`;
    const res = await fetch(url, {
        method: 'POST',
        body: {
            token,
            'bicycle_no': options.carno,
            'unlock_code': options.newCode,
            'lock_type': options.lockType,
        }
    });
    dispatch({ type: 'opChangeBrand', payload: res });
};
