import {fetch} from '../../../modules';
import {_loadToken, getUserInfo, apibase, getNowFormatDate, showWeekCN} from '../../api';
var moment = require('moment');
import {historyStack} from '../../../modules';

/**
 * 运营车牌查询
 */
export function fetchOperationNewUsers(props) {
    return (dispatch, getState)=> {
        // @mock
        // getToken.then(...)
        return getUserInfo()
            .then((d)=> {
                let userInfo = d ;
                let zone_ids, zone_names;
                let promiseList = [];
                let {token} = userInfo;
                zone_ids = userInfo['zone_ids'];
                zone_names = userInfo['zone_names'];
                let zoneNames = zone_ids.split(',').map((id, index)=> {
                    return {
                        id: id,
                        name: zone_names.split(',')[index]
                    };
                });
                let zoneId = props && props.operationZoneId;
                let currentId;
                if (zoneId) {
                    currentId = zoneId;
                } else {
                    currentId = zone_ids.split(',')[0];
                }
                dispatch({type: 'setOperationZoneId', data: currentId});
                dispatch({type: 'setOperationZoneNames', data: zoneNames});


                // 获取新增用户
                /*let operationZoneInfo = {};
                promiseList.push(fetch(`${apibase}/operation/get-repair-rate`, {
                    method: 'POST',
                    body: {token, 'zone_id': currentId, 'type': 'history'}
                }).then(respones=> {
                    if (respones['code'] === 0) {
                        operationZoneInfo['weekRate'] = respones['data']['week_average_rate'];
                        operationZoneInfo['monthRate'] = respones['data']['average_rate'];
                        operationZoneInfo['list'] = respones['data']['list'];
                        console.log('运营报修率',operationZoneInfo);
                        dispatch({type: "setOperationZoneInfo", data: operationZoneInfo});
                    }
                }));

                return Promise.all(promiseList);*/

            });
    };
}

