import {fetch} from '../../../modules';
import {_loadToken, getUserInfo, apibase, getNowFormatDate, showWeekCN} from '../../api';
var moment = require('moment');
import {historyStack} from '../../../modules';

/**
 * 运营举报列表
 */
export function fetchOperationReportManagement(props) {
    return (dispatch, getState)=> {
        // @mock
        // getToken.then(...)
        return getUserInfo()
            .then(userInfo=> {
                let promiseList = [];
                let {token} = userInfo;
                //获取举报列表
                promiseList.push(fetch(`${apibase}/report/operation-get-list`, {
                    method: 'POST',
                    body: {token, 'type': props.type, 'page': props.page || 0}
                }).then(respones=> {
                    console.log('运营举报列表', respones['data']);
                    dispatch({type: 'setOperationReportManagement', concat: props.concat, data: respones['data']['list']});
                }));
                return Promise.all(promiseList);
            });
    };
}
