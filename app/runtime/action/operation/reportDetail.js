import { fetch, JumpBack} from '../../../modules';
import {_loadToken, getUserInfo, apibase, getNowFormatDate, showWeekCN} from '../../api';
import { Toast } from '../../../base';
var moment = require('moment');
import {historyStack} from '../../../modules';

/**
 * 运营举报详情列表
 */
export function fetchOperationReportDetail(info) {

    return (dispatch, getState)=> {
        // @mock
        // getToken.then(...)
        return getUserInfo()
            .then(userInfo=> {
                let promiseList = [];
                let {token} = userInfo;
                let report_id, report_type;
                if (historyStack.get() && historyStack.get().report_id) {
                    report_id = historyStack.get().report_id;
                }
                if (historyStack.get() && historyStack.get().report_type) {
                    report_type = historyStack.get().report_type;
                } else {
                    report_type = '';
                }
                dispatch({type: 'setReportType', data: report_type});

                //获取举报详情
                if(!info) {
                    promiseList.push(fetch(`${apibase}/report/operation-get-detail`, {
                        method: 'POST',
                        body: {token, 'report_id': report_id}
                    }).then(respones=> {
                        console.log('运营举报详情列表', respones['data']);
                        dispatch({type: 'setOperationReportDetail', data: respones['data']});
                    }));
                }

                //获取举报处理结果
                if(!info && report_type) {
                    promiseList.push(fetch(`${apibase}/report/get-processed-info`, {
                        method: 'POST',
                        body: {token, 'report_id': report_id}
                    }).then(respones=> {
                        console.log('运营举报处理结果', respones['data']);
                        dispatch({type: 'setReportResult', data: respones['data']});
                    }));
                }

                //提交驳回原因
                if(info) {
                    promiseList.push(fetch(`${apibase}/report/operation-reject`, {
                        method: 'POST',
                        body: {token, 'report_id': report_id, 'info': info}
                    }).then(respones=> {
                        console.log('运营举报驳回原因', respones['data']);
                        if(respones['code'] === 0){
                            Toast.success('提交成功', 2);
                            JumpBack();
                        }else{
                            alert(respones['message']);
                        }
                    }));
                }
                return Promise.all(promiseList);
            });
    };
}
