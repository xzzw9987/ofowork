import {fetch} from '../../../modules';
import {_loadToken, getUserInfo, apibase, getNowFormatDate, showWeekCN} from '../../api';
var moment = require('moment');
import {historyStack} from '../../../modules';

/**
 * 运营坏车数据概览
 */
export function fetchBadCarData(props) {
    return (dispatch, getState)=> {
        return getUserInfo()
            .then(userInfo=> {
                let promiseList = [];
                let {token} = userInfo;
                //获取坏车数据列表
                promiseList.push(fetch(`${apibase}/operation/broken-data`, {
                    method: 'POST',
                    body: { token },
                }).then(respones=>{
                    dispatch({type: 'setBadCarData', data: respones['data']['list']});
                }));
                return Promise.all(promiseList);
            });
    };
}

/**
 * 运营坏车数据概览详情
 */
export function fetchBadCarMessage(props) {
    return (dispatch, getState)=> {
        return getUserInfo()
            .then(userInfo=> {
                let promiseList = [];
                let {token} = userInfo;
                //获取坏车数据列表
                promiseList.push(fetch(`${apibase}/operation/broken-data-detail`, {
                    method: 'POST',
                    body: { token, 'type': props.type, 'date': props.date, 'page': props.page || 0},
                }).then(respones=>{
                    console.log('~~~~~~~~', respones.data);
                    dispatch({type: 'setBadCarMessage', concat: props.concat, data: respones['data']['list']});
                }));
                return Promise.all(promiseList);
            });
    };
}

