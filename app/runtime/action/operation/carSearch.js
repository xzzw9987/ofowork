import { fetch, JumpBack } from '../../../modules';
import { _loadToken, getUserInfo, apibase, getNowFormatDate, showWeekCN } from '../../api';
import { Toast } from '../../../base';
var moment = require('moment');
import { historyStack } from '../../../modules';

/**
 * 运营车牌查询
 */
export function fetchOperationCarSearch(props) {
    return (dispatch, getState) => {
        return getUserInfo()
            .then(userInfo => {
                let promiseList = [];
                let { token } = userInfo;
                //运营车牌号查询
                promiseList.push(fetch(`${apibase}/bicycle/search-bicycle-num`, {
                    method: 'POST',
                    body: { token, 'bicycle_num': props.bicycle_num, type: props.type || '' }
                }).then(respones => {
                    console.log('运营车牌查询', respones);
                    dispatch({ type: 'setOperationCarSearch', data: respones['data'] });
                    dispatch({ type: 'setOperationCarCode', data: respones['code'] });
                    dispatch({ type: 'setOperationCarMessage', data: respones['message'] });
                }));
                return Promise.all(promiseList);
            });
    };
}

/**
 * 运营解锁码更改
 */
/*export function fetchOperationChangeUnlockCode(props) {
    return (dispatch, getState)=> {
        return getUserInfo()
            .then(userInfo=> {
                let promiseList = [];
                let {token} = userInfo;
                //新解锁码录入
                promiseList.push(fetch(`${apibase}/operation/update-unlock-code`, {
                    method: 'POST',
                    body: {token, 'bicycle_num': props.carMessage.bicycle, 'old_unlock': props.carMessage.unlock_code, 'new_unlock_code': props.new_unlock_code}
                }).then((respones)=> {
                    if(respones.code == 0){
                        let operationCarDetail = Object.assign({}, props.carMessage);
                        operationCarDetail.unlock_code = props.new_unlock_code;
                        dispatch({type: 'setOperationCarSearch', data: operationCarDetail});
                        Toast.success('解锁码更新成功', 1.5);
                        JumpBack();
                    }else{
                        Toast.fail(respones.message, 1.5);
                    }
                }));
                return Promise.all(promiseList);
            });
    };
}*/

