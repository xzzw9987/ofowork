import { fetch } from '../../../modules';
import { getUserInfo, apibase } from '../../api';

export const uploadScmOrder = options => async dispatch => {
    const { token } = await getUserInfo();
    const url = `${apibase}/scm/stock-${options.type}/delivery`;
    const res = await fetch(url, {
        method: 'POST',
        body: {
            token,
            [`stock_${options.type}_id`]: options.serialNo,
            bsns: JSON.stringify(options.bsn),
            warehouse_id: options.type === 'back' ? options.warehouseId : ''
        }
    });
    // const res = {
    //     code: 0,
    //     data: {
    //         estimate_num: 10,
    //         real_num: 1,
    //         sku: [{
    //             sku_id: 1234,
    //             estimate_num: 5,
    //             real_num: 2
    //         }]
    //     },
    //     message: ''
    // };
    dispatch({ type: 'uploadScm', payload: res.data });
    return res;
};
