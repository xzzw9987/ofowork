import { fetch } from '../../../modules';
import { getUserInfo, apibase } from '../../api';

export const fetchBsnAuth = options => async dispatch => {
    const { token } = await getUserInfo();
    const url = `${apibase}/scm/stock-${options.type}/allow-sn`;
    const res = await fetch(url, {
        method: 'POST',
        body: {
            token,
            [`stock_${options.type}_id`]: options.serialNo,
            'bsn': options.bsn
        }
    });
    // const res = {
    //     code: 0,
    //     data: [
    //         {
    //             sku_id: '200002',
    //             name: '车轱辘'
    //         },
    //         {
    //             sku_id: '200003',
    //             name: '车座子'
    //         }
    //     ],
    //     message: ''
    // };
    dispatch({ type: 'fetchBsnAuth', payload: res.data });
    return res;
};
