import { fetch } from '../../../modules';
import { getUserInfo, apibase } from '../../api';

export const fetchScmDetail = options => async dispatch => {
    const { token } = await getUserInfo();
    const url = `${apibase}/scm/stock-${options.type}/detail`;
    const res = await fetch(url, {
        method: 'POST',
        body: {
            token,
            [`stock_${options.type}_id`]: options.serialNo,
        }
    });
    // const res = {
    //     code: 0,
    //     data: {
    //         estimate_num: 10,
    //         real_num: 1,
    //         sku: [{
    //             sku_id: '200002',
    //             estimate_num: 5,
    //             real_num: 4
    //         }]
    //     },
    //     message: ''
    // };
    dispatch({ type: 'fetchScmDetail', payload: res.data });
    return res;
};
