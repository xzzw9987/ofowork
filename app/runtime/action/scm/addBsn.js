export const addBsn = ({ bsn, sku_id } = {}) => dispatch => {
    dispatch({
        type: 'addBsn',
        payload: {
            bsn,
            sku_id
        }
    });
};
