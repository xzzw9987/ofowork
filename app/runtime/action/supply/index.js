import { fetch } from '../../../modules';
import {
    _loadToken,
    getUserInfo,
    apibase,
    getNowFormatDate,
    showWeekCN
} from '../../api';

/**
 * supply chain
 */
export function submitCode(props) {
    // @mock
    // getToken.then(...)
    const { no, code, barCode } = props;
    // let accuracy = props.accuracy === null ? -1 : props.accuracy;

    return getUserInfo().then(userInfo => {
        let { token } = userInfo;
        return fetch(`${apibase}/supply/record-bicycle`, {
            method: 'POST',
            body: {
                token,
                bicycle_no: no,
                unlock_code: code,
                // lat,
                // lng,
                // accuracy,
                bicycle_id: barCode
            }
        });
    });
}

export function submitSmart(args) {
    const { bicycle_no, unlock_code, fender_sn, barCode } = args;
    //   let accuracy = args.accuracy === null ? -1 : args.accuracy;

    return getUserInfo().then(userInfo => {
        let { token } = userInfo;
        return fetch(`${apibase}/supply/record-bicycle`, {
            method: 'POST',
            body: {
                token,
                bicycle_no,
                unlock_code,
                lock_type: 2,
                // lat,
                // lng,
                // accuracy,
                fender_sn,
                bicycle_id: barCode
            }
        });
    });
}

export async function checkRepeat(args) {
    const { type, x_no } = args;
    const { token } = await getUserInfo();
    return fetch(`${apibase}/supply/check-repeat`, {
        method: 'POST',
        body: { token, type, x_no }
    });
}

export async function fetchOldSn(options) {
    const { carNo } = options;
    const { token } = await getUserInfo();
    return fetch(`${apibase}/i-lock/get-sn-by-bicycle-no`, {
        method: 'POST',
        body: { token, bicycle_no: carNo }
    });
}

export function fetchList(date) {
    return getUserInfo().then(userInfo => {
        let { token } = userInfo;
        return fetch(`${apibase}/supply/record-history`, {
            method: 'POST',
            body: { token, date }
        });
    });
}

export function del(id) {
    return getUserInfo().then(userInfo => {
        let { token } = userInfo;
        return fetch(`${apibase}/supply/delete-bicycle`, {
            method: 'POST',
            body: { token, bicycle_no: id }
        });
    });
}

export async function submitUnbind(options) {
    const { type, x_no } = options;
    const { token } = await getUserInfo();
    return fetch(`${apibase}/supply/unbind`, {
        method: 'POST',
        body: { token, type, x_no }
    });
}
