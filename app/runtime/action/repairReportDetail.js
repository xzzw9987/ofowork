import { fetch } from '../../modules';
import { _loadToken, getUserInfo, apibase, getNowFormatDate, showWeekCN } from '../api';
var moment = require('moment');
import { historyStack } from '../../modules';
import whereami from '../../base/map/whereami';

/**
 * 举报详情
 */
export function fetchReportDetail(zoneId) {

    return (dispatch, getState) => {
        // @mock
        // getToken.then(...)

        return getUserInfo()
            .then((userInfo) => {
                // let {userInfo, latitude, longitude} = d ;
                let id = historyStack.get().id;
                let bicycleNumber = historyStack.get().biycleNumber;
                let currentId = historyStack.get().currentId;
                let promiseList = [];
                let { token } = userInfo;
                //获取举报详情
                if (id) {
                    promiseList.push(fetch(`${apibase}/maintenance/detail`, {
                        method: 'POST',
                        body: { token, 'maintenance_id': id }
                    }).then(respones => {
                        dispatch({ type: 'setReportDetail', data: respones['data'] });

                    }));
                }

                return Promise.all(promiseList);

            });
    };
}
