import { fetch } from '../../modules';
import { getUserInfo, apibase } from '../api';

export const fetchBench = options => async (dispatch, getState) => {
    const { token } = await getUserInfo();
    if (!getState().api.benchList) {
        const res = await fetch(`${apibase}/user/desk-list`, {
            method: 'POST',
            body: {
                token,
            }
        });
        dispatch({ type: 'fetchBench', payload: res.data });
    }
};

