import { fetch } from '../../../modules';
import { _loadToken, getUserInfo, apibase, getNowFormatDate, showWeekCN } from '../../api';
var moment = require('moment');
import { historyStack } from '../../../modules';
import whereami from '../../../base/map/whereami';
import geoMessage from '../../../base/map/geoMessage';
/**
 * 700BikeDetail
 */
export function fetchBikeDetail(zoneId) {

    return (dispatch, getState) => {
        // @mock
        // getToken.then(...)

        return getUserInfo()
            .then(userInfo => {
                return whereami().then(d => ({
                    userInfo,
                    latitude: d.latitude,
                    longitude: d.longitude
                }));
            })
            .then((d) => {
                //let {userInfo, latitude, longitude} = d ;
                let { userInfo } = d;
                let bicycleNum = historyStack.get().bicycleNum;
                let promiseList = [];
                let { token } = userInfo;
                let info = historyStack.get().info;
                if (info) {
                    dispatch({ type: 'setBikeDetail', data: info });
                    geoMessage({ latitude: info.lat, longitude: info.lng }).then(d => {
                        dispatch({ type: 'setBikeDetail', data: { ...info, address: d.address } });
                    });
                }

            });
    };
}
