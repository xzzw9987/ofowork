import { fetch } from '../../../modules';
import { _loadToken, getUserInfo, apibase, getNowFormatDate, showWeekCN } from '../../api';
var moment = require('moment');
import { historyStack } from '../../../modules';
import whereami from '../../../base/map/whereami';
/**
 * 700bikelist
 */
export function fetchBikeList(props) {
    return (dispatch, getState) => {
        // @mock
        // getToken.then(...)

        return whereami().then(d => {
            getUserInfo().then((userInfo) => {
                let { token } = userInfo;
                let { latitude, longitude } = d;
                //let latitude = 22.6963559477,
                //longitude = 113.9580055934;
                let filter = props.filter,
                    distance = props.distance,
                    page = props.page,
                    concat = props.concat;
                let promiseList = [];
                promiseList.push(fetch(`${apibase}/cooperation-700-bike/get-list`, {
                    method: 'POST',
                    body: { token, 'distance': distance, 'filter': filter, 'page': page, 'lat': latitude, 'lng': longitude }
                }).then(respones => {
                    dispatch({ type: 'setBikeList', concat: concat, data: respones['data']['list'] });
                }));
                return Promise.all(promiseList);

            });
        });
    };
}
