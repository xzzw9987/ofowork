import { fetch } from '../../../modules';
import { getUserInfo, apibase } from '../../api';

export const factoryCodeSearch = options => async dispatch => {
    const { token } = await getUserInfo();
    const url = `${apibase}/i-lock/get-password-by-sn`;
    const res = await fetch(url, {
        method: 'POST',
        body: {
            token,
            'lock_sn': options.snCode,
        }
    });
    dispatch({ type: 'factoryCodeSearch', payload: res });
    return res;
};
