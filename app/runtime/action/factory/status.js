import { fetch } from '../../../modules';
import { getUserInfo, apibase } from '../../api';

export const fetchStatus = options => async dispatch => {
    const { token } = await getUserInfo();
    const url = `${apibase}/bicycle-factory/check-bicycle-no`;
    const res = await fetch(url, {
        method: 'POST',
        body: {
            token,
            bicycle_no: options.carno,
            type: options.type || ''
        }
    });
    dispatch({ type: 'getBicycleStatus', payload: res });
    return res;
};
