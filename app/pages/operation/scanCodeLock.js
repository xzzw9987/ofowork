import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as base from '../../base';
import { apibase, getUserInfo } from '../../runtime/api';
import { Text, View, Toast, TouchableHighlight } from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import CameraView from '../../components/camera';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, JumpBack, fetch } from '../../modules';

const platePrefix = 'http://ofo.so/plate/';
class ScanCodeLock extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            showCam: false,
        };
    }

    viewWillAppear() {
        this.haveReadBarCode = false;
        this.setState({showCam: true, });
    }

    viewWillDisappear() {
        this.setState({showCam: false});
    }

    handleBack() {
        JumpBack();
    }

    render() {
        return (
            <View style={styles.container}>
                <ActionBar title='扫码开锁' handleBack={() => this.handleBack()}>
                    <Text style={{ fontSize: 15, color: '#000', }}>返回</Text>
                </ActionBar>
                {this.state.showCam ?
                    <CameraView buttons={this.renderButtons()} torch={true}
                        onBarCodeRead={d => this.onBarCodeRead(d)} hint={'请扫描车牌二维码'}/> : null}
            </View>
        );
    }

    onBarCodeRead(d) {
        d = d.trim();
        if (this.haveReadBarCode) return;

        this.haveReadBarCode = true;
        if (!d.startsWith(platePrefix)) {
            Toast.fail('车牌号或挡泥板号错误');
            this.__timeout = setTimeout(() => this.haveReadBarCode = false, 3000);
            return;
        }

        let fromBrand = true;
        const num = d.substring(platePrefix.length, d.length);

        if (/^\d+$/.test(num)) {
            fromBrand = false;
        }

        return getUserInfo()
            .then(userInfo => {
                let { token } = userInfo;
                fetch(`${apibase}/bicycle/open-i-lock`, {
                    method: 'POST',
                    body: { token, 'type': (fromBrand ? 'fender_sn' : 'bicycle_no'), 'search': num}
                }).then(respones => {
                    if (respones['code'] === 0) {
                        Toast.success('开锁成功', 1.5, () => this.haveReadBarCode = false);
                    } else {
                        Toast.fail(respones['message'] + '（开锁失败！）', 1.5, () => this.haveReadBarCode = false);
                    }
                });
            });
    }

    renderButtons() {
        return [
            <View key={0}>
                <TouchableHighlight onPress={() => {JumpTo('scanCode/lockInput', true);}}>
                    <base.VerticalLayout style={{ alignItems: 'center', }}>
                        <base.Image src="hand" style={{ width: 64, height: 64, }} />
                        <Text style={{ marginTop: 10, fontSize: 14, backgroundColor: 'rgba(0,0,0,0)', color: '#fff', }}>手动输入</Text>
                    </base.VerticalLayout>
                </TouchableHighlight>
            </View>
        ];
    }
}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
};

export default pageWrapper(ScanCodeLock);
