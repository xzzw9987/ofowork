import React, { Component } from 'react';
import { connect } from 'react-redux';
import DashBoard from './operationDashBoardCommon';
import { JumpTo, historyStack, JumpBack } from '../../modules';

import { fetchCountryIndex } from '../../runtime/action';

class Surrogate extends Component {
    constructor(...args) {
        super(...args);
        this.connectedComponent = getConnectComponent(
            parseInt(100000 * Math.random()),
            historyStack.get()
        );
    }

    render() {
        let ConnectComponent = this.connectedComponent;
        return <ConnectComponent {...this.props} />;
    }
}

function getConnectComponent(random, historyState) {
    const mapStateToProps = state => {
        return {
            historyState,
            total: state.api[`${random}countryOrderTotal`] || 0,
            titleOfTotal: '今日订单量',
            delta: state.api[`${random}countryOrderDelta`] || 0,
            weekDelta: state.api[`${random}countryWeekOrderDelta`] || 0,
            deltaAmount: state.api[`${random}countryOrderDeltaAmount`] || 0,
            weekAmount: state.api[`${random}countryOrderWeekAmount`] || 0,
            yesterdayOrder: state.api[`${random}countryYesterdayOrder`] || 0,
            lastWeekOrder: state.api[`${random}countryLastWeekOrder`] || 0,
            chart: state.api[`${random}countryChart`] || [],
            defaultSelectedChart: state.api[`${random}countryChartDefault`] || [],
            left: {
                text: '今日排行榜',
                onPress() {
                    JumpTo('operation/orderDetailCountry', true);
                }
            },
            right: {
                text: '查看历史',
                onPress() {
                    console.log('查看历史');
                    let {
                        title = state.api.userInfo['zone_names'],
                        id = state.api.userInfo['zone_ids']
                    } = historyState || {};
                    JumpTo('operation/orderHistory', true, {
                        id,
                        level: 'country',
                        name: title,
                        title
                    });
                }
            },
            pageTitle: '全国'
        };
    },
        mapDispatchToProps = dispatch => {
            return {
                dispatch,
                fetch: props => dispatch(fetchCountryIndex({ ...props, random }))
            };
        };
    return connect(mapStateToProps, mapDispatchToProps)(DashBoard);
}

export default Surrogate;
