import React, { Component } from 'react';
import { connect } from 'react-redux';
import DashBoard from './operationDashBoardCommon';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { getUserInfo } from '../../runtime/api';

import { fetchSocietyIndex } from '../../runtime/action';

export default class Surrogate extends Component {
    constructor(...args) {
        super(...args);
        this.connectedComponent = getConnectComponent(
            parseInt(100000 * Math.random()),
            historyStack.get()
        );
    }

    render() {
        let ConnectComponent = this.connectedComponent;
        return <ConnectComponent {...this.props} />;
    }
}

function getConnectComponent(random, historyState) {
    const handleBack = props => {
        let dispatch = props.dispatch, s = historyState;
        getUserInfo().then(userInfo => {
            if (s && s.shouldLowerLevel) {
                userInfo['operation_level'] = userInfo['__origin_operation_level'];
                userInfo['__origin_operation_level'] = null;
                dispatch({ type: 'setUserInfo', payload: userInfo });
            }
        });
    };
    const mapStateToProps = state => {
        console.log(state.api[`${random}schoolDetail`]);
        let schoolDetail = state.api[`${random}schoolDetail`] || {},
            schoolChart = state.api[`${random}schoolChart`] || {},
            schoolId = state.api[`${random}schoolId`] || null,
            currentDetail = schoolDetail['' + schoolId] || {},
            currentChart = schoolChart['' + schoolId] || [];
        console.log('当前社会的信息＝＝＝＝＝＝＝＝＝＝＝＝＝＝', currentDetail);
        console.log('图表＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝', currentChart);
        console.log('societyNames', state.api[`${random}schoolNames`] || []);
        let total = Number(currentDetail['num']) || 0,
            yesterday_num = Number(currentDetail['yesterday_num']) || 0,
            lastWeek_num = Number(currentDetail['week_num']) || 0,
            deltaAmount = total - yesterday_num || 0,
            weekAmount = total - lastWeek_num || 0;

        return {
            actionRightBar: {
                text: '举报管理',
                onPress: () => JumpTo('operation/reportManagement', true, {})
            },
            // autoRefresh: false,
            schoolNames: state.api[`${random}schoolNames`] || [],
            schoolId: schoolId,
            total: total,
            titleOfTotal: '今日订单量',
            delta: Number(currentDetail['increase_proportion']) || 0,
            weekDelta: Number(currentDetail['week_increase_proportion']) || 0,
            deltaAmount: deltaAmount,
            weekAmount,
            yesterdayOrder: Number(currentDetail['yesterday_num']) || 0,
            lastWeekOrder: Number(currentDetail['week_num']) || 0,
            chart: currentChart || [],
            defaultSelectedChart: state.api[`${random}schoolChartDefault`] || [],
            left: (() => {
                let historyState = historyStack.get();
                return {
                    text: state.api.userInfo
                        ? state.api.userInfo['operation_level'] == 'society'
                            ? '今日排行榜'
                            : '今日订单详情'
                        : null,
                    onPress(props) {
                        console.log('historyStatehistoryState', historyState);
                        if (!historyState.isFirstPage) {
                            JumpBack();
                            handleBack(props);
                        } else {
                            let {
                title = state.api.userInfo['zone_names'],
                                id = state.api.userInfo['zone_ids']
              } = historyState || {};
                            JumpTo('operation/orderDetailSociety', true, {
                                schoolId: schoolId,
                                shouldRaiseLevel: !historyState ||
                                (historyState && historyState.isFirstPage)
                            });
                        }
                    }
                };
            })(),
            right: (() => {
                if (!historyState.isFirstPage) {
                    return null;
                } else {
                    return {
                        text: '查看历史',
                        onPress() {
                            console.log('查看历史');
                            let societyName = state.api[
                                `${random}schoolNames`
                            ].filter((item, index) => {
                                return item.id == schoolId;
                            })[0]['name'];
                            JumpTo('operation/orderHistory', true, {
                                id: schoolId,
                                level: 'society',
                                name: societyName,
                                title: societyName
                            });
                        }
                    };
                }
            })(),
            pageTitle: '城市',
            handleBack
        };
    },
        mapDispatchToProps = dispatch => {
            return {
                dispatch,
                fetch: props => dispatch(fetchSocietyIndex({ ...props, random })),
                setSchoolID: props => dispatch({ ...props, prefix: random })
            };
        };

    return connect(mapStateToProps, mapDispatchToProps)(DashBoard);
}
