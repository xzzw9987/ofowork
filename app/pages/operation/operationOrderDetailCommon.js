import React, {Component} from 'react';
import * as base from '../../base';
import {api, keys, loadToken, parseNum, _loadToken} from '../../runtime/api';
import {Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator} from '../../base';
import {connect} from 'react-redux';
import {JumpTo, historyStack, JumpBack} from '../../modules';
import {ActionBar} from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';


class Detail extends Component {

    static propTypes = {
        pageTitle: React.PropTypes.string,
        pane: React.PropTypes.arrayOf(React.PropTypes.shape({text: React.PropTypes.string})),
        fetch: React.PropTypes.func,
        dataSource: React.PropTypes.arrayOf(React.PropTypes.shape({
            list: React.PropTypes.arrayOf(React.PropTypes.shape({
                left: React.PropTypes.string,
                center: React.PropTypes.string,
                value: React.PropTypes.number,
                onPress: React.PropTypes.func
            }))
        }))
    };

    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            selectedPaneIndex: 0
        };
    }

    viewWillAppear() {
        this.props.fetch(this.props).then(()=>this.setState({loading: false}));
    }

    componentWillMount() {
    }

    componentDidMount() {
    }

    handleSetProfile = ()=> {
        JumpBack();
        this.props.dispatch({type: 'refresh'});
    };

    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderOperationPage();
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title={this.props.pageTitle} handleBack={this.handleSetProfile.bind(this)} rightItem="刷新"
                           rightAction={()=>this.props.fetch(this.props)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <base.View style={{position: 'relative', flexGrow: 1}}>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='loading'/>
                        <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }

    renderOperationPage() {
        return (
            <base.View style={styles.container}>
                <ActionBar title={this.props.pageTitle} handleBack={this.handleSetProfile.bind(this)} rightItem="刷新"
                           rightAction={()=>this.props.fetch(this.props)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <base.VerticalLayout style={styles.top}>
                    {this.renderPane()}
                </base.VerticalLayout>
                <base.VerticalLayout style={{flexGrow: 1}}>
                    {this.renderList()}
                </base.VerticalLayout>
            </base.View>
        );
    }

    renderPane() {
        if (!Array.isArray(this.props.pane))
            return null;
        return (
            <base.HorizontalLayout style={{height: 44, ...styles.borderBottom}}>
                {
                    this.props.pane.map((pane, index)=> {
                        let focusStyle = {
                            borderBottomWidth: 4,
                            borderBottomStyle: 'solid',
                            borderBottomColor: '#fff'
                        };
                        if (index === this.state.selectedPaneIndex) {
                            focusStyle = {
                                borderBottomWidth: 4,
                                borderBottomStyle: 'solid',
                                borderBottomColor: base.Color.yellow
                            };
                        }
                        return (
                            <base.HorizontalLayout
                                key={index}
                                style={{flexGrow: 1, justifyContent: 'center', ...focusStyle}}>
                                <base.TouchableHighlight
                                    activeOpacity={.1}
                                    underlayColor={'#f3f3f3'}
                                    style={{alignItems: 'center', justifyContent: 'center', flexGrow: 1}}
                                    onPress={()=> {
                                        this.setState({selectedPaneIndex: index});
                                    }}>
                                    <base.HorizontalLayout style={{justifyContent: 'center'}}>
                                        <Text style={{fontSize: 18}}>{pane.text}</Text>
                                    </base.HorizontalLayout>
                                </base.TouchableHighlight>
                            </base.HorizontalLayout>
                        );
                    })
                }
            </base.HorizontalLayout>);
    }

    renderList() {
        let dataSource = this.props.dataSource,
            focusedDataSource = dataSource[this.state.selectedPaneIndex];
        if (!focusedDataSource)return null;

        let fixed = focusedDataSource.fixed,
            fixedElement = this.renderFixed(fixed),
            list = focusedDataSource.list,
            max = list.reduce((prev, now)=> {
                if (now.value > prev)
                    return now.value;
                return prev;
            }, -99999);

        return (
            <base.VerticalLayout style={{flexGrow: 1, flexBasis: 0}}>
                {fixedElement}
                <ScrollView style={{flexGrow: 1, flexBasis: 0}}>
                    {
                        list.map((v, index)=> {
                            return (
                                <base.HorizontalLayout key={index} style={{
                                    height: 64,
                                    marginTop: 10,
                                    backgroundColor: '#fff',
                                    alignItems: 'center', ...styles.borderTop, ...styles.borderBottom
                                }}>
                                    <base.TouchableHighlight onPress={()=>v.onPress && v.onPress(v)}
                                                             activeOpacity={.1}
                                                             underlayColor={'#f3f3f3'}
                                                             style={{flexGrow: 1}}>
                                        <base.HorizontalLayout style={{flexGrow: 1, alignItems: 'center'}}>
                                            {/* List Item here */}
                                            <base.View style={{paddingLeft: 10, paddingRight: 10}}>
                                                {
                                                    (idx=> {
                                                        let ret;
                                                        switch (idx) {
                                                        case 0:
                                                            ret = <base.Image style={{width: 19, height: 25}}
                                                                                  src="gold"/>;
                                                            break;
                                                        case 1:
                                                            ret = <base.Image style={{width: 19, height: 25}}
                                                                                  src="silver"/>;
                                                            break;
                                                        case 2:
                                                            ret = <base.Image style={{width: 19, height: 25}}
                                                                                  src="bronze"/>;
                                                            break;
                                                        default:
                                                            ret = <Text>{1 + idx}</Text>;
                                                            break;
                                                        }
                                                        return ret;
                                                    })(index)
                                                }
                                            </base.View>
                                            <base.VerticalLayout>
                                                <base.HorizontalLayout style={{alignItems: 'center'}}>
                                                    <Text style={{
                                                        maxWidth: 150,
                                                        flexWrap: 'wrap',
                                                        fontSize: 16
                                                    }}>{`${v.left || ''}`}</Text>
                                                    <Text style={{
                                                        paddingLeft: 10,
                                                        paddingRight: 10,
                                                        fontSize: 14,
                                                        color: '#666'
                                                    }}>{`${v.center || ''}`}</Text>
                                                </base.HorizontalLayout>
                                                <base.HorizontalLayout>
                                                    {v.bottom ? (<Text style={{
                                                        fontSize: 12,
                                                        color: '#666',
                                                        position: 'relative',
                                                        top: 8
                                                    }}>{`${v.bottom}`}</Text>) : null}
                                                </base.HorizontalLayout>
                                            </base.VerticalLayout>
                                            <base.HorizontalLayout
                                                style={{alignItems: 'center', flexGrow: 1, justifyContent: 'flex-end'}}>
                                                <Text style={{
                                                    fontSize: 22,
                                                    marginRight: 10,
                                                    color: v.cal > 0 || isNaN(v.cal) ? base.Color.orange : base.Color.green
                                                }}>{v.value}</Text>
                                                <base.HorizontalLayout style={{width: 86, height: 16, marginRight: 10}}>
                                                    <base.View style={{
                                                        backgroundColor: v.cal > 0 || isNaN(v.cal) ? base.Color.orange : base.Color.green,
                                                        flexGrow: parseInt(v.value * 100 / max, 10) || 0
                                                    }}/>
                                                    <base.View style={{
                                                        backgroundColor: '#ddd',
                                                        flexGrow: 100 - (parseInt(v.value * 100 / max, 10) || 0)
                                                    }}/>
                                                </base.HorizontalLayout>
                                            </base.HorizontalLayout>
                                            {/* end */}
                                        </base.HorizontalLayout>
                                    </base.TouchableHighlight>
                                </base.HorizontalLayout>);
                        })
                    }
                </ScrollView>
            </base.VerticalLayout>
        );

    }

    renderFixed(dataSource) {
        if (!dataSource)return null;
        return (
            <base.TouchableHighlight onPress={()=>dataSource.onPress && dataSource.onPress()}>
                <base.HorizontalLayout
                    style={{...styles.borderBottom, alignItems: 'center', height: 60, backgroundColor: '#fff'}}>
                    <Text style={{marginLeft: 10, marginRight: 10, flexGrow: 1, fontSize: 16}}>{dataSource.left}</Text>
                    <base.HorizontalLayout style={{alignItems: 'center', justifyContent: 'flex-end'}}>
                        <Text style={{
                            color: numColor(dataSource.right),
                            fontSize: 22,
                            marginLeft: 10,
                            marginRight: 10
                        }}>{dataSource.center}</Text>
                        <base.HorizontalLayout style={{alignItems: 'center', marginRight: 10}}>
                            <Text
                                style={{color: numColor(dataSource.right)}}>{`${signNumber(parseFloat(dataSource.right))}%`}</Text>
                        </base.HorizontalLayout>
                    </base.HorizontalLayout>
                </base.HorizontalLayout>
            </base.TouchableHighlight>);
    }

}

const styles = {
    fs14: {
        fontSize: 14
    },
    fs16: {
        fontSize: 16
    },
    fs20: {
        fontSize: 20
    },
    borderTop: {
        borderTopStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#ddd'
    },
    borderBottom: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    borderLeft: {
        borderLeftStyle: 'solid',
        borderLeftWidth: 1,
        borderLeftColor: '#ddd'
    },
    borderRight: {
        borderRightStyle: 'solid',
        borderRightWidth: 1,
        borderRightColor: '#ddd'
    },
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    cbox: {
        justifyContent: 'center',
    },
    color2: {
        color: '#7f7f7f',
    },
    p10: {
        paddingRight: 10
    },
    list: {
        flexGrow: 1,
    },
    listBody: {
        flexGrow: 1,
        borderStyle: 'solid',
        borderTopColor: '#c6c6c6',
        borderTopWidth: 1,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    listView: {
        flexGrow: 1,
    },
    top: {
        backgroundColor: '#fff',
        flexShrink: 0
    },
    mingci: {
        paddingLeft: 6,
        paddingRight: 6,
        height: 17,
        backgroundColor: '#696969',
        borderRadius: 2,
        alignItems: 'center',
        marginLeft: 10,
    },
    btnBox: {
        marginTop: 10,
        alignItems: 'center',
    },
    num: {
        color: '#ff8300',
        fontSize: 26,
    },
    mingText: {
        fontSize: 12,
        color: '#fff'
    },
    detailBtn: {
        width: 144,
        height: 35,
        backgroundColor: '#ffd900',
        borderRadius: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    /*item */
    items: {
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        height: 60,
        justifyContent: 'center',
        position: 'relative',
        backgroundColor: '#fff',
        paddingRight: 10,
        paddingLeft: 10,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    item: {
        height: 60,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    arrow: {
        width: 8,
        height: 14,
    },
    flexBox: {
        flexGrow: 1,
        alignItems: 'center',
    },
    flexBoxCenter: {
        flexGrow: 1,
        justifyContent: 'center',
    },
    flexBoxRight: {
        flexGrow: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    mb10: {
        marginBottom: 5,
        alignItems: 'center',
    }
};

function upOrDown(val) {
    val = parseFloat(val);
    if (val >= 0) {
        return <Text style={{marginLeft: 10, height: 34, fontSize: 30, color: base.Color.orange}}>+</Text>;
    }
    return <Text style={{marginLeft: 10, height: 34, fontSize: 30, color: base.Color.green}}>-</Text>;
    /*if (val >= 0)
     return (<base.Image style={{width: 10, height: 10, marginLeft: 10, marginBottom: 14}} src="images/up.png"/>);
     return (<base.Image style={{width: 10, height: 10, marginLeft: 10, marginBottom: 14}} src="images/down.png"/>);*/
}

function numColor(val) {
    val = parseFloat(val);
    if (val >= 0)
        return base.Color.orange;
    return base.Color.green;
}

function signNumber(val) {
    if (val >= 0)
        return `+${Math.abs(val)}`;
    return `-${Math.abs(val)}`;
}


export default pageWrapper(Detail);
