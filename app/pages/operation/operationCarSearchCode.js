import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as base from '../../base';
import { Text, View, Toast, TouchableHighlight } from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import CameraView from '../../components/camera';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, JumpBack } from '../../modules';
import { fetchOperationCarSearch } from '../../runtime/action';

const platePrefix = 'http://ofo.so/plate/';
class CarSearchCode extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            showCam: false,
        };
        this.active = true;
    }

    viewWillAppear() {
        this.active = true;
        this.haveReadBarCode = false;
        this.setState({showCam: true, });

    }

    viewWillDisappear() {
        this.active = false;

        this.setState({
            showCam: false,
        });
    }

    handleBack() {
        JumpBack();
    }

    render() {
        return (
            <View style={styles.container}>
                <ActionBar title='车牌查询' handleBack={() => this.handleBack()}>
                    <Text style={{ fontSize: 15, color: '#000', }}>返回</Text>
                </ActionBar>
                {this.state.showCam ? (<CameraView onBarCodeRead={d => this.onBarCodeRead(d)} hint={'请扫描车牌二维码'} buttons={this.renderButtons()}
                                                   torch={true} />) : null}
            </View>
        );
    }

    onBarCodeRead(d) {
        d = d.trim();
        if (this.haveReadBarCode || !this.active) return;

        this.haveReadBarCode = true;
        if (!this.valid(d)) {
            Toast.fail('车牌号错误');
            this.__timeout = setTimeout(() => this.haveReadBarCode = false, 3000);
            return;
        }

        const bicycle_num = d.substring(platePrefix.length, d.length);

        this.props.fetch({'bicycle_num': bicycle_num}).then(()=>{
            if (this.props.operationCarCode != 0){
                Toast.fail(this.props.operationCarMessage, 1.5, () => this.haveReadBarCode = false);
            } else {
                clearTimeout(this.__timeout);
                if (this.props.activeInfo.is_active == false && this.props.recordInfo.is_record == false){
                    Toast.fail('未在组装厂录入该车牌!', 1.5, () => this.haveReadBarCode = false);
                } else {
                    JumpTo('operation/carInformation', true);
                }
            }
        });
    }

    valid(text) {
        return text.startsWith(platePrefix);
    }

    renderButtons() {
        return [
            <View key={0}>
                <TouchableHighlight onPress={() => {this.props.clear();JumpTo('operation/carSearch/code', true);}}>
                    <base.VerticalLayout style={{ alignItems: 'center', }}>
                        <base.Image src="hand" style={{ width: 64, height: 64, }} />
                        <Text style={{ marginTop: 10, fontSize: 14, backgroundColor: 'rgba(0,0,0,0)', color: '#fff', }}>手动输入</Text>
                    </base.VerticalLayout>
                </TouchableHighlight>
            </View>
        ];
    }
}

const mapStateToProps = state=> {
    let operationCarSearch = state.api.operationCarSearch || {};
    return {
        operationCarSearch: state.api.operationCarSearch,
        activeInfo: operationCarSearch.active_info, //激活信息
        recordInfo: operationCarSearch.record_info, //组装信息
        operationCarCode: state.api.operationCarCode,
        operationCarMessage: state.api.operationCarMessage,
    };
};
const mapDispatchToProps = dispatch=> {
    return {
        dispatch,
        fetch: props=>dispatch(fetchOperationCarSearch(props)),
        clear: ()=> {dispatch({type: '@@clear', payload: {
            activeInfo: {},
            recordInfo: {},
            operationCarSearch: {},
            operationCarCode: '',
            operationCarMessage: ''
        }});}
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
};

export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(CarSearchCode));
