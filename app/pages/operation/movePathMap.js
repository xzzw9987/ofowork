import React, { Component } from 'react';
import * as base from '../../base';
import { Map, ActivityIndicator } from '../../base';
import { apibase, getUserInfo } from '../../runtime/api';
import { JumpTo, JumpBack, fetch, historyStack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
var StaticImage = require('../../base/staticImages');
import whereami from '../../base/map/whereami';

/**
 * 运动轨迹展示
 */

const MovePathMap = pageWrapper(class extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            date: historyStack.get().date,
            user_id: historyStack.get().user_id,
            region: {
                latitude: 39.979024,
                longitude: 116.312589,
                latitudeDelta: 0.05,
                longitudeDelta: 0.05,
            },
            annotations: [],
            lines: [],
            distance: '巡检轨迹'
        };
    }

    viewWillAppear() {
        this.handleGetData();
        this.interval = setInterval(() => {
            this.handleGetData();
        }, 60000);
        this.setState({ loading: false });
    }

    viewWillDisappear() {
        this.interval && clearInterval(this.interval);
    }

    //获取数据
    handleGetData() {
        let promiseList = [];
        let reportAnnotations = [];
        whereami().then(e => {
            let latitude = e.latitude;
            let longitude = e.longitude;
            this.setState({
                region: {
                    latitude: latitude,
                    longitude: longitude,
                    latitudeDelta: 0.05,
                    longitudeDelta: 0.05
                }
            });
        });
        return getUserInfo()
            .then(userInfo => {
                let { token } = userInfo;
                promiseList.push(fetch(`${apibase}/staff-path/detail`, {
                    method: 'POST',
                    body: { token, 'date': this.state.date, 'user_id': this.state.user_id || ''}
                }).then(response => {
                    this.setState({distance : '巡检轨迹(' + response['data']['distance'] + 'km)'});
                    if (response['code'] != 0 || response['data']['list'].length == 0) {
                        return [];
                    } else {
                        response['data']['list'].map((item, index) => {
                            reportAnnotations.push({
                                key: `${index}`,
                                latitude: parseFloat(item.lat),
                                longitude: parseFloat(item.lng),
                                number: item.created_at,
                            });
                        });
                        return reportAnnotations;
                    }
                }));
                Promise.all(promiseList).then(response => {
                    let lines = response[0];
                    let annotations = [{
                        key: '0',
                        latitude: parseFloat(lines[0].latitude),
                        longitude: parseFloat(lines[0].longitude),
                        number: lines[0].number.substring(5,lines[0].number.length),
                        icon: StaticImage['movePathStart'],
                    },{
                        key: '1',
                        latitude: parseFloat(lines[lines.length-1].latitude),
                        longitude: parseFloat(lines[lines.length-1].longitude),
                        number: lines[lines.length-1].number.substring(5,lines[0].number.length),
                        icon: StaticImage['movePathEnd'],
                    }];
                    this.setState({ lines, annotations});
                });
            });
    }

    handleBack() {
        JumpBack();
    }

    render() {
        return (
            <base.View className="wrap" style={styles.container}>
                <ActionBar title={this.state.distance} handleBack={this.handleBack.bind(this)}>
                    <base.Text style={{fontSize: 15, color: '#000'}}>返回</base.Text>
                </ActionBar>
                <base.VerticalLayout style={{ flex: 1 }}>
                    {this.renderMap()}
                </base.VerticalLayout>
            </base.View>
        );
    }

    renderMap() {
        return this.state.loading ? null :
            (<Map style={{ flex: 1 }} region={this.state.region}
                  annotations={this.state.annotations}
                 lines={this.state.lines}/>);
    }
});

export default MovePathMap;

const styles = {
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff'
    },
    paneInner: {
        width: 200,
        height: 30,
        borderRadius: 6,
        overflow: 'hidden',
        borderStyle: 'solid',
        borderColor: '#ffd900',
        borderWidth: 1,
    },
    topNav: {
        height: 44,
        backgroundColor: '#fff',
        position: 'relative',
        borderBottomColor: '#ddd',
        borderBottomWidth: 1,
        borderBottomStyle: 'solid',
        flexShrink: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    vcBox: {
        alignItems: 'center',
        height: 44,
        position: 'absolute',
        left: 0,
        justifyContent: 'center',
    },
    refresh: {
        alignItems: 'center',
        height: 44,
        position: 'absolute',
        right: 10,
        justifyContent: 'center',
    },
    backBtn: {
        paddingLeft: 10,
        paddingRight: 15,
        height: 44,
        alignItems: 'center',
        justifyContent: 'center',
    },
    arrow: {
        width: 10,
        height: 16
    },
};


