import React, { Component } from 'react';
import * as base from '../../base';
import { Map, Button } from '../../base';
import { apibase, getUserInfo } from '../../runtime/api';
import { JumpTo, JumpBack, fetch } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
var StaticImage = require('../../base/staticImages');
import whereami from '../../base/map/whereami';
import platform from '../../modules/platform';

/**
 * 运营坏车标记图
 */

const OperationBadCarMap = pageWrapper(class extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            region: {
                latitude: 39.979024,
                longitude: 116.312589,
                latitudeDelta: 0.05,
                longitudeDelta: 0.05,
            },
            annotations: [],
            type: 'today',
            time: '',
        };
    }

    viewWillAppear() {
        this.handleGetData();
        this.setState({ loading: false });
    }
    //获取数据
    handleGetData() {
        let promiseList = [];
        let reportAnnotations = [];
        whereami().then(e => {
            let latitude = e.latitude;
            let longitude = e.longitude;
            this.setState({
                region: {
                    latitude: latitude,
                    longitude: longitude,
                    latitudeDelta: 0.05,
                    longitudeDelta: 0.05
                }
            });
        });
        return getUserInfo()
            .then(userInfo => {
                let { token } = userInfo;
                promiseList.push(fetch(`${apibase}/broken-bicycle/record-map`, {
                    method: 'POST',
                    body: { token, type: this.state.type }
                }).then(response => {
                    if (response['code'] != 0 || response['data']['list'].length == 0) {
                        return [];
                    } else {
                        let size = { width: 30, height: 38 };
                        response['data']['list'].map((item, index) => {
                            reportAnnotations.push({
                                key: `${index}`,
                                id: item.id,
                                latitude: parseFloat(item.lat),
                                longitude: parseFloat(item.lng),
                                icon: item.status == 1 ? StaticImage['badCarIcon'] : StaticImage['badCarDone'],
                                size: size,
                                number: item.bicycle_num,
                                imgPath: item.img_path,
                                label: item.user_name || '空',
                                status: item.status,
                                createTime: item.create_time,
                            });
                        });
                        return reportAnnotations;
                    }
                }));
                Promise.all(promiseList).then(response => {
                    let now = new Date();
                    let time = now.getFullYear() + '年' + (now.getMonth()+1) + '月' + now.getDate() + '日 ' + now.getHours() + '时' + now.getMinutes() + '分' + now.getSeconds() + '秒';
                    this.setState({ annotations: response[0], time });
                });
            });
    }

    handleBack() {
        JumpBack();
    }

    //今天历史切换
    handleSwitchType(type) {
        this.setState({ type: type, annotations: []});
        this.handleGetData();
    }

    //点击小图标
    onAnnotationClicked(key) {
        let info = this.state.annotations[key];
        if(info.status == 1){
            JumpTo('operation/badCarDetail', true, { info: info });
        } else if(info.status == 0){
            JumpTo('operation/badCarDone', true, { info: info });
        }
    }

    render() {
        let top = platform.OS === 'ios' ? 20 : 0;
        return (
            <base.View style={{ paddingTop: top, ...styles.container }} className="wrap">
                <base.HorizontalLayout style={styles.topNav}>
                    <base.HorizontalLayout style={styles.vcBox} >
                        <base.Button style={styles.backBtn} onPress={this.handleBack.bind(this)}>
                            <base.Image style={styles.arrow} src='back' />
                        </base.Button>
                    </base.HorizontalLayout>
                    {this.renderPane()}
                    <base.HorizontalLayout style={styles.refresh} >
                        <base.Button onPress={()=>{this.setState({annotations: []});this.handleGetData();}}>
                            <base.Text>刷新</base.Text>
                        </base.Button>
                    </base.HorizontalLayout>
                </base.HorizontalLayout>
                <base.VerticalLayout style={{ flex: 1, position: 'relative' }}>
                    {this.renderMap()}
                    {this.state.time != '' ?
                        <base.HorizontalLayout style={styles.time}>
                            <base.Text>上次刷新时间：{this.state.time}</base.Text>
                        </base.HorizontalLayout>: null }
                </base.VerticalLayout>
            </base.View>
        );
    }

    renderMap() {
        return this.state.loading ? null :
            (<Map style={{ flex: 1 }} region={this.state.region}
                 annotations={this.state.annotations}
                 onAnnotationClicked={this.onAnnotationClicked.bind(this)}/>);
    }

    renderPane() {
        return (
            <base.HorizontalLayout style={styles.paneInner}>
                <base.HorizontalLayout style={{ flex: 1, justifyContent: 'center', borderTopLeftRadius: 6, borderBottomLeftRadius: 6, backgroundColor: (this.state.type === 'today') ? '#ffd900' : '#fff' }}>
                    <Button clickDuration={0} style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                            onPress={() => { this.handleSwitchType('today'); }}>
                        <base.Text>今天</base.Text>
                    </Button>
                </base.HorizontalLayout>
                <base.HorizontalLayout style={{ flex: 1, justifyContent: 'center', borderTopRightRadius: 6, borderBottomRightRadius: 6, backgroundColor: (this.state.type === 'history') ? '#ffd900' : '#fff' }}>
                    <Button clickDuration={0} style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                            onPress={() => { this.handleSwitchType('history'); }}>
                        <base.Text>历史</base.Text>
                    </Button>
                </base.HorizontalLayout>
            </base.HorizontalLayout>);
    }

});

export default OperationBadCarMap;

const styles = {
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff'
    },
    paneInner: {
        width: 200,
        height: 30,
        borderRadius: 6,
        overflow: 'hidden',
        borderStyle: 'solid',
        borderColor: '#ffd900',
        borderWidth: 1,
    },
    topNav: {
        height: 44,
        backgroundColor: '#fff',
        position: 'relative',
        borderBottomColor: '#ddd',
        borderBottomWidth: 1,
        borderBottomStyle: 'solid',
        flexShrink: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    vcBox: {
        alignItems: 'center',
        height: 44,
        position: 'absolute',
        left: 0,
        justifyContent: 'center',
    },
    refresh: {
        alignItems: 'center',
        height: 44,
        position: 'absolute',
        right: 10,
        justifyContent: 'center',
    },
    backBtn: {
        paddingLeft: 10,
        paddingRight: 15,
        height: 44,
        alignItems: 'center',
        justifyContent: 'center',
    },
    arrow: {
        width: 10,
        height: 16
    },
    time: {
        height:20,
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: '#FFD900',
        borderStyle:"solid",
        borderColor:"#999999",
        borderTopWidth:1,
        borderLeftWidth:0,
        borderRightWidth:0,
        borderBottomWidth:1,
        paddingLeft:10,
        alignItems:'center',
        justifyContent: 'center'
    },
};


