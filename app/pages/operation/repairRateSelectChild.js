import React, { Component } from 'react';
import { api, keys } from '../../runtime/api';
import {
    Text,
    View,
    Br,
    Button,
    TextInput,
    TouchableHighlight,
    ScrollView,
    ActivityIndicator,
    HorizontalLayout,
    VerticalLayout,
    Image
} from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack, historyStack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import * as actions from '../../runtime/action';

const RepairRateSelectChild = pageWrapper(
    class extends Component {
        constructor(props) {
            super(props);
            this.state = {
                loading: true,
                historyState: historyStack.get()
            };
        }

        async viewWillAppear() {
            const { city, date } = this.state.historyState;
            if (city) {
                await this.props.dispatch(
                    actions.getZoneList({
                        zone_id: city.area_code,
                        current_level: this.props.zoneLevel
                    })
                );
                const { schoolList, societyList } = this.props;
                const res = await this.props.dispatch(
                    actions.fetchRepairRate({
                        startDate: date,
                        endDate: date,
                        all: true,
                        schoolIds: schoolList.map(school => school.zone_id).join(','),
                        societyIds: societyList.map(society => society.zone_id).join(',')
                    })
                );
            }
            this.setState({ loading: false });
        }

        handleGoBack() {
            JumpBack();
        }

        render() {
            if (this.state.loading) return this.renderLoading();
            return this.renderList();
        }

        renderLoading() {
            return (
                <View style={styles.container}>
                    <ActionBar title="报修率" handleBack={this.handleGoBack.bind(this)}>
                        返回
                    </ActionBar>
                    <View
                        style={{
                            position: 'absolute',
                            flexDirection: 'column',
                            left: 0,
                            top: 44,
                            right: 0,
                            bottom: 0,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <ActivityIndicator size="large" src="loading" />
                        <HorizontalLayout style={{ marginTop: 10 }}>
                            <Text>正在加载 ...</Text>
                        </HorizontalLayout>
                    </View>
                </View>
            );
        }

        renderList() {
            const list = this.props.rateList[this.state.historyState.date];
            const city = this.state.historyState.city;
            return (
                <View style={styles.container}>
                    <ActionBar title="报修率" handleBack={this.handleGoBack.bind(this)}>
                        返回
                    </ActionBar>
                    <VerticalLayout>
                        <HorizontalLayout
                            style={{ ...styles.list, height: 42, backgroundColor: '#f8f8f8' }}
                        >
                            <Text style={styles.one}>
                                {this.state.historyState.date}
                            </Text>
                        </HorizontalLayout>
                    </VerticalLayout>
                    <ScrollView className="overflowY" style={styles.listView}>
                        {city
                            ? <VerticalLayout style={{ backgroundColor: '#fff', marginBottom: 10 }}>
                                <TouchableHighlight
                                    onPress={() => {
                                        JumpTo('repairRateSixty', true, {
                                            zoneId: city.area_code,
                                            zoneLevel: city.zoneLevel,
                                            zoneName: city.area_name
                                        });
                                    }}
                                >
                                    <HorizontalLayout style={styles.list}>
                                        <HorizontalLayout style={styles.one}>
                                            <Text>{city.area_name}</Text>
                                        </HorizontalLayout>
                                        <HorizontalLayout style={styles.two}>
                                            <Text style={{ color: '#ff9000' }}>
                                                {city.repair_rate}
                                            </Text>
                                        </HorizontalLayout>
                                        <HorizontalLayout style={styles.three}>
                                            <Image style={styles.arrow} src="arrow" />
                                        </HorizontalLayout>
                                    </HorizontalLayout>
                                </TouchableHighlight>
                            </VerticalLayout>
                            : null}
                        {list.map((zone, idx) => {
                            return (
                                <VerticalLayout key={idx} style={{ backgroundColor: '#fff' }}>
                                    <TouchableHighlight
                                        onPress={() => {
                                            JumpTo('repairRateSixty', true, {
                                                zoneId: zone.area_code,
                                                zoneLevel: zone.zoneLevel,
                                                zoneName: zone.area_name
                                            });
                                        }}
                                    >
                                        <HorizontalLayout style={styles.list}>
                                            <HorizontalLayout style={styles.one}>
                                                <Text>{zone.area_name}</Text>
                                            </HorizontalLayout>
                                            <HorizontalLayout style={styles.two}>
                                                <Text style={{ color: '#ff9000' }}>
                                                    {zone.repair_rate}
                                                </Text>
                                            </HorizontalLayout>
                                            <HorizontalLayout style={styles.three}>
                                                <Image style={styles.arrow} src="arrow" />
                                            </HorizontalLayout>
                                        </HorizontalLayout>
                                    </TouchableHighlight>
                                </VerticalLayout>
                            );
                        })}
                    </ScrollView>
                </View>
            );
        }
    }
);

const mapStateToProps = state => {
    return {
        zoneLevel: state.api.userInfo.operation_level,
        societyList: state.api.societyList,
        schoolList: state.api.schoolList,
        rateList: state.api.cityRepairRateList || state.api.rateIndexRepairRateList
    };
};
const mapDispatchToProps = dispatch => {
    return {
        dispatch
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(
    RepairRateSelectChild
);

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    },
    listView: {
        flex: 1
    },
    list: {
        borderBottomWidth: 1,
        borderBottomColor: '#c6c6c6',
        borderBottomStyle: 'solid',
        justifyContent: 'space-around',
        height: 54,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 20
    },
    one: {
        flexGrow: 1,
        flexBasis: 0
    },
    two: {
        flexGrow: 0.7,
        justifyContent: 'flex-start',
        flexBasis: 0
    },
    three: {
        alignItems: 'flex-end'
    },
    arrow: {
        width: 8,
        height: 14
    }
};
