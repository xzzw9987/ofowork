import React, { Component } from 'react';
import { api, keys } from '../../runtime/api';
import {
    Text,
    View,
    Br,
    Button,
    TouchableHighlight,
    ScrollView,
    ActivityIndicator,
    HorizontalLayout,
    VerticalLayout,
    Image
} from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack, historyStack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';

const RepairRateSelectCity = pageWrapper(
    class extends Component {
        constructor() {
            super();
            this.state = {
                historyState: historyStack.get()
            };
        }

        handleGoBack() {
            JumpBack();
        }

        render() {
            const list = this.props.rateList[this.state.historyState.date];
            return (
                <View style={styles.container}>
                    <ActionBar title="报修率" handleBack={this.handleGoBack.bind(this)}>
                        返回
                    </ActionBar>
                    <VerticalLayout>
                        <HorizontalLayout
                            style={{ ...styles.list, height: 42, backgroundColor: '#f8f8f8' }}
                        >
                            <Text style={styles.one}>
                                {this.state.historyState.date}
                            </Text>
                        </HorizontalLayout>

                    </VerticalLayout>
                    <ScrollView className="overflowY" style={styles.listView}>
                        {list.map((city, idx) => {
                            return (
                                <VerticalLayout key={idx} style={{ backgroundColor: '#fff' }}>
                                    <TouchableHighlight
                                        onPress={() => {
                                            JumpTo('repairRateSelectChild', true, {
                                                date: this.state.historyState.date,
                                                city
                                            });
                                        }}
                                    >
                                        <HorizontalLayout style={styles.list}>
                                            <HorizontalLayout style={styles.one}>
                                                <Text>{city.area_name}</Text>
                                            </HorizontalLayout>
                                            <HorizontalLayout style={styles.two}>
                                                <Text style={{ color: '#ff9000' }}>
                                                    {city.repair_rate}
                                                </Text>
                                            </HorizontalLayout>
                                            <HorizontalLayout style={styles.three}>
                                                <Image style={styles.arrow} src="arrow" />
                                            </HorizontalLayout>
                                        </HorizontalLayout>
                                    </TouchableHighlight>
                                </VerticalLayout>
                            );
                        })}
                    </ScrollView>
                </View>
            );
        }
    }
);

const mapStateToProps = state => {
    return {
        rateList: state.api.rateIndexRepairRateList
    };
};
const mapDispatchToProps = dispatch => {
    return {
        dispatch
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(
    RepairRateSelectCity
);

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    },
    listView: {
        flex: 1
    },
    list: {
        borderBottomWidth: 1,
        borderBottomColor: '#c6c6c6',
        borderBottomStyle: 'solid',
        justifyContent: 'space-around',
        height: 54,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 20
    },
    one: {
        flexGrow: 1,
        flexBasis: 0
    },
    two: {
        flexGrow: 0.7,
        justifyContent: 'flex-start',
        flexBasis: 0
    },
    three: {
        alignItems: 'flex-end'
    },
    arrow: {
        width: 8,
        height: 14
    }
};
