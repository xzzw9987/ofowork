import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys } from '../../runtime/api';
import { Text, View, Br, Button, TextInput, TouchableHighlight, ActivityIndicator, Toast } from '../../base';
import { JumpTo, historyStack, JumpBack, fetch } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import styles from '../../css/operation/carSearchCss';
import Keyboard from '../../modules/keyboard';
import { apibase, getUserInfo } from '../../runtime/api';
var moment = require('moment');

/**
 * 运营解锁码更改
 */
class OperationChangeUnlockCode extends Component {

    constructor(){
        super();
        this.state={
            loading: false,
            /*carMessage: historyStack.get().carMessage,
            unlockCode: historyStack.get().carMessage.unlock_code,*/
            bicycle: '',
            newUnlockCode: '',
        };
    }

    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }

    validCode(code) {
        return /^\d{4}$/.test(code);
    }

    validBicycle(code) {
        return /^\d{4,9}$/.test(code);
    }

    onSubmit() {
        Keyboard.dismiss();
        if (!this.validBicycle(this.state.bicycle)) {
            Toast.fail('请输入4~9位车牌号', 1.5);
            return;
        }
        if (!this.validCode(this.state.newUnlockCode)) {
            Toast.fail('请输入4位解锁码', 1.5);
            return;
        }
        this.setState({ loading: true });
        return getUserInfo()
            .then(userInfo => {
                let { token } = userInfo;
                let bicycle = this.state.bicycle;
                let newUnlockCode = this.state.newUnlockCode;
                fetch(`${apibase}/operation/update-lock-password`, {
                    method: 'POST',
                    body: { token, 'bicycle_num': bicycle, 'new_unlock_code': newUnlockCode }
                }).then(respones => {
                    this.setState({ loading: false });
                    if (respones['code'] === 0) {
                        Toast.success('解锁码更新成功', 1.5);
                        this.setState({bicycle: '',newUnlockCode: ''});
                    } else {
                        Toast.fail(respones['message'], 1.5);
                    }
                });
            });
    }

    render() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="解锁码更改" handleBack={this.handleGoBack.bind(this) }>返回</ActionBar>
                <base.View style={{flexGrow: 1, flexDirection: 'column'}}>
                    <base.VerticalLayout>
                        <base.HorizontalLayout style={styles.carCode}>
                            <Text style={styles.text2}>车牌号：</Text>
                            {/*<Text style={styles.text3}>{this.state.bicycle}</Text>*/}
                            <base.TextInput keyboardType="numeric" value={this.state.bicycle} placeholder='请输入车牌号'
                                            style={this.state.bicycle != '' ? styles.newCode2 : styles.newCode}
                                            onChangeText={(text)=>{this.setState({bicycle: text});}}/>
                        </base.HorizontalLayout>
                        <base.HorizontalLayout style={styles.carCode}>
                            <Text style={styles.text2}>新解锁码：</Text>
                            <base.TextInput keyboardType="numeric" value={this.state.newUnlockCode} placeholder='请输入4位解锁码'
                                            style={this.state.newUnlockCode != '' ? styles.newCode2 : styles.newCode}
                                            onChangeText={(text)=>{this.setState({newUnlockCode: text});}}/>
                        </base.HorizontalLayout>
                        <Button onPress={()=>this.onSubmit()} clickDuration={0} style={styles.submitBtn}>确认</Button>
                    </base.VerticalLayout>
                </base.View>
                {this.state.loading ?
                    <base.View style={{ position: 'absolute', flexDirection: 'column', left: 0, top: 0, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,.5)' }}>
                        <ActivityIndicator size='large' src='loading' />
                        <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                    : null
                }
            </base.View>

        );
    }
}

export default pageWrapper(OperationChangeUnlockCode);

