import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, getUserInfo, apibase } from '../../runtime/api';
import { Text, View, Br, Button, TextInput, TouchableHighlight, ScrollView, ActivityIndicator } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack, historyStack, fetch } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';

var moment = require('moment');

/**
 * 寻迹轨迹电子围栏选择
 */
class MovePathFence extends Component {

    constructor() {
        super();
        this.state = {
            loading: true,
            operation_level: historyStack.get().operation_level,
            date: historyStack.get().date,
            type: 'school',
            zoneNames: [],
            cityZoneNames: [],
        };
    }

    viewWillAppear() {
        this.handlePathFence();
    }

    /*电子围栏*/
    handlePathFence() {
        return getUserInfo().then(userInfo => {
            let token = userInfo['token'];
            let zone_ids = userInfo['zone_ids'];
            let zone_names = userInfo['zone_names'];
            let zoneNames = zone_ids.split(',').map((id, index)=> {
                return {
                    id: id,
                    name: zone_names.split(',')[index]
                };
            });

            if(this.state.operation_level == 'city'){
                fetch(`${apibase}/zone/get-child-zone-list`, {
                    method: 'POST',
                    body: { token, 'zone_id': zoneNames[0].id, 'child_level': this.state.type, 'current_level': this.state.operation_level}
                }).then(respones => {
                    this.setState({cityZoneNames: respones.data.list, loading: false});
                });
            } else {
                this.setState({loading: false, zoneNames });
            }
        });
    }

    /**
     * tab切换
     */
    handleSwitchZone(type) {
        this.setState({type: type, loading: true});
        this.handlePathFence();
    }

    handleGoWhere(zone_id, zone_name) {
        let area_code = zone_id;
        let area_type = (this.state.operation_level == 'city') ? this.state.type : this.state.operation_level;
        let date = this.state.date;
        JumpTo('movePath/person',true,{area_code , area_type, date, 'zone_name': zone_name});
    }

    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }

    render() {
        if(this.state.operation_level == 'city'){
            return this.renderData();
        } else if(this.state.operation_level == 'school' || this.state.operation_level == 'society'){
            return this.renderList();
        }
    }

    /*运营*/
    renderList() {
        let zoneNames = this.state.zoneNames;
        return (
            <base.View style={styles.container}>
                <ActionBar title="巡检轨迹" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.VerticalLayout style={{flex: 1}}>
                    <base.HorizontalLayout style={{...styles.list, height: 42,backgroundColor: '#f8f8f8'}}>
                        <base.Text style={styles.one}>请选择电子围栏</base.Text>
                    </base.HorizontalLayout>
                    {this.state.loading ?
                        <base.View style={{backgroundColor: '#fff', position: 'absolute', flexDirection: 'column', left: 0, top: 44, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center' }}>
                            <ActivityIndicator size='large' src='loading' />
                            <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                        </base.View>
                        :
                        <ScrollView style={{backgroundColor: '#fff', flex: 1}}>
                            {
                                zoneNames.map((item,index)=>{
                                    return (
                                        <TouchableHighlight key={index} onPress={()=>{this.handleGoWhere(item.id, item.name)}}>
                                            <base.HorizontalLayout style={styles.list}>
                                                <base.HorizontalLayout style={styles.one}>
                                                    <base.Text>{item.name}</base.Text>
                                                </base.HorizontalLayout>
                                                <base.HorizontalLayout style={styles.three}>
                                                    <base.Image style={styles.arrow} src="arrow" />
                                                </base.HorizontalLayout>
                                            </base.HorizontalLayout>
                                        </TouchableHighlight>
                                    );
                                })
                            }
                        </ScrollView>
                    }
                </base.VerticalLayout>
            </base.View>
        );
    }

    /*城市经理*/
    renderData() {
        let cityZoneNames = this.state.cityZoneNames;
        return (
            <base.View style={styles.container}>
                <ActionBar title="巡检轨迹" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.VerticalLayout style={{flex: 1}}>
                    <base.HorizontalLayout style={styles.nav}>
                        <base.VerticalLayout style={styles.inner}>
                            <base.Button clickDuration={0} style={(this.state.type == 'school' ) ? styles.item1 : styles.item2} onPress={ () => { this.handleSwitchZone('school'); } }>
                                <base.Text style={(this.state.type == 'school' ) ? styles.text1 : styles.text2}>学校</base.Text>
                            </base.Button>
                        </base.VerticalLayout>
                        <base.VerticalLayout style={styles.inner}>
                            <base.Button clickDuration={0} style={(this.state.type == 'society' ) ? styles.item1 : styles.item2} onPress={ () => { this.handleSwitchZone('society'); } }>
                                <base.Text style={(this.state.type == 'society' ) ? styles.text1 : styles.text2}>城市</base.Text>
                            </base.Button>
                        </base.VerticalLayout>
                    </base.HorizontalLayout>

                    {this.state.loading ?
                        <base.View style={{
                            backgroundColor: '#fff',
                            position: 'absolute',
                            flexDirection: 'column',
                            left: 0,
                            top: 44,
                            right: 0,
                            bottom: 0,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            <ActivityIndicator size='large' src='loading'/>
                            <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                        </base.View>
                        :
                        <ScrollView style={{backgroundColor: '#fff', flex: 1}}>
                            {
                                cityZoneNames.map((item, index) => {
                                    return (
                                        <TouchableHighlight key={item.zone_id} onPress={() => {
                                            this.handleGoWhere(item.zone_id, item.zone_name)
                                        }}>
                                            <base.HorizontalLayout style={styles.list}>
                                                <base.HorizontalLayout style={styles.one}>
                                                    <base.Text>{item.zone_name}</base.Text>
                                                </base.HorizontalLayout>
                                                <base.HorizontalLayout style={styles.three}>
                                                    <base.Image style={styles.arrow} src="arrow"/>
                                                </base.HorizontalLayout>
                                            </base.HorizontalLayout>
                                        </TouchableHighlight>
                                    );
                                })
                            }

                        </ScrollView>
                    }
                </base.VerticalLayout>
            </base.View>
        );
    }

}
export default (pageWrapper(MovePathFence));

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    list: {
        borderBottomWidth: 1,
        borderBottomColor: '#c6c6c6',
        borderBottomStyle: 'solid',
        justifyContent: 'space-around',
        height: 54,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 20
    },
    one: {
        flexGrow: 1,
        flexBasis: 0,
    },
    three: {
        alignItems: 'flex-end',
    },
    arrow: {
        width: 8,
        height: 14,
    },
    nav: {
        backgroundColor: '#fff',
        borderStyle: 'solid',
        borderBottomColor: '#7d7d7d',
        borderBottomWidth:1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
    },
    inner: {
        alignItems: 'center',
        justifyContent: 'center',
        flexGrow: 1,
        height: 44,
    },
    item1: {
        paddingLeft: 20,
        paddingRight: 20,
        borderBottomColor:'#FAD500',
        borderBottomWidth: 4,
        borderStyle: 'solid',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        height: 40,
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    item2: {
        height: 44,
        paddingBottom: 4,
        flexGrow: 1,
        paddingLeft: 20,
        paddingRight: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text1: {
        alignSelf: 'center',
        flexGrow: 1,

    },
    text2: {
        alignSelf: 'center',
        flexGrow: 1,
        color: '#999',
    },
};
