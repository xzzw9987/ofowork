import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys } from '../../runtime/api';
import { Text, View, Br, Button, TextInput, TouchableHighlight, ActivityIndicator, Toast } from '../../base';
import { JumpTo, historyStack, JumpBack, fetch } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import Keyboard from '../../modules/keyboard';
import { apibase, getUserInfo } from '../../runtime/api';
var moment = require('moment');

/**
 * 运营解锁码更改
 */
class OperationUnbindBrand extends Component {

    constructor(){
        super();
        this.state={
            loading: false,
            bicycle: '',
        };
    }

    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }

    validCode(code) {
        return /^\d{4,11}$/.test(code);
    }

    onSubmit() {
        Keyboard.dismiss();
        if (!this.validCode(this.state.bicycle)) {
            Toast.fail('请输入4~11位车牌号', 1.5);
            return;
        }
        this.setState({ loading: true });
        return getUserInfo()
            .then(userInfo => {
                let { token } = userInfo;
                let bicycle = this.state.bicycle;
                fetch(`${apibase}/bicycle/unbind-bicycle-no`, {
                    method: 'POST',
                    body: { token, 'bicycle_no': bicycle}
                }).then(respones => {
                    this.setState({ loading: false });
                    if (respones['code'] === 0) {
                        Toast.success('解绑成功', 1.5);
                        this.setState({bicycle: ''});
                    } else {
                        Toast.fail(respones['message'], 1.5);
                    }
                });
            });
    }

    render() {
        return (
            <base.View style={styles.container} onStartShouldSetResponder={() => true} onResponderGrant={() => Keyboard.dismiss()}>
                <ActionBar title="智能锁-解绑车牌" handleBack={this.handleGoBack.bind(this) }>返回</ActionBar>
                <base.View style={{flexGrow: 1, flexDirection: 'column'}}>
                    <base.HorizontalLayout style={styles.carCode}>
                        <Text style={styles.text}>车牌号</Text>
                        <base.TextInput keyboardType="numeric" value={this.state.bicycle} placeholder='请输入车牌号'
                                        style={{fontSize: 16, flexGrow: 1, height: 64, paddingLeft: 10}}
                                        onChangeText={(text)=>{this.setState({bicycle: text});}}/>
                    </base.HorizontalLayout>
                    <base.HorizontalLayout>
                        <Button onPress={()=>this.onSubmit()} clickDuration={0} style={styles.submitBtn}>立即生效</Button>
                    </base.HorizontalLayout>
                    <base.HorizontalLayout>
                        <Text style={{paddingLeft: 10, fontSize: 16}}>
                            说明（必读）:<Br />
                            1.本功能只能解除车牌号和智能锁SN码的绑定<Br />
                            2.解除绑定后,车牌号变为新车牌<Br />
                            3.解绑后的车牌可以和机械锁/智能锁重新绑定
                        </Text>
                    </base.HorizontalLayout>
                </base.View>

                {this.state.loading ?
                    <base.View style={{ position: 'absolute', flexDirection: 'column', left: 0, top: 0, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,.5)' }}>
                        <ActivityIndicator size='large' src='loading' />
                        <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                    : null
                }
            </base.View>

        );
    }
}

export default pageWrapper(OperationUnbindBrand);

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    carCode: {
        paddingLeft: 20,
        paddingRight: 20,
        marginTop: 10,
        alignItems: 'center',
        backgroundColor: '#fff',
        borderTopStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#ddd',
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    text: {
        fontSize: 18,
        color: '#FF7000',
    },
    submitBtn: {
        color: '#fff',
        height: 44,
        justifyContent: 'center',
        fontSize: 18,
        alignItems: 'center',
        flexGrow: 1,
        backgroundColor: '#FF7000',
        borderRadius: 4,
        marginTop: 40,
        marginBottom: 60,
        marginLeft: 20,
        marginRight: 20,
    },

};

