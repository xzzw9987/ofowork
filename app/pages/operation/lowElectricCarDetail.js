import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, loadToken, getTodayDate, getUserInfo, apibase, timeSilence } from '../../runtime/api';
import {Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator, Map} from '../../base';
import { connect } from 'react-redux';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import { fetch } from '../../modules';
import whereami from '../../base/map/whereami';
var StaticImage = require('../../base/staticImages');
var moment = require('moment');
import getDistance from '../../base/map/getDistance';
import geoMessage from '../../base/map/geoMessage';
/**
 * 低电量车辆详情
 */

class LowElectricCarDetail extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            info: historyStack.get().info,
            type: historyStack.get().type,
            region: {
                latitude: 39.98362,
                longitude: 116.306715,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005,
            },
            annotations: [],
            distance: '',
            address: '',
            color: '',
        };
    }

    viewWillAppear() {
        let info = this.state.info;
        let icon;
        if (info) {
            geoMessage({latitude: info.latitude, longitude: info.longitude}).then(d => {
                this.setState({address: d.address});
            });
            if(this.state.type == 'low'){
                if(info.battery <= 15){
                    icon = StaticImage['lowElectric1'];
                    this.setState({color: '#e23000'});
                } else if(info.battery > 15 && info.battery <= 30)
                {
                    icon = StaticImage['lowElectric2'];
                    this.setState({color: '#ffd900'});
                } else {
                    icon = StaticImage['lowElectric3'];
                    this.setState({color: '#2EBF38'});
                }
            } else if(this.state.type == 'silent'){
                icon = StaticImage['reportIcon'];
            }

            let annotations = [{
                latitude: parseFloat(info.latitude),
                longitude: parseFloat(info.longitude),
                icon: icon,
                size: {
                    width: 30,
                    height: 31
                },
                title: '',
                key: 'hello~',
            }];

            whereami().then(e => {
                let region = {
                    latitude: parseFloat((info.latitude + e.latitude)/2),
                    longitude: parseFloat((info.longitude + e.longitude)/2),
                    latitudeDelta: (Math.abs(e.latitude - info.latitude))*1.2,
                    longitudeDelta: (Math.abs(e.longitude - info.longitude))*1.2
                };
                this.setState({ annotations, refresh: true, region });
            });

            const p1 = {
                    latitude: parseFloat(info.latitude),
                    longitude: parseFloat(info.longitude),
                } || Promise.resolve({
                    latitude: this.state.region.latitude,
                    longitude: this.state.region.longitude
                });
            const p2 = whereami();
            Promise.all([p1, p2])
                .then(d=>d[1].error ? null : getDistance(d[0], d[1]))
                .then(distance=> {
                    if (distance == null) {
                        this.setState({distance});
                        return;
                    }
                    distance = ~~distance;
                    this.setState({distance, loading: false});
                });
        }
    }

    handleBack() {
        JumpBack();
    }

    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderBikeDetail();
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="车辆详情" handleBack={this.handleBack.bind(this)}>
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>
                <base.View style={{ position: 'relative', flexGrow: 1 }}>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='loading' />
                        <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }

    renderBikeDetail() {
        return (
            <base.VerticalLayout className="wrap" style={styles.container}>
                <ActionBar title="车辆详情" handleBack={this.handleBack.bind(this)}>
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>
                <ScrollView style={{ flex: 1 }} className="overflow-y">
                    <base.VerticalLayout style={{ height: 350 }}>
                        <Map style={{ flexGrow: 1, height: 350 }} region={this.state.region}
                            annotations={this.state.annotations} />
                    </base.VerticalLayout>

                    {this.state.type == 'low' ?
                        <base.VerticalLayout style={styles.infoBox}>
                            <base.HorizontalLayout style={styles.item}>
                                <base.HorizontalLayout
                                    style={{...styles.battery, backgroundColor: this.state.color}}>
                                    <base.HorizontalLayout style={{ alignItems: 'flex-end' }}>
                                        <base.Text style={{ fontSize: 18, color: '#fff' }}>{this.state.info.battery}%</base.Text>
                                    </base.HorizontalLayout>
                                </base.HorizontalLayout>

                                <base.VerticalLayout style={{ flexGrow: 1, paddingLeft: 10 }}>
                                    <base.Text style={styles.carNo}>车牌{this.state.info.carNo}</base.Text>
                                    <base.HorizontalLayout style={{ alignItems: 'center' }}>
                                        <base.Text style={{ color: '#999', marginRight: 5 }}>距离</base.Text>
                                        <base.Text style={{ color: this.state.color, fontSize: 18 }}>{this.state.distance}m
                                        </base.Text>
                                    </base.HorizontalLayout>
                                </base.VerticalLayout>
                            </base.HorizontalLayout>
                            <base.HorizontalLayout style={styles.itemBot}>
                                <base.HorizontalLayout style={{ width: 11 }}>
                                    <base.Image style={styles.icon} src='icon' />
                                </base.HorizontalLayout>
                                <base.HorizontalLayout style={styles.address}>
                                    <base.Text style={{ flexWrap: 'wrap', paddingLeft: 10 }}>{this.state.address}</base.Text>
                                </base.HorizontalLayout>
                            </base.HorizontalLayout>
                            {this.state.info.time != '' ?
                                <base.HorizontalLayout style={styles.itemSilent}>
                                    <base.Text style={styles.title}>最后一次上报时间：</base.Text>
                                    <base.Text style={{flexWrap: 'wrap',...styles.data}}>{this.state.info.time}</base.Text>
                                </base.HorizontalLayout>: null}

                            <base.HorizontalLayout style={styles.itemSilent}>
                                <base.Text style={styles.title}>智能锁类型：</base.Text>
                                <base.Text style={styles.data}>{this.state.info.lockType}</base.Text>
                            </base.HorizontalLayout>
                        </base.VerticalLayout>
                        :
                        <base.VerticalLayout style={styles.infoBox}>
                            <base.HorizontalLayout style={styles.itemSilent}>
                                <base.Text style={styles.title}>沉默时长：</base.Text>
                                <base.Text style={styles.data}>{timeSilence(this.state.info.silentTime)}</base.Text>
                            </base.HorizontalLayout>
                            <base.HorizontalLayout style={styles.itemSilent}>
                                <base.Text style={styles.title}>车牌号：</base.Text>
                                <base.Text style={styles.data}>{this.state.info.carNo}</base.Text>
                            </base.HorizontalLayout>
                            <base.HorizontalLayout style={styles.itemSilent}>
                                <base.Text style={styles.title}>距离：</base.Text>
                                <base.Text style={{...styles.data, color: '#ff7000'}}>{this.state.distance}m</base.Text>
                            </base.HorizontalLayout>
                            <base.HorizontalLayout style={styles.itemSilent}>
                                <base.HorizontalLayout style={{ width: 11 }}>
                                    <base.Image style={styles.icon} src='icon' />
                                </base.HorizontalLayout>
                                <base.HorizontalLayout style={styles.address}>
                                    <base.Text style={{ flexWrap: 'wrap', paddingLeft: 10, ...styles.data}}>{this.state.address}</base.Text>
                                </base.HorizontalLayout>
                            </base.HorizontalLayout>
                            <base.HorizontalLayout style={styles.empty} />

                            <base.HorizontalLayout style={styles.itemSilent}>
                                <base.Text style={styles.title}>最后一次订单时间：</base.Text>
                                <base.Text style={{flexWrap: 'wrap',...styles.data}}>{this.state.info.lastOrderTime}</base.Text>
                            </base.HorizontalLayout>
                            <base.HorizontalLayout style={styles.itemSilent}>
                                <base.Text style={styles.title}>智能锁类型：</base.Text>
                                <base.Text style={styles.data}>{this.state.info.lockType}</base.Text>
                            </base.HorizontalLayout>
                        </base.VerticalLayout>
                    }

                </ScrollView>
            </base.VerticalLayout>
        );
    }

}
export default pageWrapper(LowElectricCarDetail);

const styles = {
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor:'#fff',
    },
    item:{
        borderStyle:'solid',
        borderColor:'#d6d6d6',
        borderBottomWidth:1,
        borderTopWidth:1,
        borderLeftWidth:0,
        borderRightWidth:0,
        backgroundColor:'#fff',
        padding:13,
    },
    battery:{
        width:50,
        height:50,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:25,
    },
    carNo:{
        color:'#333',
        fontSize:18,
        marginBottom:8,
        marginTop:2
    },
    itemBot:{
        paddingLeft:15,
        paddingRight:20,
        paddingTop:10,
        paddingBottom:10,
        borderStyle:"solid",
        borderBottomWidth:1,
        borderColor:"#d6d6d6",
        borderTopWidth:0,
        borderLeftWidth:0,
        borderRightWidth:0,
        flex:1,
        alignItems:'flex-start'
    },
    itemSilent: {
        paddingLeft:15,
        paddingRight:20,
        paddingTop:15,
        paddingBottom:15,
        borderStyle:"solid",
        borderBottomWidth:1,
        borderColor:"#d6d6d6",
        borderTopWidth:0,
        borderLeftWidth:0,
        borderRightWidth:0,
        flex:1,
        alignItems:'flex-start'
    },
    icon:{
        width:11,
        height:15,
    },
    empty: {
        height: 20,
        backgroundColor: '#f6f6f6',
        flex: 1,
        borderStyle:"solid",
        borderBottomWidth:1,
        borderColor:"#d6d6d6",
        borderTopWidth:0,
        borderLeftWidth:0,
        borderRightWidth:0,
    },
    title: {
        color: '#888',
    },
    data: {
        color: '#333',
    },
};



