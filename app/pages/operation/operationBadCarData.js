import React, { Component } from 'react';
import * as base from '../../base';
import { connect } from 'react-redux';
import { Text, TouchableHighlight, ScrollView, ActivityIndicator } from '../../base';
import { JumpTo, JumpBack, historyStack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
var moment = require('moment');
import { fetchBadCarData } from '../../runtime/action';

/**
 * 数据概览
 */
class OperationBadCarData extends Component {

    constructor() {
        super();
        this.state = {
            loading: true,
        };
    }

    viewWillAppear() {
        this.props.fetch().then(() => {this.setState({loading: false});});
    }

    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }

    render() {
        let dataList = this.props.badCarData;
        if (this.state.loading)
            return this.renderLoading();
        return this.renderList(dataList);
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="工作记录" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.View style={{ position: 'absolute', flexDirection: 'column', left: 0, top: 44, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator size='large' src='loading' />
                    <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                </base.View>
            </base.View>
        );
    }

    renderList(dataList) {
        return (
            <base.View style={styles.container}>
                <ActionBar title="工作记录" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.VerticalLayout style={styles.container}>
                    {dataList.length > 0 ?
                        <ScrollView className="overflowY" style={styles.listView}>
                            {dataList.map((item, idx) => {
                                return (
                                    <TouchableHighlight key={idx} style={styles.listView} onPress={() => {JumpTo('operation/badCarMessage', true, {item: item})}}>
                                        <base.HorizontalLayout style={styles.list}>
                                            <base.Text style={styles.one}>{item.date}</base.Text>
                                            <base.Text style={styles.two}>标记:{item.record}辆</base.Text>
                                            <base.HorizontalLayout style={styles.three}>
                                                <base.Text>处理:{item.process}辆</base.Text>
                                                <base.Image style={styles.arrow} src='arrow' />
                                            </base.HorizontalLayout>
                                        </base.HorizontalLayout>
                                    </TouchableHighlight>
                                );
                            })}
                        </ScrollView>
                        :
                        <base.View style={{ position: 'absolute', flexDirection: 'column', left: 0, top: 88, right: 0, bottom: 44, alignItems: 'center', justifyContent: 'center' }}>
                            <base.Image style={{ height: 60, width: 60, }} src='noData' />
                            <base.HorizontalLayout style={{ marginTop: 10 }}><Text>暂无数据</Text></base.HorizontalLayout>
                        </base.View>
                    }
                </base.VerticalLayout>
            </base.View>
        );
    }
}

const mapStateToProps = state => {
    return {
        badCarData: state.api.badCarData || [],
    };
};
const mapDispatchToProps = dispatch => {
    return {
        dispatch,
        fetch: props => dispatch(fetchBadCarData(props)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(OperationBadCarData));


const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    listView: {
        flexGrow: 1,
        flexBasis: 0,
    },
    scrollBorder: {
        borderStyle: 'solid',
        borderTopColor: '#c6c6c6',
        borderTopWidth: 1,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        marginTop: 20,
    },
    list: {
        borderBottomWidth: 1,
        borderBottomColor: '#c6c6c6',
        borderBottomStyle: 'solid',
        justifyContent: 'space-around',
        height: 54,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10
    },
    one: {
        flexGrow: .8,
        flexBasis: 0,
    },
    two: {
        flexGrow: 1,
        justifyContent: 'flex-start',
        textAlign: 'center',
        flexBasis: 0,
    },
    three: {
        flexGrow: 1,
        flexBasis: 0,
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    arrow: {
        width: 8,
        height: 14,
    },
};
