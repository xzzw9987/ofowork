import React, { Component } from 'react';
import * as base from '../../base';
import { Map, ActivityIndicator } from '../../base';
import { apibase, getUserInfo } from '../../runtime/api';
import { JumpTo, JumpBack, fetch } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
var StaticImage = require('../../base/staticImages');
import whereami from '../../base/map/whereami';
import platform from '../../modules/platform';

/**
 * 路面信息展示
 */

const CompetitorInformationMap = pageWrapper(class extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            region: {
                latitude: 39.979024,
                longitude: 116.312589,
                latitudeDelta: 0.05,
                longitudeDelta: 0.05,
            },
            annotations: [],
            type: 'today',
        };
    }

    viewWillAppear() {
        this.handleGetData();
        this.setState({ loading: false });
    }

    //获取数据
    handleGetData() {
        let promiseList = [];
        let reportAnnotations = [];
        whereami().then(e => {
            let latitude = e.latitude;
            let longitude = e.longitude;
            this.setState({
                region: {
                    latitude: latitude,
                    longitude: longitude,
                    latitudeDelta: 0.05,
                    longitudeDelta: 0.05
                }
            });
        });
        return getUserInfo()
            .then(userInfo => {
                let start_date;
                let end_date;
                if(this.state.type == 'today'){
                    start_date = this.GetDateStr(0);
                    end_date = this.GetDateStr(0);
                } else {
                    start_date = this.GetDateStr(-7);
                    end_date = this.GetDateStr(-1);
                }
                let { token } = userInfo;
                promiseList.push(fetch(`${apibase}/competitor/record-list`, {
                    method: 'POST',
                    body: { token, start_date, end_date}
                }).then(response => {
                    if (response['code'] != 0 || response['data']['list'].length == 0) {
                        return [];
                    } else {
                        let size = { width: 30, height: 38 }, icon;
                        response['data']['list'].map((item, index) => {
                            if((item.info[0].type == 1 && item.info[0].num >= 1.1 * item.info[1].num) || (item.info[0].type == 2 && item.info[1].num >= item.info[0].num * 1.1)){
                                icon = StaticImage['ofo'];
                            } else {
                                icon = StaticImage['other'];
                            }
                            reportAnnotations.push({
                                key: `${index}`,
                                id: item.id,
                                latitude: parseFloat(item.lat),
                                longitude: parseFloat(item.lng),
                                icon: icon,
                                size: size,
                            });
                        });
                        return reportAnnotations;
                    }
                }));
                Promise.all(promiseList).then(response => {
                    this.setState({ annotations: response[0]});
                });
            });
    }

    GetDateStr(AddDayCount) {
        let dd = new Date();
        dd.setDate(dd.getDate()+AddDayCount);
        let y = dd.getFullYear();
        let m = ((dd.getMonth()+1) < 10) ? ('0'+(dd.getMonth()+1)) : (dd.getMonth()+1);
        let d = (dd.getDate() < 10) ? ('0'+(dd.getDate())) : dd.getDate();
        return y+"-"+m+"-"+d;
    }

    handleBack() {
        JumpBack();
    }

    //今天历史切换
    handleSwitchType(type) {
        this.setState({ type: type, annotations: []});
        this.handleGetData();
    }

    //点击小图标
    onAnnotationClicked(key) {
        let info = this.state.annotations[key];
        JumpTo('competitorInformation/detail', true, { info: info.id });
    }

    render() {
        let top = platform.OS === 'ios' ? 20 : 0;
        return (
            <base.View style={{ paddingTop: top, ...styles.container }} className="wrap">
                <base.HorizontalLayout style={styles.topNav}>
                    <base.HorizontalLayout style={styles.vcBox} >
                        <base.Button style={styles.backBtn} onPress={this.handleBack.bind(this)}>
                            <base.Image style={styles.arrow} src='back' />
                        </base.Button>
                    </base.HorizontalLayout>
                    {this.renderPane()}
                </base.HorizontalLayout>
                <base.VerticalLayout style={{ flex: 1 }}>
                    {this.renderMap()}
                </base.VerticalLayout>
            </base.View>
        );
    }

    renderMap() {
        return this.state.loading ? null :
            (<Map style={{ flex: 1 }} region={this.state.region}
                 annotations={this.state.annotations}
                 onAnnotationClicked={this.onAnnotationClicked.bind(this)}/>);
    }

    renderPane() {
        return (
            <base.HorizontalLayout style={styles.paneInner}>
                <base.HorizontalLayout style={{ flex: 1, justifyContent: 'center', borderTopLeftRadius: 6, borderBottomLeftRadius: 6, backgroundColor: (this.state.type === 'today') ? '#ffd900' : '#fff' }}>
                    <base.Button clickDuration={0} style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                            onPress={() => { this.handleSwitchType('today'); }}>
                        <base.Text>今天</base.Text>
                    </base.Button>
                </base.HorizontalLayout>
                <base.HorizontalLayout style={{ flex: 1, justifyContent: 'center', borderTopRightRadius: 6, borderBottomRightRadius: 6, backgroundColor: (this.state.type === 'history') ? '#ffd900' : '#fff' }}>
                    <base.Button clickDuration={0} style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                            onPress={() => { this.handleSwitchType('history'); }}>
                        <base.Text>近7天</base.Text>
                    </base.Button>
                </base.HorizontalLayout>
            </base.HorizontalLayout>);
    }

});

export default CompetitorInformationMap;

const styles = {
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff'
    },
    paneInner: {
        width: 200,
        height: 30,
        borderRadius: 6,
        overflow: 'hidden',
        borderStyle: 'solid',
        borderColor: '#ffd900',
        borderWidth: 1,
    },
    topNav: {
        height: 44,
        backgroundColor: '#fff',
        position: 'relative',
        borderBottomColor: '#ddd',
        borderBottomWidth: 1,
        borderBottomStyle: 'solid',
        flexShrink: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    vcBox: {
        alignItems: 'center',
        height: 44,
        position: 'absolute',
        left: 0,
        justifyContent: 'center',
    },
    refresh: {
        alignItems: 'center',
        height: 44,
        position: 'absolute',
        right: 10,
        justifyContent: 'center',
    },
    backBtn: {
        paddingLeft: 10,
        paddingRight: 15,
        height: 44,
        alignItems: 'center',
        justifyContent: 'center',
    },
    arrow: {
        width: 10,
        height: 16
    },
};


