import React, {Component} from 'react';
import * as base from '../../base';
import {apibase, getUserInfo} from '../../runtime/api';
import {Text, Button, ScrollView, ActivityIndicator, Toast} from '../../base';
import {historyStack, JumpBack, fetch} from '../../modules';
import {ActionBar} from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import geoMessage from '../../base/map/geoMessage';
import Keyboard from '../../modules/keyboard';
import platform from '../../modules/platform';
import whereami from '../../base/map/whereami';
import externalApp from '../../modules/externalApp';

/**
 * 坏车标记详情
 */

const OperationBadCarDetail = pageWrapper(class extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            info: historyStack.get().info,
            num: '',
            address: '',
            keyboardHeight: 0
        };
        this.scrollViewHeight = 0;
        this.scrollContentHeight = 0;
    }


    viewWillAppear() {
        let info = this.state.info;
        if (info) {
            geoMessage({latitude: info.latitude, longitude: info.longitude}).then(d => {
                this.setState({address: d.address});
            });
        }
        this.setState({loading: false});
    }

    onKeyboardShow(e) {
        if (platform.OS === 'ios'){
            this.setState({keyboardHeight: e.endCoordinates.height}, () => {
                setTimeout(() => {
                    this.scrollContentHeight > this.scrollViewHeight
                        ? this.refs['scrollView'].scrollTo({
                            y: this.scrollContentHeight - this.scrollViewHeight,
                            x: 0
                        })
                        : void 0;
                }, 10);
            });
        }
    }

    onKeyboardHide() {
        this.refs['scrollView'].scrollTo({y: 0, x: 0});
        this.setState({keyboardHeight: 0});
    }

    handleBack() {
        JumpBack();
    }

    handleSure() {
        if (!((/^([1-9]\d|\d)$/).test(this.state.num))) {
            Toast.fail('请输入0-99之间的整数', 1.5);
        } else {
            let info = this.state.info;
            let num = this.state.num;
            return getUserInfo().then(userInfo => {
                let {token} = userInfo;
                fetch(`${apibase}/operation/collect-broken-bicycle`, {
                    method: 'POST',
                    body: {token, 'id': info.id, 'bicycle_num': info.number, 'actually_broken_num': num}
                }).then(respones => {
                    if (respones['code'] === 0) {
                        Toast.success('提交成功', 1.5);
                        JumpBack();
                    }
                    else {
                        Toast.fail(respones['message'], 1.5);
                        this.setState({num: ''});
                    }
                })
            });
        }
    }

    handleGoThere(){
        whereami().then(d => {
            let conf = {
                sourceApplication: 'ofo work',
                sid: 'BGVIS1',
                did: 'BGVIS2',
                dlat: parseFloat(this.state.info.latitude),
                dlon: parseFloat(this.state.info.longitude),
                t: 'car',
                dev: 0
            };
            if (!d.error) {
                /* no error */
                conf = Object.assign({}, conf, {
                    slat: parseFloat(d.latitude),
                    slon: parseFloat(d.longitude)
                });
            }
            externalApp('amap', 'path', conf);
        });
    }

    render() {
        if (this.state.loading) {
            return (
                <base.View className="wrap" style={styles.container}>
                    <ActionBar title="坏车处理" handleBack={this.handleBack.bind(this)}>
                        <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                    </ActionBar>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 44,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='loading'/>
                        <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            );
        } else {
            return this.renderDetail();
        }
    }

    renderDetail() {
        let info = this.state.info || {};
        let a = {};
        if (platform.OS !== 'web') {
            a = {
                onStartShouldSetResponder: () => this.keyboardShow,
                onResponderGrant: () => Keyboard.dismiss(),
            };
        }
        return (
            <base.View className="wrap" style={styles.container} {...a}>
                <ActionBar title="坏车处理" handleBack={this.handleBack.bind(this)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <base.View style={{flex: 1, position: 'relative'}}>
                    <ScrollView
                        ref="scrollView"
                        onContentSizeChange={(contentWidth, contentHeight) => this.scrollContentHeight = contentHeight}
                        onLayout={e => this.scrollViewHeight = e.nativeEvent.layout.height}
                        style={{position: 'absolute', top: 0, right: 0, bottom: this.state.keyboardHeight, left: 0}}
                        className="overflow-y">
                        <base.VerticalLayout style={{flex: 1}}>
                            <base.HorizontalLayout style={{justifyContent: 'center'}}>
                                <base.Image className="contain" resizeMode="contain" style={{height: 300, flex: 1}}
                                            src={info.imgPath ? info.imgPath : null}/>
                            </base.HorizontalLayout>

                            <base.HorizontalLayout style={styles.data}>
                                <base.HorizontalLayout style={{alignItems: 'center'}}>
                                    <base.Text style={styles.name}>{info.label ? info.label : null}</base.Text>
                                </base.HorizontalLayout>
                                <base.HorizontalLayout style={{alignItems: 'center'}}>
                                    <base.Text style={styles.num}>坏车数量</base.Text>
                                    <base.Text style={{...styles.num, color: '#333'}}>{info.number ? info.number : null}辆</base.Text>
                                </base.HorizontalLayout>
                            </base.HorizontalLayout>

                            <base.HorizontalLayout style={styles.data}>
                                <base.Text style={{fontSize: 18, color: '#666'}}>
                                    地址：{this.state.address ? this.state.address : null}</base.Text>
                            </base.HorizontalLayout>

                            <base.HorizontalLayout style={styles.data}>
                                <base.Text style={{fontSize: 18, color: '#666'}}>
                                    时间：{info.createTime ? info.createTime : null}</base.Text>
                            </base.HorizontalLayout>

                            <base.HorizontalLayout style={styles.inputBox}>
                                <base.Text style={{fontSize: 20, color: '#333', marginRight: 10}}>实收坏车数</base.Text>
                                <base.TextInput keyboardType="numeric" placeholder="输入数量"
                                                style={{fontSize: 16, height: 44, flexGrow: 1}}
                                                value={this.state.num}
                                                onChangeText={(text) => {
                                                    this.setState({num: text});
                                                }}/>
                            </base.HorizontalLayout>

                        </base.VerticalLayout>
                    </ScrollView>
                </base.View>
                <base.VerticalLayout style={{height: 70}}>
                    <base.HorizontalLayout style={styles.choose}>
                        <base.VerticalLayout>
                            <base.Button clickDuration={0} onPress={this.handleGoThere.bind(this)}
                                    style={{...styles.button, backgroundColor: '#dfdfdf'}}>到这里去</base.Button>
                        </base.VerticalLayout>
                        <base.VerticalLayout>
                            <base.Button clickDuration={0} onPress={this.handleSure.bind(this)}
                                    style={{...styles.button, backgroundColor: '#ffd900'}}>确定</base.Button>
                        </base.VerticalLayout>
                    </base.HorizontalLayout>
                </base.VerticalLayout>
            </base.View>
        );
    }

});
export default OperationBadCarDetail;

const styles = {
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff'
    },
    topImg: {
        height: 300,
        justifyContent: 'center'
    },
    data: {
        padding: 20,
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#d8d8d8',
    },
    name: {
        fontSize: 18,
        color: '#666',
        marginRight: 30,
    },
    num: {
        fontSize: 18,
        color: '#666',
        marginRight: 10,
    },
    inputBox: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    choose: {
        height: 74,
        borderTopColor: '#d8d8d8',
        borderStyle: 'solid',
        borderRightWidth: 0,
        borderLeftWidth: 0,
        borderBottomWidth: 0,
        borderTopWidth: 1,
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    button: {
        width: 150,
        height: 44,
        borderRadius: 6,
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 20,
        color: '#333',
    },
};



