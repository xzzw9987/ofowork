import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator, TextInput, HorizontalLayout } from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack, ResetTo } from '../../modules';
import * as actions from '../../runtime/action';
import Keyboard from '../../modules/keyboard';

const CodeReset = pageWrapper(class extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: false,
            carno: '',
            newCode: '',
            historyState: historyStack.get(),
        };
        this.from = this.state.historyState.from;
    }

    handleBack() {
        JumpBack();
    }

    renderLoading() {
        return (
            <View style={styles.container}>
                <ActionBar title={this.from === 'mech' ? '更换车牌' : '智能锁·更换车牌'}>
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>
                <View style={{ position: 'relative', flexGrow: 1 }}>
                    <View style={styles.inner}>
                        <ActivityIndicator size='large' src='loading' />
                        <HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></HorizontalLayout>
                    </View>
                </View>
            </View>
        );
    }

    renderPanel() {
        return (
            <View style={styles.container} onStartShouldSetResponder={() => true} onResponderGrant={() => Keyboard.dismiss()}>
                <ActionBar title={this.from === 'mech' ? '更换车牌' : '智能锁·更换车牌'} handleBack={() => this.handleBack()}>
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>

                <View style={{ flexGrow: 1, flexDirection: 'column' }}>
                    <HorizontalLayout style={{
                        ...styles.borderTop, ...styles.borderBottom,
                        paddingLeft: 20,
                        paddingRight: 20,
                        marginTop: 10,
                        alignItems: 'center',
                        backgroundColor: '#fff'
                    }}>
                        <Text style={{ fontSize: 18, marginRight: 10 }}>新车牌</Text>
                        <TextInput
                            onChangeText={(text) => {
                                this.setState({ carno: text });
                            }}
                            keyboardType="numeric"
                            placeholder="请输入车牌号"
                            style={{ fontSize: 14, flexGrow: 1, height: 64 }} />
                    </HorizontalLayout>
                    <HorizontalLayout style={{
                        ...styles.borderTop, ...styles.borderBottom,
                        paddingLeft: 20,
                        paddingRight: 20,
                        marginTop: 10,
                        alignItems: 'center',
                        backgroundColor: '#fff'
                    }}>
                        <Text style={{ fontSize: 18, marginRight: 10, color: '#fd7000' }}>{this.from === 'mech' ? '解锁码' : '锁SN码'}</Text>
                        <TextInput onChangeText={(text) => {
                            this.setState({ newCode: text });
                        }} placeholder={this.from === 'mech' ? '请输入4位解锁码' : '请输入12到14位锁SN码'} style={{ fontSize: 14, flexGrow: 1, height: 64, color: '#fd7000' }} />
                    </HorizontalLayout>

                    <View style={{ height: 150 }} />

                    <View>
                        <TouchableHighlight onPress={() => this.onSubmit()}
                            style={{ backgroundColor: '#2c2c2c', marginLeft: 20, marginRight: 20, flexGrow: 1 }}>
                            <HorizontalLayout style={{ height: 44, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#fff' }}>
                                    立即生效
                                </Text>
                            </HorizontalLayout>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        );
    }

    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderPanel();
    }

    async onSubmit() {
        Keyboard.dismiss();
        if (!this.valid(this.state.carno)) {
            Toast.fail('更换失败！车牌格式有误！', 1);
            return;
        }
        if (this.from === 'mech' && !this.validCode(this.state.newCode)) {
            Toast.fail('更换失败！密码格式有误！', 1);
            return;
        }
        if (this.from === 'smart' && !this.validSN(this.state.newCode)) {
            Toast.fail('更换失败！SN码格式有误！', 1);
            return;
        }
        this.setState({ loading: true });

        await this.props.fetch({ ...this.state, lockType: this.from === 'mech' ? '1' : '2' });
        this.setState({ loading: false, carno: '', newCode: '' });
        const { res } = this.props;
        if (res.code === 0) {
            Toast.success('更换成功', 1);
        } else {
            Toast.fail(res.message, 1);
        }
    }

    valid(text) {
        return /^\d{4,11}$/.test(text);
    }

    validCode(code) {
        return /^\d{4}$/.test(code);
    }

    validSN(code) {
        return /[\d][A-Z]{2}\d+/g.test(code);
    }
});

const mapStateToProps = (state, ownProps) => {
    return {
        res: state.api.changeBrandStatus,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch,
        fetch: props => dispatch(actions.opChangeBrand({ ...props }))
    };
};


const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    borderTop: {
        borderTopStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#ddd'
    },
    borderBottom: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    borderLeft: {
        borderLeftStyle: 'solid',
        borderLeftWidth: 1,
        borderLeftColor: '#ddd'
    },
    borderRight: {
        borderRightStyle: 'solid',
        borderRightWidth: 1,
        borderRightColor: '#ddd'
    },
    inner: {
        position: 'absolute',
        flexDirection: 'column',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
};

export default connect(mapStateToProps, mapDispatchToProps)(CodeReset);
