import React, {Component} from 'react';
import * as base from '../../base';
import {apibase, getUserInfo} from '../../runtime/api';
import {Text, Button, ScrollView, ActivityIndicator, Toast} from '../../base';
import {historyStack, JumpBack, fetch} from '../../modules';
import {ActionBar} from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import geoMessage from '../../base/map/geoMessage';
import Swiper, {Item} from '../../components/swiper';

/**
 * 路面信息详情
 */

const CompetitorInformationDetail = pageWrapper(class extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            id: historyStack.get().info,
            address: '',
            message: '',
        };
    }

    viewWillAppear() {
        let id = this.state.id;
        console.log(id);
        if(id){
            return getUserInfo().then(userInfo => {
                let { token } = userInfo;
                fetch(`${apibase}/competitor/record-detail`, {
                    method: 'POST',
                    body: { token, 'id': id }
                }).then(respones => {
                    this.setState({loading: false, message: respones.data});
                    geoMessage({latitude: respones.data.lat, longitude: respones.data.lng}).then(d => {
                        this.setState({address: d.address});
                    });
                });
            });
        }
    }

    handleBack() {
        JumpBack();
    }

    render() {
        if (this.state.loading) {
            return (
                <base.View className="wrap" style={styles.container}>
                    <ActionBar title="详情" handleBack={this.handleBack.bind(this)}>
                        <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                    </ActionBar>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 44,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='loading'/>
                        <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            );
        } else {
            return this.renderDetail();
        }
    }

    renderDetail() {
        let message = this.state.message || {};
        let ofoNum = (message.info[0].type == 1) ? message.info[0].num : message.info[1].num;
        let otherNum = (message.info[0].type == 2) ? message.info[0].num : message.info[1].num;
        return (
            <base.View className="wrap" style={styles.container}>
                <ActionBar title="详情" handleBack={this.handleBack.bind(this)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <ScrollView className="overflow-y">
                    <base.VerticalLayout style={{flex: 1}}>
                        <base.VerticalLayout style={{height: 300}}>
                            <Swiper>
                                {
                                    message.img_path.split(',').map((item, index)=> (
                                        <Item key={index} style={{height: 300, justifyContent: 'center'}}>
                                            <base.Image className="contain" resizeMode="contain" style={{height: 300}}
                                                        src={`${item}?x-oss-process=image/resize,h_320`} />
                                        </Item>
                                    ))
                                }
                            </Swiper>
                        </base.VerticalLayout>

                        <base.HorizontalLayout style={styles.data}>
                            <base.Text style={{fontSize: 18, color: '#666'}}>ofo小黄车：{ofoNum}辆</base.Text>
                        </base.HorizontalLayout>
                        <base.HorizontalLayout style={styles.data}>
                            <base.Text style={{fontSize: 18, color: '#666'}}>橙色单车：{otherNum}辆</base.Text>
                        </base.HorizontalLayout>
                        <base.HorizontalLayout style={styles.data}>
                            <base.Text style={{fontSize: 18, color: '#666', flexWrap: 'wrap'}}>地址：{this.state.address}</base.Text>
                        </base.HorizontalLayout>

                        <base.HorizontalLayout style={{height: 20, backgroundColor: '#f8f8f8'}} />

                        <base.HorizontalLayout style={{...styles.data,borderTopWidth: 1,}}>
                            <base.Text style={{fontSize: 18, color: '#666'}}>上报人：{message.user_name}</base.Text>
                        </base.HorizontalLayout>
                        <base.HorizontalLayout style={styles.data}>
                            <base.Text style={{fontSize: 18, color: '#666'}}>上报时间：{message.time}</base.Text>
                        </base.HorizontalLayout>
                        {/*<base.HorizontalLayout style={styles.data}>
                            <base.Text style={{fontSize: 18, color: '#666', flexWrap: 'wrap'}}>上报位置：我是地址我是地址我是地址我是地址我是地址我是地址</base.Text>
                        </base.HorizontalLayout>*/}
                    </base.VerticalLayout>
                </ScrollView>
            </base.View>
        );
    }

});
export default CompetitorInformationDetail;

const styles = {
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff'
    },
    topImg: {
        height: 300,
        justifyContent: 'center'
    },
    data: {
        padding: 20,
        borderStyle: 'solid',
        borderBottomWidth: 1,
        borderColor: '#d8d8d8',
    },
    name: {
        fontSize: 18,
        color: '#666',
    },
    num: {
        fontSize: 18,
        color: '#666',
        marginRight: 5,
    },
};



