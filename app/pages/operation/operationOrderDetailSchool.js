import {connect} from 'react-redux';
import OrderList from './operationOrderDetailCommon';
import {JumpTo, JumpBack, historyStack} from '../../modules';
import {getUserInfo} from '../../runtime/api';
import {
    fetchSchoolDetail
} from '../../runtime/action';

const mapStateToProps = (state)=> {
        return {
            schoolId: historyStack.get().schoolId,
            pageTitle: '今日订单量排行',
            pane: [{text: '城市'}, {text: '校园'}],
            dataSource: [
                {
                    list: state.api.schoolCityList || []
                },
                {
                    list: beautifyList(state.api.schoolZoneList || [])
                }
            ]
        };
    },
    mapDispatchToProps = dispatch=> {
        return {
            dispatch,
            fetch: props=> dispatch(fetchSchoolDetail(props))
        };
    };

export default connect(mapStateToProps, mapDispatchToProps)(OrderList);
function beautifyList(list) {
    if (!list)
        return [];

    return list.map(v=> {
        return {
            ...v,
            onPress(){
                /* set history */
                getUserInfo().then(userInfo=> {
                    let opt = {id: v.id, title: v.title}, historyState = historyStack.get();
                    if (historyState && historyState.shouldRaiseLevel) {
                        if (!userInfo['__origin_operation_level']) {
                            userInfo['__origin_operation_level'] = userInfo['operation_level'];
                            userInfo['operation_level'] = 'country';
                            opt.shouldLowerLevel = true;
                        }
                    }

                    JumpTo('operation/dashBoard/school', true, opt);
                });
            }
        };
    });
}
