import React, { Component } from 'react';
import * as base from '../../base';
import {
    api,
    keys,
    loadToken,
    parseNum,
    _loadToken,
    numberSplit
} from '../../runtime/api';
import {
    Text,
    View,
    Br,
    Button,
    Tabs,
    TabPane,
    ScrollView,
    Toast,
    TouchableHighlight,
    ActivityIndicator
} from '../../base';
import { connect } from 'react-redux';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';

import {
    VerticalBarChart,
    HorizontalBarChart,
    LineChart
} from '../../components/chart';
import Swiper, { Item } from '../../components/swiper';
import pageWrapper from '../../modules/pageWrapper';
import timeoutQueue from '../../modules/timeoutQueue';

/**
 * 运营数据(全国)
 */
class DashBoard extends Component {
    static propTypes = {
        schoolNames: React.PropTypes.array,
        schoolId: React.PropTypes.any,
        pageTitle: React.PropTypes.string,
        titleOfTotal: React.PropTypes.string,
        total: React.PropTypes.any,
        delta: React.PropTypes.number,
        deltaAmount: React.PropTypes.number,
        chart: React.PropTypes.array,
        defaultSelectedChart: React.PropTypes.arrayOf(React.PropTypes.number),
        left: React.PropTypes.shape({
            text: React.PropTypes.string,
            onPress: React.PropTypes.func
        }),
        right: React.PropTypes.shape({
            text: React.PropTypes.string,
            onPress: React.PropTypes.func
        }),
        fetch: React.PropTypes.func,
        backButton: React.PropTypes.element,
        handleBack: React.PropTypes.func
    };

    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            defaultSelectedChart: [],
            historyState: historyStack.get()
        };
    }

    componentWillMount() { }

    execInSerial(cb) {
        let busy = false;
        return (...args) => {
            if (busy) return;
            busy = true;
            const always = d => {
                busy = false;
                return d;
            };
            return cb(...args).then(always).catch(always);
        };
    }

    viewWillAppear() {
        const fetch = this.execInSerial(() => this.props.fetch(this.props));
        fetch().then(() => this.state.loading && this.setState({ loading: false }));
        /* fetch data every 5000ms*/
        if (this.props.autoRefresh !== false) {
            this.unsubscribe = timeoutQueue.subscribe(fetch);
        }
    }

    viewWillDisappear() {
        this.unsubscribe && this.unsubscribe();
    }

    componentDidMount() { }

    handleBack() {
        !this.state.historyState ||
            (this.state.historyState && this.state.historyState.isFirstPage)
            ? JumpTo('profile/index', true)
            : (() => {
                this.props.handleBack && this.props.handleBack(this.props);
                JumpBack();
            })();
    }

    //切换学校详情
    handleSchoolDetail = schoolId => {
        console.log('schoolIdschoolId', schoolId);
        this.props.setSchoolID &&
            this.props.setSchoolID({ type: 'setSchoolId', data: schoolId });
    };

    render() {
        if (this.state.loading) return this.renderLoading();
        return this.renderOperationPage();
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar
                    title={this.props.pageTitle}
                    handleBack={() => this.handleBack()}
                    rightItem={
                        this.props.actionRightBar ? this.props.actionRightBar.text : '刷新'
                    }
                    rightAction={() => {
                        this.props.actionRightBar
                            ? this.props.actionRightBar.onPress()
                            : this.props.fetch(this.props);
                    }}
                >
                    {!this.state.historyState ||
                        (this.state.historyState && this.state.historyState.isFirstPage)
                        ? <base.Image style={{ width: 17, height: 17 }} src="setting" />
                        : <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>}
                </ActionBar>
                <base.View style={{ position: 'relative', flexGrow: 1 }}>
                    <base.View
                        style={{
                            position: 'absolute',
                            flexDirection: 'column',
                            left: 0,
                            top: 0,
                            right: 0,
                            bottom: 0,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <ActivityIndicator size="large" src="loading" />
                        <base.HorizontalLayout style={{ marginTop: 10 }}>
                            <Text>正在加载 ...</Text>
                        </base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }

    renderOperationPage() {
        return (
            <base.View style={styles.container}>
                <ActionBar
                    title={this.props.pageTitle}
                    handleBack={() => this.handleBack()}
                    rightItem={
                        this.props.actionRightBar ? this.props.actionRightBar.text : '刷新'
                    }
                    rightAction={() => {
                        this.props.actionRightBar
                            ? this.props.actionRightBar.onPress()
                            : this.props.fetch(this.props);
                    }}
                >
                    {!this.state.historyState ||
                        (this.state.historyState && this.state.historyState.isFirstPage)
                        ? <base.Image style={{ width: 17, height: 17 }} src="setting" />
                        : <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>}
                </ActionBar>
                {this.props.schoolNames
                    ? <base.HorizontalLayout style={styles.nav}>
                        <ScrollView
                            bounces={false}
                            showsHorizontalScrollIndicator={false}
                            horizontal={true}
                            style={{ height: 45 }}
                        >
                            <base.HorizontalLayout className="overflowX hiddenY">
                                {this.props.schoolNames.map((item, idx) => {
                                    return (
                                        <base.HorizontalLayout key={item.id} style={styles.inner}>
                                            <Button
                                                clickDuration={0}
                                                style={
                                                    this.props.schoolId == item.id
                                                        ? Object.assign({}, styles.itemBtn, styles.light)
                                                        : styles.itemBtn
                                                }
                                                key={item.id}
                                                onPress={() => {
                                                    this.handleSchoolDetail(item.id);
                                                }}
                                            >
                                                <base.Text
                                                    style={
                                                        this.props.schoolId === item.id
                                                            ? styles.active
                                                            : styles.color2
                                                    }
                                                >
                                                    {item.name}
                                                </base.Text>
                                            </Button>
                                        </base.HorizontalLayout>
                                    );
                                })}
                            </base.HorizontalLayout>
                        </ScrollView>
                    </base.HorizontalLayout>
                    : null}
                <base.VerticalLayout style={styles.top}>
                    {this.renderOrder()}
                </base.VerticalLayout>
                <base.VerticalLayout>
                    {this.renderButton()}
                </base.VerticalLayout>
                <base.VerticalLayout style={{ flexGrow: 1 }}>
                    {this.renderChart()}
                </base.VerticalLayout>
            </base.View>
        );
    }

    renderOrder() {
        return (
            <base.VerticalLayout
                style={{
                    ...styles.top,
                    minHeight: 130,
                    backgroundColor: '#fff',
                    paddingTop: 20
                }}
            >
                <base.HorizontalLayout style={{ justifyContent: 'center' }}>
                    <Text style={{ paddingBottom: 2, fontSize: 14, color: '#000' }}>
                        {this.props.titleOfTotal}
                    </Text>
                </base.HorizontalLayout>
                <base.HorizontalLayout
                    style={{
                        height: 42,
                        marginTop: 6,
                        marginBottom: 6,
                        alignItems: 'flex-end'
                    }}
                >
                    <Text
                        style={{
                            fontSize: 42,
                            color: numColor(this.props.deltaAmount)
                        }}
                    >
                        {numberSplit(this.props.total)}
                    </Text>
                </base.HorizontalLayout>
                <base.HorizontalLayout style={{ height: 26, paddingTop: 5 }}>
                    {this.props.deltaAmount === undefined
                        ? null
                        : <base.HorizontalLayout style={{ alignItems: 'center' }}>
                            <Text
                            >{`比昨天此时${this.props.deltaAmount > 0 ? '多' : '少'}`}</Text>
                            <Text
                                style={{
                                    marginLeft: 6,
                                    fontSize: 20,
                                    color: numColor(this.props.deltaAmount)
                                }}
                            >{`${Math.abs(this.props.delta)}%`}</Text>
                            <Text
                                style={{
                                    marginLeft: 6,
                                    fontSize: 20,
                                    color: numColor(this.props.deltaAmount)
                                }}
                            >{`(${this.props.yesterdayOrder})`}</Text>
                        </base.HorizontalLayout>}
                </base.HorizontalLayout>

                <base.HorizontalLayout style={{ height: 26, paddingTop: 5 }}>
                    {this.props.weekAmount === undefined
                        ? null
                        : <base.HorizontalLayout style={{ alignItems: 'center' }}>
                            <Text>{`比上周此时${this.props.weekAmount > 0 ? '多' : '少'}`}</Text>
                            <Text
                                style={{
                                    marginLeft: 6,
                                    fontSize: 20,
                                    color: numColor(this.props.weekAmount)
                                }}
                            >{`${Math.abs(this.props.weekDelta)}%`}</Text>
                            <Text
                                style={{
                                    marginLeft: 6,
                                    fontSize: 20,
                                    color: numColor(this.props.weekAmount)
                                }}
                            >{`(${this.props.lastWeekOrder})`}</Text>
                        </base.HorizontalLayout>}
                </base.HorizontalLayout>
            </base.VerticalLayout>
        );
    }

    renderButton() {
        return (
            <base.HorizontalLayout
                style={{
                    borderTopWidth: 1,
                    borderBottomWidth: 1,
                    borderTopColor: '#ddd',
                    borderBottomColor: '#ddd',
                    borderTopStyle: 'solid',
                    borderBottomStyle: 'solid'
                }}
            >
                {!this.props.left
                    ? null
                    : <base.TouchableHighlight
                        onPress={() =>
                            this.props.left.onPress && this.props.left.onPress(this.props)}
                        style={{ flexGrow: 1 }}
                    >
                        <base.HorizontalLayout
                            style={{
                                backgroundColor: '#fff',
                                alignItems: 'center',
                                justifyContent: 'center',
                                height: 44
                            }}
                        >
                            <base.Image style={{ width: 17, height: 22 }} src="flag" />
                            <Text style={{ marginLeft: 6 }}>{this.props.left.text}</Text>
                        </base.HorizontalLayout>
                    </base.TouchableHighlight>}

                {this.props.left && this.props.right
                    ? <base.View style={{ width: 1, backgroundColor: '#ddd' }} />
                    : null}

                {!this.props.right
                    ? null
                    : <base.TouchableHighlight
                        onPress={() =>
                            this.props.right.onPress &&
                            this.props.right.onPress(this.props)}
                        style={{ flexGrow: 1 }}
                    >
                        <base.HorizontalLayout
                            style={{
                                backgroundColor: '#fff',
                                alignItems: 'center',
                                justifyContent: 'center',
                                height: 44
                            }}
                        >
                            <base.Image
                                style={{ width: 22, height: 22 }}
                                src="lookhistory"
                            />
                            <Text style={{ marginLeft: 6 }}>{this.props.right.text}</Text>
                        </base.HorizontalLayout>
                    </base.TouchableHighlight>}

            </base.HorizontalLayout>
        );
    }

    renderChartItem(chart, ChartType, dataIndex, onPress, markPoint) {
        let extraData = dataIndex === undefined ? {} : chart.extraData[dataIndex];
        return (
            <Item>
                <base.View>
                    <base.VerticalLayout
                        style={{ paddingLeft: 15, paddingTop: 20, height: 88 }}
                    >
                        <base.HorizontalLayout>
                            <Text
                                style={{
                                    ...styles.fs12,
                                    color: '#fff',
                                    paddingLeft: 6,
                                    paddingRight: 6,
                                    paddingTop: 3,
                                    paddingBottom: 3,
                                    marginBottom: 3,
                                    marginRight: 10,
                                    backgroundColor: base.Color.orange
                                }}
                            >
                                {makeTrendTitle(chart.type)}
                            </Text>
                        </base.HorizontalLayout>
                        <base.HorizontalLayout style={{ alignItems: 'center' }}>
                            <Text style={{ ...styles.fs14 }}>{extraData.d1 || ''}</Text>
                        </base.HorizontalLayout>
                        <base.HorizontalLayout>
                            <base.View>{makeChartTipElement(extraData.d2 || '')}</base.View>
                            <base.View style={{ marginLeft: 20 }}>
                                {makeChartTipElement(extraData.d3 || '')}
                            </base.View>
                        </base.HorizontalLayout>
                    </base.VerticalLayout>
                </base.View>
                <base.VerticalLayout style={{ flexGrow: 1 }}>
                    {this.props.chart.length
                        ? <ChartType
                            defaultDataIndex={dataIndex}
                            onPress={idx => onPress && onPress(idx)}
                            style={{ flexGrow: 1 }}
                            xAxis={chart.x}
                            data={chart.y}
                            markPoint={markPoint}
                        />
                        : null}
                </base.VerticalLayout>
                <base.View style={{ height: 34 }} />
            </Item>
        );
    }

    renderChart() {
        return (
            <base.VerticalLayout
                style={{
                    ...styles.borderTop,
                    flexGrow: 1,
                    backgroundColor: '#fff',
                    marginTop: 10
                }}
            >
                <Swiper>
                    {this.props.chart.map((chart, index) => {
                        let ChartType, element, markPoint = false;
                        switch (chart.type) {
                            case 'verticalChart':
                                ChartType = VerticalBarChart;
                                markPoint = true;
                                break;
                            case 'horizontalChart':
                                ChartType = HorizontalBarChart;
                                break;
                            case 'lineChart':
                                ChartType = LineChart;
                                break;
                            default:
                                break;
                        }
                        let dataIndex = this.state.defaultSelectedChart[index] === undefined
                            ? this.props.defaultSelectedChart[index]
                            : this.state.defaultSelectedChart[index];

                        function numberify(val) {
                            if (val instanceof Array) {
                                return val.map(v => numberify(v));
                            }
                            return parseInt(val, 10);
                        }

                        chart.y = numberify(chart.y);

                        element = this.renderChartItem(
                            chart,
                            ChartType,
              /* selected dataIndex here */ dataIndex,
              /* onPress */ dataIndex => {
                                let { defaultSelectedChart } = this.state;
                                defaultSelectedChart[index] = dataIndex;
                                this.setState({
                                    defaultSelectedChart
                                });
                            },
                            /* need markPoint ?*/
                            markPoint
                        );
                        return React.cloneElement(element, { key: index });
                    })}
                </Swiper>
            </base.VerticalLayout>
        );
    }
}

const styles = {
    fs12: {
        fontSize: 12
    },
    fs14: {
        fontSize: 14
    },
    fs16: {
        fontSize: 16
    },
    fs20: {
        fontSize: 20
    },
    borderTop: {
        borderTopStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#ddd'
    },
    borderBottom: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    borderLeft: {
        borderLeftStyle: 'solid',
        borderLeftWidth: 1,
        borderLeftColor: '#ddd'
    },
    borderRight: {
        borderRightStyle: 'solid',
        borderRightWidth: 1,
        borderRightColor: '#ddd'
    },
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    },
    cbox: {
        justifyContent: 'center'
    },
    color2: {
        color: '#7f7f7f'
    },
    p10: {
        paddingRight: 10
    },
    list: {
        flexGrow: 1
    },
    listBody: {
        flexGrow: 1,
        borderStyle: 'solid',
        borderTopColor: '#c6c6c6',
        borderTopWidth: 1,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0
    },
    listView: {
        flexGrow: 1
    },
    top: {
        backgroundColor: '#fff',
        alignItems: 'center'
    },
    mingci: {
        paddingLeft: 6,
        paddingRight: 6,
        height: 17,
        backgroundColor: '#696969',
        borderRadius: 2,
        alignItems: 'center',
        marginLeft: 10
    },
    btnBox: {
        marginTop: 10,
        alignItems: 'center'
    },
    num: {
        color: '#ff8300',
        fontSize: 26
    },
    mingText: {
        fontSize: 12,
        color: '#fff'
    },
    detailBtn: {
        width: 144,
        height: 35,
        backgroundColor: '#ffd900',
        borderRadius: 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    /*item */
    items: {
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        height: 60,
        justifyContent: 'center',
        position: 'relative',
        backgroundColor: '#fff',
        paddingRight: 10,
        paddingLeft: 10,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0
    },
    item: {
        height: 60,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    arrow: {
        width: 8,
        height: 14
    },
    flexBox: {
        flexGrow: 1,
        alignItems: 'center'
    },
    flexBoxCenter: {
        flexGrow: 1,
        justifyContent: 'center'
    },
    flexBoxRight: {
        flexGrow: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    mb10: {
        marginBottom: 5,
        alignItems: 'center'
    },
    /*学校nav */
    nav: {
        height: 45,
        backgroundColor: '#fff',
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 1
    },
    inner: {
        height: 45,
        alignItems: 'center',
        flexShrink: 0
    },
    itemBtn: {
        height: 46,
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 0,
        marginRight: 10,
        marginLeft: 10,
        borderStyle: 'solid',
        borderBottomColor: 'transparent',
        borderBottomWidth: 4,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0
    },
    light: {
        borderBottomColor: '#ffd900'
    },
    active: {
        color: '#000',
        fontWeight: 'bold'
    }
};

function now() {
    const now = new Date(),
        hour = now.getHours() < 10 ? '0' + now.getHours() : now.getHours(),
        minute = now.getMinutes() < 10 ? '0' + now.getMinutes() : now.getMinutes();
    return `${hour}:${minute}`;
}

function upOrDown(val) {
    val = parseFloat(val);
    if (val >= 0) {
        return (
            <Text
                style={{
                    marginLeft: 10,
                    marginBottom: 8,
                    fontSize: 20,
                    color: base.Color.orange
                }}
            >
                +
      </Text>
        );
    }
    return (
        <Text
            style={{
                marginLeft: 10,
                marginBottom: 8,
                fontSize: 20,
                color: base.Color.green
            }}
        >
            -
    </Text>
    );
    /*if (val >= 0)
       return (<base.Image style={{width: 10, height: 10, marginLeft: 10, marginBottom: 14}} src="images/up.png"/>);
       return (<base.Image style={{width: 10, height: 10, marginLeft: 10, marginBottom: 14}} src="images/down.png"/>);*/
}

function numColor(val) {
    val = parseFloat(val);
    if (val >= 0) return base.Color.orange;
    return base.Color.green;
}

function makeChartTipElement(text) {
    return <Text style={{ ...styles.fs14, color: '#666' }}>{text}</Text>;
}

function makeTrendTitle(t) {
    switch (t) {
        case 'verticalChart':
            return '日趋势';
        case 'horizontalChart':
            return '周趋势';
        case 'lineChart':
            return '时段趋势';
    }
    return '';
}

export default pageWrapper(DashBoard);
