import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, parseNum, apibase, showWeekCN, getUserInfo } from '../../runtime/api';
import { Text, View, Br, Button, ScrollView, TouchableHighlight, ActivityIndicator } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack, historyStack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import {fetch} from '../../modules';

var moment = require('moment');

/**
 * 订单历史
 */
class OperationOrderHistory extends Component {

    constructor(...args){
        super(...args);
        this.state = {
            loading: true
        };
    }
    componentWillMount() {
        let end_time = moment().format('YYYY-MM-DD');
        let start_time = moment().add(-60, 'days').format('YYYY-MM-DD');
        let id = historyStack.get().id,
            level = historyStack.get().level;
        return getUserInfo()
            .then(userInfo=> {
                let {token} = userInfo;
                fetch(`${apibase}/order/get-zone-history-order`, {
                    method: 'POST',
                    body: {token, 'zone_id': id, 'level': level, 'start_time': start_time, 'end_time': end_time}
                }).then(respones=>{
                    this.setState({historyList: respones['data']['list'], });
                });
            });

    }

    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }

    render(){


        if(!this.state.historyList){
            return (
                <base.View style={styles.container}>
                    <ActionBar title="历史订单量" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                    <base.View style={{position: 'relative', flexGrow: 1}}>
                        <base.View style={{
                            position: 'absolute',
                            flexDirection: 'column',
                            left: 0,
                            top: 0,
                            right: 0,
                            bottom: 0,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            <ActivityIndicator size='large' src='loading'/>
                            <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                        </base.View>
                    </base.View>
                </base.View>
            );
        }
        let zoneOrderHistor = this.state.historyList || [];
        let maxNum = 0;
        let historyOrders = zoneOrderHistor.map((item) => { return item.num; });
        if (historyOrders) {
            maxNum = Math.max(...(historyOrders));
        }
        return (
            <base.View style={styles.container}>
                <ActionBar title="历史订单量" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.VerticalLayout style={styles.list}>

                    <base.VerticalLayout style={{flexShrink: 0}}>
                            <base.HorizontalLayout  style={styles.items}>
                            <base.HorizontalLayout  className="te" style={styles.list}>
                                <Text className="te">{historyStack.get().name}</Text>
                            </base.HorizontalLayout>
                            <base.VerticalLayout style={styles.width}>
                                <base.HorizontalLayout style={styles.center}>
                                    <base.Image style={styles.icon} src='hot'></base.Image>
                                    <Text>{parseNum(maxNum)}</Text>
                                </base.HorizontalLayout>
                                <Text style={styles.smallText}>近60日订单峰值</Text>
                            </base.VerticalLayout>
                        </base.HorizontalLayout>
                    <Br/>
                    </base.VerticalLayout>
                    <base.VerticalLayout style={styles.scrollBorder}></base.VerticalLayout>
                    <ScrollView className="overflowY" style={styles.listView}>

                        {zoneOrderHistor.map(((item, idx) => {
                            return (
                                <TouchableHighlight key={idx}  >
                                    <base.HorizontalLayout style={styles.items}>
                                        <base.HorizontalLayout style={styles.one}>
                                            <Text>{item.date}</Text>
                                        </base.HorizontalLayout>
                                        <base.HorizontalLayout style={styles.two}>
                                            <Text>{showWeekCN(moment(item.date).format('dddd'))}</Text>
                                        </base.HorizontalLayout>

                                            {item.num == maxNum ?
                                            <base.VerticalLayout style={styles.right}>
                                                <base.HorizontalLayout style={styles.center}>
                                                    <base.Image style={styles.icon} src='hot'></base.Image>
                                                    <Text>{parseNum(item.num)}</Text>
                                                </base.HorizontalLayout>
                                                <Text style={styles.smallText}>近60日订单峰值</Text>
                                            </base.VerticalLayout>
                                                : <base.VerticalLayout style={styles.three}>
                                                <Text>{parseNum(item.num)}</Text>
                                                </base.VerticalLayout> }

                                    </base.HorizontalLayout>
                                </TouchableHighlight>
                            );
                        }))}

                    </ScrollView>

                </base.VerticalLayout>
            </base.View>
        );
    }


}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
    /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    color2: {
        color: '#7f7f7f'
    },
    smallText: {
        fontSize: 14,
        color: '#7f7f7f'
    },
    center: {
        alignItems: 'center',
    },
  /*item */
    items: {
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        height: 60,
        position: 'relative',
        backgroundColor: '#fff',
        paddingRight: 10,
        paddingLeft: 10,
        justifyContent: 'space-between',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        alignItems: 'center',
    },
    itemText: {
        height: 60,
        alignItems: 'center',
    },
    list: {
        flexGrow: 1
    },
    listView: {
        flexGrow: 1,
        flexBasis: 0
    },
    scrollBorder: {
        borderStyle: 'solid',
        borderTopColor: '#c6c6c6',
        borderTopWidth: 1,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    item: {
        height: 60,
        alignItems: 'center',
    },
    right: {
        alignItems: 'flex-end',
        flexGrow: 1,
        flexBasis: 0
    },
    width: {
        alignItems: 'flex-end',
    },
    icon: {
        width: 11,
        height: 15,
        marginRight: 3
    },
    one: {
        flexGrow: 1,
        flexBasis: 0
    },
    two: {
        flexGrow: 1,
        justifyContent: 'center',
        flexBasis: 0
    },
    three: {
        flexGrow: 1,
        alignItems: 'flex-end',
        flexBasis: 0
    }
};
export default OperationOrderHistory;
