import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, _loadToken, apibase } from '../../runtime/api';
import { Text, Button, Toast, ActivityIndicator } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack, historyStack, fetch } from '../../modules';
import pageWrapper from '../../modules/pageWrapper';
import { ActionBar } from '../../components/actionbar';
import RefreshControl from '../../components/refreshControl';
import ListView from '../../components/ListView';
var moment = require('moment');
import { fetchBadCarMessage } from '../../runtime/action';

/**
 * 坏车标记详情
 */
class OperationBadCarMessage extends Component {

    constructor() {
        super();
        this.state = {
            loading: true,
            type: 'record',
            item: historyStack.get().item,
            pageCount: 0,
        };
    }

    viewWillAppear() {
        this.props.fetch({'type': this.state.type, 'date': this.state.item.date}).then(() => {
            this.setState({loading: false});
        });
    }

    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }

    /**
     * tab切换
     */
    handleSwitchZone(type) {
        this.props.clear();
        this.setState({ loading: true, type: type, pageCount: 0 });
        if (this.state.type !== type) {
            this.props.fetch({'type': type, 'date': this.state.item.date}).then(() => {
                this.setState({ loading: false});
                setTimeout(() => this.refs['listView'] && this.refs['listView'].scrollTo({ x: 0, y: 0, animated: false }), 50);
            });
        }
    }

    /**
     * 下拉刷新
     */
    makeRefreshControl() {
        return (<RefreshControl
            refreshing={this.state.isRefreshing}
            onRefresh={() => {
                this.setState({ isRefreshing: true });
                this.props.fetch({'type': this.state.type, 'date': this.state.item.date})
                    .then(() => {this.setState({isRefreshing: false, pageCount: 0 }); Toast.success('刷新成功', 1); });
            }}
        />);
    }

    /**
     * 上拉加载
     */
    makePullLoading() {
        if (this.state.pageCount > 8) return;
        this.setState({ pageCount: ++this.state.pageCount });
        this.props.fetch({'type': this.state.type, 'date': this.state.item.date, concat: true, 'page': this.state.pageCount}).then(() => {
            this.setState({isRefreshing: false});
        });
    }

    render() {
        let message = this.props.badCarMessage;
        if (this.state.loading)
            return this.renderLoading();
        return this.renderPageList(message);
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title={this.state.item.date} style={{ borderBottomColor: '#7d7d7d' }} handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.VerticalLayout style={styles.listView}>
                    <base.HorizontalLayout style={styles.nav}>
                        <base.VerticalLayout style={styles.inner}>
                            <Button clickDuration={0} style={(this.state.type === 'record') ? styles.item1 : styles.item2} onPress={() => { this.handleSwitchZone('record'); }}>
                                <base.Text style={(this.state.type === 'record') ? styles.text1 : styles.text2}>标记({this.state.item.record}辆)</base.Text>
                            </Button>
                        </base.VerticalLayout>
                        <base.VerticalLayout style={styles.inner}>
                            <Button clickDuration={0} style={(this.state.type === 'process') ? styles.item1 : styles.item2} onPress={() => { this.handleSwitchZone('process'); }}>
                                <base.Text style={(this.state.type === 'process') ? styles.text1 : styles.text2}>处理({this.state.item.process}辆)</base.Text>
                            </Button>
                        </base.VerticalLayout>
                    </base.HorizontalLayout>
                </base.VerticalLayout>
                <base.View style={{ position: 'absolute', flexDirection: 'column', left: 0, top: 44, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator size='large' src='loading' />
                    <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                </base.View>
            </base.View>
        );
    }

    renderPageList(message) {
        return (
            <base.View style={styles.container}>
                <ActionBar title={this.state.item.date} style={{ borderBottomColor: '#7d7d7d' }} handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.VerticalLayout style={styles.listView}>
                    <base.HorizontalLayout style={styles.nav}>
                        <base.VerticalLayout style={styles.inner}>
                            <Button clickDuration={0} style={(this.state.type === 'record') ? styles.item1 : styles.item2} onPress={() => { this.handleSwitchZone('record'); }}>
                                <base.Text style={(this.state.type === 'record') ? styles.text1 : styles.text2}>标记({this.state.item.record}辆)</base.Text>
                            </Button>
                        </base.VerticalLayout>
                        <base.VerticalLayout style={styles.inner}>
                            <Button clickDuration={0} style={(this.state.type === 'process') ? styles.item1 : styles.item2} onPress={() => { this.handleSwitchZone('process'); }}>
                                <base.Text style={(this.state.type === 'process') ? styles.text1 : styles.text2}>处理({this.state.item.process}辆)</base.Text>
                            </Button>
                        </base.VerticalLayout>
                    </base.HorizontalLayout>
                    {message.length > 0 ?
                        <ListView ref="listView" onEndReached={() => { this.makePullLoading(); }}
                                  dataSource={message}
                                  renderRow={(item, idx) => {
                                      return (
                                          <base.HorizontalLayout key={idx} style={styles.list}>
                                              <base.Text style={{flex: 1, textAlign: 'center'}}>{item.name}</base.Text>
                                              <base.HorizontalLayout style={{flex: 1,justifyContent: 'center'}}>
                                                <base.Text>{this.state.type == 'record' ? '标记:' + item.record + '辆' : '处理:' + item.process + '辆'}</base.Text>
                                              </base.HorizontalLayout>
                                          </base.HorizontalLayout>
                                      );
                                  }}
                                  refreshControl={this.makeRefreshControl()} className="overflowY" style={styles.listView} />
                        :
                        <base.View style={{ position: 'absolute', flexDirection: 'column', left: 0, top: 88, right: 0, bottom: 44, alignItems: 'center', justifyContent: 'center' }}>
                            <base.Image style={{ height: 60, width: 60, }} src='noData' />
                            <base.HorizontalLayout style={{ marginTop: 10 }}><Text>暂无数据</Text></base.HorizontalLayout>
                        </base.View>
                    }
                </base.VerticalLayout>

            </base.View>
        );
    }

}

const mapStateToProps = state => {
    return {
        badCarMessage: state.api.badCarMessage,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        dispatch,
        fetch: props => dispatch(fetchBadCarMessage(props)),
        clear: () => {
            dispatch({
                type: '@@clear', payload: {
                    badCarMessage: [],
                }
            });
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(OperationBadCarMessage));

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    nav: {
        backgroundColor: '#fff',
        borderStyle: 'solid',
        borderBottomColor: '#7d7d7d',
        borderBottomWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
    },
    inner: {
        alignItems: 'center',
        justifyContent: 'center',
        flexGrow: 1,
        height: 44,
    },
    item1: {
        paddingLeft: 20,
        paddingRight: 20,
        borderBottomColor: '#FAD500',
        borderBottomWidth: 4,
        borderStyle: 'solid',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        height: 40,
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    item2: {
        height: 44,
        paddingBottom: 4,
        flexGrow: 1,
        paddingLeft: 20,
        paddingRight: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text1: {
        alignSelf: 'center',
        flexGrow: 1,

    },
    text2: {
        alignSelf: 'center',
        flexGrow: 1,
        color: '#999',
    },
    listView: {
        flexGrow: 1,
        flexBasis: 0,
        backgroundColor: '#fff',
    },
    list: {
        borderBottomColor: '#d6d6d6',
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        height: 54,
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    name: {
        fontSize: 14,
        marginLeft: 5,
    },
};
