import React, {Component} from 'react';
import * as base from '../../base';
import {Text, View, Toast, ActivityIndicator, TouchableHighlight, ScrollView} from '../../base';
import {connect} from 'react-redux';
import {JumpTo, JumpBack} from '../../modules';
import {ActionBar} from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import platform from '../../modules/platform';
import ImagePicker from '../../components/gallery';
import Keyboard from '../../modules/keyboard';
import { fetchCompetitorInformation } from '../../runtime/action';

/**
 * 路面信息采集
 */

const CompetitorInformation = pageWrapper(class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ofoNum: '',
            otherNum: '',
            imgPath: [],
            loading: false,
            keyboardHeight: 0,
        };
        this.scrollViewHeight = 0;
        this.scrollContentHeight = 0;
    }

    // 返回
    handleBack() {
        this.props.clear && this.props.clear();
        JumpBack();
    }

    onKeyboardShow(e) {
        if (platform.OS === 'ios'){
            this.setState({keyboardHeight: e.endCoordinates.height}, () => {
                setTimeout(() => {
                    this.scrollContentHeight > this.scrollViewHeight
                        ? this.refs['scrollView'].scrollTo({
                            y: this.scrollContentHeight - this.scrollViewHeight,
                            x: 0
                        })
                        : void 0;
                }, 10);
            });
        }
    }

    onKeyboardHide() {
        this.refs['scrollView'].scrollTo({y: 0, x: 0});
        this.setState({keyboardHeight: 0});
    }

    repairBadCarMarkPos() {
        Keyboard.dismiss();
        JumpTo('repairer/repairBadCarMarkPos', true);
    }

    submitInfo() {
        Keyboard.dismiss();
        if (this.state.imgPath.length == 0) {
            Toast.fail('请上传照片', 2);
            return;
        }
        let regu = /^(0|([1-9]\d{0,2}))$/;
        if (!regu.test(this.state.ofoNum) || !regu.test(this.state.otherNum)) {
            Toast.fail('车辆数应介于0-999之间', 2);
            return;
        }
        if(!this.props.subBadCarMarkPos.address){
            Toast.fail('请选择地址', 2);
            return;
        }
        this.setState({loading: true});
        this.props.fetch({'img': this.state.imgPath}).then(() => {
            let collectCompetitorImg = this.props.collectCompetitorImg;
            if(collectCompetitorImg && collectCompetitorImg.length > 0){
                let img_path = collectCompetitorImg.join(',');
                let lat = this.props.subBadCarMarkPos.latitude;
                let lng = this.props.subBadCarMarkPos.longitude;
                let info = [
                    {'type': 1, 'num': parseInt(this.state.ofoNum)},
                    {'type': 2, 'num': parseInt(this.state.otherNum)}
                ];
                this.props.fetch({img_path, lat, lng, info}).then(() => {
                    if(this.props.collectCompetitorInformation.code == 0){
                        Toast.success('提交成功', 1);
                        this.setState({
                            ofoNum: '',
                            otherNum: '',
                            imgPath: [],
                            loading: false,
                        });
                        this.props.clear && this.props.clear();
                    } else {
                        Toast.fail('提交失败', 1);
                        this.setState({imgPath: [], loading: false,});
                    }
                });
            } else {
                Toast.fail('图片上传失败', 2);
                this.setState({loading: false, imgPath: []});
            }
        });
    }

    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderRepairBadCarMark();
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="路面信息采集" handleBack={this.handleBack.bind(this)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <base.View style={{position: 'relative', flexGrow: 1}}>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='images/loading.gif'/>
                        <base.HorizontalLayout style={{marginTop: 10}}><Text>图片上传中 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }

    renderRepairBadCarMark() {
        let imgPath = this.state.imgPath || [];
        return (
            <base.VerticalLayout className="wrap" style={styles.container}>
                <ActionBar title="路面信息采集" handleBack={this.handleBack.bind(this)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <base.View style={{flex: 1, position: 'relative'}}>
                    <ScrollView keyboardShouldPersistTaps="always"
                                ref="scrollView"
                                onContentSizeChange={(contentWidth, contentHeight) => this.scrollContentHeight = contentHeight}
                                onLayout={e => this.scrollViewHeight = e.nativeEvent.layout.height}
                                style={{position: 'absolute', top: 0, right: 0, bottom: this.state.keyboardHeight, left: 0,}}
                                className="overflow-y">
                        <TouchableHighlight underlayColor={'#fff'} clickDuration={0} onPress={()=>{Keyboard.dismiss()}}>
                            <View style={{flex: 1}}>
                                <base.HorizontalLayout style={styles.imgWrap}>
                                    <ImagePicker options={{chooseFromLibraryButtonTitle: null}}
                                                 onSelectPhoto={base64Data => {let arr = this.state.imgPath; arr[0] = base64Data; this.setState({imgPath: arr});}}
                                                 style={{backgroundColor: '#fff'}} desc="请拍摄坏车和背景环境照片"/>

                                    {imgPath.length != 0 ?
                                        imgPath.map((item,index)=>{
                                            if(index < 2){
                                                return(
                                                    <ImagePicker options={{chooseFromLibraryButtonTitle: null}} key={index+1}
                                                                 onSelectPhoto={base64Data => {let arr = this.state.imgPath; arr[index+1] = base64Data; this.setState({imgPath: arr});}}
                                                                 style={{backgroundColor: '#fff'}} desc="再次拍摄"/>)}
                                        }): null
                                    }
                                </base.HorizontalLayout>
                                <base.HorizontalLayout style={{...styles.inputBox, borderTopWidth: 1,}}>
                                    <base.Text style={{color: '#333', marginRight: 10, width: 90}}>ofo小黄车</base.Text>
                                    <base.TextInput keyboardType="numeric" placeholder="请输入车辆数" style={{fontSize: 16, height: 44, flexGrow: 1}}
                                                    value={this.state.ofoNum} onChangeText={(text) => {this.setState({ofoNum: text});}}/>
                                </base.HorizontalLayout>
                                <base.HorizontalLayout style={styles.inputBox}>
                                    <base.Text style={{color: '#333', marginRight: 10, width: 90}}>橙色单车</base.Text>
                                    <base.TextInput keyboardType="numeric" placeholder="请输入车辆数" style={{fontSize: 16, height: 44, flexGrow: 1}}
                                                    value={this.state.otherNum} onChangeText={(text) => {this.setState({otherNum: text});}}/>
                                </base.HorizontalLayout>
                                <base.VerticalLayout style={styles.address}>
                                    <base.HorizontalLayout style={styles.chooseText}>
                                        <base.Text style={{color: '#333'}}>地址</base.Text>
                                        <base.Button style={styles.gps} onPress={this.repairBadCarMarkPos.bind(this)}>请选择位置</base.Button>
                                    </base.HorizontalLayout>
                                    {this.props.subBadCarMarkPos.address ?
                                        <base.HorizontalLayout style={{marginTop: 5}}>
                                            <base.Text style={{color: '#666'}}>{this.props.subBadCarMarkPos.address}</base.Text>
                                        </base.HorizontalLayout> : null}
                                </base.VerticalLayout>
                                <base.Button style={styles.requireBtn} onPress={this.submitInfo.bind(this)}>提交</base.Button>
                            </View>
                        </TouchableHighlight>
                    </ScrollView>
                </base.View>
            </base.VerticalLayout>
        );
    }

});

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#f8f8f8',
    },
    imgWrap: {
        paddingTop: 20,
        paddingBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
        height: 140,
        borderStyle:"solid",
        borderBottomWidth:1,
        borderColor:"#d6d6d6",
        backgroundColor: '#fff',
        marginBottom: 10,
    },
    inputBox: {
        borderStyle: 'solid',
        borderBottomWidth: 1,
        borderColor: '#ddd',
        paddingLeft: 15,
        paddingRight: 15,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: '#fff',
    },
    address: {
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 8,
        paddingBottom: 8,
        backgroundColor: '#fff',
        borderStyle:"solid",
        borderBottomWidth:1,
        borderColor:"#d6d6d6",
    },
    chooseText: {
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    requireBtn: {
        height: 48,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        backgroundColor: '#3f3f3f',
        fontSize: 15,
        marginTop: 30,
        color: '#ffdc00',
        marginLeft: 15,
        marginRight: 15,
    },
    gps: {
        backgroundColor: '#ffd900',
        borderRadius: 50,
        height: 30,
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
};

const mapStateToProps = state => {
    return {
        subBadCarMarkPos: state.api.subBadCarMarkPos || {},
        collectCompetitorImg: state.api.collectCompetitorImg || [],
        collectCompetitorInformation: state.api.collectCompetitorInformation || {},
    };
};

const mapDispatchToProps = dispatch => {
    return {
        dispatch,
        fetch: props=>dispatch(fetchCompetitorInformation(props)),
        clear: () => {
            dispatch({
                type: '@@clear', payload: {
                    subBadCarMarkPos: {},
                    collectCompetitorImg: [],
                    collectCompetitorInformation: {},
                }
            });
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CompetitorInformation);



