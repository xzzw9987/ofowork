import React, { Component } from 'react';
import { connect } from 'react-redux';
import DashBoard from './operationDashBoardCommon';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { getUserInfo } from '../../runtime/api';

import { fetchCityIndex } from '../../runtime/action';

class Surrogate extends Component {
    constructor(...args) {
        super(...args);
        this.connectedComponent = getConnectComponent(
            parseInt(100000 * Math.random()),
            historyStack.get()
        );
    }

    render() {
        let ConnectComponent = this.connectedComponent;
        return <ConnectComponent {...this.props} />;
    }
}

function getConnectComponent(random, historyState) {
    const mapStateToProps = state => {
        return {
            historyState,
            total: state.api[`${random}cityOrderTotal`] || 0,
            titleOfTotal: '今日订单量',
            delta: state.api[`${random}cityOrderDelta`] || 0,
            weekDelta: state.api[`${random}cityWeekOrderDelta`] || 0,
            deltaAmount: state.api[`${random}cityOrderDeltaAmount`] || 0,
            weekAmount: state.api[`${random}cityOrderWeekAmount`] || 0,
            yesterdayOrder: state.api[`${random}cityYesterdayOrder`] || 0,
            lastWeekOrder: state.api[`${random}cityLastWeekOrder`] || 0,
            chart: state.api[`${random}cityChart`] || [],
            defaultSelectedChart: state.api[`${random}cityChartDefault`] || [],
            left: {
                text: state.api.userInfo
                    ? state.api.userInfo['operation_level'] == 'city' ? '今日排行榜' : '今日订单详情'
                    : null,
                onPress() {
                    let {
            title = state.api.userInfo['zone_names'],
                        id = state.api.userInfo['zone_ids']
          } = historyState || {};
                    JumpTo('operation/orderDetailCity', true, {
                        title,
                        id,
                        shouldRaiseLevel: !historyState ||
                        (historyState && historyState.isFirstPage)
                    });
                    console.log('今日排行榜');
                }
            },
            right: {
                text: '查看历史',
                onPress() {
                    console.log('查看历史');
                    let {
            title = state.api.userInfo['zone_names'],
                        id = state.api.userInfo['zone_ids']
          } = historyState || {};
                    JumpTo('operation/orderHistory', true, {
                        id,
                        level: 'city',
                        name: title,
                        title
                    });
                }
            },
            pageTitle: (() => {
                if (!state.api.userInfo) return null;
                let {
          title = state.api.userInfo['zone_names'],
                    id = state.api.userInfo['zone_ids']
        } = historyState || {};
                return title;
            })(),
            handleBack(props) {
                let dispatch = props.dispatch, s = historyState;
                getUserInfo().then(userInfo => {
                    if (s && s.shouldLowerLevel) {
                        userInfo['operation_level'] = userInfo['__origin_operation_level'];
                        userInfo['__origin_operation_level'] = null;
                        dispatch({ type: 'setUserInfo', payload: userInfo });
                    }
                });
            }
        };
    },
        mapDispatchToProps = dispatch => {
            return {
                dispatch,
                fetch: props => dispatch(fetchCityIndex({ ...props, random }))
            };
        };
    return connect(mapStateToProps, mapDispatchToProps)(DashBoard);
}

export default Surrogate;
