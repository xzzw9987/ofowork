import { connect } from 'react-redux';
import DashBoard from './operationDashBoardCommon';
import { JumpTo, JumpBack, historyStack } from '../../modules';
import { getUserInfo } from '../../runtime/api';

import {
    fetchSchoolIndex
} from '../../runtime/action';

const mapStateToProps = state => {
    let schoolDetail = state.api.schoolDetail || {},
        schoolChart = state.api.schoolChart || {},
        schoolId = state.api.schoolId || 0,
        currentDetail = schoolDetail['' + schoolId] || {},
        currentChart = schoolChart['' + schoolId] || [];
    let total = Number(currentDetail['num']) || 0,
        yesterday_num = Number(currentDetail['yesterday_num']) || 0,
        lastWeek_num = Number(currentDetail['week_num']) || 0,
        deltaAmount = total - yesterday_num || 0,
        weekAmount = total - lastWeek_num || 0;


    return {
        // autoRefresh: false,
        schoolNames: state.api.schoolNames || [],
        schoolId: schoolId,
        total: total,
        titleOfTotal: '今日订单量',
        delta: Number(currentDetail['increase_proportion']) || 0,
        weekDelta: Number(currentDetail['week_increase_proportion']) || 0,
        deltaAmount: deltaAmount,
        weekAmount,
        yesterdayOrder: Number(currentDetail['yesterday_num']) || 0,
        lastWeekOrder: Number(currentDetail['week_num']) || 0,
        chart: currentChart || [],
        defaultSelectedChart: state.api.schoolChartDefault || [],
        left: (() => {
            let historyState = historyStack.get();
            if (!state.api.userInfo) {
                return null;
            }
            if (state.api.userInfo['operation_level'] != 'school') {
                return {
                    text: '查看历史',
                    onPress() {
                        let { title = state.api.userInfo['zone_names'], id = state.api.userInfo['zone_ids'] } = historyState || {};
                        JumpTo('operation/orderHistory', true, { 'id': id, 'level': historyState.level || 'school', 'name': title });
                    }
                };
            }
            return {
                text: '今日排行榜',
                onPress() {
                    JumpBack();
                }
            };
        })(),

        right: null,
        pageTitle: (() => {
            if (!state.api.userInfo)
                return null;
            let { title = state.api.userInfo['zone_names'], id = state.api.userInfo['zone_ids'] } = historyStack.get() || {};
            return title;
        })(),
        handleBack(props) {
            let dispatch = props.dispatch,
                s = historyStack.get();
            getUserInfo().then(userInfo => {
                if (s && s.shouldLowerLevel) {
                    userInfo['operation_level'] = userInfo['__origin_operation_level'];
                    userInfo['__origin_operation_level'] = null;
                    dispatch({ type: 'setUserInfo', payload: userInfo });
                }
            });
        }
    };
},
    mapDispatchToProps = dispatch => {
        return {
            dispatch,
            fetch: props => dispatch(fetchSchoolIndex({ ...props, random: '' }))
        };
    };


export default connect(mapStateToProps, mapDispatchToProps)(DashBoard);
