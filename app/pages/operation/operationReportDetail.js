import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, _loadToken, getTodayDate } from '../../runtime/api';
import { Text, View, Br, Button, Toast, ListView, List, ScrollView, TouchableHighlight, ActivityIndicator, Map, TextInput} from '../../base';
import { connect } from 'react-redux';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import pageWrapper from '../../modules/pageWrapper';
import styles from '../../css/operation/reportDetailCss';
import Swiper, {Item} from '../../components/swiper';
import KeyboardAvoid from '../../components/keyboardAvoid';
import { ActionBar } from '../../components/actionbar';
import whereami from '../../base/map/whereami';

var StaticImage = require('../../base/staticImages');

var moment = require('moment');
import { fetchOperationReportDetail } from '../../runtime/action';

/**
 * 举报详情
 */
class OperationReportDetail extends Component {

    constructor(){
        super();
        this.state = {
            loading: true,
            historyState: historyStack.get(),
            region: {
                latitude: 39.98362,
                longitude: 116.306715,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005,
            },
            initialPosition: undefined,
            lastPosition: undefined,
            annotations: [],
        };
    }

    viewWillAppear() {
        this.props.fetch().then(()=>{
            console.log('position', this.props.carLocation);
            let annotations = [{
                latitude: parseFloat(this.props.carLocation.lat),
                longitude: parseFloat(this.props.carLocation.lng),
                icon: StaticImage['reportIcon'],
                size: {
                    width: 30,
                    height: 30
                },
                title: '',
                key: ''
            }];
            const region = {latitude: parseFloat(this.props.carLocation.lat), longitude: parseFloat(this.props.carLocation.lng), latitudeDelta: 0.005, longitudeDelta: 0.005};
            this.setState({annotations, refresh: true, region});
            this.setState({loading: false});
        });
    }

    /**
     * 返回
     */
    handleGoBack() {
        setTimeout(()=>this.props.clear(), 1000);
        JumpBack();
    }

    render() {
        let reportDetail = this.props.operationReportDetail || {};
        let reportType = this.props.reportType;
        if (this.state.loading) {
            return this.renderLoading(reportType);
        } else {
            if(reportType == 10 || reportType == 20 ){
                return this.renderPageReport(reportDetail, reportType);
            } else {
                return this.renderPageList(reportDetail, reportType);
            }
        }
    }

    renderLoading(reportType) {
        let title;
        if(reportType == 10){
            title = '已处理';
        } else if(reportType == 20){
            title = '未找到车辆';
        } else if(reportType == 30){
            title = '暂不处理';
        } else {
            title = '举报信息';
        }
        return (
            <base.View style={styles.container}>
                <ActionBar title={title} handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.View style={{ position: 'absolute', flexDirection: 'column', left: 0, top: 44, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator size='large' src='loading' />
                    <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                </base.View>
            </base.View>
        );
    }


    /**
     * 未发现车辆（type=20），已处理(type=10）
     */
    renderPageReport(reportDetail, reportType) {
        let reasons = reportDetail.reason || [];
        let reportResult = this.props.reportResult || {};
        return (
            <base.View style={styles.container}>
                <ActionBar title={(reportType == 10) ? '已处理' : '未找到车辆'} handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <ScrollView className="overflowY" style={styles.listView2}>
                    <base.VerticalLayout>
                        <base.HorizontalLayout style={{justifyContent: 'center'}}>
                            <base.Image className="contain" resizeMode="contain" style={{height: 300, flex: 1}} src={reportResult.img != '' ? reportResult.img : 'defaultImage2'} ></base.Image>
                        </base.HorizontalLayout>
                        <base.VerticalLayout style={styles.intro3}>
                            <base.HorizontalLayout style={styles.firstLine}>
                                <base.Text style={styles.introTitle}>处理人</base.Text>
                                <base.Text style={styles.name}>{reportResult.name != '' ? reportResult.name : null}</base.Text>
                                <base.Text style={styles.introTime}>{getTodayDate(reportResult.time)}</base.Text>
                            </base.HorizontalLayout>
                            <base.HorizontalLayout style={{wordBreak: 'break-all'}}>
                                <base.Text style={styles.introMessage}>{reportResult.info}</base.Text>
                            </base.HorizontalLayout>
                        </base.VerticalLayout>
                        <base.HorizontalLayout style={styles.firstHorizontalText2}>
                            <base.Text style={styles.carNo}>车牌{reportDetail.car_no === '-----' ? '无' : reportDetail.car_no}</base.Text>
                        </base.HorizontalLayout>
                        {reportDetail.reason != '' ?
                            <base.HorizontalLayout style={{flexGrow: 1, alignItems: 'flex-start', justifyContent: 'center', backgroundColor: '#fff', flexWrap: 'wrap'}}>
                                {reasons.map(function (reason, index) {
                                    return (
                                        <base.HorizontalLayout  key={index} style={styles.reasons}>
                                            <base.Text style={styles.reasonItem} key={index}>{reason}</base.Text>
                                        </base.HorizontalLayout>
                                    );
                                })}
                            </base.HorizontalLayout> : null}
                        {reportDetail.descr != '' ?
                            <base.HorizontalLayout style={styles.intro}>
                                <base.Text style={styles.introText}>备注：{reportDetail.descr}</base.Text>
                            </base.HorizontalLayout> : null}
                        {this.props.images != '' ?
                            <base.VerticalLayout style={{height: 300, backgroundColor: '#fff'}}>
                                <Swiper>
                                    {
                                        this.props.images.map((item, index)=> {
                                            return (
                                                <Item key={index} style={{justifyContent: 'center'}}>
                                                    <base.Image className="contain" resizeMode="contain" style={{height: 300}} src={item}></base.Image>
                                                </Item>);
                                        })
                                    }
                                </Swiper>
                            </base.VerticalLayout>
                            :
                            <base.HorizontalLayout style={{justifyContent: 'center'}}>
                                <base.Image style={{height: 300, flex: 1}} src='defaultImage2'></base.Image>
                            </base.HorizontalLayout>
                        }

                    </base.VerticalLayout>
                </ScrollView>
            </base.View>
        );
    }

    /**
     * 待处理详情页、暂不处理（type=30）
     */
    renderPageList(reportDetail, reportType) {
        let reasons = reportDetail.reason || [];
        let reportResult = this.props.reportResult || {};
        return (
            <base.View style={styles.container} onStartShouldSetResponder={()=>this.getKeyboardState()} onResponderGrant={e=>this.dismissKeyboard && this.dismissKeyboard(e)}>
                <ActionBar title={(reportType == 30) ? '暂不处理' : '举报信息'} handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <ScrollView className="overflowY" style={styles.listView}>
                    <base.VerticalLayout style={{flex: 1}}>
                        {this.props.images != ''?
                            <base.VerticalLayout style={{height: 300}}>
                                <Swiper>
                                    {
                                        this.props.images.map((item, index)=> {
                                            return (
                                                <Item key={index} style={{justifyContent: 'center'}}>
                                                    <base.Image className="contain" resizeMode="contain" style={{height: 300}} src={item}></base.Image>
                                                </Item>);
                                        })
                                    }
                                </Swiper>
                            </base.VerticalLayout> :
                            <base.HorizontalLayout style={{justifyContent: 'center'}}>
                                <base.Image style={{height: 300, flex: 1}} src='defaultImage2'></base.Image>
                            </base.HorizontalLayout>
                        }
                        <base.HorizontalLayout style={styles.firstHorizontalText1}>
                            <base.Text style={styles.carNo}>车牌{reportDetail.car_no === '-----' ? '无' : reportDetail.car_no}</base.Text>
                        </base.HorizontalLayout>
                        {(reportDetail.reason != '') ?
                            <base.HorizontalLayout style={{flexGrow: 1, alignItems: 'flex-start', justifyContent: 'center', marginBottom: 5, flexWrap: 'wrap'}}>
                                {reasons.map(function (reason, index) {
                                    return (
                                        <base.HorizontalLayout  key={index} style={styles.reasons}>
                                            <base.Text style={styles.reasonItem}>{reason}</base.Text>
                                        </base.HorizontalLayout>
                                    );
                                })}
                            </base.HorizontalLayout> : null}
                        {(reportDetail.descr != '') ?
                            <base.HorizontalLayout style={styles.intro}>
                                <base.Text style={styles.introText}>备注：{reportDetail.descr}</base.Text>
                            </base.HorizontalLayout> : null}
                        <base.HorizontalLayout style={styles.line}>
                            {(reportType == 30) ? null :
                                <base.HorizontalLayout style={styles.lockCode}>
                                    <base.HorizontalLayout style={styles.lineBox} >
                                        <base.Text style={styles.time}>{getTodayDate(reportDetail.time)}</base.Text>
                                    </base.HorizontalLayout>
                                    <base.Text style={styles.code}>解锁码{reportDetail.unlock_code != '' ? reportDetail.unlock_code : '****'}</base.Text>
                                </base.HorizontalLayout>}
                        </base.HorizontalLayout>
                        {(reportType == 30) ?
                            <base.VerticalLayout style={styles.intro2}>
                                <base.HorizontalLayout style={styles.firstLine}>
                                    <base.Text style={styles.introTitle}>备注</base.Text>
                                    <base.Text style={styles.introTime}>{getTodayDate(reportResult.time)}</base.Text>
                                </base.HorizontalLayout>
                                <base.HorizontalLayout style={{wordBreak: 'break-all'}}>
                                    <base.Text style={styles.introMessage}>{reportResult.info}</base.Text>
                                </base.HorizontalLayout>
                            </base.VerticalLayout> : null}
                        {(reportDetail.address != '') ?
                            <base.HorizontalLayout style={styles.address}>
                                <base.HorizontalLayout >
                                    <base.Image style={{width: 13, height: 16}} src='icon'></base.Image>
                                    <base.Text style={styles.introAddress}>{reportDetail.address}</base.Text>
                                </base.HorizontalLayout>
                            </base.HorizontalLayout> : null}
                        <base.HorizontalLayout style={{height: 300}}>
                            <Map style={{flexGrow: 1, height: 300, }}  region={this.state.region} annotations={this.state.annotations} />
                        </base.HorizontalLayout>
                    </base.VerticalLayout>

                </ScrollView>
                {(reportType == 30) ? null :
                    <base.VerticalLayout style={{height: 50}}>
                        <base.HorizontalLayout style={styles.choose}>
                            <base.VerticalLayout>
                                <Button onPress={()=>{this.setState({showInput: true});}} className="blackBtn" clickDuration={0} style={styles.button}>暂不处理</Button>
                            </base.VerticalLayout>
                            <base.VerticalLayout>
                                <Button className="blackBtn" clickDuration={0} style={styles.button} onPress={this.handleGoBack.bind(this)}>立即处理</Button>
                            </base.VerticalLayout>
                        </base.HorizontalLayout>
                    </base.VerticalLayout>}
                {this.renderTextInput()}
            </base.View>
        );
    }

    renderTextInput(){
        return this.state.showInput ? (
            <KeyboardAvoid containerStyle={{backgroundColor: '#ddd'}} onDismissKeyboard={()=>this.setState({showInput: false})}>
                <TextInput
                    multiline={false}
                    maxLength={140}
                    onChangeText={txt=>this.setState({txt})}
                    onSubmitEditing={ () => {if(this.state.txt){this.props.fetch(this.state.txt).then(()=>this.setState({loading: false, showInput: false}));} else {this.setState({showInput: true});Toast.fail('请填写原因！');}} }
                    placeholder="请填写原因。例：照片与举报车辆无关。"
                    autoFocus={true}
                    className="blackBtn" clickDuration={0}
                    style={{backgroundColor: '#fff', borderRadius: 5, flexGrow: 1, margin: 10, height: 36}} />
            </KeyboardAvoid>) : null;
    }
}


const mapStateToProps = state=> {
    let reportDetail = state.api.operationReportDetail || {img_path: ''};
    return {
        loading: state.api.loading,
        token: state.api.token,
        images: (state.api.operationReportDetail || {img_path: ''}).img_path.split(',') || [],
        operationReportDetail: state.api.operationReportDetail,
        reportType: state.api.reportType,
        reportResult: state.api.reportResult,
        carLocation: {
            lat: reportDetail.img_lat || reportDetail.lat || 0,
            lng: reportDetail.img_lng || reportDetail.lng || 0,
        }
    };
};

const mapDispatchToProps = dispatch=> {
    return {
        dispatch,
        fetch: props=>dispatch(fetchOperationReportDetail(props)),
        clear: ()=> {dispatch({type: '@@clear', payload: {
            operationReportDetail: null,
            reportType: null,
            reportResult: {}
        }});}
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(OperationReportDetail));

