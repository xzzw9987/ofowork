import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, getUserInfo, apibase } from '../../runtime/api';
import { Text, View, Br, Button, TextInput, TouchableHighlight, ScrollView, ActivityIndicator } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack, historyStack, fetch } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';

var moment = require('moment');

/**
 * 寻迹轨迹电子围栏下的运营人员
 */
class MovePathPerson extends Component {

    constructor() {
        super();
        this.state = {
            loading: true,
            zone_name: historyStack.get().zone_name,
            area_code: historyStack.get().area_code,
            area_type: historyStack.get().area_type,
            date: historyStack.get().date,
            list: [],
        };
    }

    viewWillAppear() {
        return getUserInfo().then(userInfo => {
            let token = userInfo['token'];
            fetch(`${apibase}/operation/staff-path`, {
                method: 'POST',
                body: { token, 'area_code': this.state.area_code, 'area_type': this.state.area_type, 'date': this.state.date}
            }).then(respones => {
                this.setState({list: respones.data.list, loading: false});
            });
        });
    }

    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }

    handleGoWhere(user_id) {
        JumpTo('movePath/map',true,{'date': this.state.date, 'user_id': user_id});
    }

    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderList();
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="巡检轨迹" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.VerticalLayout style={{flex: 1}}>
                    <base.HorizontalLayout style={{...styles.list, height: 42,backgroundColor: '#f8f8f8'}}>
                        <base.Text style={styles.one}>{this.state.date}  {this.state.zone_name}</base.Text>
                    </base.HorizontalLayout>
                    <base.View style={{backgroundColor: '#fff', position: 'absolute', flexDirection: 'column', left: 0, top: 44, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center' }}>
                        <ActivityIndicator size='large' src='loading' />
                        <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.VerticalLayout>
            </base.View>
        );
    }

    renderList() {
        let list = this.state.list;
        return (
            <base.View style={styles.container}>
                <ActionBar title="巡检轨迹" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.VerticalLayout style={{flex: 1}}>
                    <base.HorizontalLayout style={{...styles.list, height: 42,backgroundColor: '#f8f8f8'}}>
                        <base.Text style={styles.one}>{this.state.date}  {this.state.zone_name}</base.Text>
                    </base.HorizontalLayout>

                    {list.length > 0 ?
                        <ScrollView style={{backgroundColor: '#fff', flex: 1}}>
                            {list.map((item,index)=>{
                                return(
                                    <TouchableHighlight key={item.user_id} onPress={()=>{this.handleGoWhere(item.user_id)}}>
                                        <base.HorizontalLayout style={styles.list}>
                                            <base.HorizontalLayout style={styles.one}>
                                                <base.Text>{item.user_name}</base.Text>
                                            </base.HorizontalLayout>
                                            <base.HorizontalLayout style={styles.three}>
                                                <base.Text>查看轨迹 ></base.Text>
                                            </base.HorizontalLayout>
                                        </base.HorizontalLayout>
                                    </TouchableHighlight>
                                );
                            })}
                        </ScrollView>
                        :
                        <base.View style={{backgroundColor: '#fff',position: 'absolute', flexDirection: 'column', left: 0, top: 42, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center' }}>
                            <base.Image style={{height: 60, width: 60, }} src='noData' />
                            <base.HorizontalLayout style={{ marginTop: 10 }}><Text>暂无数据</Text></base.HorizontalLayout>
                        </base.View>}
                </base.VerticalLayout>
            </base.View>
        );
    }
}


/*const mapStateToProps = state => {
    let workRecord = state.api.workRecord || {};
    return {
        list: workRecord.list || [],
        name: workRecord.name || '无',
        phone: workRecord.phone || historyStack.get().phone,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        dispatch,
        fetch: props => dispatch(fetchWorkRecord(props)),
    };
};*/
export default (pageWrapper(MovePathPerson));


const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    list: {
        borderBottomWidth: 1,
        borderBottomColor: '#c6c6c6',
        borderBottomStyle: 'solid',
        justifyContent: 'space-around',
        height: 54,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 20
    },
    one: {
        flexGrow: .8,
        flexBasis: 0,
    },
    two: {
        flexGrow: .7,
        justifyContent: 'flex-start',
        flexBasis: 0,
    },
    three: {
        alignItems: 'flex-end',
    },
};
