import React, { Component } from 'react';
import { connect } from 'react-redux';
import OrderList from './operationOrderDetailCommon';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { getUserInfo } from '../../runtime/api';
import {
    fetchCityDetail
} from '../../runtime/action';


class Surrogate extends Component {
    constructor(...args) {
        super(...args);
        this.connectedComponent = getConnectComponent(parseInt(100000 * Math.random()), historyStack.get());
    }

    render() {
        let ConnectComponent = this.connectedComponent;
        return <ConnectComponent {...this.props} />;
    }
}


function getConnectComponent(random, historyState) {
    const mapStateToProps = (state) => {
        return {
            historyState,
            userInfo: state.api.userInfo || {},
            pageTitle: (() => {
                if (!state.api.userInfo)
                    return null;
                if (state.api.userInfo['operation_level'] === 'city')
                    return '今日订单量排行';
                let { title = state.api.userInfo['zone_names'], id = state.api.userInfo['zone_ids'] } = historyState || {};
                return `${title}订单量详情`;
            })(),
            pane: getPane(state, random),
            dataSource: getDataSource(state, random, historyState)
        };
    },
        mapDispatchToProps = dispatch => {
            return {
                dispatch,
                fetch: props => dispatch(fetchCityDetail({ ...props, random }))
            };
        };
    return connect(mapStateToProps, mapDispatchToProps)(OrderList);
}

export default Surrogate;

function beautifyList(list, historyState) {
    if (!list)
        return [];

    return list.map(v => {
        return {
            ...v,
            onPress() {
                if (v.id === 'other')
                    return;
                getUserInfo().then(userInfo => {
                    let opt = { id: v.id, title: v.title };
                    if (historyState && historyState.shouldRaiseLevel) {
                        if (!userInfo['__origin_operation_level']) {
                            userInfo['__origin_operation_level'] = userInfo['operation_level'];
                            userInfo['operation_level'] = 'country';
                            opt.shouldLowerLevel = true;
                        }
                    }
                    /* set history */
                    console.log(v);
                    JumpTo(`operation/dashBoard/${v.name}`, true, opt);
                });
            }
        };
    });
}


function getPane(state) {
    if (!state.api.userInfo)
        return null;
    if (state.api.userInfo['operation_level'] != 'city')
        return [{ text: '城市' }, { text: '校园' }];
    return null;
}

function getDataSource(state, random, historyState) {
    if (!state.api.userInfo)
        return [];
    if (state.api.userInfo['operation_level'] != 'city') {
        return [
            {
                fixed: {
                    left: '订单量',
                    center: state.api[`${random}cityDistrictOrderTotal`],
                    right: state.api[`${random}cityDistrictOrderDelta`],
                    onPress() {
                        getUserInfo().then(userInfo => {
                            let opt = { id: historyState.id, title: `${historyState.title}-城市`, level: 'society_all' };
                            if (historyState && historyState.shouldRaiseLevel) {
                                if (!userInfo['__origin_operation_level']) {
                                    userInfo['__origin_operation_level'] = userInfo['operation_level'];
                                    userInfo['operation_level'] = 'country';
                                    opt.shouldLowerLevel = true;
                                }
                            }
                            /* set history */
                            JumpTo('operation/dashBoard/district', true, opt);
                        });
                    }
                },
                list: beautifyList(state.api[`${random}cityDistrictList`] || [], historyState)
            },
            {
                fixed: {
                    left: '订单量',
                    center: state.api[`${random}citySchoolOrderTotal`],
                    right: state.api[`${random}citySchoolOrderDelta`],
                    onPress() {
                        getUserInfo().then(userInfo => {
                            let opt = { id: historyState.id, title: `${historyState.title}-校园`, level: 'school_all' };
                            if (historyState && historyState.shouldRaiseLevel) {
                                if (!userInfo['__origin_operation_level']) {
                                    userInfo['__origin_operation_level'] = userInfo['operation_level'];
                                    userInfo['operation_level'] = 'country';
                                    opt.shouldLowerLevel = true;
                                }
                            }
                            /* set history */
                            JumpTo('operation/dashBoard/district', true, opt);
                        });
                    }
                },
                list: beautifyList(state.api[`${random}citySchoolList`] || [], historyState)
            }
        ];
    }
    else {
        return [
            {
                list: beautifyList(state.api[`${random}cityCityList`] || [], historyState)
            }
        ];
    }
}
