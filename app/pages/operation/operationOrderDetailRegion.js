import React, {Component} from 'react';
import {connect} from 'react-redux';
import OrderList from './operationOrderDetailCommon';
import {JumpTo, historyStack, JumpBack} from '../../modules';
import {getUserInfo} from '../../runtime/api';
import {
    fetchRegionDetail
} from '../../runtime/action';



class Surrogate extends Component {
    constructor(...args) {
        super(...args);
        this.connectedComponent = getConnectComponent(parseInt(100000 * Math.random()), historyStack.get());
    }

    render(){
        let ConnectComponent = this.connectedComponent ;
        return <ConnectComponent {...this.props}/>;
    }
}


function getConnectComponent(random, historyState) {
    const mapStateToProps = (state)=> {
            return {
                historyState,
                userInfo: state.api.userInfo || {},
                pageTitle: (()=> {
                    if (!state.api.userInfo)
                        return null;
                    if (state.api.userInfo['operation_level'] === 'region')
                        return '今日订单量排行';
                    let {title = state.api.userInfo['zone_names'], id = state.api.userInfo['zone_ids']} = historyState || {};
                    return `${title}订单量详情`;
                })(),
                pane: getPane(state, random),
                dataSource: getDataSource(state, random, historyState)
            };
        },
        mapDispatchToProps = dispatch=> {
            return {
                dispatch,
                fetch: props=> dispatch(fetchRegionDetail({...props, random}))
            };
        };
    return connect(mapStateToProps, mapDispatchToProps)(OrderList);
}

export default Surrogate;

function beautifyList(list, historyState) {
    if (!list)
        return [];

    return list.map(v=> {
        return {
            ...v,
            onPress(){
                /* set history */
                getUserInfo().then(userInfo=> {
                    let opt = {id: v.id, title: v.title};
                    if (historyState && historyState.shouldRaiseLevel) {
                        if (!userInfo['__origin_operation_level']) {
                            userInfo['__origin_operation_level'] = userInfo['operation_level'];
                            userInfo['operation_level'] = 'country';
                            opt.shouldLowerLevel = true;
                        }
                    }
                    /* set history */
                    JumpTo(`operation/dashBoard/${v.name}`, true, opt);
                });
            }
        };
    });
}

function getPane(state) {
    if (!state.api.userInfo)
        return null;
    if (state.api.userInfo['operation_level'] == 'region')
        return [{text: '城市'}, {text: '战区'}];
    return null;
}


function getDataSource(state, random, historyState) {
    if (!state.api.userInfo)
        return [];
    if (state.api.userInfo['operation_level'] == 'region') {
        return [
            {
                list: beautifyList(state.api[`${random}regionCityList`] || [], historyState)
            },
            {
                list: beautifyList(state.api[`${random}regionZoneList`] || [], historyState)
            }
        ];
    }
    else {
        return [
            {
                list: beautifyList(state.api[`${random}regionCityList`] || [], historyState)
            }
        ];
    }
}
