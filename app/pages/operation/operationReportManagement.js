import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, _loadToken, getDateDiff } from '../../runtime/api';
import { Text, View, Br, Button, List, Toast, ScrollView, TouchableHighlight, ActivityIndicator} from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import pageWrapper from '../../modules/pageWrapper';
import styles from '../../css/operation/reportManagementCss';
import { ActionBar } from '../../components/actionbar';
import Storage from '../../base/storage';
import RefreshControl from '../../components/refreshControl';
import ListView from '../../components/ListView';

var moment = require('moment');
import {
    fetchOperationReportManagement
} from '../../runtime/action';

/**
 * 举报管理
 */
class OperationReportManagement extends Component {

    constructor(){
        super();
        this.state={
            type: 'pending',
            selected: [],
            loading: true,
            pageCount: 0,
        };
    }

    viewWillAppear() {
        this.props.fetch({'type': this.state.type}).then(()=>this.setState({loading: false}));
        Storage.getItem('operationReportList').then(d=>{
            if(!d)return ;
            d = JSON.parse(d) ;
            this.setState({ selected: d });
        });
    }
    viewWillDisappear(){
        Storage.setItem('operationReportList', JSON.stringify(this.state.selected));
    }

    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }

    /**
     * 已处理未处理切换
     */
    handleSwitchZone(type) {
        this.setState({type: type, pageCount: 0});
        if(this.state.type != type){
            this.props.fetch({'type': type}).then(()=>{
                this.setState({loading: false});
                setTimeout(()=>this.refs['listView'] && this.refs['listView'].scrollTo({x: 0, y: 0, animated: false}), 50);
            });
        }
    }

    /**
     * 已处理状态
     */
    handleProcessingState(type) {
        if(type == 10){
            return '已处理';
        } else if(type == 20){
            return '未找到车辆';
        } else {
            return '暂不处理';
        }
    }

    render() {
        let reportManagement = this.props.operationReportManagement || [];
        if (this.state.loading)
            return this.renderLoading();
        return this.renderPageList(reportManagement);
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="举报管理" style={{borderBottomColor: '#7d7d7d'}}></ActionBar>
                <base.VerticalLayout style={styles.listView}>
                    <base.HorizontalLayout style={styles.nav}>
                        <base.VerticalLayout style={styles.inner}>
                            <Button clickDuration={0} style={(this.state.type == 'pending' ) ? styles.item1 : styles.item2} onPress={ () => { this.handleSwitchZone('pending'); } }>
                                <base.Text style={(this.state.type == 'pending' ) ? styles.text1 : styles.text2}>待处理</base.Text>
                            </Button>
                        </base.VerticalLayout>
                        <base.VerticalLayout style={styles.inner}>
                            <Button clickDuration={0} style={(this.state.type == 'processed' ) ? styles.item1 : styles.item2} onPress={ () => { this.handleSwitchZone('processed'); } }>
                                <base.Text style={(this.state.type == 'processed' ) ? styles.text1 : styles.text2}>已处理</base.Text>
                            </Button>
                        </base.VerticalLayout>
                    </base.HorizontalLayout>
                </base.VerticalLayout>
                <base.View style={{ position: 'absolute', flexDirection: 'column', left: 0, top: 44, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator size='large' src='loading' />
                    <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                </base.View>
            </base.View>
        );
    }

    renderDot(item){
        return this.state.selected.indexOf(item.id) > -1 ? null: (
            <base.View style={(this.state.type == 'processed' ) ? null : styles.reminder1}></base.View>
        );
    }

    makeRefreshControl(){
        return <RefreshControl
            refreshing={this.state.isRefreshing}
            onRefresh={()=>{
                this.setState({isRefreshing: true});
                this.props.fetch({'type': this.state.type}).then(()=>{this.setState({isRefreshing: false, pageCount: 0});Toast.success('刷新成功', 1); });
            }}
        />;
    }

    renderPageList(reportManagement) {
        return (
            <base.View style={styles.container}>
                <ActionBar title="举报管理" style={{borderBottomColor: '#7d7d7d'}} handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.VerticalLayout style={styles.listView}>
                    <base.HorizontalLayout style={styles.nav}>
                        <base.VerticalLayout style={styles.inner}>
                            <Button clickDuration={0} style={(this.state.type == 'pending' ) ? styles.item1 : styles.item2} onPress={ () => { this.handleSwitchZone('pending'); } }>
                                <base.Text style={(this.state.type == 'pending' ) ? styles.text1 : styles.text2}>待处理</base.Text>
                            </Button>
                        </base.VerticalLayout>
                        <base.VerticalLayout style={styles.inner}>
                            <Button clickDuration={0} style={(this.state.type == 'processed' ) ? styles.item1 : styles.item2} onPress={ () => { this.handleSwitchZone('processed'); } }>
                                <base.Text style={(this.state.type == 'processed' ) ? styles.text1 : styles.text2}>已处理</base.Text>
                            </Button>
                        </base.VerticalLayout>
                    </base.HorizontalLayout>
                    {reportManagement != '' ?
                        <ListView
                            ref="listView"
                            onEndReached={()=>{  if(this.state.pageCount > 3) return ; this.setState({pageCount: ++this.state.pageCount}); this.props.fetch({'type': this.state.type, concat: true, 'page': this.state.pageCount}).then(()=>{this.setState({isRefreshing: false}); });}}
                            dataSource={reportManagement}
                            renderRow={(item, idx) => {
                                return (
                                    <TouchableHighlight key={item.id} onPress={()=>{let arr = this.state.selected; arr.push(item.id); this.setState({selected: arr}); item.onPress && item.onPress(item);}} underlayColor={'#fff'}>
                                        <base.HorizontalLayout style={styles.list}>
                                            <base.HorizontalLayout>
                                                <base.Image style={{width: 80, height: 80, marginRight: 10, }} src={item.img_path ? item.img_path : 'defaultImage'} ></base.Image>
                                            </base.HorizontalLayout>

                                            <base.HorizontalLayout style={{flex: 1}}>
                                                <base.VerticalLayout style={{flex: 1}}>
                                                    <base.HorizontalLayout style={styles.firstHorizontal}>
                                                        <base.Text style={styles.firstHorizontalText1}>车牌{(item.car_no === '-----') ? '无' : item.car_no}</base.Text>
                                                        <base.Text style={styles.time}>{getDateDiff(item.time)}</base.Text>
                                                        { this.renderDot(item) }
                                                    </base.HorizontalLayout>

                                                    {(item.reason != '') ?
                                                        <base.HorizontalLayout>
                                                            {item.reason.map(function (reason, index) {
                                                                return (
                                                                    <base.HorizontalLayout style={styles.reasons}  key={index}>
                                                                        <base.Text style={styles.reasonItem}>{reason}</base.Text>
                                                                    </base.HorizontalLayout>
                                                                );
                                                            })}
                                                        </base.HorizontalLayout> : null
                                                    }

                                                    {(item.address != '') ?
                                                        <base.HorizontalLayout>
                                                            <base.Text style={styles.address}>{item.address}</base.Text>
                                                        </base.HorizontalLayout> : null
                                                    }

                                                    {item.result ?
                                                        <base.HorizontalLayout>
                                                            <base.HorizontalLayout style={styles.resultTab}>
                                                                <base.Text style={{fontSize: 14, paddingLeft: 3, paddingRight: 3, color: (item.result.type == 20 ? '#000' : '#fff'), backgroundColor: (item.result.type == 10 ? '#2EBF38' : (item.result.type == 20 ? '#FAD500' : '#FF7000'))}}>{this.handleProcessingState(item.result.type)}</base.Text>
                                                                <base.Text style={styles.name}>{item.result.name}</base.Text>
                                                            </base.HorizontalLayout>
                                                        </base.HorizontalLayout>
                                                        :
                                                        <base.HorizontalLayout style={styles.resultTab}>
                                                            <base.Text style={styles.pending}>待处理</base.Text>
                                                        </base.HorizontalLayout>
                                                    }
                                                </base.VerticalLayout>
                                            </base.HorizontalLayout>
                                        </base.HorizontalLayout>
                                    </TouchableHighlight>
                                );
                            }}
                            refreshControl={this.makeRefreshControl()} className="overflowY" style={styles.listView} />
                            :
                        <base.View style={{ position: 'absolute', flexDirection: 'column', left: 0, top: 88, right: 0, bottom: 44, alignItems: 'center', justifyContent: 'center' }}>
                            <base.Image  style={{height: 60, width: 60, }} src='noData' ></base.Image>
                            <base.HorizontalLayout style={{ marginTop: 10 }}><Text>暂无数据</Text></base.HorizontalLayout>
                        </base.View>
                    }
                </base.VerticalLayout>

            </base.View>
        );
    }
}


const mapStateToProps = state=> {
    return {
        loading: state.api.loading,
        token: state.api.token,
        operationReportManagement: handleReportMessage(state.api.operationReportManagement),
    };
};
const mapDispatchToProps = dispatch=> {
    return {
        dispatch,
        fetch: props=>dispatch(fetchOperationReportManagement(props))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(OperationReportManagement));
function handleReportMessage(operationReportManagement) {
    if (!operationReportManagement)
        return [];

    return operationReportManagement.map(item=> {
        return {
            ...item,
            onPress(){
                JumpTo('operation/reportDetail', true, {report_id: item.id, report_type: item.result ? item.result.type : null});
            }
        };
    });
}

