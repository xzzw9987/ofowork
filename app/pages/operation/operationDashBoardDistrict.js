import React, { Component } from 'react';
import { connect } from 'react-redux';
import DashBoard from './operationDashBoardCommon';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { getUserInfo } from '../../runtime/api';
import {
    fetchDistrictIndex
} from '../../runtime/action';

class Surrogate extends Component {
    constructor(...args) {
        super(...args);
        this.connectedComponent = getConnectComponent(parseInt(100000 * Math.random()), historyStack.get());
    }

    render() {
        let ConnectComponent = this.connectedComponent;
        return <ConnectComponent {...this.props} />;
    }
}


function getConnectComponent(random, historyState) {
    const mapStateToProps = state => {
        return {
            historyState,
            total: state.api[`${random}districtOrderTotal`] || 0,
            titleOfTotal: '今日订单量',
            delta: state.api[`${random}districtOrderDelta`] || 0,
            weekDelta: state.api[`${random}districtWeekOrderDelta`] || 0,
            deltaAmount: state.api[`${random}districtOrderDeltaAmount`] || 0,
            weekAmount: state.api[`${random}districtOrderWeekAmount`] || 0,
            yesterdayOrder: state.api[`${random}districtYesterdayOrder`] || 0,
            lastWeekOrder: state.api[`${random}districtLastWeekOrder`] || 0,
            chart: state.api[`${random}districtChart`] || [],
            defaultSelectedChart: state.api[`${random}districtChartDefault`] || [],
            right: {
                text: '查看历史',
                onPress() {
                    let { title = state.api.userInfo['zone_names'], id = state.api.userInfo['zone_ids'] } = historyState || {};
                    JumpTo('operation/orderHistory', true, {
                        id,
                        level: historyState.level || 'society',
                        name: title,
                        title
                    });
                }
            },
            pageTitle: (() => {
                if (!state.api.userInfo)
                    return null;
                let { title = state.api.userInfo['zone_names'], id = state.api.userInfo['zone_ids'] } = historyState || {};
                return title;
            })(),
            handleBack(props) {
                let dispatch = props.dispatch,
                    s = historyState;
                getUserInfo().then(userInfo => {
                    if (s && s.shouldLowerLevel) {
                        userInfo['operation_level'] = userInfo['__origin_operation_level'];
                        userInfo['__origin_operation_level'] = null;
                        dispatch({ type: 'setUserInfo', payload: userInfo });
                    }
                });
            }
        };
    },
        mapDispatchToProps = dispatch => {
            return {
                dispatch,
                fetch: props => dispatch(fetchDistrictIndex({ ...props, random }))
            };
        };
    return connect(mapStateToProps, mapDispatchToProps)(DashBoard);
}



export default Surrogate;
