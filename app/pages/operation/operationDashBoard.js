import React, { Component } from 'react';
import { connect } from 'react-redux';
import DashBoard from './operationDashBoardCommon';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { getUserInfo } from '../../runtime/api';

import { fetchSchoolIndex } from '../../runtime/action';

class Surrogate extends Component {
    constructor(...args) {
        super(...args);
        this.connectedComponent = getConnectComponent(
            parseInt(100000 * Math.random()),
            historyStack.get()
        );
    }

    render() {
        let ConnectComponent = this.connectedComponent;
        return <ConnectComponent {...this.props} />;
    }
}

function getConnectComponent(random, historyState) {
    const mapStateToProps = state => {
        let schoolDetail = state.api[`${random}schoolDetail`] || {},
            schoolChart = state.api[`${random}schoolChart`] || {},
            schoolId = state.api[`${random}schoolId`] || null,
            currentDetail = schoolDetail['' + schoolId] || {},
            currentChart = schoolChart['' + schoolId] || [];
        let total = Number(currentDetail['num']) || 0,
            yesterday_num = Number(currentDetail['yesterday_num']) || 0,
            lastWeek_num = Number(currentDetail['week_num']) || 0,
            deltaAmount = total - yesterday_num || 0,
            weekAmount = total - lastWeek_num || 0;

        return {
            actionRightBar: {
                text: '举报管理',
                onPress: () => JumpTo('operation/reportManagement', true, {})
            },
            // autoRefresh: false,
            schoolNames: state.api[`${random}schoolNames`] || [],
            schoolId: schoolId,
            total: total,
            titleOfTotal: '今日订单量',
            delta: Number(currentDetail['increase_proportion']) || 0,
            weekDelta: Number(currentDetail['week_increase_proportion']) || 0,
            deltaAmount: deltaAmount,
            weekAmount,
            yesterdayOrder: Number(currentDetail['yesterday_num']) || 0,
            lastWeekOrder: Number(currentDetail['week_num']) || 0,
            chart: currentChart || [],
            defaultSelectedChart: state.api[`${random}schoolChartDefault`] || [],
            left: {
                text: '今日排行榜',
                onPress() {
                    JumpTo('operation/orderDetailSchool', true, {
                        schoolId,
                        shouldRaiseLevel: !historyState ||
                        (historyState && historyState.isFirstPage)
                    });
                    console.log('今日排行榜');
                }
            },
            right: {
                text: '查看历史',
                onPress() {
                    let schoolName = state.api[
                        `${random}schoolNames`
                    ].filter((item, index) => {
                        return item.id == schoolId;
                    })[0]['name'];
                    JumpTo('operation/orderHistory', true, {
                        id: schoolId,
                        level: 'school',
                        name: schoolName
                    });
                }
            },
            handleBack(props) {
                let dispatch = props.dispatch, s = historyState;
                getUserInfo().then(userInfo => {
                    if (s && s.shouldLowerLevel) {
                        userInfo['operation_level'] = userInfo['__origin_operation_level'];
                        userInfo['__origin_operation_level'] = null;
                        dispatch({ type: 'setUserInfo', payload: userInfo });
                    }
                });
            },
            pageTitle: '学校'
        };
    },
        mapDispatchToProps = dispatch => {
            return {
                dispatch,
                fetch: props => dispatch(fetchSchoolIndex({ ...props, random })),
                setSchoolID: props => dispatch({ ...props, prefix: random })
            };
        };
    return connect(mapStateToProps, mapDispatchToProps)(DashBoard);
}

export default Surrogate;
