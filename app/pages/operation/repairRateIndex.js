import React, { Component } from 'react';
import { api, keys } from '../../runtime/api';
import {
    Text,
    View,
    Br,
    Button,
    TextInput,
    TouchableHighlight,
    ScrollView,
    ActivityIndicator,
    HorizontalLayout,
    VerticalLayout,
    Image
} from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack, historyStack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import * as actions from '../../runtime/action';

const RepairRateIndex = pageWrapper(
    class extends Component {
        constructor() {
            super();
            this.state = {
                loading: true
            };
        }

        async viewWillAppear() {
            const { zoneLevel, zoneIds } = this.props;
            const res = await this.props.dispatch(
                actions.fetchRepairRate({
                    startDate: this.handleDate(7),
                    endDate: this.handleDate(1),
                    zoneLevel,
                    zoneIds,
                    prefix: 'rateIndex'
                })
            );
            this.setState({
                loading: false
            });
        }

        handleDate(num) {
            const ff = new Date();
            ff.setDate(ff.getDate() - num);
            return `${ff.getFullYear()}-${ff.getMonth() + 1}-${ff.getDate()}`;
        }

        /**
         * 返回
         */
        handleGoBack() {
            JumpBack();
        }

        render() {
            if (this.state.loading) return this.renderLoading();
            return this.renderList();
        }

        renderLoading() {
            return (
                <View style={styles.container}>
                    <ActionBar title="报修率" handleBack={this.handleGoBack.bind(this)}>
                        返回
          </ActionBar>
                    <View
                        style={{
                            position: 'absolute',
                            flexDirection: 'column',
                            left: 0,
                            top: 44,
                            right: 0,
                            bottom: 0,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <ActivityIndicator size="large" src="loading" />
                        <HorizontalLayout style={{ marginTop: 10 }}>
                            <Text>正在加载 ...</Text>
                        </HorizontalLayout>
                    </View>
                </View>
            );
        }

        renderList() {
            const { repairRateList } = this.props;
            return (
                <View style={styles.container}>
                    <ActionBar title="报修率" handleBack={this.handleGoBack.bind(this)}>
                        返回
                    </ActionBar>
                    <VerticalLayout style={{ flex: 1 }}>
                        <HorizontalLayout
                            style={{ ...styles.list, height: 42, backgroundColor: '#f8f8f8' }}
                        >
                            <Text style={styles.one}>请选择日期</Text>
                        </HorizontalLayout>
                        {Object.keys(repairRateList).map((item, index) => {
                            return (
                                <VerticalLayout key={index} style={{ backgroundColor: '#fff' }}>
                                    <TouchableHighlight
                                        onPress={() => {
                                            this.props.zoneLevel === 'city'
                                                ? JumpTo('repairRateSelectCity', true, { date: item })
                                                : JumpTo('repairRateSelectChild', true, { date: item });
                                        }}
                                    >
                                        <HorizontalLayout style={styles.list}>
                                            <HorizontalLayout style={styles.one}>
                                                <Text>{item}</Text>
                                            </HorizontalLayout>
                                            <HorizontalLayout style={styles.three}>
                                                <Image style={styles.arrow} src="arrow" />
                                            </HorizontalLayout>
                                        </HorizontalLayout>
                                    </TouchableHighlight>
                                </VerticalLayout>
                            );
                        })}
                    </VerticalLayout>
                </View>
            );
        }
    }
);

const mapStateToProps = state => {
    return {
        zoneLevel: state.api.userInfo.operation_level,
        zoneIds: state.api.zone_ids,
        repairRateList: state.api.rateIndexRepairRateList
    };
};
const mapDispatchToProps = dispatch => {
    return {
        dispatch
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(RepairRateIndex);

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    },
    list: {
        borderBottomWidth: 1,
        borderBottomColor: '#c6c6c6',
        borderBottomStyle: 'solid',
        justifyContent: 'space-between',
        height: 54,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 20
    },
    one: {
        flexGrow: 1,
        flexBasis: 0
    },
    two: {
        flexGrow: 0.7,
        justifyContent: 'flex-start',
        flexBasis: 0
    },
    three: {
        alignItems: 'flex-end'
    },
    arrow: {
        width: 8,
        height: 14
    }
};
