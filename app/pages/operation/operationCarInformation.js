import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys } from '../../runtime/api';
import { Text, View, Br, Button, TextInput, TouchableHighlight } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import styles from '../../css/operation/carSearchCss';

var moment = require('moment');

/**
 * 运营车牌信息
 */
class OperationCarInformation extends Component {

    constructor(){
        super();
        this.state={
            loading: true,
        };
    }

    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }

    /**
     * 更改解锁码
     */
    /*handleChangeUnlockCode() {
        JumpTo('operation/changeUnlockCode', true, {carMessage: this.props.operationCarSearch});
    }*/

    render() {
        const carMessage = this.props.operationCarSearch;
        const activeInfo = carMessage.active_info || {};
        const recordInfo = carMessage.record_info || {};
        return (
            <base.View style={styles.container}>
                <ActionBar title="车辆信息" handleBack={this.handleGoBack.bind(this) }>返回</ActionBar>
                <base.VerticalLayout style={styles.list}>
                    {activeInfo.is_active ? null :
                        <base.HorizontalLayout style={styles.item}>
                            <Text style={styles.text2}>车辆状态：</Text>
                            <Text style={{...styles.status, color: '#999', borderColor: '#999'}}>未激活车辆</Text>
                        </base.HorizontalLayout>}
                    <base.HorizontalLayout style={styles.item}>
                        <Text style={styles.text2}>车牌号：</Text>
                        <Text style={styles.text4}>{carMessage.bicycle}</Text>
                    </base.HorizontalLayout>
                    <base.HorizontalLayout style={{...styles.item, justifyContent: 'space-between', }}>
                        <base.HorizontalLayout style={{alignItems: 'center'}}>
                            <Text style={styles.text2}>解锁码：</Text>
                            <Text style={styles.text4}>{carMessage.unlock_code}</Text>
                        </base.HorizontalLayout>
                        {/*{!activeInfo.is_active ? null :
                            <Button onPress={this.handleChangeUnlockCode.bind(this)} style={{...styles.text2, color: '#ff7000'}}>更改解锁码＞</Button>}*/}
                    </base.HorizontalLayout>
                    {activeInfo.is_active ?
                        <base.HorizontalLayout style={styles.item}>
                            <Text style={styles.text2}>车辆状态：</Text>
                            <Text style={styles.status2}>{carMessage.broken_info ? '故障车辆' : '正常'}</Text>
                        </base.HorizontalLayout> : null}
                    <base.HorizontalLayout style={styles.item}>
                        <Text style={styles.text2}>{activeInfo.is_active ? '激活人：' : '组装录入：'}</Text>
                        <Text style={styles.text4}>{activeInfo.is_active ? activeInfo.user_name : recordInfo.user_name}</Text>
                    </base.HorizontalLayout>
                    <base.HorizontalLayout style={styles.item}>
                        <Text style={styles.text2}>{activeInfo.is_active ? '激活时间：' : '录入时间：'}</Text>
                        <Text style={styles.text4}>{activeInfo.is_active ? activeInfo.active_time : recordInfo.record_time}</Text>
                    </base.HorizontalLayout>
                </base.VerticalLayout>
            </base.View>);
    }
}

const mapStateToProps = state=> {
    return {
        operationCarSearch: state.api.operationCarSearch || {},
    };
};
const mapDispatchToProps = dispatch=> {
    return {
        dispatch,
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(OperationCarInformation));
