import {connect} from 'react-redux';
import OrderList from './operationOrderDetailCommon';
import {JumpTo, JumpBack, historyStack} from '../../modules';
import {getUserInfo} from '../../runtime/api';
import {
    fetchSocietyDetail
} from '../../runtime/action';

const mapStateToProps = (state)=> {
        return {
            schoolId: historyStack.get().schoolId,
            pageTitle: '今日订单量排行',
            pane: [{text: '城市'}, {text: '校园'}],
            dataSource: [
                {
                    list: beautifyList(state.api.societyCityList || [])
                },
                {
                    list: state.api.societyZoneList || []
                }
            ]
        };
    },
    mapDispatchToProps = dispatch=> {
        return {
            dispatch,
            fetch: props=> dispatch(fetchSocietyDetail(props))
        };
    };

export default connect(mapStateToProps, mapDispatchToProps)(OrderList);
function beautifyList(list) {
    if (!list)
        return [];

    return list.map(v=> {
        return {
            ...v,
            onPress(){
                if(v.id == 'other'){
                    return;
                }

                getUserInfo().then(userInfo=> {
                    let opt = {id: v.id, title: v.title}, historyState = historyStack.get();
                    if (historyState && historyState.shouldRaiseLevel) {
                        if (!userInfo['__origin_operation_level']) {
                            userInfo['__origin_operation_level'] = userInfo['operation_level'];
                            userInfo['operation_level'] = 'country';
                            opt.shouldLowerLevel = true;
                        }
                    }
                    JumpTo('operation/dashBoard/society', true, opt);

                });
            }
        };
    });
}

