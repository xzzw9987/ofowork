import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, parseNum } from '../../runtime/api';
import { Text, View, Br, Button, ScrollView, TouchableHighlight, ActivityIndicator } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';

var moment = require('moment');

/**
 * 订单详情
 */
class OperationOrderDetail extends Component {

    constructor(){
        super();
        this.state={
            cityTotalOrder: 0,
            parentZoneOrderSummary: undefined,

        };
    }

    componentWillMount() {
        let zoneOrderSummary = this.props.zoneOrderSummary[this.props.zoneIndex || 0];
        this.setState({parentZoneOrderSummary: zoneOrderSummary});
        // 获取子区域列表
        this.props.dispatch(api(keys.zoneChildZoneList, {token: this.props.token, zone_id: zoneOrderSummary.zoneId, child_level: zoneOrderSummary.childLevel, current_level: zoneOrderSummary.currentLevel}));

        if(zoneOrderSummary.currentLevel === 'city') {
            this.props.dispatch(api(keys.zoneCityTodayOrderList, {token: this.props.token, zone_ids: zoneOrderSummary.zoneId, level: 'city'}));
        }
    }

    componentWillReceiveProps(nextProps){
        if(typeof nextProps.zoneOrderSummary !== undefined && nextProps.zoneOrderSummary != this.props.zoneOrderSummary) {
            let zoneOrderSummary = nextProps.zoneOrderSummary[this.props.zoneIndex || 0];
            // 获取子区域列表
            this.props.dispatch(api(keys.zoneChildZoneList, {token: this.props.token, zone_id: zoneOrderSummary.zoneId, child_level: zoneOrderSummary.childLevel, current_level: zoneOrderSummary.currentLevel}));
        }

        // 更新子区域列表
        if(typeof nextProps.childZoneList !== undefined && nextProps.childZoneList != this.props.childZoneList) {
            let zoneOrderSummary = this.props.zoneOrderSummary[this.props.zoneIndex || 0];
            // 获取特定区域内今日订单量
            let zone_ids = nextProps.childZoneList.map((item) => { return item.zone_id; }).join(',');
            this.props.dispatch(api(keys.zoneTodayOrderList, {token: this.props.token, zone_ids, level: zoneOrderSummary.childLevel}));
        }

        // 更新城市今日订单
        if(typeof nextProps.zoneCityTodayOrderList !== undefined && nextProps.zoneCityTodayOrderList != this.props.zoneCityTodayOrderList) {
            console.log('OperationOrderDetail componentWillReceiveProps', nextProps.zoneCityTodayOrderList);
        }
    }

    /**
     * 返回
     */
    handleGoBack() {
        if(this.props.zoneOrderSummary.currentLevel === 'city') {
            let parentZoneOrderSummary = this.state.parentZoneOrderSummary;
            this.props.dispatch({type: 'zoneOrderSummary', data: parentZoneOrderSummary});
            // this.props.dispatch(api(keys.zoneChildZoneList,{token: this.props.token, zone_id: parentZoneOrderSummary.zoneId, child_level: parentZoneOrderSummary.childLevel, current_level: parentZoneOrderSummary.currentLevel}));
        } else {
            JumpBack();
        }
    }

    /**
     * 历史订单
     */
    handleOrderHistory() {
        let zoneOrderSummary = this.props.zoneOrderSummary[this.props.zoneIndex || 0];
        // 默认取当前所属权限的历史订单
        let zone_id = zoneOrderSummary.zoneId;
        let level = zoneOrderSummary.currentLevel;
        let zone_name = zoneOrderSummary.zoneName;

        this.props.dispatch({type: 'zoneHistoryLevel', data: {zone_name, zone_id, level}});
        JumpTo('operation/orderHistory', true);
    }

    handleOrderDetailOrHistory(zone_name, zone_id, level) {

        if(level === 'city') {
            let zoneIds = this.props.zone_ids;
            let zoneOrderList = this.props.zoneTodayOrderList.filter((item) => { return item.area_id == zone_id; });
            let orderNum = zoneOrderList ? zoneOrderList[0].num : 0;
            let zoneName = this.props.childZoneList.filter((item) => { return item.zone_id == zone_id; })[0].zone_name;

            let currentLevel = 'city';
            let childLevel = 'school';

            let zoneOrderSummary = [];
            zoneOrderSummary.push({
                zoneId: zone_id,
                zoneIds: zoneIds,
                zoneName,
                rank: undefined,
                orderNum,
                currentLevel,
                childLevel
            });
            this.props.dispatch({type: 'zoneOrderSummary', data: zoneOrderSummary});

            // 获取子区域列表
            // this.props.dispatch(api(keys.zoneChildZoneList,{token: this.props.token, zone_id: zoneOrderSummary.zoneId, child_level: zoneOrderSummary.childLevel, current_level: zoneOrderSummary.currentLevel}));

        } else {
            this.props.dispatch({type: 'zoneHistoryLevel', data: {zone_name, zone_id, level}});
            JumpTo('operation/orderHistory', true);
        }
    }

    render() {
        let zoneTodayOrderList = this.props.zoneTodayOrderList;
        let zoneOrderSummary = this.props.zoneOrderSummary[this.props.zoneIndex || 0];
        if(!zoneTodayOrderList || !zoneOrderSummary) {
            return (
                <base.View style={{position: 'absolute', flexDirection: 'column', left: 0, top: 0, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center'}}>
                    <ActivityIndicator size='large' src='loading'/>
                    <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                </base.View>
            );
        } else {
            return this.renderOperationPage(zoneOrderSummary);
        }
    }

    renderOperationPage(zoneOrderSummary) {
        return (
            <base.View style={styles.container}>
                <ActionBar title="今日订单量" handleBack={this.handleGoBack.bind(this)} rightAction={this.handleOrderHistory.bind(this)}  rightItem="历史">返回</ActionBar>
                <base.VerticalLayout style={styles.list}>

                    <base.VerticalLayout style={styles.top}>
                    {this.props.zoneOrderSummary.map(((item, idx) => {
                        return (
                            <TouchableHighlight  key={idx}>
                                <base.VerticalLayout>
                                    <base.HorizontalLayout style={Object.assign({}, styles.cbox, styles.mb10)}>
                                        <Text style={{fontSize: 16, color: '#000'}}>{item.zoneName}</Text>
                                        <base.HorizontalLayout style={styles.mingci}> <Text style={styles.mingText}>第{item.rank}名</Text></base.HorizontalLayout>
                                    </base.HorizontalLayout>
                                    <base.HorizontalLayout style={styles.cbox}><Text style={styles.num}>{parseNum(item.orderNum)}</Text></base.HorizontalLayout>
                                </base.VerticalLayout>
                            </TouchableHighlight>
                        );
                    }))}
                    </base.VerticalLayout>
                    <Br/>
                 <base.VerticalLayout style={styles.scrollBorder}></base.VerticalLayout>
                 <ScrollView className="overflowY" style={styles.listView}>

                        {this.props.zoneTodayOrderList.map(((item, idx) => {
                            let zone = this.props.childZoneList.filter((i) => {
                                return i.zone_id === item.area_id;
                            })[0];
                            let zoneName = zone ? zone.zone_name : '--';
                            return (
                                <TouchableHighlight key={idx} style={styles.items} className="itemActive" onClick={this.handleOrderDetailOrHistory.bind(this, this.props.childZoneList[idx] ? this.props.childZoneList[idx].zone_name : '--', item.area_id, zoneOrderSummary.childLevel)} underlayColor={'#ccc'}>
                                    <base.HorizontalLayout style={styles.item}>
                                        <base.HorizontalLayout  style={styles.p10}>
                                                <base.HorizontalLayout style={styles.p10}><Text>{idx + 1}</Text></base.HorizontalLayout>
                                                <Text style={{fontSize: 16, color: '#000'}}>{zoneName}</Text>
                                         </base.HorizontalLayout>
                                         <base.HorizontalLayout style={styles.rightBox}>
                                            <base.HorizontalLayout style={styles.p10}><Text style={{fontSize: 16, color: '#000'}}>{parseNum(item.num)}</Text></base.HorizontalLayout>
                                            <base.Image style={styles.arrow} src='arrow'></base.Image>
                                         </base.HorizontalLayout>
                                     </base.HorizontalLayout>
                                </TouchableHighlight>);
                        }))}
                        {this.props.zoneCityTodayOrderList && this.props.zoneOrderSummary[this.props.zoneIndex || 0].currentLevel === 'city' ?
                            <TouchableHighlight style={styles.items}>
                            <base.HorizontalLayout style={styles.item}>
                                <base.HorizontalLayout  style={styles.p10}>
                                    <base.HorizontalLayout style={styles.p10}>
                                        <Text style={{fontSize: 16, color: '#000'}}>*</Text>
                                    </base.HorizontalLayout>
                                    <Text style={{fontSize: 16, color: '#000'}}>城市订单</Text>
                                </base.HorizontalLayout>
                                <Text style={{fontSize: 16, color: '#000'}}>{parseNum(this.props.zoneCityTodayOrderList[0].num)}</Text>
                            </base.HorizontalLayout>
                        </TouchableHighlight> : ''}

                   </ScrollView>
               </base.VerticalLayout>
            </base.View>
        );
    }

}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
    /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    cbox: {
        justifyContent: 'center',
    },
    color2: {
        color: '#7f7f7f',
    },
    p10: {
        paddingRight: 10
    },
    list: {
        flexGrow: 1
    },
    listView: {
        flexGrow: 1,
    },
    scrollBorder: {
        borderStyle: 'solid',
        borderTopColor: '#c6c6c6',
        borderTopWidth: 1,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    top: {
        backgroundColor: '#fff',
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        paddingTop: 20,
        paddingBottom: 20,
        alignItems: 'center',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    mingci: {
        paddingLeft: 6,
        paddingRight: 6,
        height: 17,
        backgroundColor: '#696969',
        borderRadius: 2,
        alignItems: 'center',
        marginLeft: 10,
    },
    btnBox: {
        marginTop: 10,
        alignItems: 'center',
    },
    num: {
        color: '#ff8300',
        fontSize: 26,
    },
    mingText: {
        fontSize: 12,
        color: '#fff'
    },
    detailBtn: {
        width: 144,
        height: 35,
        backgroundColor: '#ffd900',
        borderRadius: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
  /*item */
    items: {
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        height: 60,
        position: 'relative',
        backgroundColor: '#fff',
        paddingRight: 10,
        paddingLeft: 10,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    item: {
        height: 60,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    mb10: {
        marginBottom: 5,
        alignItems: 'center',
    },
    arrow: {
        width: 8,
        height: 14,
    },
    rightBox: {
        alignItems: 'center',
    }
};

export default connect((state) => {
    return {
        token: state.api.token,
        userInfo: state.api.userInfo,
        zone_ids: state.api.zone_ids,
        zone_names: state.api.zone_names,
        zoneIndex: state.user.zoneIndex,
        childZoneList: state.api.childZoneList,
        zoneTodayOrderList: state.api.zoneTodayOrderList,
        zoneCityTodayOrderList: state.api.zoneCityTodayOrderList,
        zoneOrderSummary: state.user.zoneOrderSummary
    };
})(OperationOrderDetail);
