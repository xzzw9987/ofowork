import React, {Component} from 'react';
import createTab from '../../modules/createTab';
import WorkBench from './operationBench';
import ProfileIndex from '../profile/profileIndexWithoutBack';
import {historyStack} from '../../modules';
//import {Index} from historyStack.get().page;

export default class extends Component {
    constructor(...args) {
        super(...args);
        let Index = historyStack.get().page;
        this.tabContainer = createTab([
            {
                icon: require('../../public/images/workIndex.png'),
                selectedIcon: require('../../public/images/workIndexActive.png'),
                title: '首页',
                view: <Index/>
            },
            {
                icon: require('../../public/images/workBench.png'),
                selectedIcon: require('../../public/images/workBenchActive.png'),
                title: '工作台',
                view: <WorkBench/>
            },
            {
                icon: require('../../public/images/self.png'),
                selectedIcon: require('../../public/images/selfActive.png'),
                title: '我',
                view: <ProfileIndex/>
            }
        ]);
    }

    render() {
        const TabContainer = this.tabContainer;
        return <TabContainer {...this.props}/>;
    }
}
