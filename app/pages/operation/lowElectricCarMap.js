import React, { Component } from 'react';
import * as base from '../../base';
import { Map, ActivityIndicator } from '../../base';
import { apibase, getUserInfo } from '../../runtime/api';
import { JumpTo, JumpBack, fetch } from '../../modules';
import pageWrapper from '../../modules/pageWrapper';
var StaticImage = require('../../base/staticImages');
import whereami from '../../base/map/whereami';
import platform from '../../modules/platform';

/**
 * 沉默车辆、低电车辆
 */

const LowElectricCar = pageWrapper(class extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            dataLoading: true,
            region: {
                latitude: 39.979024,
                longitude: 116.312589,
                latitudeDelta: 0.05,
                longitudeDelta: 0.05,
            },
            annotations: [],
            type: 'silent',
            clusterMode: true,
        };
    }

    viewWillAppear() {
        this.handleGetData(this.state.type);
        this.setState({ loading: false });
    }

    //获取数据
    handleGetData(type) {
        let promiseList = [];
        let reportAnnotations = [];
        let url;
        whereami().then(e => {
            let latitude = e.latitude;
            let longitude = e.longitude;
            this.setState({
                region: {
                    latitude: latitude,
                    longitude: longitude,
                    latitudeDelta: 0.05,
                    longitudeDelta: 0.05
                }
            });
        });
        return getUserInfo()
            .then(userInfo => {
                let { token } = userInfo;
                url = (type == 'silent') ? `${apibase}/i-lock/silent` : `${apibase}/i-lock/monitor`;
                promiseList.push(fetch(url, {
                    method: 'POST',
                    body: { token }
                }).then(response => {
                    if (response['code'] != 0 || response['data']['list'].length == 0) {
                        return [];
                    } else {
                        if(type == 'low'){
                            let size = { width: 24, height: 27 };
                            let icon;
                            response['data']['list'].map((item, index) => {
                                if(item.battery <= 15){
                                    icon = StaticImage['lowElectric1'];
                                } else if(item.battery > 15 && item.battery <= 30)
                                {
                                    icon = StaticImage['lowElectric2'];
                                } else {
                                    icon = StaticImage['lowElectric3'];
                                }
                                reportAnnotations.push({
                                    key: `${index}`,
                                    latitude: parseFloat(item.lat),
                                    longitude: parseFloat(item.lng),
                                    icon: icon,
                                    size: size,
                                    carNo: item.bicycle_no,
                                    battery: item.battery,
                                    lockType: item.lock_type,
                                    time: item.time || '',
                                    number: item.lock_type.substring(0,1),
                                });
                            });
                        }
                        if(type == 'silent') {
                            let size = { width: 30, height: 31 };
                            let icon = StaticImage['badCarIcon'];
                            response['data']['list'].map((item, index) => {
                                reportAnnotations.push({
                                    key: `${index}`,
                                    latitude: parseFloat(item.lat),
                                    longitude: parseFloat(item.lng),
                                    icon: icon,
                                    size: size,
                                    silentTime: item.silent_time,
                                    carNo: item.car_no,
                                    lastOrderTime: item.last_order_time,
                                    lockType: item.lock_type,
                                    number: item.lock_type.substring(0,1),
                                });
                            });
                        }
                        return reportAnnotations;
                    }
                }));
                Promise.all(promiseList).then(response => {
                    this.setState({ annotations: response[0], dataLoading: false });
                });
            });
    }

    //封禁低电车辆切换
    handleSwitchType(type) {
        this.setState({ type: type, annotations: [], dataLoading: true});
        if(type == 'silent'){
            this.setState({clusterMode: true});
        } else {
            this.setState({clusterMode: false});
        }
        this.handleGetData(type);
    }

    handleBack() {
        JumpBack();
    }

    //点击小图标
    onAnnotationClicked(key) {
        let info = this.state.annotations[key];
        JumpTo('operation/lowElectricCar/detail', true, { info: info, type: this.state.type });
    }

    render() {
        let top = platform.OS === 'ios' ? 20 : 0;
        return (
            <base.View style={{ paddingTop: top, ...styles.container }} className="wrap">
                <base.HorizontalLayout style={styles.topNav}>
                    <base.HorizontalLayout style={styles.vcBox} >
                        <base.Button style={styles.backBtn} onPress={this.handleBack.bind(this)}>
                            <base.Text style={{ fontSize: 15, color: '#000' }}>返回</base.Text>
                        </base.Button>
                    </base.HorizontalLayout>
                    {this.renderPane()}
                </base.HorizontalLayout>
                <base.VerticalLayout style={{ flex: 1 }}>
                    {this.renderMap()}
                </base.VerticalLayout>

                {this.state.dataLoading ?
                    <base.View style={{ position: 'absolute', flexDirection: 'column', left: 0, top: 0, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,.5)' }}>
                        <ActivityIndicator size='large' src='loading' />
                        <base.HorizontalLayout style={{ marginTop: 10 }}><base.Text>正在加载 ...</base.Text></base.HorizontalLayout>
                    </base.View>
                    : null
                }
            </base.View>
        );
    }

    renderMap() {
        return this.state.loading ? null :
            (<Map style={{ flex: 1 }} region={this.state.region} clusterMode={this.state.clusterMode}
                 annotations={this.state.annotations}
                 onAnnotationClicked={this.onAnnotationClicked.bind(this)}/>);
    }

    renderPane() {
        return (
            <base.HorizontalLayout style={styles.paneInner}>
                <base.HorizontalLayout style={{ flex: 1, justifyContent: 'center', borderTopLeftRadius: 6, borderBottomLeftRadius: 6, backgroundColor: (this.state.type === 'silent') ? '#ffd900' : '#fff' }}>
                    <base.Button clickDuration={0} style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                            onPress={() => { this.handleSwitchType('silent'); }}>
                        <base.Text>沉默车辆</base.Text>
                    </base.Button>
                </base.HorizontalLayout>
                <base.HorizontalLayout style={{ flex: 1, justifyContent: 'center', borderTopRightRadius: 6, borderBottomRightRadius: 6, backgroundColor: (this.state.type === 'low') ? '#ffd900' : '#fff' }}>
                    <base.Button clickDuration={0} style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                            onPress={() => { this.handleSwitchType('low'); }}>
                        <base.Text>低电车辆</base.Text>
                    </base.Button>
                </base.HorizontalLayout>
            </base.HorizontalLayout>);
    }
});

export default LowElectricCar;

const styles = {
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff'
    },
    topNav: {
        height: 44,
        backgroundColor: '#fff',
        position: 'relative',
        borderBottomColor: '#ddd',
        borderBottomWidth: 1,
        borderBottomStyle: 'solid',
        flexShrink: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    vcBox: {
        alignItems: 'center',
        height: 44,
        position: 'absolute',
        left: 0,
        justifyContent: 'center',
    },
    backBtn: {
        paddingLeft: 10,
        paddingRight: 15,
        height: 44,
        alignItems: 'center',
        justifyContent: 'center',
    },
    paneInner: {
        width: 200,
        height: 30,
        borderRadius: 6,
        overflow: 'hidden',
        borderStyle: 'solid',
        borderColor: '#ffd900',
        borderWidth: 1,
    },
};


