import React, { Component } from 'react';
import { connect } from 'react-redux';
import DashBoard from './operationDashBoardCommon';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { getUserInfo } from '../../runtime/api';

import { fetchRegionIndex } from '../../runtime/action';

class Surrogate extends Component {
    constructor(...args) {
        super(...args);
        this.connectedComponent = getConnectComponent(
            parseInt(100000 * Math.random()),
            historyStack.get()
        );
    }

    render() {
        let ConnectComponent = this.connectedComponent;
        return <ConnectComponent {...this.props} />;
    }
}

function getConnectComponent(random, historyState) {
    const mapStateToProps = state => {
        return {
            historyState,
            total: state.api[`${random}regionOrderTotal`] || 0,
            titleOfTotal: '今日订单量',
            delta: state.api[`${random}regionOrderDelta`] || 0,
            weekDelta: state.api[`${random}regionWeekOrderDelta`] || 0,
            deltaAmount: state.api[`${random}regionOrderDeltaAmount`] || 0,
            weekAmount: state.api[`${random}regionOrderWeekAmount`] || 0,
            yesterdayOrder: state.api[`${random}regionYesterdayOrder`] || 0,
            lastWeekOrder: state.api[`${random}regionLastWeekOrder`] || 0,
            chart: state.api[`${random}regionChart`] || [],
            defaultSelectedChart: state.api[`${random}regionChartDefault`] || [],
            left: {
                text: state.api.userInfo
                    ? state.api.userInfo['operation_level'] == 'region'
                        ? '今日排行榜'
                        : '今日订单详情'
                    : null,
                onPress() {
                    let {
            title = state.api.userInfo['zone_names'],
                        id = state.api.userInfo['zone_ids']
          } = historyState || {};
                    JumpTo('operation/orderDetailRegion', true, {
                        title,
                        id,
                        shouldRaiseLevel: !historyState ||
                        (historyState && historyState.isFirstPage)
                    });
                    console.log('今日排行榜');
                }
            },
            right: {
                text: '查看历史',
                onPress() {
                    console.log('查看历史');
                    let {
            title = state.api.userInfo['zone_names'],
                        id = state.api.userInfo['zone_ids']
          } = historyState || {};
                    JumpTo('operation/orderHistory', true, {
                        id,
                        level: 'region',
                        name: title,
                        title
                    });
                }
            },
            pageTitle: (() => {
                if (!state.api.userInfo) return null;
                let {
          title = state.api.userInfo['zone_names'],
                    id = state.api.userInfo['zone_ids']
        } = historyState || {};
                return title;
            })(),
            handleBack(props) {
                let dispatch = props.dispatch, s = historyState;
                getUserInfo().then(userInfo => {
                    if (s && s.shouldLowerLevel) {
                        userInfo['operation_level'] = userInfo['__origin_operation_level'];
                        userInfo['__origin_operation_level'] = null;
                        dispatch({ type: 'setUserInfo', payload: userInfo });
                    }
                });
            }
        };
    },
        mapDispatchToProps = dispatch => {
            return {
                dispatch,
                fetch: props => dispatch(fetchRegionIndex({ ...props, random }))
            };
        };
    return connect(mapStateToProps, mapDispatchToProps)(DashBoard);
}

export default Surrogate;
