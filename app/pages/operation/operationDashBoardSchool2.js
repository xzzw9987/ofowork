import React, {Component} from 'react';
import {connect} from 'react-redux';
import DashBoard from './operationDashBoardCommon';
import {JumpTo, historyStack, JumpBack} from '../../modules';
import {getUserInfo} from '../../runtime/api';

import {
    fetchSchool2Index as fetchSchoolIndex
} from '../../runtime/action';

class Surrogate extends Component {
    constructor(...args) {
        super(...args);
        this.connectedComponent = getConnectComponent(parseInt(100000 * Math.random()), historyStack.get());
    }

    render(){
        let ConnectComponent = this.connectedComponent ;
        return <ConnectComponent {...this.props}/>;
    }
}


function getConnectComponent(random, historyState) {
    const mapStateToProps = state=> {
            return {
                total: state.api[`${random}schoolOrderTotal`] || 0,
                titleOfTotal: '今日订单量',
                delta: state.api[`${random}schoolOrderDelta`] || 0,
                deltaAmount: state.api[`${random}schoolOrderDeltaAmount`] || 0,
                chart: state.api[`${random}schoolChart`] || [],
                defaultSelectedChart: state.api[`${random}schoolChartDefault`] || [],
                /*left: {
                    text: state.api.userInfo ?
                        (state.api.userInfo['operation_level'] == 'region' ? '今日排行榜' : '今日订单详情')
                        : null,
                    onPress(){
                        let {title = state.api.userInfo['zone_names'], id = state.api.userInfo['zone_ids']} = historyState || {};
                        JumpTo('operation/orderDetailRegion', true, {
                            title,
                            id,
                            shouldRaiseLevel: !historyState
                        });
                        console.log('今日排行榜')
                    }
                },*/
                right: {
                    text: '查看历史',
                    onPress(){
                        console.log('查看历史');
                    }
                },
                pageTitle: (()=> {
                    if (!state.api.userInfo)
                        return null;
                    let {title = state.api.userInfo['zone_names'], id = state.api.userInfo['zone_ids']} = historyState || {};
                    return title;
                })(),
                handleBack(props){
                    let dispatch = props.dispatch,
                        s = historyState;
                    getUserInfo().then(userInfo=> {
                        if (s && s.shouldLowerLevel) {
                            userInfo['operation_level'] = userInfo['__origin_operation_level'];
                            userInfo['__origin_operation_level'] = null;
                            console.log(userInfo);
                            dispatch({type: 'setUserInfo', payload: userInfo});
                        }
                    });
                }
            };
        },
        mapDispatchToProps = dispatch=> {
            return {
                dispatch,
                fetch: props=>dispatch(fetchSchoolIndex({...props, random}))
            };
        };
    return connect(mapStateToProps, mapDispatchToProps)(DashBoard);
}

export default Surrogate;
