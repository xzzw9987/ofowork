import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, getUserInfo } from '../../runtime/api';
import { Text, View, Br, Button, TextInput, TouchableHighlight, ScrollView, ActivityIndicator } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack, historyStack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';

var moment = require('moment');

/**
 * 寻迹轨迹日期选择
 */
class MovePathDate extends Component {

    constructor() {
        super();
        this.state = {
            loading: true,
            date: [],
            identity: '',
            operation_level: '',
        };
    }

    viewWillAppear() {
        let today = new Date().getFullYear() + '-' +  (new Date().getMonth() + 1) + '-' + new Date().getDate();
        let dd = new Date();
        dd.setDate(dd.getDate()-59);
        let end = dd.getFullYear() + '-' + (dd.getMonth()+1) + '-' + dd.getDate();
        this.Todo(end,today);
        return getUserInfo().then(userInfo => {
            this.setState({identity: userInfo.identity, operation_level: (userInfo.operation_level ? userInfo.operation_level : '')});
        });
    }

    Todo(begin, end) {
        let ab = begin.split("-");
        let ae = end.split("-");
        let db = new Date();
        db.setFullYear(ab[0], ab[1] - 1, ab[2]);
        let de = new Date();
        de.setFullYear(ae[0], ae[1] - 1, ae[2]);
        let a = [],k = 60;
        for (let i = 0, temp = db; temp <= de; i++) {
            let month = ((temp.getMonth()+1) < 10) ? ('0'+(temp.getMonth()+1)) : (temp.getMonth()+1);
            let day = (temp.getDate() < 10) ? ('0'+(temp.getDate())) : temp.getDate();
            a[i] = temp.getFullYear() + "-" + month + "-" +  day;
            temp.setTime(temp.getTime() + 24 * 60 * 60 * 1000);
            /*if(a[i] == '2017-04-07'){
                k = 60 - i;
            }*/
        }
        this.setState({date: (a.reverse().slice(0,k)), loading: false});
    }

    handleGoWhere(item) {
        if(this.state.identity == 'repairer'){
            JumpTo('movePath/map',true, {date: item});
        } else {
            JumpTo('movePath/fence',true, {operation_level: this.state.operation_level, date: item});
        }
    }

    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }

    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderList();
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="巡检轨迹" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.VerticalLayout style={{flex: 1}}>
                    <base.HorizontalLayout style={{...styles.list, height: 42,backgroundColor: '#f8f8f8'}}>
                        <base.Text style={styles.one}>请选择日期</base.Text>
                    </base.HorizontalLayout>
                    <base.View style={{backgroundColor: '#fff', position: 'absolute', flexDirection: 'column', left: 0, top: 44, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center' }}>
                        <ActivityIndicator size='large' src='loading' />
                        <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.VerticalLayout>
            </base.View>
        );
    }

    renderList() {
        let list = this.state.date;
        return (
            <base.View style={styles.container}>
                <ActionBar title="巡检轨迹" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.VerticalLayout style={{flex: 1}}>
                    <base.HorizontalLayout style={{...styles.list, height: 42,backgroundColor: '#f8f8f8'}}>
                        <base.Text style={styles.one}>请选择日期</base.Text>
                    </base.HorizontalLayout>
                    <ScrollView style={{backgroundColor: '#fff', flex: 1}}>
                        {
                            list.map((item,index)=>{
                                return (
                                    <TouchableHighlight key={index} onPress={()=>{this.handleGoWhere(item)}}>
                                        <base.HorizontalLayout style={styles.list}>
                                            <base.HorizontalLayout style={styles.one}>
                                                <base.Text>{item}{(index == 0) ? '(今天)' : null}</base.Text>
                                            </base.HorizontalLayout>
                                            <base.HorizontalLayout style={styles.three}>
                                                <base.Image style={styles.arrow} src="arrow" />
                                            </base.HorizontalLayout>
                                        </base.HorizontalLayout>
                                    </TouchableHighlight>
                                );
                            })

                        }
                    </ScrollView>
                </base.VerticalLayout>
            </base.View>
        );
    }
}

export default (pageWrapper(MovePathDate));


const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    list: {
        borderBottomWidth: 1,
        borderBottomColor: '#c6c6c6',
        borderBottomStyle: 'solid',
        justifyContent: 'space-between',
        height: 44,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 20
    },
    one: {
        flexGrow: 1,
        flexBasis: 0,
    },
    two: {
        flexGrow: .7,
        justifyContent: 'flex-start',
        flexBasis: 0,
    },
    three: {
        alignItems: 'flex-end',
    },
    arrow: {
        width: 8,
        height: 14,
    },
};
