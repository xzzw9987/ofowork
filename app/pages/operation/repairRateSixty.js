import React, { Component } from 'react';
import { api, keys } from '../../runtime/api';
import {
    Text,
    View,
    Br,
    Button,
    TextInput,
    TouchableHighlight,
    ScrollView,
    ActivityIndicator,
    HorizontalLayout,
    VerticalLayout,
    Image
} from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack, historyStack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import * as actions from '../../runtime/action';

const RepairRateSixty = pageWrapper(
    class extends Component {
        constructor() {
            super();
            this.state = {
                loading: true,
                historyState: historyStack.get()
            };
        }

        handleGoBack() {
            JumpBack();
        }

        async viewWillAppear() {
            const { zoneLevel, zoneId } = this.state.historyState;
            await this.props.dispatch(
                actions.fetchRepairRate({
                    startDate: this.handleDate(60),
                    endDate: this.handleDate(1),
                    zoneLevel,
                    zoneIds: zoneId,
                    prefix: 'specific'
                })
            );
            this.setState({
                loading: false
            });
        }

        handleDate(num) {
            const ff = new Date();
            ff.setDate(ff.getDate() - num);
            return `${ff.getFullYear()}-${ff.getMonth() + 1}-${ff.getDate()}`;
        }

        render() {
            if (this.state.loading) return this.renderLoading();
            return this.renderList();
        }

        renderLoading() {
            return (
                <View style={styles.container}>
                    <ActionBar title="近60天报修率" handleBack={this.handleGoBack.bind(this)}>
                        返回
          </ActionBar>
                    <View
                        style={{
                            position: 'absolute',
                            flexDirection: 'column',
                            left: 0,
                            top: 44,
                            right: 0,
                            bottom: 0,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <ActivityIndicator size="large" src="loading" />
                        <HorizontalLayout style={{ marginTop: 10 }}>
                            <Text>正在加载 ...</Text>
                        </HorizontalLayout>
                    </View>
                </View>
            );
        }

        renderList() {
            const { repairRateList } = this.props;
            return (
                <View style={styles.container}>
                    <ActionBar title="近60天报修率" handleBack={this.handleGoBack.bind(this)}>
                        返回
          </ActionBar>
                    <VerticalLayout>
                        <HorizontalLayout
                            style={{ ...styles.list, height: 42, backgroundColor: '#f8f8f8' }}
                        >
                            <Text style={styles.one}>
                                {this.state.historyState.zoneName}
                            </Text>
                        </HorizontalLayout>
                    </VerticalLayout>
                    <ScrollView className="overflowY" style={styles.listView}>
                        {Object.keys(repairRateList).map((item, idx) => {
                            return (
                                <VerticalLayout key={idx} style={{ backgroundColor: '#fff' }}>
                                    <HorizontalLayout style={styles.list}>
                                        <HorizontalLayout style={styles.one}>
                                            <Text>{item}</Text>
                                        </HorizontalLayout>
                                        <HorizontalLayout style={styles.two}>
                                            <Text style={{ color: '#ff9000' }}>
                                                {repairRateList[item][0].repair_rate}
                                            </Text>
                                        </HorizontalLayout>
                                    </HorizontalLayout>
                                </VerticalLayout>
                            );
                        })}
                    </ScrollView>
                </View>
            );
        }
    }
);

const mapStateToProps = state => {
    return {
        repairRateList: state.api.specificRepairRateList
    };
};
const mapDispatchToProps = dispatch => {
    return {
        dispatch
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(RepairRateSixty);

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    },
    listView: {
        flex: 1
    },
    list: {
        borderBottomWidth: 1,
        borderBottomColor: '#c6c6c6',
        borderBottomStyle: 'solid',
        justifyContent: 'space-around',
        height: 54,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 20
    },
    one: {
        flexGrow: 1,
        flexBasis: 0
    },
    two: {
        flexGrow: 0.7,
        justifyContent: 'flex-start',
        flexBasis: 0
    },
    three: {
        alignItems: 'flex-end'
    },
    arrow: {
        width: 8,
        height: 14
    }
};
