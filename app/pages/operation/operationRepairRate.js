import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys } from '../../runtime/api';
import { Text, View, Br, Button, TextInput, TouchableHighlight, ScrollView, ActivityIndicator } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import styles from '../../css/operation/repairRateCss';

var moment = require('moment');
import {
    fetchOperationRepairRate
} from '../../runtime/action';

/**
 * 运营报修率
 */
class OperationRepairRate extends Component {

    constructor(){
        super();
        this.state={
            loading: true,
            operationZoneId: '',
        };
    }

    viewWillAppear() {
        this.props.fetch({operationZoneId: this.state.operationZoneId}).then(()=>this.setState({loading: false}));
    }

    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }

    /**
     * 切换区域
     */
    handleSwitchZone(operationZoneId){
        this.props.clear();
        this.setState({operationZoneId: operationZoneId});
        this.props.fetch({operationZoneId: operationZoneId}).then(()=>this.setState({loading: false}));
    }

    showWeekCN(dayOfEN) {
        return {
            'Monday': '周一',
            'Tuesday': '周二',
            'Wednesday': '周三',
            'Thursday': '周四',
            'Friday': '周五',
            'Saturday': '周六',
            'Sunday': '周日',
        }[dayOfEN] || '未知';
    }

    render() {
        if (this.state.loading){
            return this.renderLoading();
        }
        return this.renderPage();
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="报修率" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.View style={{ position: 'absolute', flexDirection: 'column', left: 0, top: 44, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator size='large' src='loading' />
                    <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                </base.View>
            </base.View>
        );
    }

    renderPage() {
        let info = this.props.operationZoneInfo || {};
        return (
            <base.View style={styles.container}>
                <ActionBar title="报修率" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.HorizontalLayout style={styles.nav}>
                    <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} style={{height: 45}}>
                        <base.HorizontalLayout className="overflowX hiddenY">
                            {this.props.operationZoneNames.map(((item, idx) => {
                                return (
                                    <base.HorizontalLayout key={idx} style={styles.inner}>
                                        <Button clickDuration={0} onPress={()=>{this.handleSwitchZone(item.id);}} key={item.id}
                                                style={(this.props.operationZoneId == item.id) ? Object.assign({}, styles.itemBtn, styles.light) : styles.itemBtn }>
                                            <base.Text style={(this.props.operationZoneId == item.id) ? styles.active : {color: '#7f7f7f'}}>{item.name}</base.Text>
                                        </Button>
                                    </base.HorizontalLayout>);
                            }))}
                        </base.HorizontalLayout>
                    </ScrollView>
                </base.HorizontalLayout>
                {!info.list ?
                    <base.VerticalLayout style={{justifyContent: 'center', alignItems: 'center', flexGrow: 1, flexBasis: 0}}>
                        <ActivityIndicator size='large' src='loading'/>
                        <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.VerticalLayout> : this.renderData(info)
                }
            </base.View>
        );
    }

    renderData(info) {
        return (
            <base.VerticalLayout style={styles.container}>
                <base.HorizontalLayout style={styles.topBox}>
                    <base.VerticalLayout style={styles.leftBox}>
                        <base.HorizontalLayout style={styles.center}>
                            <base.Text style={info.weekRate >= 10 ? styles.num1 : (info.weekRate > 5 ? styles.num2 : styles.num3)}>{info.weekRate}</base.Text>
                            <base.Text style={info.weekRate >= 10 ? styles.rate1 : (info.weekRate > 5 ? styles.rate2 : styles.rate3)}>%</base.Text>
                            <base.HorizontalLayout style={info.weekRate >= 10 ? styles.intro1 : (info.weekRate > 5 ? styles.intro2 : styles.intro3)}>
                                <base.Text style={styles.titleText}>{info.weekRate >= 10 ? '极高' : (info.weekRate > 5 ? '很高' : '正常')}</base.Text>
                            </base.HorizontalLayout>
                        </base.HorizontalLayout>
                        <base.HorizontalLayout style={styles.center}>
                            <base.Text style={{fontSize: 18, color: '#666'}}>近7天报修率</base.Text>
                        </base.HorizontalLayout>
                    </base.VerticalLayout>

                    <base.VerticalLayout style={{flexGrow: 1}}>
                        <base.HorizontalLayout style={styles.center}>
                            <base.Text style={info.monthRate >= 10 ? styles.num1 : (info.monthRate > 5 ? styles.num2 : styles.num3)}>{info.monthRate}</base.Text>
                            <base.Text style={info.monthRate >= 10 ? styles.rate1 : (info.monthRate > 5 ? styles.rate2 : styles.rate3)}>%</base.Text>
                            <base.HorizontalLayout style={info.monthRate >= 10 ? styles.intro1 : (info.monthRate > 5 ? styles.intro2 : styles.intro3)}>
                                <base.Text style={styles.titleText}>{info.monthRate >= 10 ? '极高' : (info.monthRate > 5  ? '很高' : '正常')}</base.Text>
                            </base.HorizontalLayout>
                        </base.HorizontalLayout>
                        <base.HorizontalLayout style={styles.center}>
                            <base.Text style={{fontSize: 18, color: '#666'}}>近30天报修率</base.Text>
                        </base.HorizontalLayout>
                    </base.VerticalLayout>
                </base.HorizontalLayout>

                <base.VerticalLayout style={styles.scrollBorder}></base.VerticalLayout>
                {info.list && info.list.length == 0 ?
                    <base.VerticalLayout style={{flexGrow: 1, flexBasis: 0, justifyContent: 'center', alignItems: 'center'}}>
                        <base.Image style={{width: 70, height: 70, marginBottom: 5}} src='noData'/>
                        <base.Text style={{fontSize: 16, color: '#666'}}>暂无数据</base.Text>
                    </base.VerticalLayout>
                    :
                    <ScrollView className="overflowY" style={styles.listView}>
                        {info.list.map(((item, idx) => {
                            return (
                                <base.HorizontalLayout style={styles.list} key={idx}>
                                    <base.HorizontalLayout>
                                        <base.Text style={{fontSize: 16, color: '#333', marginRight: 15}}>{item.date}</base.Text>
                                        <base.Text style={{fontSize: 16, color: '#666'}}>{this.showWeekCN(moment(item.date).format('dddd'))}</base.Text>
                                    </base.HorizontalLayout>
                                    <base.HorizontalLayout>
                                        <base.Text style={{fontSize: 20, color: '#333'}}>{item.rate}</base.Text>
                                        <base.Text style={{fontSize: 16, color: '#333', alignSelf: 'center'}}>%</base.Text>
                                    </base.HorizontalLayout>
                                </base.HorizontalLayout>
                            );
                        }))}
                    </ScrollView>
                }

            </base.VerticalLayout>
        );
    }
}


const mapStateToProps = state=> {
    return {
        operationZoneId: state.api.operationZoneId || '',
        operationZoneNames: state.api.operationZoneNames || [],
        operationZoneInfo: state.api.operationZoneInfo || {},
    };
};
const mapDispatchToProps = dispatch=> {
    return {
        dispatch,
        fetch: props=>dispatch(fetchOperationRepairRate(props)),
        clear: ()=> {dispatch({type: '@@clear', payload: {
            operationZoneInfo: {},
        }});}
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(OperationRepairRate));

