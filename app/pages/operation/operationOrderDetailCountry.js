import {connect} from 'react-redux';
import OrderList from './operationOrderDetailCommon';
import {JumpTo, historyStack, JumpBack} from '../../modules';
import {
    fetchCountryDetail
} from '../../runtime/action';

const mapStateToProps = (state)=> {
        return {
            userInfo: state.api.userInfo || {},
            pageTitle: '今日订单量排行',
            pane: [{text: '城市'}, {text: '战区'}],
            dataSource: [
                {
                    list: beautifyList(state.api.countryCityList || [])
                },
                {
                    list: beautifyList(state.api.countryZoneList || [])
                }
            ]
        };
    },
    mapDispatchToProps = dispatch=> {
        return {
            dispatch,
            fetch: props=> dispatch(fetchCountryDetail(props))
        };
    };

export default connect(mapStateToProps, mapDispatchToProps)(OrderList);

function beautifyList(list) {
    if (!list)
        return [];

    return list.map(v=> {
        return {
            ...v,
            onPress(){
                /* set history */
                JumpTo(`operation/dashBoard/${v.name}`, true, {id: v.id, title: v.title});
            }
        };
    });
}
