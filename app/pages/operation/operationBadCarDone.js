import React, {Component} from 'react';
import * as base from '../../base';
import {apibase, getUserInfo} from '../../runtime/api';
import {Text, Button, ScrollView, ActivityIndicator, Toast} from '../../base';
import {historyStack, JumpBack, fetch} from '../../modules';
import {ActionBar} from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import geoMessage from '../../base/map/geoMessage';

/**
 * 坏车标记详情
 */

const OperationBadCarDone = pageWrapper(class extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            info: historyStack.get().info,
            address: '',
            message: '',
        };
    }

    viewWillAppear() {
        let info = this.state.info;
        if (info) {
            geoMessage({latitude: info.latitude, longitude: info.longitude}).then(d => {
                this.setState({address: d.address});
            });
        }
        return getUserInfo().then(userInfo => {
            let { token } = userInfo;
            fetch(`${apibase}/broken-bicycle/process-detail`, {
                method: 'POST',
                body: { token, 'id': info.id }
            }).then(respones => {
                this.setState({loading: false, message: respones.data});
            });
        });

    }

    handleBack() {
        JumpBack();
    }

    render() {
        if (this.state.loading) {
            return (
                <base.View className="wrap" style={styles.container}>
                    <ActionBar title="坏车处理" handleBack={this.handleBack.bind(this)}>
                        <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                    </ActionBar>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 44,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='loading'/>
                        <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            );
        } else {
            return this.renderDetail();
        }
    }

    renderDetail() {
        let info = this.state.info || {};
        let message = this.state.message || {};
        return (
            <base.View className="wrap" style={styles.container}>
                <ActionBar title="坏车处理" handleBack={this.handleBack.bind(this)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <ScrollView className="overflow-y">
                    <base.VerticalLayout style={{flex: 1}}>
                        <base.HorizontalLayout style={{justifyContent: 'center'}}>
                            <base.Image className="contain" resizeMode="contain" style={{height: 300, flex: 1}}
                                        src={info.imgPath ? info.imgPath : null}/>
                        </base.HorizontalLayout>

                        <base.HorizontalLayout style={{...styles.data, justifyContent: 'space-between'}}>
                            <base.Text style={{flex: 1, fontSize: 18, color: '#666'}}>收车人:{message.user_name ? message.user_name : null}</base.Text>
                            <base.HorizontalLayout style={{flex: 0.8, alignItems: 'center'}}>
                                <base.Text style={styles.num}>收车数量</base.Text>
                                <base.Text style={{...styles.num, color: '#333'}}>{message.num ? message.num : null}辆</base.Text>
                            </base.HorizontalLayout>
                        </base.HorizontalLayout>

                        <base.HorizontalLayout style={styles.data}>
                            <base.Text style={{fontSize: 18, color: '#666'}}>
                                时间：{message.time ? message.time : null}</base.Text>
                        </base.HorizontalLayout>

                        <base.HorizontalLayout style={{height: 20, backgroundColor: '#f8f8f8'}} />

                        <base.HorizontalLayout style={{...styles.data, justifyContent: 'space-between', borderTopStyle: 'solid', borderTopWidth: 1, borderTopColor: '#d8d8d8'}}>
                            <base.Text style={{flex: 1, fontSize: 18, color: '#666'}}>标记人:{info.label ? info.label : null}</base.Text>
                            <base.HorizontalLayout style={{flex: 0.8, alignItems: 'center'}}>
                                <base.Text style={styles.num}>标记数量</base.Text>
                                <base.Text style={{...styles.num, color: '#333'}}>{info.number ? info.number : null}辆</base.Text>
                            </base.HorizontalLayout>
                        </base.HorizontalLayout>

                        <base.HorizontalLayout style={styles.data}>
                            <base.Text style={{fontSize: 18, color: '#666'}}>
                                标记时间：{info.createTime ? info.createTime : null}</base.Text>
                        </base.HorizontalLayout>

                        <base.HorizontalLayout style={styles.data}>
                            <base.Text style={{fontSize: 18, color: '#666'}}>
                                地址：{this.state.address ? this.state.address : null}</base.Text>
                        </base.HorizontalLayout>

                    </base.VerticalLayout>
                </ScrollView>
            </base.View>
        );
    }

});
export default OperationBadCarDone;

const styles = {
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff'
    },
    topImg: {
        height: 300,
        justifyContent: 'center'
    },
    data: {
        padding: 20,
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#d8d8d8',
    },
    name: {
        fontSize: 18,
        color: '#666',
    },
    num: {
        fontSize: 18,
        color: '#666',
        marginRight: 5,
    },
};



