import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys } from '../../runtime/api';
import { Text, View, Br, Button, TextInput, TouchableHighlight, ActivityIndicator } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import styles from '../../css/operation/carSearchCss';
import Keyboard from '../../modules/keyboard';
import platform from '../../modules/platform';

var moment = require('moment');
import {
    fetchOperationCarSearch
} from '../../runtime/action';

/**
 * 运营车牌查询
 */
class OperationCarSearch extends Component {

    constructor(){
        super();
        this.state={
            loading: false,
            bicycle_num: '',
        };
    }

    /**
     * 返回
     */
    handleBack() {
        this.props.clear();
        JumpBack();
    }

    /**
     * 车牌号删除
     */
    deleteSearch(){
        this.setState({bicycle_num: ''});
        this.props.clear();
    }

    /**
     * 车辆详情
     */
    handleSearchDetail(){
        JumpTo('operation/carInformation', true);
    }

    /**
     * 查询
     */
    handleSearch() {
        this.setState({loading: true});
        this.props.fetch({'bicycle_num': this.state.bicycle_num}).then(()=>this.setState({loading: false}));
    }

    render() {
        return (
            <base.View style={styles.container} onStartShouldSetResponder={() => true} onResponderGrant={() => Keyboard.dismiss()}>
                <ActionBar title="车牌查询" handleBack={this.handleBack.bind(this)}>返回</ActionBar>
                <base.HorizontalLayout  className="input" style={styles.searchBox}>
                    <base.HorizontalLayout style={styles.leftBox}>

                        <base.HorizontalLayout style={styles.searchBtn}>
                            <base.Image style={styles.search} src='search2' />
                        </base.HorizontalLayout>

                        <TextInput style={styles.searchInput} placeholder='请输入查询的车牌号' keyboardType="numeric" value={this.state.bicycle_num}
                                   onChangeText={(text)=>{this.setState({bicycle_num: text});if(text == ''){this.props.clear();}}}
                                   onFocus={()=>{if(this.state.bicycle_num == ''){this.props.clear();}}}/>

                        {this.state.bicycle_num ?
                            <TouchableHighlight style={styles.deleteBtn} onPress={this.deleteSearch.bind(this)}>
                                <base.HorizontalLayout>
                                    <base.Image style={styles.deleteImg} src='deleteIcon' />
                                </base.HorizontalLayout>
                            </TouchableHighlight> : null}
                    </base.HorizontalLayout>

                    <base.HorizontalLayout style={styles.rightBox}>
                        <Button style={styles.rightBtn} onPress={this.handleSearch.bind(this)}>查询</Button>
                    </base.HorizontalLayout>
                </base.HorizontalLayout>

                {this.state.loading ?
                    <base.VerticalLayout style={{justifyContent: 'center', alignItems: 'center', flexGrow: 1, flexBasis: 0}}>
                        <ActivityIndicator size='large' src='loading'/>
                        <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.VerticalLayout>
                    :
                    (!this.props.operationCarSearch ? null : this.renderPage())
                }

            </base.View>
        );
    }

    renderPage(){
        return(
            <base.View style={styles.searchDone}>
                {this.props.activeInfo.is_active == true || this.props.recordInfo.is_record == true ?
                    <base.VerticalLayout style={platform.OS == 'web' ? {flex: 1} : null}>
                        <TouchableHighlight underlayColor={'#fff'} onPress={this.handleSearchDetail.bind(this)}>
                            <base.HorizontalLayout style={{...styles.topBox, justifyContent: 'space-between', }}>
                                <base.HorizontalLayout style={styles.boxMessage}>
                                    {this.props.activeInfo.is_active == true ?
                                        <Text style={styles.status}>已激活车辆</Text>
                                        : <Text style={{...styles.status, color: '#999', borderColor: '#999'}}>未激活车辆</Text>
                                    }
                                    {this.props.activeInfo.is_active == true && this.props.repairStatus == true ?
                                        <Text style={styles.status2}>故障车辆</Text> : null}
                                </base.HorizontalLayout>
                                <Text style={styles.text2}>查看车辆＞</Text>
                            </base.HorizontalLayout>
                        </TouchableHighlight>
                        <base.VerticalLayout style={styles.bottomBox}>
                            <base.HorizontalLayout style={styles.boxMessage}>
                                <Text style={styles.text2}>车牌号：</Text>
                                <Text style={styles.text3}>{this.props.bicycle}</Text>
                            </base.HorizontalLayout>
                            <base.HorizontalLayout style={styles.boxMessage}>
                                <Text style={styles.text2}>解锁码：</Text>
                                <Text style={styles.text3}>{this.props.unlockCode}</Text>
                            </base.HorizontalLayout>
                        </base.VerticalLayout>
                    </base.VerticalLayout> : null
                }

                {this.props.activeInfo.is_active == false && this.props.recordInfo.is_record == false ?
                    <base.VerticalLayout style={styles.bottomBox}>
                        <base.HorizontalLayout>
                            <Text style={{fontSize: 16, color: '#e23000'}}>未在组装厂录入该车牌!</Text>
                        </base.HorizontalLayout>
                    </base.VerticalLayout> : null
                }

                {this.props.operationCarCode != 0 ?
                    <base.VerticalLayout style={styles.bottomBox}>
                        <base.HorizontalLayout>
                            <Text style={{fontSize: 16, color: '#e23000'}}>{this.props.operationCarMessage}</Text>
                        </base.HorizontalLayout>
                    </base.VerticalLayout> : null
                }
            </base.View>
        );
    }
}


const mapStateToProps = state=> {
    let operationCarSearch = state.api.operationCarSearch || {};
    return {
        operationCarSearch: state.api.operationCarSearch,
        repairStatus: operationCarSearch.broken_info || '', //是否故障
        bicycle: operationCarSearch.bicycle || '', //车牌号
        unlockCode: operationCarSearch.unlock_code || '', //解锁码
        activeInfo: operationCarSearch.active_info || {}, //激活信息
        recordInfo: operationCarSearch.record_info || {}, //组装信息
        operationCarCode: state.api.operationCarCode || '',
        operationCarMessage: state.api.operationCarMessage || '',
    };
};
const mapDispatchToProps = dispatch=> {
    return {
        dispatch,
        fetch: props=>dispatch(fetchOperationCarSearch(props)),
        clear: ()=> {dispatch({type: '@@clear', payload: {
            activeInfo: {},
            recordInfo: {},
            repairStatus: {},
            operationCarSearch: {},
            bicycle: '',
            unlockCode: '',
            operationCarCode: '',
            operationCarMessage: ''
        }});}
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(OperationCarSearch));
