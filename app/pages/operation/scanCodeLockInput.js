import React, { Component } from 'react';
import * as base from '../../base';
import { apibase, getUserInfo } from '../../runtime/api';
import { Text, View, Br, Button, Toast, TextInput, TouchableHighlight, ActivityIndicator } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack, fetch } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import styles from '../../css/operation/carSearchCss';
import Keyboard from '../../modules/keyboard';
var moment = require('moment');

/**
 * 车牌查询开锁
 */
class ScanCodeLockInput extends Component {
    constructor(){
        super();
        this.state={
            loading: false,
            type: 'bicycle_no',
            num: '',
            message: '',
        };
    }

    /**
     * 返回
     */
    handleBack() {
        JumpBack();
    }

    /**
     * 车牌号删除
     */
    deleteSearch(){
        this.setState({num: '', message: ''});
    }

    /**
     * 查询
     */
    handleSearch() {
        this.setState({loading: true, message: ''});
        return getUserInfo()
            .then(userInfo => {
                let { token } = userInfo;
                fetch(`${apibase}/bicycle/open-i-lock`, {
                    method: 'POST',
                    body: { token, 'type': this.state.type , 'search': this.state.num}
                }).then(respones => {
                    this.setState({loading: false});
                    if (respones['code'] === 0) {
                        this.setState({message: '开锁成功！'});
                    } else {
                        this.setState({message: respones['message'] + '（开锁失败！）'});
                    }
                });
            });
    }

    render() {
        return (
            <base.View style={styles.container} onStartShouldSetResponder={() => true} onResponderGrant={() => Keyboard.dismiss()}>
                <ActionBar title="扫码开锁" handleBack={this.handleBack.bind(this)}>返回</ActionBar>
                <base.HorizontalLayout  className="input" style={styles.searchBox}>
                    <base.HorizontalLayout style={styles.leftBox}>

                        <base.HorizontalLayout style={styles.searchBtn}>
                            <base.Image style={styles.search} src='search2' />
                        </base.HorizontalLayout>

                        <TextInput style={styles.searchInput} placeholder='请输入查询的车牌号' keyboardType="numeric" value={this.state.num}
                                   onChangeText={(text)=>{this.setState({num: text})}}/>

                        {this.state.num ?
                            <TouchableHighlight style={styles.deleteBtn} onPress={this.deleteSearch.bind(this)}>
                                <base.HorizontalLayout>
                                    <base.Image style={styles.deleteImg} src='deleteIcon' />
                                </base.HorizontalLayout>
                            </TouchableHighlight> : null}
                    </base.HorizontalLayout>

                    <base.HorizontalLayout style={styles.rightBox}>
                        <base.Button style={styles.rightBtn} onPress={this.handleSearch.bind(this)}>开锁</base.Button>
                    </base.HorizontalLayout>
                </base.HorizontalLayout>

                {this.state.loading ?
                    <base.VerticalLayout style={{justifyContent: 'center', alignItems: 'center', flexGrow: 1, flexBasis: 0}}>
                        <ActivityIndicator size='large' src='loading'/>
                        <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.VerticalLayout> : null}
                {this.state.message != '' ?
                    <base.VerticalLayout style={{...styles.bottomBox, borderStyle: 'solid', borderColor: '#c6c6c6', borderBottomWidth: 1, borderTWidth: 1}}>
                        <base.HorizontalLayout>
                            <Text style={{fontSize: 16, color: '#e23000'}}>{this.state.message}</Text>
                        </base.HorizontalLayout>
                    </base.VerticalLayout> : null}

            </base.View>
        );
    }
}

export default pageWrapper(ScanCodeLockInput);
