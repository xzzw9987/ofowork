import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys } from '../../runtime/api';
import { Text, View, Br, Button, TextInput, TouchableHighlight, ScrollView } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import styles from '../../css/operation/repairRateCss';

var moment = require('moment');
import {
    fetchOperationNewUsers
} from '../../runtime/action';

/**
 * 新增用户
 */
class OperationNewUsers extends Component {

    constructor(){
        super();
        this.state={
            loading: true,
            operationZoneId: '',
        };
    }

    viewWillAppear() {
        this.props.fetch({operationZoneId: this.state.operationZoneId}).then(()=>this.setState({loading: false}));
    }
    viewWillDisappear(){
    }

    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }

    /**
     * 切换区域
     */
    handleSwitchZone(operationZoneId){
        this.setState({operationZoneId: operationZoneId});
        this.props.fetch({operationZoneId: operationZoneId}).then(()=>this.setState({loading: false}));
    }

    render() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="新增用户" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.HorizontalLayout style={styles.nav}>
                    <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} style={{height: 45}}>
                        <base.HorizontalLayout className="overflowX hiddenY">
                            {this.props.operationZoneNames.map(((item, idx) => {
                                return (
                                    <base.HorizontalLayout key={idx} style={styles.inner}>
                                        <Button clickDuration={0}
                                                style={(this.props.operationZoneId == item.id) ? Object.assign({}, styles.itemBtn, styles.light) : styles.itemBtn }
                                                key={item.id} onPress={ () => {
                                                    this.handleSwitchZone(item.id);
                                                } }>
                                            <base.Text
                                                style={(this.props.operationZoneId == item.id) ? styles.active : {color: '#7f7f7f'}}>{item.name}</base.Text>
                                        </Button>
                                    </base.HorizontalLayout>
                                );
                            }))}
                        </base.HorizontalLayout>
                    </ScrollView>
                </base.HorizontalLayout>

                <base.HorizontalLayout style={styles.topBox}>
                    <base.VerticalLayout style={styles.leftBox}>
                        <base.HorizontalLayout style={styles.center}>
                            <base.Text style={styles.num3}>958</base.Text>
                            <base.Text style={styles.rate3}>人</base.Text>
                        </base.HorizontalLayout>
                        <base.HorizontalLayout style={styles.center}>
                            <base.Text style={{fontSize: 18, color: '#666'}}>近7天新增</base.Text>
                        </base.HorizontalLayout>
                    </base.VerticalLayout>

                    <base.VerticalLayout style={{flexGrow: 1}}>
                        <base.HorizontalLayout style={styles.center}>
                            <base.HorizontalLayout style={styles.center}>
                                <base.Text style={styles.num3}>9583</base.Text>
                                <base.Text style={styles.rate3}>人</base.Text>
                            </base.HorizontalLayout>
                        </base.HorizontalLayout>
                        <base.HorizontalLayout style={styles.center}>
                            <base.Text style={{fontSize: 18, color: '#666'}}>近30天新增</base.Text>
                        </base.HorizontalLayout>
                    </base.VerticalLayout>
                </base.HorizontalLayout>

                <base.VerticalLayout style={styles.scrollBorder}></base.VerticalLayout>
                <ScrollView className="overflowY" style={styles.listView}>

                    <base.HorizontalLayout style={styles.list}>
                        <base.HorizontalLayout>
                            <base.Text style={{fontSize: 16, color: '#333', marginRight: 10}}>2017-10-12</base.Text>
                            <base.Text style={{fontSize: 16, color: '#666'}}>周一</base.Text>
                        </base.HorizontalLayout>
                        <base.HorizontalLayout>
                            <base.Text style={{fontSize: 20, color: '#333'}}>14.5</base.Text>
                            <base.Text style={{fontSize: 16, color: '#333', alignSelf: 'center'}}>%</base.Text>
                        </base.HorizontalLayout>
                    </base.HorizontalLayout>

                </ScrollView>

            </base.View>
        );
    }
}


const mapStateToProps = state=> {
    return {
        operationZoneId: state.api.operationZoneId || '',
        operationZoneNames: state.api.operationZoneNames || [],
    };
};
const mapDispatchToProps = dispatch=> {
    return {
        dispatch,
        fetch: props=>dispatch(fetchOperationNewUsers(props))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(OperationNewUsers));

