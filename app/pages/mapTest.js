import React, {Component} from 'react';
import base from '../base';
import {Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator, TextInput} from '../base';
import pageWrapper from '../modules/pageWrapper';
import CameraView from '../components/camera';
import {ActionBar} from '../components/actionbar';
import {JumpTo, historyStack, JumpBack, ResetTo} from '../modules';
import whereami from '../base/map/whereami';
import ImagePicker from '../components/gallery';
import KeyboardAvoid from '../components/keyboardAvoid';

export default pageWrapper(class extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            historyState: historyStack.get(),
            pwd: ''
        };
        this.active = true
    }

    viewWillAppear() {
        this.active = true;
        /**
         * get my position
         */
        whereami().then(d=>{
            // alert(JSON.stringify(d));
        })
    }

    viewWillDisappear() {
        this.active = false;
    }

    handleBack() {
    }

    render() {
        return (
            <View style={styles.container}>
                <View>
                    {/* 上传图片 */}
                    <ImagePicker onSelectPhoto={base64Data=>{ /* do something with base64 */}} style={{backgroundColor: '#fff'}}  desc="请拍摄举报位置周围真实环境"/>
                </View>

                <KeyboardAvoid containerStyle={{backgroundColor: '#ddd'}}>
                    <TextInput
                        onChangeText={txt=>this.setState({txt})}
                        onSubmitEditing={()=>{ /* do something with this.state.txt */}}
                        placeholder="hello#@!"
                        style={{backgroundColor: '#fff', borderRadius: 5, flexGrow: 1, margin: 10, height: 36}} />
                </KeyboardAvoid>
            </View>
        )
    }

})

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#03f439'
    },
    borderTop: {
        borderTopStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#ddd'
    },
    borderBottom: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    borderLeft: {
        borderLeftStyle: 'solid',
        borderLeftWidth: 1,
        borderLeftColor: '#ddd'
    },
    borderRight: {
        borderRightStyle: 'solid',
        borderRightWidth: 1,
        borderRightColor: '#ddd'
    }
};