import React, { Component } from 'react';
import { Text, View, Br } from '../base';
import { JumpTo } from '../modules';

export class Splash extends Component {

  componentDidMount(){
    setTimeout(()=>{
        JumpTo('login');
    },3000);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{position:'relative',flexDirection:'column',backgroundColor:'white',borderRadius:10}}>
          <Text style={styles.welcome}>
            This is the splash Page2!
          </Text>
        </View>
      </View>
    );
  }
}

const styles = {
  map:{
    position:'absolute',
    left:0,
    top:0,
    right:0,
    bottom:0
  },
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color:'red'
  },
  instructions: {
    textAlign: 'center',
    color: 'blue',
    marginBottom: 5,
  },
};

