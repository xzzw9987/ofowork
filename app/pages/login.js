import React, { Component } from 'react';
import * as base from '../base';

import {
    Map,
    Text,
    Button,
    ListView,
    HorizontalLayout,
    ActivityIndicator,
    ScrollView
} from '../base';
import { api, keys, apibase } from '../runtime/api';
import { connect } from 'react-redux';
import { JumpTo } from '../modules';

import Chart from '../components/chart';

import Storage from '../base/storage';
import { ActionBar } from '../components/actionbar';
import { fetch } from '../modules';
import Keyboard from '../modules/keyboard';
import OperationDashBoard from './operation/operationDashBoard';
import OperationDashBoardSociety from './operation/operationDashBoardSociety';
import OperationDashBoardCity from './operation/operationDashBoardCity';

import platform from '../modules/platform';

var StaticImage = require('../base/staticImages');
class Login extends Component {
    constructor() {
        super();
        this.state = {
            phone: '',
            password: '',
            btnVerifyCodeDisabled: false,
            visible: false,
            loading: false,
            msg: false,
            type: '',
            loginType: true,
            getCode: false,
            time: 60
        };
    }

    componentWillMount() {
        this.props.dispatch({
            type: 'clear',
            data: { code: undefined, alert: undefined }
        });
        this.setState({
            phone: this.props.userInfo ? this.props.userInfo.mobile : ''
        });
        Storage.getItem('token').then(token => {
            if (token) {
                this.props.dispatch({ type: 'token', data: '' });
                this.setState({ loading: true });
                this.props.dispatch({ type: 'token', data: token });
            }
        });
    }

    componentWillUnmount() {
        this.interval && clearInterval(this.interval);
    }

    componentDidMount() {
        this.setState({
            phone: this.props.userInfo ? this.props.userInfo.mobile : ''
        });
    }

    handleLogin() {
        this.props.dispatch(
            api(keys.login, {
                phone: this.state.phone,
                password: this.state.password,
                type: this.state.type
            })
        );
    }

    handleGetCode() {
        fetch(`${apibase}/user/get-verify-code`, {
            method: 'POST',
            body: { phone: this.state.phone }
        }).then(d => {
            if (d.code !== 0) {
                return this.props.dispatch({ type: keys.getVerifyCode, response: d });
            } else {
                this.props.dispatch({ type: keys.getVerifyCode, response: d });
                this.setState({ getCode: true });
                this.interval = setInterval(() => {
                    if (this.state.time === 1) {
                        this.interval && clearInterval(this.interval);
                        this.setState({ time: 60, getCode: false });
                        return;
                    }
                    this.setState({ time: --this.state.time });
                }, 1000);
            }
        });
    }

    handleLoginType() {
        this.setState({
            loginType: !this.state.loginType,
            password: '',
            phone: '',
            type: this.state.loginType ? 'verify_code' : ''
        });
        this.props.dispatch({
            type: 'clear',
            data: { code: undefined, alert: undefined }
        });
        this.interval && clearInterval(this.interval);
        this.setState({ time: 60, getCode: false });
    }

    onPhoneChange(phone) {
        this.setState({ phone });
    }

    JumpTo(...args) {
        // if (!this._hasJump) {
        if (!args[2]) args[2] = {};
        args[2] = { ...args[2], isFirstPage: true };
        JumpTo(...args);
        // }
        // this._hasJump = true;
    }

    componentWillReceiveProps(nextProps) {
        let JumpTo = this.JumpTo.bind(this);
        if (
            nextProps.code &&
            nextProps.code !== 0 &&
            nextProps.token &&
            this.state.loading
        ) {
            this.setState({ loading: false });
            this.setState({ msg: true });
            Storage.clear();
            return;
        }


        // 获取取用户信息
        if (nextProps.token && nextProps.token !== this.props.token) {
            // is default password
            if (nextProps.defaultPassword) {
                JumpTo('profile/modifyPasswordDone', true);
            } else {
                // save to storage
                let token = nextProps.token;
                Storage.setItem('token', token).then(() => { });
                this.props.dispatch(api(keys.userInfo, { token: nextProps.token }));
            }
        }

        // 判断用户类别
        if (nextProps.userInfo && nextProps.userInfo !== this.props.userInfo) {
            // 修车师傅
            if (nextProps.userInfo.identity === 'repairer') {
                JumpTo('repair/entry', false);
                // 运营人员
            } else if (nextProps.userInfo.identity === 'operation') {
                // 运营人员
                /*JumpTo('operation/lowElectricCar/map', false);
                        return;*/
                //社会
                if (nextProps.userInfo.operation_level === 'society') {
                    /*JumpTo('operation/dashBoard/society', false);*/
                    JumpTo('operation/entry', false, {
                        isFirstPage: true,
                        page: OperationDashBoardSociety
                    });
                    // 学校负责人
                } else if (nextProps.userInfo.operation_level === 'school') {
                    //JumpTo('operation/dashBoard', false);
                    JumpTo('operation/entry', false, {
                        isFirstPage: true,
                        page: OperationDashBoard
                    });
                    // 城市负责人
                } else if (nextProps.userInfo.operation_level === 'city') {
                    /*JumpTo('operation/dashBoard/city', false);*/
                    JumpTo('operation/entry', false, {
                        isFirstPage: true,
                        page: OperationDashBoardCity
                    });
                    // 战区负责人
                } else if (nextProps.userInfo.operation_level === 'region') {
                    JumpTo('operation/dashBoard/region', false);

                    // 全国负责人
                } else if (nextProps.userInfo.operation_level === 'country') {
                    JumpTo('operation/dashBoard/country', false);
                }
            } else if (nextProps.userInfo.identity === 'supply') {
                // 供应链
                JumpTo('supply/entry', false);
            } else if (nextProps.userInfo.identity === 'manufacture') {
                // 车厂
                JumpTo('factory/entry', false);
            } else if (nextProps.userInfo.identity === 'hx') {
                // 合作
                JumpTo('hx/entry', false);
            } else if (nextProps.userInfo.identity === 'lock_factory') {
                // 锁厂
                JumpTo('factory/entry', false);
            } else if (nextProps.userInfo.identity === 'new_supply') {
                JumpTo('supply/entry', false);
            } else if (nextProps.userInfo.identity === 'warehouse') {
                // 库管
                JumpTo('scm/entry', false);
            }
        }
    }

    componentDidUpdate(oldProps) {
        if (this.props.code === 1001 && oldProps.code !== 1001) {
            this.setState({ password: '' });
        }
    }

    render() {
        let a = {}, b = {};
        if (platform.OS !== 'web') {
            a = {
                onStartShouldSetResponder: () => true,
                onResponderGrant: () => Keyboard.dismiss()
            };
            b = {
                underlineColorAndroid: 'transparent'
            };
        }
        return (
            <base.View style={styles.container} {...a}>
                <ActionBar title="登录" />

                <base.HorizontalLayout style={styles.hint}>
                    <Text style={{ fontSize: 17, color: '#000' }}>
                        {' '}{this.props.msg && !this.state.msg ? this.props.msg : null}
                    </Text>
                </base.HorizontalLayout>

                <base.VerticalLayout style={styles.wrap}>
                    <base.TextInput
                        keyboardType="numeric"
                        className="input"
                        style={
                            this.props.code === 1000
                                ? Object.assign({}, styles.input, styles.error)
                                : styles.input
                        }
                        placeholder="请输入手机号"
                        value={this.state.phone}
                        onChangeText={this.onPhoneChange.bind(this)}
                        {...b}
                    />
                    <base.HorizontalLayout style={{ position: 'relative' }}>
                        <base.TextInput
                            className="input"
                            style={
                                this.props.code === 1001 || this.props.code === 1014
                                    ? Object.assign({}, styles.input, styles.error)
                                    : styles.input
                            }
                            placeholder={this.state.loginType ? '首次登录,请使用初始密码' : '请输入验证码'}
                            value={this.state.password}
                            onChangeText={text => {
                                this.setState({ password: text });
                            }}
                            {...b}
                        />

                        {this.state.loginType
                            ? null
                            : <Button
                                onPress={this.handleGetCode.bind(this)}
                                disabled={this.state.getCode}
                                style={{
                                    color: '#fff',
                                    position: 'absolute',
                                    top: 9,
                                    right: 10,
                                    fontSize: 14,
                                    padding: 3,
                                    backgroundColor: this.state.getCode ? '#bebebe' : '#2c2c2c'
                                }}
                            >
                                {this.state.getCode ? this.state.time + '秒后重试' : '获取验证码'}
                            </Button>}
                    </base.HorizontalLayout>

                    <Button
                        className="blackBtn"
                        style={styles.submitBtn}
                        disabled={this.state.btnLoginDisabled}
                        onPress={this.handleLogin.bind(this)}
                    >
                        登录
          </Button>

                    <base.HorizontalLayout
                        style={{ alignSelf: 'center', alignItems: 'center', marginTop: 20 }}
                    >
                        <Button
                            style={{ alignSelf: 'center', color: '#666', fontSize: 18 }}
                            onPress={this.handleLoginType.bind(this)}
                        >
                            {this.state.loginType ? '使用短信验证码登录' : '使用密码登录'}
                        </Button>
                        <base.Image style={{ width: 8, height: 10 }} src="triangle" />
                    </base.HorizontalLayout>

                </base.VerticalLayout>
                {this.state.loading
                    ? <base.View
                        style={{
                            position: 'absolute',
                            flexDirection: 'column',
                            left: 0,
                            top: 0,
                            right: 0,
                            bottom: 0,
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: 'rgba(0,0,0,.5)'
                        }}
                    >
                        <ActivityIndicator size="large" src="loading" />
                        <base.HorizontalLayout style={{ marginTop: 10 }}>
                            <Text>登录中 ...</Text>
                        </base.HorizontalLayout>
                    </base.View>
                    : null}
            </base.View>
        );
    }
}

const styles = {
    map: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0
    },
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: 'red'
    },
    instructions: {
        textAlign: 'center',
        color: 'blue',
        marginBottom: 5
    },
    wrap: {
        paddingLeft: 16,
        paddingRight: 16
    },
    hint: {
        height: 60,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        height: 44,
        backgroundColor: '#2C2C2C',
        alignItems: 'center',
        justifyContent: 'center'
    },
    titleText: {
        fontSize: 17,
        color: '#fff'
    },
    /*input */
    input: {
        flexGrow: 1,
        borderColor: '#c2c2c2',
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 2,
        height: 40,
        fontSize: 15,
        paddingLeft: 10,
        marginBottom: 10
    },
    error: {
        borderColor: '#f33001'
    },
    /*btn */
    submitBtn: {
        backgroundColor: '#2C2C2C',
        color: '#fff',
        marginTop: 15,
        height: 44,
        borderRadius: 2,
        justifyContent: 'center',
        fontSize: 15,
        alignItems: 'center',
        flexGrow: 1
    }
};

export default connect(state => {
    return {
        captcha: state.api.captcha,
        token: state.api.token,
        defaultPassword: state.api.defaultPassword,
        msg: state.api.alert,
        userInfo: state.api.userInfo,
        code: state.api.code
    };
})(Login);
