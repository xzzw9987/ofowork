import React, { Component } from 'react';
import { Text, View, Br, Map } from '../base';

export class OFO extends Component {
  constructor(){
    super();
    this.state={
      region:{
        latitude: 37.8518423,
        longitude: 112.5528852,
        latitudeDelta: 0.21,
        longitudeDelta: 0.21,
      }
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Map style={styles.map} region={this.state.region} showsUserLocation={true} annotations={[{latitude: 40, longitude: 116, title: 'TEST BEIJING', key: 'peking'}, {latitude: 30, longitude: 120, title: 'TEST NANJING', key: 'nankin'}]} onLocationChange={r => console.log('233', r)} onAnnotationClicked={r => console.log('XDD', r)} />
        <View style={{position:'relative',flexDirection:'column',backgroundColor:'white',borderRadius:10}}>
          <Text style={styles.welcome}>
            Welcome to React Native!
          </Text>
          <Text style={styles.instructions}>
            To get started, edit app/index.js
          </Text>
          <Text style={styles.instructions}>
            Press Cmd+R to reload,<Br/>
            Cmd+D or shake for dev menu
          </Text>
        </View>
      </View>
    );
  }
}

const styles = {
  map:{
    position:'absolute',
    left:0,
    top:0,
    right:0,
    bottom:0
  },
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color:'red'
  },
  instructions: {
    textAlign: 'center',
    color: 'blue',
    marginBottom: 5,
  },
};

