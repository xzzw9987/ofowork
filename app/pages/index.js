import BaseComponent from './baseComponent';
import Login from './login';
import Splash from './splash';
import ProfileIndex from './profile/profileIndex';
import ProfileModifyPassword from './profile/profileModifyPassword';
import ProfileModifyPasswordDone from './profile/profileModifyPasswordDone';
import RepairCarList from './repairer/repairCarList';
import RepairCarMap from './repairer/repairCarMap';
import RepairCarDetail from './repairer/repairCarDetail';
import RepairCarRecord from './repairer/repairCarRecord';
import RepairBadCarMark from './repairer/repairBadCarMark';
import RepairBadCarMarkPos from './repairer/repairBadCarMarkPos';
import RepairCarSearch from './repairer/repairCarSearch';
import RepairCheckIn from './repairer/repairCheckIn';
import RepairCheckRecords from './repairer/repairCheckRecords';
import RepairCarLocation from './repairer/repairCarLocation';
import RepairCarSearchDone from './repairer/repairCarSearchDone';
import OperationDashBoard from './operation/operationDashBoard';
import OperationOrderHistory from './operation/operationOrderHistory';
import OperationOrderDetail from './operation/operationOrderDetail';
import OperationOrderDetailCity from './operation/operationOrderDetailCity';
import OperationOrderDetailSchool from './operation/operationOrderDetailSchool';
import OperationDashBoardCity from './operation/operationDashBoardCity';
import OperationDashBoardRegion from './operation/operationDashBoardRegion';
import OperationDashBoardCountry from './operation/operationDashBoardCountry';
import RepairRateList from './repairer/repairRateList';
import RepairUnlockCode from './repairer/repairUnlockCode';
import OperationDashBoardDistrict from './operation/operationDashBoardDistrict';
import OperationDashBoardSchool from './operation/operationDashBoardSchool';
import OperationOrderDetailCountry
  from './operation/operationOrderDetailCountry';
import OperationOrderDetailRegion from './operation/operationOrderDetailRegion';
import OperationDashBoardSociety from './operation/operationDashBoardSociety';
import OperationOrderDetailSociety
  from './operation/operationOrderDetailSociety';
import RepairReportVirtualPile from './repairer/repairReportVirtualPile';
import RepairSelectVirtualPosition
  from './repairer/repairSelectVirtualPosition';
import ChainIndex from './supply/chainIndex';
import ChainInput from './supply/chainInput';
import ChainInputNew from './supply_new/chainInput';
import ChainSubmit from './supply/chainSubmit';
import ChainSubmitNew from './supply_new/chainSubmit';
import ChainList from './supply/chainList';
import RepairerIndex from './repairer/repairerIndex';
import OperationReportManagement from './operation/operationReportManagement';
import OperationReportDetail from './operation/operationReportDetail';
import RepairReportDetail from './repairer/repairReportDetail';
import ActivePanel from './repairer/carActive';
import RepairReportDone from './repairer/repairReportDone';
import RepairerMapIndex from './repairer/repairMapIndex';
import MapTest from './mapTest';
import RepairEntry from './repairer/repairEntry';
import ActiveInput from './repairer/activeInput';
import CodeReset from './repairer/codeReset';
import ChooseZone from './repairer/chooseZoneList';
import RepairDetail from './repairer/repairDetail';
import BikeIndex from './700bike/700BikeIndex';
import BikeMapIndex from './700bike/700BikeMap';
import BikeDetail from './700bike/700BikeDetail';
import ChainCode from './supply/chainCode';
import OperationEntry from './operation/operationEntry';
import CarSchelduling from './repairer/transportation/carSchelduling';
import TransportSub from './repairer/transportation/transportSub';
import FacScan from './factory/scaner';
import FacInput from './factory/manually';
import OperationCarSearch from './operation/operationCarSearch';
import OperationCarInformation from './operation/operationCarInformation';
import OperationChangeUnlockCode from './operation/operationChangeUnlockCode';
import OperationRepairRate from './operation/operationRepairRate';
import OperationNewUsers from './operation/operationNewUsers';
import OperationHx from './hx/operationHx';
import HxEntry from './hx/hxEntry';
import CreateRecord from './hx/createRecord';
import CreateDetail from './hx/createDetail';
import WorkRecord from './hx/workRecord';
import WorkRecordDetail from './hx/workRecordDetail';
import HxList from './hx/list';
import HxMapIndex from './hx/map';
import HxProcessDone from './hx/processDone';
import HxDetail from './hx/detail';
import HxHotSpot from './hx/hotSpot';
import OperationChangeBrand from './operation/opChangeBrand';
import OperationBadCarMap from './operation/operationBadCarMap';
import OperationBadCarDetail from './operation/operationBadCarDetail';
import OperationBadCarDone from './operation/operationBadCarDone';
import OperationBadCarData from './operation/operationBadCarData';
import OperationBadCarMessage from './operation/operationBadCarMessage';
import OperationUnbindBrand from './operation/operationUnbindBrand';
import LowElectricCar from './operation/lowElectricCarMap';
import LowElectricCarDetail from './operation/lowElectricCarDetail';
import FactoryEntry from './factory/factoryEntry';
import FactorySnScan from './factory/snScan';
import FactorySnInput from './factory/snInput';
import CarSearchCode from './operation/operationCarSearchCode';
import SupplyEntry from './supply/supplyEntry';
import SupplyCodeSearch from './supply/supplyCodeSearch';
import SupplySearchInput from './supply/supplySearchInput';
import SupplyBarCode from './supply/supplyBarCode';
import SupplyBarInput from './supply/supplyBarInput';
import CompetitorInformation from './operation/collectCompetitorInformation';
import CompetitorInformationMap from './operation/competitorInformationMap';
import CompetitorInformationDetail
  from './operation/competitorInformationDetail';
import MovePathMap from './operation/movePathMap';
import MovePathDate from './operation/movePathDate';
import MovePathFence from './operation/movePathFence';
import MovePathPerson from './operation/movePathPerson';
import ChainSnNew from './supply_new/chainSnNew';
import SupplyFenderInput from './supply_new/supplyFenderInput';
import SupplyActive from './supply_new/carActiveNew';
import RepairRateIndex from './operation/repairRateIndex';
import RepairRateSelectCity from './operation/repairRateSelectCity';
import RepairRateSelectChild from './operation/repairRateSelectChild';
import RepairRateSixty from './operation/repairRateSixty';
import ChainCodeUpgrade from './supply/chainCodeUpgrade';
import ScanCodeLock from './operation/scanCodeLock';
import ScanCodeLockInput from './operation/scanCodeLockInput';
import ScmEntry from './scm/entry';
import ScmInform from './scm/scmInform';
import ScmScan from './scm/scmScan';
import ScmScanList from './scm/scmScanList';
import ScmConfirm from './scm/scmConfirm';
import SupplyUnbind from './supply/supplyUnbind';

export {
  BaseComponent,
  Login,
  Splash,
  ProfileIndex,
  ProfileModifyPassword,
  ProfileModifyPasswordDone,
  RepairCarList,
  RepairCarMap,
  RepairCarDetail,
  RepairCarSearch,
  RepairCheckIn,
  RepairCheckRecords,
  RepairCarLocation,
  RepairCarSearchDone,
  RepairCarRecord,
  RepairBadCarMark,
  RepairBadCarMarkPos,
  OperationDashBoard,
  OperationOrderHistory,
  OperationOrderDetail,
  OperationOrderDetailCity,
  OperationOrderDetailSchool,
  OperationDashBoardCity,
  OperationDashBoardRegion,
  OperationDashBoardCountry,
  RepairRateList,
  RepairUnlockCode,
  OperationDashBoardDistrict,
  OperationDashBoardSchool,
  OperationOrderDetailCountry,
  OperationOrderDetailRegion,
  OperationDashBoardSociety,
  OperationOrderDetailSociety,
  RepairReportVirtualPile,
  RepairSelectVirtualPosition,
  ChainIndex,
  ChainInput,
  ChainInputNew,
  ChainSubmit,
  ChainSubmitNew,
  ChainList,
  RepairerIndex,
  OperationReportManagement,
  OperationReportDetail,
  RepairReportDetail,
  ActivePanel,
  RepairReportDone,
  RepairerMapIndex,
  MapTest,
  RepairEntry,
  ActiveInput,
  CodeReset,
  ChooseZone,
  RepairDetail,
  BikeIndex,
  BikeMapIndex,
  BikeDetail,
  ChainCode,
  OperationEntry,
  CarSchelduling,
  TransportSub,
  FacScan,
  FacInput,
  OperationCarSearch,
  OperationCarInformation,
  OperationChangeUnlockCode,
  OperationRepairRate,
  OperationNewUsers,
  OperationHx,
  HxEntry,
  CreateRecord,
  CreateDetail,
  WorkRecord,
  WorkRecordDetail,
  HxList,
  HxMapIndex,
  HxProcessDone,
  HxDetail,
  HxHotSpot,
  OperationChangeBrand,
  OperationBadCarMap,
  OperationBadCarDetail,
  OperationUnbindBrand,
  OperationBadCarDone,
  OperationBadCarData,
  OperationBadCarMessage,
  LowElectricCar,
  LowElectricCarDetail,
  FactoryEntry,
  FactorySnScan,
  FactorySnInput,
  CarSearchCode,
  SupplyEntry,
  SupplyCodeSearch,
  SupplySearchInput,
  SupplyBarCode,
  SupplyBarInput,
  CompetitorInformation,
  CompetitorInformationMap,
  CompetitorInformationDetail,
  MovePathMap,
  MovePathDate,
  MovePathFence,
  MovePathPerson,
  ChainSnNew,
  SupplyFenderInput,
  SupplyActive,
  RepairRateIndex,
  RepairRateSelectCity,
  RepairRateSelectChild,
  RepairRateSixty,
  ScanCodeLock,
  ScanCodeLockInput,
  ChainCodeUpgrade,
  ScmEntry,
  ScmInform,
  ScmScan,
  ScmScanList,
  ScmConfirm,
  SupplyUnbind
};
