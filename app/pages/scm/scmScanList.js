import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Text,
    View,
    VerticalLayout,
    HorizontalLayout,
    TextInput,
    Toast,
    TouchableHighlight,
    ScrollView
} from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import Modal from '../../components/commonModal';
import Keyboard from '../../modules/keyboard';

const ScmListContainer = pageWrapper(
    class extends Component {
        constructor(...args) {
            super(...args);
            this.state = {
                historyState: historyStack.get(),
            };
        }

        viewWillAppear() {
            this.setTitle(this.props.scmPrefix);
        }

        setTitle(pre) {
            switch (pre) {
            case 'stock-in':
                this.setState({ prefix: '入库' });
                break;
            case 'stock-out':
                this.setState({ prefix: '出库' });
                break;
            case 'stock-back':
                this.setState({ prefix: '回库' });
                break;
            default:
                return '';
            }
        }


        handleBack() {
            JumpBack();
        }

        handleClear() {
            Modal({
                content: '是否确认清空本次扫码记录？',
                buttons: [{
                    text: '确定',
                    onPress: () => {
                        this.props.dispatch({
                            type: '@@clear',
                            payload: { bsnList: [] }
                        });
                    }
                }, {
                    text: '取消',
                    onPress: () => {
                        console.log('cancel');
                    }
                }]
            });
        }

        render() {
            const { prefix } = this.state;
            const { bsnList, scmPrefix } = this.props;
            return (
                <VerticalLayout style={styles.container} onStartShouldSetResponder={() => true} onResponderGrant={() => Keyboard.dismiss()}>
                    <ActionBar title='本次扫码记录' handleBack={() => this.handleBack()}>
                        <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    </ActionBar>

                    <VerticalLayout style={{ flexGrow: 1 }}>
                        <HorizontalLayout
                            style={{
                                ...styles.borderTop,
                                ...styles.borderBottom,
                                paddingLeft: 20,
                                paddingRight: 20,
                                marginTop: 10,
                                alignItems: 'center',
                                backgroundColor: '#fff',
                                height: 40
                            }}>
                            <Text style={{ fontSize: 18, marginRight: 10 }}>{prefix}通知单:</Text>
                            <Text style={{ fontSize: 18 }}
                            >{this.state.historyState.serialNo}</Text>
                        </HorizontalLayout>
                        <HorizontalLayout
                            style={{
                                ...styles.borderTop,
                                ...styles.borderBottom,
                                paddingLeft: 20,
                                paddingRight: 20,
                                alignItems: 'center',
                                backgroundColor: '#fff',
                                height: 40
                            }}>
                            <Text style={{ fontSize: 18, marginRight: 10 }}>本次{prefix}数量:</Text>
                            <Text style={{ fontSize: 18 }}
                            >{bsnList.length}</Text>
                        </HorizontalLayout>
                        <HorizontalLayout
                            style={{
                                ...styles.borderTop,
                                ...styles.borderBottom,
                                paddingLeft: 20,
                                paddingRight: 20,
                                alignItems: 'center',
                                backgroundColor: '#fff',
                                height: 40
                            }}>
                            <Text style={{ fontSize: 18, marginRight: 10 }}>已输入条形码:</Text>
                        </HorizontalLayout>
                        <ScrollView className="overflowY" style={styles.listView}>
                            {bsnList.map((item, idx) => {
                                return (
                                    <VerticalLayout key={idx}><Text style={{ textAlign: 'center' }}>{item.bsn}</Text></VerticalLayout>
                                );
                            })}
                        </ScrollView>

                        {/*<VerticalLayout style={{ height: 150 }} />*/}

                        <VerticalLayout>
                            <TouchableHighlight
                                onPress={() => this.handleClear()}
                                style={{
                                    backgroundColor: '#2c2c2c',
                                    marginLeft: 20,
                                    marginRight: 20
                                }}>
                                <HorizontalLayout
                                    style={{
                                        height: 44,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}>
                                    <Text style={{ color: '#fff' }}>
                                        清空记录
                                    </Text>
                                </HorizontalLayout>
                            </TouchableHighlight>
                        </VerticalLayout>
                        <VerticalLayout>
                            <TouchableHighlight
                                onPress={() => JumpTo('scm/confirm', true, { serialNo: this.state.historyState.serialNo })}
                                style={{
                                    backgroundColor: '#2c2c2c',
                                    marginTop: 50,
                                    marginLeft: 20,
                                    marginRight: 20
                                }}>
                                <HorizontalLayout
                                    style={{
                                        height: 44,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}>
                                    <Text style={{ color: '#fff' }}>
                                        {scmPrefix === 'stock-out' ? '发' : '收'}货确认
                                    </Text>
                                </HorizontalLayout>
                            </TouchableHighlight>
                        </VerticalLayout>
                    </VerticalLayout>
                </VerticalLayout>
            );
        }
    }
);

const mapStateToProps = (state, ownProps) => {
    return {
        scmPrefix: state.api.scmPrefix,
        bsnList: state.api.bsnList || []
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    },
    listView: {
        flex: 1
    },
    borderTop: {
        borderTopStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#ddd'
    },
    borderBottom: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    borderLeft: {
        borderLeftStyle: 'solid',
        borderLeftWidth: 1,
        borderLeftColor: '#ddd'
    },
    borderRight: {
        borderRightStyle: 'solid',
        borderRightWidth: 1,
        borderRightColor: '#ddd'
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(
    ScmListContainer
);
