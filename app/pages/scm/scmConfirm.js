import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Text,
    View,
    VerticalLayout,
    HorizontalLayout,
    TextInput,
    Toast,
    TouchableHighlight,
    ActivityIndicator
} from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { fetchScmDetail, uploadScmOrder } from '../../runtime/action';
import Modal from '../../components/commonModal';
import Keyboard from '../../modules/keyboard';

const ScmConfirmContainer = pageWrapper(
    class extends Component {
        constructor(...args) {
            super(...args);
            this.state = {
                historyState: historyStack.get(),
                loading: true
            };
        }

        async viewWillAppear() {
            const { prefix } = this.props;
            this.setTitle(prefix);
            const res = await this.props.dispatch(fetchScmDetail({
                serialNo: this.state.historyState.serialNo,
                type: prefix === 'stock-in' ? 'in' : (prefix === 'stock-out' ? 'out' : 'back')
            }));
            this.setState({ loading: false });

        }

        setTitle(pre) {
            switch (pre) {
            case 'stock-in':
                this.setState({ prefix: '入库' });
                break;
            case 'stock-out':
                this.setState({ prefix: '出库' });
                break;
            case 'stock-back':
                this.setState({ prefix: '回库' });
                break;
            default:
                return '';
            }
        }


        handleBack() {
            JumpBack();
        }

        async onSubmit() {
            const { prefix, scmDetail } = this.props;
            Modal({
                content: '是否确认提交？',
                buttons: [{
                    text: '确定',
                    onPress: async () => {
                        this.setState({ loading: true });
                        const res = await this.props.dispatch(uploadScmOrder({
                            serialNo: this.state.historyState.serialNo,
                            bsn: this.props.bsnList,
                            type: prefix === 'stock-in' ? 'in' : (prefix === 'stock-out' ? 'out' : 'back'),
                            warehouseId: prefix === 'stock-back' ? scmDetail['warehouse_id'] : ''
                        }));
                        this.setState({ loading: false });

                        if (res.code !== 0) {
                            Toast.fail(res.message, 1.5);
                            return;
                        }

                        this.props.dispatch({
                            type: '@@clear',
                            payload: { bsnList: [] }
                        });

                        const j = url => {
                            if (this.props.navigator && this.props.navigator.getCurrentRoutes) {
                                const navigator = this.props.navigator,
                                    route = navigator.getCurrentRoutes().filter(d => d.url === url)[0];
                                if (route) {
                                    navigator.popToRoute(route);
                                    return true;
                                }
                                return false;
                            } else {
                                ResetTo(url, true, {});
                                return true;
                            }
                        };

                        setTimeout(() => {
                            j('scm/inform');
                        }, 500);
                    }
                }, {
                    text: '取消',
                    onPress: () => {
                        console.log('cancel');
                    }
                }]
            });
        }

        renderLoading() {
            const { prefix } = this.state;
            return (
                <View style={styles.container}>
                    <ActionBar title={prefix}>
                        <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    </ActionBar>
                    <View style={{ position: 'relative', flexGrow: 1 }}>
                        <View style={styles.inner}>
                            <ActivityIndicator size="large" src="loading" />
                            <HorizontalLayout style={{ marginTop: 10 }}>
                                <Text>正在加载 ...</Text>
                            </HorizontalLayout>
                        </View>
                    </View>
                </View>
            );
        }

        renderPanel() {
            const { prefix } = this.state;
            const { bsnList, scmDetail, estimate, already, awaiting } = this.props;
            return (
                <VerticalLayout style={styles.container} onStartShouldSetResponder={() => true} onResponderGrant={() => Keyboard.dismiss()}>
                    <ActionBar title={`${prefix}确认`} handleBack={() => this.handleBack()}>
                        <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    </ActionBar>

                    <VerticalLayout style={{ flexGrow: 1 }}>
                        <HorizontalLayout
                            style={{
                                ...styles.borderTop,
                                ...styles.borderBottom,
                                paddingLeft: 20,
                                paddingRight: 20,
                                marginTop: 10,
                                alignItems: 'center',
                                backgroundColor: '#fff',
                                height: 40
                            }}>
                            <Text style={{ fontSize: 18, marginRight: 10 }}>{prefix}通知单:</Text>
                            <Text style={{ fontSize: 18 }}
                            >{this.state.historyState.serialNo}</Text>
                        </HorizontalLayout>
                        <HorizontalLayout
                            style={{
                                ...styles.borderTop,
                                ...styles.borderBottom,
                                paddingLeft: 20,
                                paddingRight: 20,
                                alignItems: 'center',
                                backgroundColor: '#fff',
                                height: 40
                            }}>
                            <Text style={{ fontSize: 18, marginRight: 10 }}>仓库:</Text>
                            <Text style={{ fontSize: 18 }}
                            >{scmDetail['warehouse_name']}</Text>
                        </HorizontalLayout>
                        {this.props.prefix !== 'stock-back' ? (
                            <VerticalLayout>
                                <HorizontalLayout
                                    style={{
                                        ...styles.borderTop,
                                        ...styles.borderBottom,
                                        paddingLeft: 20,
                                        paddingRight: 20,
                                        alignItems: 'center',
                                        backgroundColor: '#fff',
                                        height: 40
                                    }}>
                                    <Text style={{ fontSize: 18, marginRight: 10 }}>预计{prefix}量:</Text>
                                    <Text style={{ fontSize: 18 }}>{estimate}</Text>
                                </HorizontalLayout>
                                <HorizontalLayout
                                    style={{
                                        ...styles.borderTop,
                                        ...styles.borderBottom,
                                        paddingLeft: 20,
                                        paddingRight: 20,
                                        alignItems: 'center',
                                        backgroundColor: '#fff',
                                        height: 40
                                    }}>
                                    <Text style={{ fontSize: 18, marginRight: 10 }}>已{prefix}量:</Text>
                                    <Text style={{ fontSize: 18 }}>{already}</Text>
                                </HorizontalLayout>
                                <HorizontalLayout
                                    style={{
                                        ...styles.borderTop,
                                        ...styles.borderBottom,
                                        paddingLeft: 20,
                                        paddingRight: 20,
                                        alignItems: 'center',
                                        backgroundColor: '#fff',
                                        height: 40
                                    }}>
                                    <Text style={{ fontSize: 18, marginRight: 10 }}>待{prefix}量:</Text>
                                    <Text style={{ fontSize: 18 }}>{awaiting}</Text>
                                </HorizontalLayout>
                            </VerticalLayout>
                        ) : null}
                        <HorizontalLayout
                            style={{
                                ...styles.borderTop,
                                ...styles.borderBottom,
                                paddingLeft: 20,
                                paddingRight: 20,
                                alignItems: 'center',
                                backgroundColor: '#fff',
                                height: 40
                            }}>
                            <Text style={{ fontSize: 18, marginRight: 10 }}>本次{prefix}量:</Text>
                            <Text style={{ fontSize: 18 }}>{bsnList.length}</Text>
                        </HorizontalLayout>

                        <VerticalLayout style={{ height: 150 }} />

                        <VerticalLayout>
                            <TouchableHighlight
                                onPress={() => this.onSubmit()}
                                style={{
                                    backgroundColor: '#2c2c2c',
                                    marginLeft: 20,
                                    marginRight: 20
                                }}>
                                <HorizontalLayout
                                    style={{
                                        height: 44,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}>
                                    <Text style={{ color: '#fff' }}>
                                        提交{prefix}
                                    </Text>
                                </HorizontalLayout>
                            </TouchableHighlight>
                        </VerticalLayout>
                    </VerticalLayout>
                </VerticalLayout>
            );
        }

        render() {
            if (this.state.loading) return this.renderLoading();
            return this.renderPanel();
        }
    }
);

const mapStateToProps = (state, ownProps) => {
    return {
        prefix: state.api.scmPrefix,
        bsnList: state.api.bsnList || [],
        scmDetail: state.api.scmDetail,
        // warehouse: state.api.scmDetail['warehouse_name'] || '',
        estimate: state.api.scmDetail['estimate_num'] || 0,
        already: state.api.scmDetail['real_num'] || 0,
        awaiting: state.api.scmDetail['estimate_num'] - state.api.scmDetail['real_num'] || 0
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    },
    inner: {
        position: 'absolute',
        flexDirection: 'column',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    borderTop: {
        borderTopStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#ddd'
    },
    borderBottom: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    borderLeft: {
        borderLeftStyle: 'solid',
        borderLeftWidth: 1,
        borderLeftColor: '#ddd'
    },
    borderRight: {
        borderRightStyle: 'solid',
        borderRightWidth: 1,
        borderRightColor: '#ddd'
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(
    ScmConfirmContainer
);
