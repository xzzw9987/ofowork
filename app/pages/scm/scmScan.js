import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Text,
    View,
    VerticalLayout,
    HorizontalLayout,
    TextInput,
    Toast,
    TouchableHighlight,
    ActivityIndicator,
    Modal
} from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { fetchBsnAuth, addBsn } from '../../runtime/action';
import Keyboard from '../../modules/keyboard';
import CustomModal from '../../components/commonModal';

const ScmScanContainer = pageWrapper(
    class extends Component {
        constructor(...args) {
            super(...args);
            this.state = {
                historyState: historyStack.get(),
                scmSn: '',
            };
            // this.counter = {};
        }

        viewWillAppear() {
            // this.setState({ scmSn: '' });
            this._textInput.getFocus();
            // this.props.scmDetail.sku.map(sku => {
            //     this.counter[sku.sku_id] = 0;
            // });
        }


        handleBack() {
            CustomModal({
                content: '返回将清空本次扫描记录',
                buttons: [{
                    text: '确定',
                    onPress: () => {
                        this.props.dispatch({
                            type: '@@clear',
                            payload: { bsnList: [] }
                        });
                        JumpBack();
                    }
                }, {
                    text: '取消',
                    onPress: () => console.log('cancel')
                }]
            });
        }

        handleTextChange(text) {
            if (text.length - this.state.scmSn.length > 1) {
                this.setState({ scmSn: text });
                this.onSubmit(text);
            } else {
                this.setState({ scmSn: text });
            }
        }

        checkRepeat(sn) {
            if (sn === '') {
                Toast.fail('不能为空', 1.5, () => this._textInput.getFocus());
                return false;
            }
            const tmp = this.props.bsnList.map(bsn => {
                if (bsn.bsn.indexOf(sn) > -1) {
                    this.setState({ scmSn: '' });
                    Toast.fail('重复录入', 1.5, () => this._textInput.getFocus());
                    return false;
                }
                return true;
            })
                .filter(sku => sku);
            return tmp.length === this.props.bsnList.length;
        }

        renderLoading() {
            const { prefix } = this.state;
            return (
                <View style={styles.container}>
                    <ActionBar title={prefix}>
                        <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    </ActionBar>
                    <View style={{ position: 'relative', flexGrow: 1 }}>
                        <View style={styles.inner}>
                            <ActivityIndicator size="large" src="loading" />
                            <HorizontalLayout style={{ marginTop: 10 }}>
                                <Text>正在加载 ...</Text>
                            </HorizontalLayout>
                        </View>
                    </View>
                </View>
            );
        }

        renderPanel() {
            const { historyState } = this.state;
            const { bsnList, currentBsn, prefix } = this.props;
            return (
                <VerticalLayout style={styles.container}>
                    <ActionBar title='扫描车身条形码' handleBack={() => this.handleBack()} rightItem="扫码记录"
                        rightAction={() => {
                            JumpTo('scm/scanList', true, { serialNo: historyState.serialNo });
                        }}>
                        <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    </ActionBar>

                    <VerticalLayout style={{ flexGrow: 1 }}>
                        <HorizontalLayout
                            style={{
                                ...styles.borderTop,
                                ...styles.borderBottom,
                                paddingLeft: 20,
                                paddingRight: 20,
                                marginTop: 10,
                                alignItems: 'center',
                                backgroundColor: '#fff',
                                height: 64
                            }}>
                            <Text style={{ fontSize: 18, marginRight: 10 }}>上一个条形码:</Text>
                            <Text style={{ fontSize: 18 }}
                            >{bsnList.length ? bsnList[bsnList.length - 1]['bsn'] : ''}</Text>
                        </HorizontalLayout>
                        <HorizontalLayout
                            style={{
                                ...styles.borderTop,
                                ...styles.borderBottom,
                                paddingLeft: 20,
                                paddingRight: 20,
                                marginTop: 10,
                                alignItems: 'center',
                                backgroundColor: '#fff'
                            }}>
                            <Text style={{ fontSize: 18, marginRight: 10 }}>车身条形码:</Text>
                            <TextInput
                                value={this.state.scmSn}
                                // autoFocus={true}
                                onChangeText={text => {
                                    this.handleTextChange(text);
                                }}
                                placeholder='请扫描'
                                style={{ fontSize: 14, flexGrow: 1, height: 64 }}
                                ref={c => this._textInput = c} />
                        </HorizontalLayout>

                        <VerticalLayout style={{ height: 150 }} />

                        <VerticalLayout>
                            <TouchableHighlight
                                onPress={() => this.onSubmit()}
                                style={{
                                    backgroundColor: '#2c2c2c',
                                    marginLeft: 20,
                                    marginRight: 20
                                }}>
                                <HorizontalLayout
                                    style={{
                                        height: 44,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}>
                                    <Text style={{ color: '#fff' }}>
                                        确定输入
                                    </Text>
                                </HorizontalLayout>
                            </TouchableHighlight>
                        </VerticalLayout>
                        <VerticalLayout>
                            <TouchableHighlight
                                onPress={() => JumpTo('scm/confirm', true, { serialNo: this.state.historyState.serialNo })}
                                style={{
                                    backgroundColor: '#2c2c2c',
                                    marginTop: 50,
                                    marginLeft: 20,
                                    marginRight: 20
                                }}>
                                <HorizontalLayout
                                    style={{
                                        height: 44,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}>
                                    <Text style={{ color: '#fff' }}>
                                        {prefix === 'stock-out' ? '发' : '收'}货确认
                                    </Text>
                                </HorizontalLayout>
                            </TouchableHighlight>
                        </VerticalLayout>
                    </VerticalLayout>
                    <Modal
                        style={styles.modal}
                        swipeToClose={false}
                        ref={c => this._modal = c}>
                        <HorizontalLayout
                            style={{
                                ...styles.borderTop,
                                ...styles.borderBottom,
                                paddingLeft: 20,
                                paddingRight: 20,
                                marginTop: 10,
                                alignItems: 'center',
                                backgroundColor: '#fff',
                                height: 40
                            }}>
                            <Text style={{ fontSize: 18, marginRight: 10 }}>sku</Text>
                            <Text style={{ fontSize: 18 }}>物料名称</Text>
                        </HorizontalLayout>
                        {currentBsn.map((item, idx) => {
                            return (
                                <TouchableHighlight key={idx} onPress={() => this.onSelectItem(item)}>
                                    <HorizontalLayout
                                        style={{
                                            ...styles.borderTop,
                                            paddingLeft: 20,
                                            paddingRight: 20,
                                            alignItems: 'center',
                                            backgroundColor: '#fff',
                                            height: 40
                                        }}>
                                        <Text style={{ fontSize: 18, marginRight: 10 }}>{item.sku_id}</Text>
                                        <Text style={{ fontSize: 18 }}>{item.name}</Text>
                                    </HorizontalLayout>
                                </TouchableHighlight>
                            );
                        })}
                    </Modal>
                </VerticalLayout>
            );
        }

        render() {
            if (this.state.loading) return this.renderLoading();
            return this.renderPanel();
        }

        onSelectItem(item) {
            // this.counter[item.sku_id]++;
            // Object.keys(this.counter).map(key => {
            //     this.props.scmDetail.sku.map(sku => {
            //         if (sku.sku_id === key && (this.counter[key] > sku.estimate_num - sku.real_num)) {
            //             Toast.fail(' 超过剩余数量', 2);
            //             return false;
            //         }
            //     });
            // });
            const { bsnList, scmDetail } = this.props;
            const alreadyIn = bsnList.filter(bsn => bsn.sku_id === item.sku_id);
            const target = scmDetail.sku.filter(sku => sku.sku_id === item.sku_id);
            if (alreadyIn.length >= target[0].estimate_num - target[0].real_num) {
                this.setState({ scmSn: '' });
                this._modal.close();
                Toast.fail(' 超过剩余数量', 2, () => this._textInput.getFocus());
                return false;
            }
            this.props.dispatch(addBsn({
                bsn: this.state.scmSn,
                sku_id: item.sku_id
            }));
            this._modal.close();
            this.setState({ scmSn: '' });
            this._textInput.getFocus();
        }

        async onSubmit(text) {
            const { prefix, scmDetail, bsnList } = this.props;
            Keyboard.dismiss();
            if (!this.checkRepeat(text || this.state.scmSn)) {
                return false;
            }
            this.setState({ loading: true });
            const res = await this.props.dispatch(fetchBsnAuth({
                serialNo: this.state.historyState.serialNo,
                bsn: text || this.state.scmSn,
                type: prefix === 'stock-in' ? 'in' : (prefix === 'stock-out' ? 'out' : 'back')
            }));

            this.setState({ loading: false });
            if (res.code === 0) {
                if (res.data.length > 1) {
                    this._modal.open();
                } else {
                    // this.counter[res.data[0].sku_id]++;
                    // console.log(this.counter);
                    // Object.keys(this.counter).map(key => {
                    //     scmDetail.sku.map(sku => {
                    //         if (sku.sku_id === key && (this.counter[key] > sku.estimate_num - sku.real_num)) {
                    //             Toast.fail(' 超过剩余数量', 2, () => this._textInput.getFocus());
                    //             return false;
                    //         }
                    //     });
                    // });
                    const alreadyIn = bsnList.filter(item => item.sku_id === res.data[0].sku_id);
                    if (alreadyIn.length >= scmDetail.sku[0].estimate_num - scmDetail.sku[0].real_num) {
                        this.setState({ scmSn: '' });
                        Toast.fail(' 超过剩余数量', 2, () => this._textInput.getFocus());
                        return false;
                    }

                    this.props.dispatch(addBsn({
                        bsn: text || this.state.scmSn,
                        sku_id: res.data[0].sku_id
                    }));
                    this.setState({ scmSn: '' });
                    this._textInput.getFocus();
                }
            } else {
                this.setState({ scmSn: '' });
                Toast.fail(res.message, 1.5, () => this._textInput.getFocus());
            }

        }
    }
);

const mapStateToProps = (state, ownProps) => {
    return {
        prefix: state.api.scmPrefix,
        scmDetail: state.api.scmDetail,
        currentBsn: state.api.bsnAuth || [],
        bsnList: state.api.bsnList || []
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    },
    inner: {
        position: 'absolute',
        flexDirection: 'column',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    borderTop: {
        borderTopStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#ddd'
    },
    borderBottom: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    borderLeft: {
        borderLeftStyle: 'solid',
        borderLeftWidth: 1,
        borderLeftColor: '#ddd'
    },
    borderRight: {
        borderRightStyle: 'solid',
        borderRightWidth: 1,
        borderRightColor: '#ddd'
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 300,
        width: 300
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(
    ScmScanContainer
);
