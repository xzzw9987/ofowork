import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Text,
    View,
    VerticalLayout,
    HorizontalLayout,
    TextInput,
    Toast,
    TouchableHighlight,
    ActivityIndicator
} from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { setPrefix, fetchScmDetail } from '../../runtime/action';
import Keyboard from '../../modules/keyboard';

const ScmInformContainer = pageWrapper(
    class extends Component {
        constructor(...args) {
            super(...args);
            this.state = {
                historyState: historyStack.get(),
                loading: true,
                serialNo: ''
            };
            this.active = true;
        }

        viewWillAppear() {
            this.setState({ serialNo: '' });
            const { fromWhere } = this.state.historyState;
            this.props.dispatch(setPrefix({ fromWhere }));
            this.setTitle(fromWhere);
            this.setState({ loading: false });
            this._textInput.getFocus();
        }

        setTitle(pre) {
            switch (pre) {
            case 'stock-in':
                this.setState({ title: '收货入库', prefix: '入库' });
                break;
            case 'stock-out':
                this.setState({ title: '发货出库', prefix: '出库' });
                break;
            case 'stock-back':
                this.setState({ title: '投放回库', prefix: '回库', releasePre: '投放出库' });
                break;
            default:
                return '';
            }
        }

        handleBack() {
            JumpBack();
        }

        handleTextChange(text) {
            this.setState({ serialNo: text });
            if (text.length - this.state.serialNo.length > 1) {
                this.onSubmit(text);
            }
        }

        renderLoading() {
            const { title } = this.state;
            return (
                <View style={styles.container}>
                    <ActionBar title={title}>
                        <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    </ActionBar>
                    <View style={{ position: 'relative', flexGrow: 1 }}>
                        <View style={styles.inner}>
                            <ActivityIndicator size="large" src="loading" />
                            <HorizontalLayout style={{ marginTop: 10 }}>
                                <Text>正在加载 ...</Text>
                            </HorizontalLayout>
                        </View>
                    </View>
                </View>
            );
        }

        renderPanel() {
            const { title, prefix, releasePre } = this.state;
            return (
                <VerticalLayout style={styles.container}>
                    <ActionBar title={title} handleBack={() => this.handleBack()}>
                        <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    </ActionBar>

                    <VerticalLayout style={{ flexGrow: 1 }}>
                        <HorizontalLayout
                            style={{
                                ...styles.borderTop,
                                ...styles.borderBottom,
                                paddingLeft: 20,
                                paddingRight: 20,
                                marginTop: 10,
                                alignItems: 'center',
                                backgroundColor: '#fff'
                            }}
                        >
                            <Text style={{ fontSize: 18, marginRight: 10 }}>{releasePre ? `${releasePre}单` : `${prefix}通知单`}</Text>
                            <TextInput
                                value={this.state.serialNo}
                                // autoFocus={true}
                                onChangeText={text => {
                                    this.handleTextChange(text);
                                }}
                                placeholder={'请输入单号'}
                                style={{ fontSize: 14, flexGrow: 1, height: 64 }}
                                ref={c => this._textInput = c}
                            />
                        </HorizontalLayout>

                        <VerticalLayout style={{ height: 150 }} />

                        <VerticalLayout>
                            <TouchableHighlight
                                onPress={() => this.onSubmit()}
                                style={{
                                    backgroundColor: '#2c2c2c',
                                    marginLeft: 20,
                                    marginRight: 20
                                }}>
                                <HorizontalLayout
                                    style={{
                                        height: 44,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}>
                                    <Text style={{ color: '#fff' }}>
                                        下一步
                                    </Text>
                                </HorizontalLayout>
                            </TouchableHighlight>
                        </VerticalLayout>
                    </VerticalLayout>
                </VerticalLayout>
            );
        }

        render() {
            if (this.state.loading) return this.renderLoading();
            return this.renderPanel();
        }

        async onSubmit(text) {
            Keyboard.dismiss();
            const { prefix } = this.props;
            this.setState({ loading: true });
            const res = await this.props.dispatch(fetchScmDetail({
                serialNo: text || this.state.serialNo,
                type: prefix === 'stock-in' ? 'in' : (prefix === 'stock-out' ? 'out' : 'back')
            }));
            // JumpTo('scm/scan', true, { serialNo: this.state.serialNo });
            this.setState({ loading: false });
            if (res.code === 0) {
                JumpTo('scm/scan', true, { serialNo: this.state.serialNo });
            } else {
                this.setState({ serialNo: '' });
                Toast.fail(res.message, 1.5, () => this._textInput.getFocus());
            }
        }
    }
);

const mapStateToProps = (state, ownProps) => {
    return {
        prefix: state.api.scmPrefix
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    },
    inner: {
        position: 'absolute',
        flexDirection: 'column',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    borderTop: {
        borderTopStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#ddd'
    },
    borderBottom: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    borderLeft: {
        borderLeftStyle: 'solid',
        borderLeftWidth: 1,
        borderLeftColor: '#ddd'
    },
    borderRight: {
        borderRightStyle: 'solid',
        borderRightWidth: 1,
        borderRightColor: '#ddd'
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(
    ScmInformContainer
);
