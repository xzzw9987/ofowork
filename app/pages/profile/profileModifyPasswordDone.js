import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys } from '../../runtime/api';
import { Text, Input, Button, Toast } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';

/**
 * 修改密码(输入新密码)
 */
class ProfileModifyPasswordDone extends Component {

    constructor(){
        super();
        this.state={
            msg: ''
        };
    }
    componentWillMount() {
        this.props.dispatch({type: 'clear', data: {code: undefined, alert: undefined}});
    }
    handleNextStep() {
        let oldPassword = this.state.oldPassword;
        let newPassword = this.state.newPassword;
        let passwordRepeat = this.state.passwordRepeat;
        let alertText = '';
        console.log(oldPassword);
        if( oldPassword == undefined || newPassword==undefined || passwordRepeat==undefined){
            alertText = '密码不能为空';
            this.setState({msg: alertText});
        }else if(oldPassword.lenth < 6|| newPassword.length < 6 || passwordRepeat.length < 6 ||
        oldPassword.lenth > 20|| newPassword.length > 20 || passwordRepeat.length > 20){
            alertText = '请输入6-20位密码';
            this.setState({msg: alertText});
        }else if(newPassword != passwordRepeat){
            alertText = '确认密码与新密码输入不一致';
            this.setState({msg: alertText});
        }else{
            this.props.dispatch(api(keys.userModifyPassword, {token: this.props.token, old_password: oldPassword, new_password: newPassword}));
        }

    }

    handleGoBack() {
        this.props.dispatch({type: 'clear', data: {code: undefined, alert: undefined}});
        // JumpTo('login', false);
        JumpBack();
    }

    componentWillReceiveProps(nextProps){
        console.log('ProfileModifyPasswordDone', nextProps.code);
        if (nextProps.code == 0) {
            Toast.success('修改成功', 1);
            this.props.dispatch({type: 'clear', data: {code: undefined, alert: undefined}});
            setTimeout(function(){
                JumpTo('login', false);
            }, 1000);
        }
    }

    render() {
        console.log('render', this.props.code);
        return (
            <base.View style={styles.container}>
                <ActionBar title="修改密码" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.HorizontalLayout style={styles.hint}>
                   <Text style={{fontSize: 17}}> {this.props.alert ||this.state.msg ? this.props.alert ||this.state.msg : null}</Text>
                </base.HorizontalLayout>
                <base.VerticalLayout style={styles.wrap}>
                    <base.HorizontalLayout>
                         <base.TextInput className="input" style={this.props.code == 1002 ? Object.assign({}, styles.input, styles.error):styles.input} placeholder='请输入旧密码' onChangeText={ (text) => { this.state.oldPassword = text; }}></base.TextInput>
                     </base.HorizontalLayout>
                    <base.HorizontalLayout>
                        <base.TextInput className="input" style={this.props.code == 1004 ||this.props.code == 1008 ? Object.assign({}, styles.input, styles.error):styles.input} placeholder='请输入6-20位密码' onChangeText={ (text) => { this.state.newPassword = text; }}></base.TextInput>
                    </base.HorizontalLayout>
                    <base.HorizontalLayout>
                        <base.TextInput className="input" style={this.props.code == 1004||this.props.code == 1008 ? Object.assign({}, styles.input, styles.error):styles.input} placeholder='请再次输入新密码' onChangeText={ (text) => { this.state.passwordRepeat = text; }}></base.TextInput>
                    </base.HorizontalLayout>

                        <base.Button className="blackBtn" style={styles.submitBtn} onPress={this.handleNextStep.bind(this)}>完成</base.Button>


                </base.VerticalLayout>

            </base.View>
        );
    }
}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
    /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    wrap: {
        paddingLeft: 16,
        paddingRight: 16,
    },
    hint: {
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
    },
    font17: {
        fontSize: 17,
    },
  /*input */
    input: {
        flexGrow: 1,
        borderColor: '#c2c2c2',
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 2,
        height: 40,
        fontSize: 15,
        paddingLeft: 10,
        marginBottom: 10,
    },
    error: {
        borderColor: '#f33001'
    },
  /*btn */
    submitBtn: {
        backgroundColor: '#2C2C2C',
        color: '#fff',
        marginTop: 15,
        height: 44,
        borderRadius: 2,
        justifyContent: 'center',
        fontSize: 15,
        alignItems: 'center',
        flexGrow: 1,
    },
};

export default connect((state) => {
    return {
        token: state.api.token,
        alert: state.api.alert,
        code: state.api.code,
    };
})(ProfileModifyPasswordDone);

