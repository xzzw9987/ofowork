import React, { Component } from 'react';
import * as base from '../../base';
import { Text, View, Br, Button, Span, List, ActionSheet, TouchableHighlight} from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack, ResetTo} from '../../modules';
import { ActionBar } from '../../components/actionbar';

import 'antd-mobile/lib/action-sheet/style';

import Storage from '../../base/storage';
import {clear} from '../../runtime/api';
import appVersion from '../../modules/version';

/**
 * 个人设置页
 */
class ProfileIndex extends Component {

    handleModifyPassword() {
        JumpTo('profile/modifyPasswordDone', true);
    }

    handleLogout() {
        this.setState({ btnLoginDisabled: true });
        const BUTTONS = ['退出登录', '取消'];
        ActionSheet.showActionSheetWithOptions({
            options: BUTTONS,
            cancelButtonIndex: BUTTONS.length - 1,
            destructiveButtonIndex: BUTTONS.length - 2,
            maskClosable: true,
            'data-seed': 'logId',
        }, (buttonIndex) => {
            if(buttonIndex === 0) {
                this.setState({ token: '', msg: '' });
                Storage.setItem('token', '').then(() => {
                });
                clear();
                ResetTo('login');
                this.props.dispatch({type: '@@clearAll'});
            }
        });
    }

    handleGoBack() {
    }

    render() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="我" handleBack={this.handleGoBack.bind(this)}></ActionBar>
                <TouchableHighlight style={Object.assign({}, styles.items, styles.one)} className="itemActive" onPress={this.handleModifyPassword.bind(this) } underlayColor={'#ccc'}>
                    <base.HorizontalLayout>
                        <base.HorizontalLayout style={styles.text}>
                            <base.Text style={styles.item}>修改密码</base.Text>
                        </base.HorizontalLayout>
                        <base.Image style={styles.arrow} src='arrow'></base.Image>
                    </base.HorizontalLayout>
                </TouchableHighlight>
                <TouchableHighlight style={styles.items} className="itemActive" onPress={this.handleLogout.bind(this)} underlayColor={'#ccc'}>
                    <base.HorizontalLayout>
                        <base.HorizontalLayout style={styles.text}>
                            <Text style={styles.item}>退出登录</Text>
                        </base.HorizontalLayout>
                        <base.Image style={styles.arrow} src='arrow'></base.Image>
                    </base.HorizontalLayout>
                </TouchableHighlight>

                <base.HorizontalLayout style={{position: 'absolute', justifyContent: 'center', bottom: 10, left: 0, right: 0}}>
                    <Text style={{fontSize: 14}}>当前版本 {`${appVersion}`}</Text>
                </base.HorizontalLayout>
            </base.View>
        );
    }
}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    items: {
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        height: 44,
        position: 'relative',
        backgroundColor: '#fff',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    one: {
        borderStyle: 'solid',
        borderTopColor: '#d6d6d6',
        borderTopWidth: 1,
        marginTop: 10,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    item: {
        color: '#000',
        fontSize: 15,
        paddingLeft: 10,
    },
    text: {
        height: 44,
        alignItems: 'center',
    },
    arrow: {
        position: 'absolute',
        right: 10,
        width: 8,
        height: 14,
        top: 15,
    }
};

export default connect((state) => {
    return {

    };
})(ProfileIndex);
