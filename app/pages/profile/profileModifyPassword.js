import React, { Component } from 'react';
import * as base from '../../base';
import { Text, Input, Button } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';

/**
 * 修改密码(输入旧密码)
 */
class ProfileModifyPassword extends Component {

    constructor() {
        super();
        this.state = {
            alert: '',
            oldPassword: ''
        };
    }

    handleNextStep() {

        if(this.state.oldPassword == ''){
            this.setState({alert: '请输入正确的旧密码'});
        }else{
            this.props.dispatch({type: 'oldPassword', data: this.state.oldPassword});
            JumpTo('profile/modifyPasswordDone', true);
        }

    }

    handleGoBack() {
        this.props.dispatch({type: 'clear', data: {alert: undefined}});
        JumpBack();
    }

    render() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="修改密码" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.HorizontalLayout style={styles.hint}>
                    <Text style={{fontSize: 17}}> {this.state.alert ? this.state.alert : null}</Text>
                </base.HorizontalLayout>
                <base.VerticalLayout style={styles.wrap}>
                    <base.HorizontalLayout>
                        <base.TextInput className="input" style={this.state.alert ? Object.assign({}, styles.input, styles.error):styles.input} placeholder='请输入旧密码' onChangeText={ (text) => { this.state.oldPassword = text; }}></base.TextInput>
                    </base.HorizontalLayout>
                    <base.HorizontalLayout>
                        <base.Button className="blackBtn" style={Object.assign({}, styles.submitBtn, styles.mt50)} onPress={this.handleNextStep.bind(this)}>下一步</base.Button>
                    </base.HorizontalLayout>
                </base.VerticalLayout>

            </base.View>
        );
    }
}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    wrap: {
        paddingLeft: 16,
        paddingRight: 16,
    },
    hint: {
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
    },
    font17: {
        fontSize: 17,
    },
  /*input */
    input: {
        flexGrow: 1,
        borderColor: '#c2c2c2',
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 2,
        height: 40,
        fontSize: 15,
        paddingLeft: 10,
        marginBottom: 10,
    },
    error: {
        borderColor: '#f33001'
    },
  /*btn */
    submitBtn: {
        backgroundColor: '#2C2C2C',
        color: '#fff',
        marginTop: 15,
        height: 44,
        borderRadius: 2,
        justifyContent: 'center',
        fontSize: 15,
        alignItems: 'center',
        flexGrow: 1
    },
    mt50: {
        marginTop: 50,
    }
};

export default connect((state) => {
    return {
    };
})(ProfileModifyPassword);
