import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, parseNum, loadToken } from '../../runtime/api';
import { Text, View, Br, Button, ScrollView, TouchableHighlight, ActivityIndicator, Toast } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';

/**
 * 修改解锁码
 */
class RepairUnlockCode extends Component {
    constructor(){
        super();
        this.state={
            newCode: '',
        };
    }
    componentWillMount() {
        this.props.dispatch({type: 'clear', data: {code: undefined, alert: undefined}});
    }
    componentWillReceiveProps(newProps){
        if ( newProps.ret && newProps.ret.code == 0) {
            Toast.success('修改成功', 1.5);
            this.props.dispatch({type: 'clear', data: {ret: undefined, retAlert: undefined}});
            let carDetail = Object.assign({}, this.props.carDetail);
            carDetail.unlock_code = this.state.newCode;
            this.props.dispatch({type: 'carDetail', data: carDetail});
            setTimeout(function(){
                JumpBack();
            }, 2000);
        }else if( newProps.ret && newProps.ret.code != 0){
            Toast.fail(newProps.retAlert.alert, 1.5);
            this.setState({ newCode: '' });
            this.props.dispatch({type: 'clear', data: {ret: undefined, retAlert: undefined}});
        }

    }
    /**
     * 返回
     */
    componentWillUnmount() {
        this.props.dispatch({type: 'clear', data: {ret: undefined, retAlert: undefined}});
    }
    handleGoBack() {

        this.props.dispatch({type: 'clear', data: {ret: undefined, retAlert: undefined}});
        JumpBack();
    }
    /**
     * 修改解锁码
     */
    handleChangeCode(){
        if(this.state.newCode.length != 4){
            this.setState({msg: '请输入4位数字密码'});
            return;
        }

        if(isNaN(parseInt(this.state.newCode))){
            this.setState({msg: '请输入4位数字密码'});
            return;
        }
        this.props.dispatch(api(keys.updateUnlockCode, {token: this.props.token, bicycle_num: this.props.carDetail.bicycle, new_unlock_code: this.state.newCode, old_unlock_code: this.props.carDetail.unlock_code}, 'updateUnlockCode'));
    }
    render() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="解锁码修改" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <Br/>
                <base.HorizontalLayout style={styles.hint}>
                    <Text style={{fontSize: 17, color: '#000'}}> {this.state.msg ? this.state.msg : null}</Text>
                </base.HorizontalLayout>
                <base.VerticalLayout style={styles.borderTop}>
                    <base.HorizontalLayout style={styles.item}>
                        <Text style={styles.titleText}>车牌号</Text>
                        <Text style={{fontSize: 18}}>{this.props.carDetail.bicycle}</Text>
                    </base.HorizontalLayout>
                    <base.HorizontalLayout style={styles.item}>
                        <Text style={styles.titleText}>解锁码</Text>{/*this.props.carDetail.unlock_code*/}
                        <base.VerticalLayout style={styles.rightBox}>
                            <base.TextInput keyboardType="numeric" style={styles.textInput}  placeholder='请输入新密码' value={this.state.newCode} onChangeText={(text) => { this.setState({ newCode: text }); } } />
                        </base.VerticalLayout>
                    </base.HorizontalLayout>
                    <base.HorizontalLayout style={styles.btnBox}>
                         <Button className="blackBtn" style={styles.btn} onPress={this.handleChangeCode.bind(this)}><Text style={{color: '#fff'}}>确认修改</Text></Button>
                    </base.HorizontalLayout>
                </base.VerticalLayout>
            </base.View>
        );

    }
}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
    /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    color2: {
        color: '#7f7f7f',
        fontSize: 12
    },
    hint: {
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
    },
    vcbox: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    rightBox: {
        flexGrow: 1,
        justifyContent: 'center',
    },
    titleText: {
        width: 75,
    },
    item: {
        alignItems: 'center',
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        height: 60,
        paddingLeft: 10,
        backgroundColor: '#fff',
    },
    borderTop: {
        borderStyle: 'solid',
        borderTopColor: '#c6c6c6',
        borderTopWidth: 1,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderBottomWidth: 0,
    },
    textInput: {
        fontSize: 18,
        borderWidth: 0,
        height: 44,
        padding: 0
    },
    btnBox: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 40,
        flexGrow: 1,
        justifyContent: 'center',
        height: 80
    },
    btn: {
        height: 44,
        backgroundColor: '#2c2c2c',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 2,
        flexGrow: 1,
        width: 300
    }

};

export default connect((state) => {
    return {
        token: state.api.token,
        carDetail: state.user.carDetail,
        updateUnlockCode: state.api.updateUnlockCode,
        retAlert: state.api.retAlert,
        ret: state.api.ret,
    };
})(RepairUnlockCode);
