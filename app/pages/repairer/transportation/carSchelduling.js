import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as base from '../../../base';
import { Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator } from '../../../base';
import pageWrapper from '../../../modules/pageWrapper';
import CameraView from '../../../components/camera';
import Modal from '../../../components/commonModal';
import { ActionBar } from '../../../components/actionbar';
import { JumpTo, historyStack, JumpBack, ResetTo } from '../../../modules';
import * as actions from '../../../runtime/action';

// const alert = Modal.alert;

const platePrefix = 'http://ofo.so/plate/';
const carRecycle = pageWrapper(class extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            historyState: historyStack.get(),
            showCam: false,
            loading: true,
        };
        this.active = true;
        this.from = this.state.historyState.from;
    }

    async viewWillAppear() {
        this.active = true;
        this.haveReadBarCode = false;
        await this.props.dispatch(actions.getLocation());
        this.props.locationInfo.error ?
            this.setState({ loading: false, }) :
            this.setState({ loading: false, showCam: true, });
    }

    viewWillDisappear() {
        this.active = false;

        this.setState({
            showCam: false,
        });
    }

    handleBack() {
        JumpBack();
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title={this.from === 'recycle' ? '收车' : '投车'}>
                    <Text style={{ fontSize: 15, color: '#000', }}>返回</Text>
                </ActionBar>
                <base.View style={{ position: 'relative', flexGrow: 1, }}>
                    <base.View style={styles.inner}>
                        <ActivityIndicator size='large' src='loading' />
                        <base.HorizontalLayout style={{ marginTop: 10, }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }

    renderPanel() {
        return (
            <View style={styles.container}>
                <ActionBar title={this.from === 'recycle' ? '收车' : '投车'} handleBack={() => this.handleBack()}>
                    {
                        !this.state.historyState
                            ? (<base.Image style={{ width: 17, height: 17, }} src='setting.png' />) :
                            <Text style={{ fontSize: 15, color: '#000', }}>返回</Text>
                    }
                </ActionBar>

                {this.state.showCam ? (<CameraView onBarCodeRead={d => this.onBarCodeRead(d)} hint={'扫描获取解锁码'}
                    buttons={this.renderButtons()}
                    torch={true} />) : null}
            </View>
        );
    }

    onBarCodeRead(d) {
        d = d.trim();
        if (this.haveReadBarCode || !this.active) return;
        // do something with d
        this.haveReadBarCode = true;
        if (!this.valid(d)) {
            Toast.fail('请输入正确的车牌号码', 2, () => this.haveReadBarCode = false);
            return;
        }
        const carno = d.substring(platePrefix.length, d.length);
        const { latitude, longitude, accuracy, } = this.props.locationInfo;
        const type = this.from === 'recycle' ? 'in' : 'out';

        // alert(`车牌号：${carno}`, '', [{
        //     text: '放弃', onPress: () => {
        //         this.haveReadBarCode = false;
        //     },
        // }, {
        //     text: this.from === 'recycle' ? '收车' : '投车', onPress: async () => {
        //         await this.props.dispatch(actions.bicycleRecycle({
        //             carno, latitude, longitude, accuracy, type,
        //         }));
        //         this.props.result.code === 0 ?
        //             Toast.success('成功', 1, () => this.haveReadBarCode = false) :
        //             Toast.fail(this.props.result.message, 1, () => this.haveReadBarCode = false);
        //     },
        // }]);

        Modal({
            content: `车牌号：${carno}`,
            buttons: [
                {
                    text: '放弃', onPress: () => {
                        this.haveReadBarCode = false;
                    },
                },
                {
                    text: this.from === 'recycle' ? '收车' : '投车', onPress: async () => {
                        await this.props.dispatch(actions.bicycleRecycle({
                            carno, latitude, longitude, accuracy, type,
                        }));
                        this.props.result.code === 0 ?
                            Toast.success('成功', 1, () => this.haveReadBarCode = false) :
                            Toast.fail(this.props.result.message, 1, () => this.haveReadBarCode = false);
                    },
                }
            ],
        });

    }

    valid(text) {
        return text.startsWith(platePrefix) && /^\d{5,7}$/.test(text.substring(platePrefix.length, text.length));
    }

    handlePress() {
        this.props.locationInfo.error ? Toast.fail('定位糙了') : JumpTo('repairer/transportSubmit', true, { from: this.from, });
    }

    renderButtons() {
        return [
            <View key={0}>
                <TouchableHighlight onPress={() => this.handlePress()}>
                    <base.VerticalLayout style={{ alignItems: 'center', }}>
                        <base.Image src="hand" style={{ width: 64, height: 64, }} />
                        <Text style={{ marginTop: 10, fontSize: 14, backgroundColor: 'rgba(0,0,0,0)', color: '#fff', }}>手动输入</Text>
                    </base.VerticalLayout>
                </TouchableHighlight>
            </View>
        ];
    }

    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderPanel();
    }
});

const mapStateToProps = (state, ownProps) => {
    return {
        userInfo: state.api.userInfo || {},
        locationInfo: state.api.locationInfo,
        result: state.api.transportRes,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch,
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    inner: {
        position: 'absolute',
        flexDirection: 'column',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
};

export default connect(mapStateToProps, mapDispatchToProps)(carRecycle);
