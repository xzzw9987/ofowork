import React, { Component } from 'react';
import * as base from '../../../base';
import { connect } from 'react-redux';
import { Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator } from '../../../base';
import pageWrapper from '../../../modules/pageWrapper';
import { ActionBar } from '../../../components/actionbar';
import { JumpTo, historyStack, JumpBack, ResetTo } from '../../../modules';
import platform from '../../../modules/platform';
import * as actions from '../../../runtime/action';
import Keyboard from '../../../modules/keyboard';

const TransportSub = pageWrapper(class extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            historyState: historyStack.get(),
            loading: false,
            carno: '',
        };
        this.from = this.state.historyState.from;
    }

    handleBack() {
        JumpBack();
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title={this.from === 'recycle' ? '收车' : '投车'}>
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>
                <base.View style={{ position: 'relative', flexGrow: 1 }}>
                    <base.View style={styles.inner}>
                        <ActivityIndicator size='large' src='loading' />
                        <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }

    renderPanel() {
        return (
            <View style={styles.container}>
                <ActionBar title={this.from === 'recycle' ? '收车' : '投车'}
                    handleBack={() => this.handleBack()}>
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>

                <View style={{ flexGrow: 1, flexDirection: 'column' }}>
                    <base.HorizontalLayout style={{
                        ...styles.borderTop, ...styles.borderBottom,
                        paddingLeft: 20,
                        paddingRight: 20,
                        marginTop: 10,
                        alignItems: 'center',
                        backgroundColor: '#fff'
                    }}>
                        <Text style={{ fontSize: 18, marginRight: 10 }}>车牌号</Text>
                        <base.TextInput keyboardType="numeric" onChangeText={(text) => {
                            this.setState({ carno: text });
                        }}
                            value={this.state.carno}
                            placeholder="请输入车牌号" style={{ fontSize: 14, flexGrow: 1, height: 64 }} />
                    </base.HorizontalLayout>

                    <View style={{ height: 150 }} />

                    <View>
                        <TouchableHighlight onPress={() => this.onSubmit()}
                            style={{ backgroundColor: '#2c2c2c', marginLeft: 20, marginRight: 20, flexGrow: 1 }}>
                            <base.HorizontalLayout style={{ height: 44, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#fff' }}>
                                    {this.from === 'recycle' ? '收车' : '投车'}
                                </Text>
                            </base.HorizontalLayout>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        );
    }

    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderPanel();
    }

    async onSubmit() {
        Keyboard.dismiss();
        if (!this.valid(this.state.carno)) {
            Toast.fail('请输入正确的车牌号码', 2);
            return;
        }
        const { carno } = this.state;
        const { latitude, longitude, accuracy } = this.props.locationInfo;
        const type = this.from === 'recycle' ? 'in' : 'out';

        await this.props.dispatch(actions.bicycleRecycle({
            carno, latitude, longitude, accuracy, type,
        }));
        this.props.result.code === 0 ?
            Toast.success('成功', 1, () => this.setState({ carno: '' })) :
            Toast.fail(this.props.result.message, 1);
    }

    valid(text) {
        return /^\d{5,7}$/.test(text);
    }
});

const mapStateToProps = (state, ownProps) => {
    return {
        locationInfo: state.api.locationInfo,
        result: state.api.transportRes,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch,
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    borderTop: {
        borderTopStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#ddd'
    },
    borderBottom: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    borderLeft: {
        borderLeftStyle: 'solid',
        borderLeftWidth: 1,
        borderLeftColor: '#ddd'
    },
    borderRight: {
        borderRightStyle: 'solid',
        borderRightWidth: 1,
        borderRightColor: '#ddd'
    },
    inner: {
        position: 'absolute',
        flexDirection: 'column',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
};

export default connect(mapStateToProps, mapDispatchToProps)(TransportSub);
