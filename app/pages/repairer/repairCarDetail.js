import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys } from '../../runtime/api';
import { Text, View, Br, Button, List, HorizontalLayout, Toast, WingBlank, Modal, Map, TouchableHighlight, ActivityIndicator, ScrollView } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';

/**
 * 待维修车辆详情
 */
class RepairCarDetail extends Component {

    constructor(){
        super();
        this.state={
            visible: false,
            alertCopy: undefined,
            initialPosition: undefined,
            lastPosition: undefined,
            region: {
                latitude: 39.98362,
                longitude: 116.306715,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005,
            },
            annotations: []
        };
    }
    componentWillMount() {
        this.props.dispatch({type: 'clear', data: {code: undefined, alert: undefined}});
    }
    componentDidMount() {
        if(this.props.carWaitForRepairDetail && (!this.props.carDetail) ){
            let info = this.props.carWaitForRepairDetail;
            this.props.dispatch({type: 'carDetail', data: info});
        }
        // navigator.geolocation.getCurrentPosition(
        //     (position) => {
        //         console.log('InitialPosition position', position);
        //         this.setState({initialPosition: position});
        //     },
        //     (error) => alert(console.log(error)),
        //     {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
        // );
    }
    handleGoBack() {
        this.props.dispatch({type: 'clear', data: {alert: undefined, repairDone: undefined}});
        this.props.dispatch({type: 'clearSearchInfo'});
        this.props.dispatch({type: 'clearWaitForRepairDetail'});
        this.props.dispatch({type: 'clearCarDetail'});
        JumpBack();
    }

    /**
     * 查看报修地址
     */
    handleCarLocation(lat, lng) {
        let userLat = this.state.initialPosition ? this.state.initialPosition.coords.latitude : undefined;
        let userLng = this.state.initialPosition ? this.state.initialPosition.coords.longitude : undefined;
        console.log('jump to car location', lat, lng, userLat, userLng);
        this.props.dispatch({type: 'carLocation', data: {lat, lng, userLat, userLng}});
        JumpTo('repairer/carLocation', true);
    }

    /**
     * 维修完成
     */
    repairDone() {

        this.props.dispatch(api(keys.repairerRepairDone, {
            token: this.props.token,
            bicycle_number: this.props.carWaitForRepairDetail.bicycle
        }));

    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.alert && nextProps.alert != this.props.alert) {
            console.warn('error', nextProps.alert);
            this.setState({alertCopy: nextProps.alert});
            this.setState({visible: true});
        }
        if(nextProps.carWaitForRepairDetail && (!this.props.carDetail)){
            let info = nextProps.carWaitForRepairDetail;
            this.props.dispatch({type: 'carDetail', data: info});
        }

        if(nextProps.repairDone === true && !this.props.repairDone) {
            console.log('Repair done', nextProps.repairDone);
            Toast.success('维修完成', 2);
            setTimeout(function(){
                JumpBack();
            }, 2500);
        }
    }

    /**
     * 点击确认
     */
    handleConfirm() {
        this.setState({visible: false });
        this.props.dispatch({type: 'clear', data: {alert: undefined}});
    }

    onLocationChange(e) {
        let latitude = e.latitude;
        let longitude = e.longitude;
        this.setState({initialPosition: {coords: {latitude, longitude}}});
    }
    /**
     * 修改解锁码
     */
    changeUnlockCode(){
        JumpTo('repairer/unlockCode', true);
    }
    render() {
        let carSearchInfo = this.props.carSearchInfo;
        let carWaitForRepairDetail = this.props.carWaitForRepairDetail;
        console.log(carSearchInfo);
        console.log(carWaitForRepairDetail);
        if(carSearchInfo || carWaitForRepairDetail){
            return this.renderDetailPage();
        }else {
            if (this.props.msg) {
                return (<base.View><Text>{this.props.msg}</Text></base.View>);
            } else {
                return (
                    <base.View style={styles.container}>
                        <ActionBar title="车辆信息" handleBack={this.handleGoBack.bind(this) }>返回</ActionBar>
                        <base.View style={{position: 'absolute', flexDirection: 'column', left: 0, top: 44, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center'}}>
                            <ActivityIndicator size='large' src='loading'/>
                            <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                        </base.View>
                    </base.View>
                );
            }
            // 车辆详情
        }
    }
    renderDetailPage(){
        let carDetail = this.props.carDetail || {};
        return (
            <base.View style={styles.container}>
            <ActionBar title="车辆信息" handleBack={this.handleGoBack.bind(this) }>返回</ActionBar>
            <Br/>
            <ScrollView className="overflowY" style={styles.list}>

                <base.HorizontalLayout style={Object.assign({}, styles.item, styles.borderTop)}>
                    <Text>车牌号</Text>
                    <Text>{carDetail.bicycle}</Text>
                </base.HorizontalLayout>
                <TouchableHighlight style={styles.item2}  underlayColor={'#fff'}>
                    <base.HorizontalLayout style={styles.inner}>
                        <base.HorizontalLayout><Text>解锁码</Text></base.HorizontalLayout>
                        <base.HorizontalLayout style={styles.center}>
                            <Text>{carDetail.unlock_code}</Text>
                        </base.HorizontalLayout>
                    </base.HorizontalLayout>
                </TouchableHighlight>
                <Br/>

                <base.HorizontalLayout style={Object.assign({}, styles.item, styles.borderTop)}>
                    <Text>报修状态</Text>
                    <base.HorizontalLayout style={{alignItems: 'center'}}><Text>{carDetail.status === 0?'已报修' :'未报修'}</Text>
                        <base.Image style={styles.icon} src={carDetail.status === 0?'icon2' :'icon1'}  ></base.Image>
                    </base.HorizontalLayout>
                </base.HorizontalLayout>
                {carDetail.status === 0?
                    <base.VerticalLayout>
                    <base.HorizontalLayout style={styles.item}>
                        <Text>报修时间</Text>
                        <Text>{carDetail.time}</Text>
                    </base.HorizontalLayout>
                    <TouchableHighlight onPress={ this.handleCarLocation.bind(this, carDetail.address.latitude, carDetail.address.longitude) }  style={styles.item2} underlayColor={'#fff'}>
                    <base.HorizontalLayout style={styles.inner}>
                        <base.HorizontalLayout>
                            <Text>报修地址</Text>
                        </base.HorizontalLayout>
                        <base.HorizontalLayout style={styles.inner2}>
                            <Text style={{flexWrap: 'wrap'}}>{carDetail.address.actual}</Text>
                        </base.HorizontalLayout>
                        <base.HorizontalLayout>
                        <base.Image style={styles.arrow} src='arrow'></base.Image>
                        </base.HorizontalLayout>

                    </base.HorizontalLayout>
                </TouchableHighlight>
                <Br/>
                <base.VerticalLayout style={Object.assign({}, styles.itemBox, styles.borderTop)}>
                    <base.HorizontalLayout style={styles.itemTitle}>
                        <Text style={styles.itemText}>报修原因</Text>
                    </base.HorizontalLayout>
                    <base.HorizontalLayout style={styles.reasonBox}>
                        {carDetail.reason.map((item, idx) => {
                            return (
                                <base.HorizontalLayout style={styles.reason} key={idx}>
                                    <Text>{item.reason_tag} * {item.reason_num}</Text>
                                </base.HorizontalLayout>
                            );
                        })}
                    </base.HorizontalLayout>
                </base.VerticalLayout>
                    <base.HorizontalLayout style={styles.box}><Button className="blackBtn" style={styles.submitBtn} onPress={this.repairDone.bind(this)}>维修完成</Button></base.HorizontalLayout>
                    <WingBlank>
                        <Modal
                            transparent
                            visible={this.state.visible}>
                            <base.VerticalLayout style={styles.modelBox}>
                                <base.VerticalLayout style={styles.modelTitle}>
                                    <Text style={{fontSize: 17}}>{this.state.alertCopy}</Text>
                                </base.VerticalLayout>
                                <Button style={styles.modelBtn} onPress={()=> {
                                    this.setState({visible: false});
                                    this.props.dispatch({type: 'clear', data: {alert: undefined}});
                                }}>确定</Button>
                            </base.VerticalLayout>
                        </Modal>
                    </WingBlank>


                </base.VerticalLayout>:null}
            </ScrollView>
        </base.View>);
    }
}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
    /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    list: {
        flexGrow: 1
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    item: {
        height: 60,
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#fff',
        paddingLeft: 10,
        paddingRight: 10,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    inner: {
        height: 60,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    inner2: {
        justifyContent: 'flex-end',
        flexGrow: 1,
        paddingLeft: 10
    },
    borderTop: {
        borderStyle: 'solid',
        borderTopColor: '#c6c6c6',
        borderTopWidth: 1,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    item2: {
        height: 60,
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        justifyContent: 'space-between',
        backgroundColor: '#fff',
        paddingLeft: 10,
        paddingRight: 10,
        borderTopColor: '#c6c6c6',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    submitBtn: {
        backgroundColor: '#2C2C2C',
        color: '#fff',
        marginTop: 15,
        height: 44,
        borderRadius: 2,
        justifyContent: 'center',
        fontSize: 15,
        alignItems: 'center',
        flexGrow: 1,
        width: 300
    },
    box: {
        paddingLeft: 16,
        paddingRight: 16,
        justifyContent: 'center'
    },
    itemBox: {
        backgroundColor: '#fff',
        borderBottomWidth: 0
    },
    itemTitle: {
        height: 40,
        alignItems: 'center',
        paddingLeft: 10,
        borderStyle: 'solid',
        borderBottomColor: '#d8d8d8',
        borderBottomWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    itemText: {
        fontSize: 15,
    },
    reasonBox: {
        paddingLeft: 16,
        paddingRight: 16,
        flexWrap: 'wrap',
        paddingTop: 10,
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    reason: {
        height: 34,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#979797',
        borderStyle: 'solid',
        borderRadius: 8,
        marginBottom: 10,
        paddingLeft: 10,
        paddingRight: 10,
        marginRight: 10
    },
    icon: {
        width: 18,
        height: 18,
        marginLeft: 5,
    },
    arrow: {
        width: 8,
        height: 14,
        marginLeft: 5,
    },
    modelBox: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    modelTitle: {
        marginBottom: 15
    },
    modelBtn: {
        width: 200,
        height: 36,
        backgroundColor: '#ffd900',
        borderRadius: 2,
        alignItems: 'center',
        justifyContent: 'center',
    }
};

export default connect((state) => {
    return {
        token: state.api.token,
        alert: state.api.alert,
        carWaitForRepairDetail: state.api.carWaitForRepairDetail,
        repairDone: state.api.repairDone,
        carSearchInfo: state.api.carSearchInfo,
        carDetail: state.user.carDetail,
    };
})(RepairCarDetail);
