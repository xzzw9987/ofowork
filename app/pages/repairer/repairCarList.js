import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys } from '../../runtime/api';
import { Text, View, Br, Button, HorizontalLayout, VerticalLayout, ListView, List, TouchableHighlight, ActivityIndicator, ScrollView } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';

/**
 * 待维修车辆列表
 */
class RepairCarList extends Component {

    constructor(){
        super();
        this.state={
            loading: true
        };
    }

    renderLoading (){
        return (
            <base.View style={styles.container}>
                <ActionBar title="未修车辆" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.View style={{position: 'absolute', flexDirection: 'column', left: 0, top: 0, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center'}}>
                    <ActivityIndicator size='large' src='loading'/>
                    <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                </base.View>
            </base.View>
        );
    }
    componentWillMount() {
        if(this.state.loading){
            this.renderLoading();
        }
        this.props.dispatch(api(keys.carsWaitForRepairList, {token: this.props.token, page: 0, zone_id: this.props.zone_ids[this.props.zoneIndex || 0]}));
    }

    componentWillReceiveProps(nextProps){
        if(typeof nextProps.carsWaitForRepairList !== undefined && nextProps.carsWaitForRepairList != this.props.carsWaitForRepairList) {
        }
    }

    handleDetail(bicycle) {
        this.props.dispatch(api(keys.carWaitForRepairDetail, {token: this.props.token, zone_id: this.props.zone_ids[this.props.zoneIndex || 0], bicycle_number: bicycle.bicycle_num}));
        this.props.dispatch({type: 'clear', data: {alert: undefined, repairDone: undefined}});
        JumpTo('repairer/carDetail', true);
    }
    handleGoBack() {
        JumpBack();
    }
    render() {
        if(!this.props.carsWaitForRepairList){
            return (
                <base.View style={styles.container}>
                    <ActionBar title="未修车辆" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                    <base.View style={{position: 'absolute', flexDirection: 'column', left: 0, top: 44, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center'}}>
                        <ActivityIndicator size='large' src='loading'/>
                        <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            );
        }else{
            return this.renderListPage();
        }
    }
    renderListPage (){
        let carsWaitForRepairList = this.props.carsWaitForRepairList || {list: []};
        return (
            <base.View style={styles.container}>
                <ActionBar title="未修车辆" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <Br/>
                <ScrollView className="overflowY" style={styles.list}>

                        {carsWaitForRepairList && carsWaitForRepairList.list && carsWaitForRepairList.list.length > 0 ?
                            carsWaitForRepairList.list.map(((item, i) => {
                                let reasones = item.reason.trim().split(',');
                                return (
                                    <TouchableHighlight style={styles.item} key={i} onPress={ () => { this.handleDetail(item); } } underlayColor={'#fff'}>
                                    <base.VerticalLayout>
                                        <base.HorizontalLayout style={styles.top}>
                                            <Text style={{fontSize: 16, color: '#000'}}>车牌号：{item.bicycle_num}</Text>
                                            <Text style={styles.color2}>{item.time}</Text>
                                        </base.HorizontalLayout>
                                        <base.HorizontalLayout style={styles.infoTitle}>
                                            <Text style={styles.color2}>报修原因</Text>
                                        </base.HorizontalLayout>
                                        <base.HorizontalLayout style={styles.infoText}>
                                            {reasones.map((i, idx) => {
                                                let content = i;
                                                if(idx < reasones.length - 1) {
                                                    content += '，';
                                                }
                                                //console.log(content);
                                                return (
                                                    <base.HorizontalLayout style={styles.reason} key={idx}>
                                                        <Text style={{fontSize: 16, color: '#000'}}>{content}</Text>
                                                    </base.HorizontalLayout>);
                                            })}
                                        </base.HorizontalLayout>
                                     </base.VerticalLayout>
                                    </TouchableHighlight>
                                );
                            }))
                            : <base.HorizontalLayout><Text style={{fontSize: 16, color: '#000'}}>暂无维修车辆</Text></base.HorizontalLayout>}

                 </ScrollView>
            </base.View>
        );
    }
}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
    /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    color2: {
        color: '#7f7f7f',
        fontSize: 15,
    },
    list: {
        flexGrow: 1
    },
    item: {
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        borderTopColor: '#c6c6c6',
        borderTopWidth: 1,
        marginBottom: 8,
        backgroundColor: '#fff',
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    top: {
        height: 44,
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        paddingRight: 10,
        paddingLeft: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    info: {
        paddingRight: 10,
        paddingLeft: 10,
    },
    infoTitle: {
        height: 30,
        alignItems: 'center',
        paddingRight: 10,
        paddingLeft: 10,
    },
    infoText: {
        flexDirection: 'column',
        flexWrap: 'wrap',
        paddingRight: 10,
        paddingLeft: 10,
    },
    textItem: {
        height: 25,
        alignItems: 'center',
    },
    reason: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 25,
        paddingBottom: 10,
    }
};

export default connect((state) => {
    return {
        token: state.api.token,
        userInfo: state.api.userInfo,
        zone_ids: state.api.zone_ids,
        zoneIndex: state.user.zoneIndex,
        carsWaitForRepairList: state.api.carsWaitForRepairList
    };
})(RepairCarList);
