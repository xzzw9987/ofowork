import React, { Component } from 'react';
import * as base from '../../base';
// import * as actions from '../../runtime/action';
import { api, keys } from '../../runtime/api';
import { Text, View, Br, Button, TextInput, HorizontalLayout, VerticalLayout, List, WingBlank, Modal, Toast, Map, TouchableHighlight, ActivityIndicator } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';

var moment = require('moment');

/**
 * 修车师傅签到
 */
class RepairCheckIn extends Component {
    constructor() {
        super();
        this.state = {
            visible: false,
            alertCopy: undefined,
            initialPosition: undefined,
            lastPosition: undefined,
            region: {
                latitude: 39.98362,
                longitude: 116.306715,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005,
            },
            annotations: []
        };
    }

    componentWillMount() {
        this.props.dispatch({ type: 'clear', data: { code: undefined, alert: undefined } });
        let today = moment().format('YYYY-MM-DD');
        this.props.dispatch(api(keys.repairerCheckRecords, { token: this.props.token, date: today, page: 0 }));
    }

    /**
     * 签到记录
     */
    handlecheckRecords() {
        this.props.dispatch({ type: 'clear', data: { ret: undefined, alert: undefined } });
        JumpTo('repairer/checkRecords', true);
    }

    /**
     * 签到
     */
    handleCheckIn() {

        let checkRecord = this.props.checkRecords.list[0];
        if (checkRecord.sign_in_time) {
            // have checked in
            Toast.success('今天已签到', 2);
            return;
        }

        if (!this.state.initialPosition) {
            return Toast.fail('定位失败', 2);
        }
        let initialPosition = this.state.initialPosition;
        let lat = initialPosition ? initialPosition.coords.latitude : undefined;
        let lng = initialPosition ? initialPosition.coords.longitude : undefined;
        if (!lat || !lng) {
            Toast.fail('定位失败', 2);
        } else {
            // this.props.dispatch(actions.postAttendance({
            //     type: 'in',
            //     lat,
            //     lng,
            // }));
            this.props.dispatch(api(keys.repairerCheckInOut, { token: this.props.token, type: 'in', lat, lng }));
        }
    }

    componentWillReceiveProps(nextProps) {
        let isCode = this.props.ret && nextProps.ret ? nextProps.ret.code != this.props.ret.code : true;
        // 签到成功
        if (nextProps.ret && nextProps.ret.code === 0 && isCode) {
            Toast.success('操作成功', 2);
            this.props.dispatch({ type: 'clear', data: { ret: undefined, alert: undefined } });
            let today = moment().format('YYYY-MM-DD');
            this.props.dispatch(api(keys.repairerCheckRecords, { token: this.props.token, date: today, page: 0 }));

        }
        // 签到失败
        if ((nextProps.code != 0 && nextProps.alert) && nextProps.alert) {
            this.setState({ alertCopy: nextProps.alert });
            this.setState({ visible: true });
        }
    }

    componentWillUnmount() {
        this.props.dispatch({ type: 'clear', data: { ret: undefined, alert: undefined } });
        navigator.geolocation.clearWatch(this.watchID);
    }

    /**
     * 签退
     */
    handleCheckOut() {

        let checkRecord = this.props.checkRecords.list[0];
        if (checkRecord.sign_out_time) {
            // have checked out
            Toast.success('今天已签退', 2);
            return;
        }

        if (!checkRecord.sign_in_time) {
            this.setState({ alertCopy: '今天还未签到' });
            this.setState({ visible: true });
            return;
        }

        if (!this.state.initialPosition) {
            return Toast.fail('定位失败');
            //return this.setState({visible: true});
        }
        let lat = this.state.initialPosition.coords.latitude;
        let lng = this.state.initialPosition.coords.longitude;
        if (!lat || !lng) {
            return Toast.fail('定位失败');
            //return this.setState({visible: true});
        } else {
            // this.props.dispatch(actions.postAttendance({
            //     type: 'out',
            //     lat,
            //     lng,
            // }));
            this.props.dispatch(api(keys.repairerCheckInOut, { token: this.props.token, type: 'out', lat, lng }));
        }
    }

    /**
     * 点击确认
     */
    handleConfirm() {
        this.setState({ visible: false });
        this.props.dispatch({ type: 'clear', data: { ret: undefined, alert: undefined } });
    }

    handleGoBack() {
        this.props.dispatch({ type: 'clear', data: { ret: undefined, alert: undefined } });
        JumpBack();
    }

    onLocationChange(e) {
        let latitude = e.latitude;
        let longitude = e.longitude;
        this.setState({ initialPosition: { coords: { latitude, longitude } } });
    }

    render() {
        let checkRecords = this.props.checkRecords;
        if (!checkRecords || !checkRecords.list) {
            return (
                <base.View style={styles.container}>
                    <ActionBar title="考勤" handleBack={this.handleGoBack.bind(this)} rightItem="考勤记录" rightAction={this.handlecheckRecords.bind(this)}>返回</ActionBar>

                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 44,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='loading' />
                        <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            );
        }

        let checkRecord = checkRecords.list[0];
        return (
            <base.View style={styles.container}>
                <ActionBar title="考勤" handleBack={this.handleGoBack.bind(this)} rightItem="考勤记录"
                    rightAction={this.handlecheckRecords.bind(this)}>返回</ActionBar>
                <Br />
                <base.VerticalLayout style={{ flexGrow: 1 }}>
                    <TouchableHighlight style={styles.item} className="boxSizing"
                        onPress={this.handleCheckIn.bind(this)} underlayColor={'#fff'}>
                        <base.VerticalLayout>
                            {checkRecord.sign_in_time ?
                                <base.VerticalLayout style={styles.center}>
                                    <base.Image style={styles.icon} src='notcheck'></base.Image>
                                    <base.VerticalLayout className="height30" style={styles.box}>
                                        <Text style={styles.title}>已签到</Text>
                                    </base.VerticalLayout>
                                    <base.VerticalLayout style={{ height: 24 }}><Text
                                        style={{ fontSize: 16, color: '#000' }}>{checkRecord.sign_in_time}</Text>
                                    </base.VerticalLayout>
                                </base.VerticalLayout> :
                                <base.VerticalLayout style={styles.center}>
                                    <base.Image style={styles.icon} src='checkin'></base.Image>
                                    <base.VerticalLayout className="height30" style={styles.box}>
                                        <Text style={styles.title}>上班签到</Text>
                                    </base.VerticalLayout>
                                    <base.VerticalLayout style={{ height: 24 }}><Text style={styles.text}>未签到</Text></base.VerticalLayout>
                                </base.VerticalLayout>}
                        </base.VerticalLayout>
                    </TouchableHighlight>


                    <Br />


                    <TouchableHighlight style={styles.item} className="boxSizing"
                        onPress={this.handleCheckOut.bind(this)} underlayColor={'#fff'}>
                        <base.VerticalLayout>
                            {checkRecord.sign_out_time || !checkRecord.sign_in_time ?
                                <base.VerticalLayout style={styles.center}>
                                    <base.Image style={styles.icon} src='notcheck'></base.Image>
                                    <base.VerticalLayout className="height30" style={styles.box}>
                                        <Text style={styles.title}> {checkRecord.sign_out_time ? '已签退' : '下班签退'}</Text>
                                    </base.VerticalLayout>
                                    <base.VerticalLayout style={{ height: 24 }}><Text style={{
                                        fontSize: 16,
                                        color: '#000'
                                    }}>{checkRecord.sign_out_time ? checkRecord.sign_out_time : '未签退'}</Text></base.VerticalLayout>
                                </base.VerticalLayout> :
                                <base.VerticalLayout style={styles.center}>
                                    <base.Image style={styles.icon} src='checkout'></base.Image>
                                    <base.VerticalLayout className="height30" style={styles.box}>
                                        <Text style={styles.title}>下班签退</Text>
                                    </base.VerticalLayout>
                                    <base.VerticalLayout style={{ height: 24 }}><Text style={styles.text}>未签退</Text></base.VerticalLayout>
                                </base.VerticalLayout>}
                        </base.VerticalLayout>
                    </TouchableHighlight>


                    <WingBlank>
                        <Modal
                            transparent
                            visible={this.state.visible}>
                            <base.VerticalLayout style={styles.modelBox}>
                                <base.VerticalLayout style={styles.modelTitle}>
                                    <Text style={{ fontSize: 17 }}>{this.state.alertCopy}</Text>
                                </base.VerticalLayout>
                                <Button style={styles.modelBtn} onPress={() => {
                                    this.setState({ visible: false });
                                }}>知道了</Button>
                            </base.VerticalLayout>
                        </Modal>
                    </WingBlank>
                </base.VerticalLayout>

                <base.VerticalLayout style={styles.bottom}>
                    <base.VerticalLayout style={styles.botTitle}><Text style={{ fontSize: 16, color: '#000' }}>负责区域</Text>
                    </base.VerticalLayout>
                    {this.props.zoneList.map((item, idx) => {
                        return (
                            <base.VerticalLayout style={styles.name} key={idx}><Text style={styles.text}>{item}</Text>
                            </base.VerticalLayout>
                        );
                    })}
                </base.VerticalLayout>
                <Map style={{ width: 1, height: 1 }} region={this.state.region} annotations={this.state.annotations} showsUserLocation={true} onLocationChange={this.onLocationChange.bind(this)} />
            </base.View>
        );
    }
}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    center: {
        alignItems: 'center',
    },
    item: {
        backgroundColor: '#fff',
        paddingTop: 15,
        paddingBottom: 10,
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        borderTopColor: '#c6c6c6',
        borderTopWidth: 1,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    icon: {
        width: 60,
        height: 60,
    },
    box: {
        justifyContent: 'center',
        height: 20,
    },
    text: {
        color: '#7f7f7f',
        fontSize: 15
    },
    botTitle: {
        height: 40,
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    name: {
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
    },
    bottom: {
        backgroundColor: '#fff',
        borderStyle: 'solid',
        borderTopColor: '#c6c6c6',
        borderTopWidth: 1,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },

    modelBox: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    modelTitle: {
        marginBottom: 15
    },
    modelBtn: {
        width: 200,
        height: 36,
        backgroundColor: '#ffd900',
        borderRadius: 2,
        alignItems: 'center',
        justifyContent: 'center',
    }
};

export default connect((state) => {
    return {
        token: state.api.token,
        ret: state.api.ret,
        alert: state.api.alert,
        zoneList: state.user.zoneList,
        checkRecords: state.api.checkRecords,
    };
})(RepairCheckIn);
