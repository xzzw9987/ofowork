import React, {Component} from 'react';
import * as base from '../../base';
import {api, keys, apibase, getUserInfo} from '../../runtime/api';
import {Text, View, Br, Button, Toast, TextInput, ActivityIndicator, TouchableHighlight, Map, Image} from '../../base';
import {connect} from 'react-redux';
import {JumpTo, historyStack, JumpBack} from '../../modules';
import {ActionBar} from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import whereami from '../../base/map/whereami';
import * as actions from '../../runtime/action';
import ImagePicker from '../../components/gallery';
import {fetch} from '../../modules';
import StaticImage from '../../base/staticImages';
import geoMessage from '../../base/map/geoMessage';

/**
 * 坏车标记
 */

const RepairBadCarMark = pageWrapper(class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            num: '',
            imgPath: '',
            token: '',
            latitude: '',
            longitude: '',
            address: '',
            loading: false

        };
    }


    componentDidMount() {
        this.props.componentDidMount && this.props.componentDidMount();
    }

    // 返回
    handleBack() {
        this.props.clear && this.props.clear();
        JumpBack();
    }

    // 提交
    async submitInfo() {
        if (this.state.imgPath === '') {
            Toast.fail('请上传照片', 2);
            return;
        }
        let regu = /^([1-9]\d?)$/;
        let reguRst = regu.test(this.state.num);
        if (!reguRst) {
            Toast.fail('车辆数应介于1-99之间', 2);
            return;
        }
        this.setState({loading: true});
        let res = await this.props.submitImg({img: this.state.imgPath});
        if (res.code === 0) {
            res = await this.props.submitInfo({
                num: this.state.num,
                img_path: res.data.img_path,
                location: {
                    latitude: this.props.subBadCarMarkPos.latitude,
                    longitude: this.props.subBadCarMarkPos.longitude,
                }
            });
            if (res.code === 0) {
                Toast.success('上传成功', 1);
                this.setState({
                    loading: false,
                    num: '',
                    imgPath: ''
                });
            } else {
                Toast.fail('上传失败', 1);
                this.setState({loading: false});
            }
        } else {
            Toast.fail('图片上传失败', 1);
            this.setState({loading: false});
        }
    }

    repairBadCarMarkPos() {
        this.props.dispatch({type: 'clear', data: {ret: undefined, alert: undefined}});
        JumpTo('repairer/repairBadCarMarkPos', true);
    }

    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderRepairBadCarMark();
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="坏车标记" handleBack={this.handleBack.bind(this)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <base.View style={{position: 'relative', flexGrow: 1}}>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='images/loading.gif'/>
                        <base.HorizontalLayout style={{marginTop: 10}}><Text>图片上传中 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }

    renderRepairBadCarMark() {
        return (
            <base.VerticalLayout className="wrap" style={styles.container}
                                 onStartShouldSetResponder={() => this.getKeyboardState()}
                                 onResponderGrant={e => this.dismissKeyboard && this.dismissKeyboard(e)}>
                <ActionBar title="坏车标记" handleBack={this.handleBack.bind(this)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <base.VerticalLayout style={styles.imgWrap}>
                    {/* 上传图片 */}
                    <ImagePicker options={{chooseFromLibraryButtonTitle: null}} onSelectPhoto={base64Data => {
                        this.setState({imgPath: base64Data});
                    }} style={{backgroundColor: '#fff'}} desc="请拍摄坏车和背景环境照片"/>
                </base.VerticalLayout>
                <base.HorizontalLayout style={{...styles.searchBox, ...createBorderDetailStyle(1, 0, 1, 0)}}>
                    <base.VerticalLayout>
                        <base.Text>坏车数</base.Text>
                    </base.VerticalLayout>
                    <base.VerticalLayout style={styles.searchWrap}>
                        <TextInput style={styles.searchInput} placeholder='请输入车辆数' keyboardType="numeric"
                                   value={this.state.num} onChangeText={(text) => {
                                       this.setState({num: parseInt(text)});
                                   } }></TextInput>
                    </base.VerticalLayout>
                </base.HorizontalLayout>
                <base.VerticalLayout style={{...createPaddingStyle(20, 15, 0, 15)}}>
                    <base.HorizontalLayout style={styles.chooseText}>
                        <base.Text style={styles.gpsText}>地址</base.Text>
                        <TouchableHighlight style={{...styles.gps, ...createPaddingStyle(0, 20, 0, 20)}}
                                            onPress={this.repairBadCarMarkPos.bind(this)}>
                            <base.HorizontalLayout>
                                <Image className="fit" src="map" style={styles.mapImg}></Image>
                                <base.Text>请选择位置</base.Text>
                            </base.HorizontalLayout>
                        </TouchableHighlight>
                    </base.HorizontalLayout>
                    <base.HorizontalLayout>
                        <base.Text style={{
                            fontSize: 14,
                            color: '#666',
                            marginTop: 15
                        }}>{this.props.subBadCarMarkPos.address ? this.props.subBadCarMarkPos.address : null}</base.Text>
                    </base.HorizontalLayout>
                </base.VerticalLayout>
                <Button style={{...styles.requireBtn, ...createMarginStyle(30, 15, 10, 15)}}
                        onPress={this.submitInfo.bind(this)}>提交</Button>
            </base.VerticalLayout>
        );
    }

});

// 整合样式
const createStyle = (type) => (top, right, bottom, left) => ({
    [`${type}Top`]: top,
    [`${type}Right`]: right,
    [`${type}Bottom`]: bottom,
    [`${type}Left`]: left,
});
const createBordersStyle = (type) => (top, right, bottom, left) => ({
    [`${type}TopWidth`]: top,
    [`${type}RightWidth`]: right,
    [`${type}BottomWidth`]: bottom,
    [`${type}LeftWidth`]: left,
});
const createBorderStyle = createStyle('border');
const createMarginStyle = createStyle('margin');
const createPaddingStyle = createStyle('padding');
const createBorderDetailStyle = createBordersStyle('border');

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff',
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    imgWrap: {
        paddingTop: 30,
        paddingBottom: 30,
        alignItems: 'center',
        justifyContent: 'center',
        height: 160
    },
    searchWrap: {
        marginLeft: 20,
        flex: 1,
    },
    searchBox: {
        height: 65,
        backgroundColor: '#fff',
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderTopColor: 'rgb(221,221,221)',
        alignItems: 'center',
        paddingLeft: 15,
    },
    searchInput: {
        height: 44,
        fontSize: 16,
        flexGrow: 1,
        flexBasis: 0,
    },
    chooseText: {
        justifyContent: 'space-between',
        backgroundColor: 'rgb(255, 255, 255)',
        flexDirection: 'row',
        display: 'flex',
    },
    requireBtn: {
        display: 'flex',
        height: 48,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        backgroundColor: '#333',
        fontSize: 15,
        color: '#fff',
    },
    gpsText: {
        display: 'flex',
        alignItems: 'center',
        paddingTop: 5,
        color: 'rgb(170, 170, 170)',
        justifyContent: 'center'
    },
    gps: {
        display: 'flex',
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#eee',
        backgroundColor: '#eee',
        borderRadius: 50,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    mapImg: {
        width: 15,
        height: 16,
        marginRight: 5,
    },
    choiceText: {
        display: 'flex',
        height: 30,
        alignItems: 'center',
        justifyContent: 'center'
    }
};

const mapStateToProps = state => {
    return {
        subBadCarMarkPos: state.api.subBadCarMarkPos || {}
    };
};

const mapDispatchToProps = dispatch => {
    return {
        dispatch,
        async submitImg(message){
            const {token} = await getUserInfo();
            return await fetch(`${apibase}/user/upload-img-with-base64`, {
                method: 'POST',
                body: {token, ...message}
            });
        },

        async submitInfo(options){
            const {token} = await getUserInfo();
            const url = `${apibase}/repairer/record-broken-bicycle`;
            return await fetch(url, {
                method: 'POST',
                body: {
                    token,
                    img_path: options.img_path,
                    num: options.num,
                    lat: options.location.latitude,
                    lng: options.location.longitude,
                    // 'accuracy': 15,
                }
            });
        },

        async componentDidMount(){
            const combinedLocationMessage =
                await [
                    whereami,
                    geoMessage
                ].reduce(
                    (prev, next) => prev.then(d => next(d).then(d2 => ({...d, ...d2}))),
                    Promise.resolve({})
                );
            dispatch(actions.subBadCarMarkPos(combinedLocationMessage));
        },

        clear(){
            return dispatch(actions.clearBadCarPos());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(RepairBadCarMark);



