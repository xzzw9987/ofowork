import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, Br, Button, ListView, List, Toast, ScrollView, TouchableHighlight, ActivityIndicator, HorizontalLayout, VerticalLayout, Image } from '../../base';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import WorkBench from '../../components/workBench';
import Loading from '../../modules/loading';
import * as actions from '../../runtime/action';

const RepairerBench = pageWrapper(class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
        };
    }

    viewWillAppear() {
        this.props.fetch(this.props).then(() => this.setState({ loading: false, }));
        this.props.dispatch({ type: 'clearSearchInfo', });
        this.props.dispatch({ type: 'clearCarDetail', });
        this.props.dispatch({ type: 'clearVirtualPileInfo', });
    }

    renderBench() {
        const { signInData } = this.props;
        return (
            <View style={styles.container}>
                <ActionBar title="工作台"></ActionBar>
                <VerticalLayout style={styles.sign}>
                    <HorizontalLayout style={styles.title}>
                        <Text>本月考勤</Text>
                    </HorizontalLayout>
                    <HorizontalLayout style={styles.signIn}>
                        <VerticalLayout style={styles.signItem}>
                            <Text style={styles.f24}>{signInData.sign_in_cnt}</Text>
                            <Text>签到天数</Text>
                        </VerticalLayout>
                        <VerticalLayout style={styles.line}>
                            <View style={styles.vLine}></View>
                        </VerticalLayout>
                        <VerticalLayout style={styles.signItem}>
                            <Text style={styles.f24}>{signInData.sign_out_cnt}</Text>
                            <Text>签退天数</Text>
                        </VerticalLayout>
                        <VerticalLayout style={styles.line}>
                            <View style={styles.vLine}></View>
                        </VerticalLayout>
                        <VerticalLayout style={styles.signItem}>
                            <Text style={styles.f24}>{signInData.work_time}</Text>
                            <Text>累计工作小时</Text>
                        </VerticalLayout>
                    </HorizontalLayout>
                </VerticalLayout>
                <VerticalLayout style={styles.jump}>
                    <WorkBench buttonNum={3} showTitle={true} isVertical={true} {...this.props}/>
                </VerticalLayout>
            </View>
        );
    }

    render() {
        if (this.state.loading)
            return <Loading/>;
        return this.renderBench();
    }
});

const mapStateToProps = (state) => {
    return {
        signInData: state.api.signInData,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        dispatch,
        fetch: props => dispatch(actions.fetchRepairerBench({ ...props }))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(RepairerBench);

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff',
    },
    jump: {
        borderTopStyle: 'solid',
        borderTopColor: '#d8d8d8',
        borderTopWidth: 1,
        position: 'relative',
        flexGrow: 1,
    },
    bannerContainer: {
        alignItems: 'center'
    },
    banner: {
        flexGrow: 1,
        flexBasis: 0,
    },
    sign: {
        height: 107,
        backgroundColor: '#fff',
        paddingLeft: 10,
        paddingRight: 10,
        borderBottomStyle: 'solid',
        borderBottomColor: '#d8d8d8',
        borderBottomWidth: 1,
    },
    title: {
        height: 40,
        alignItems: 'center',
        borderBottomStyle: 'solid',
        borderBottomColor: '#d8d8d8',
        borderBottomWidth: 1,
    },
    signIn: {
        height: 66,
        justifyContent: 'center',
    },
    signItem: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexShrink: 1,
        flexBasis: 0,
    },
    line: {
        flexGrow: 0,
        flexShrink: 0,
        flexBasis: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    vLine: {
        width: 1,
        height: 40,
        backgroundColor: '#d8d8d8',
    },
    f24: {
        fontSize: 24,
    },
};
