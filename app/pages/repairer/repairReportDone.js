import React, {Component} from 'react';
import * as base from '../../base';
import {api, keys, loadToken, apibase, _loadToken, getUserInfo} from '../../runtime/api';
import {Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator, Map, TextInput, UselessTextInput} from '../../base';
import {connect} from 'react-redux';
import {JumpTo, historyStack, JumpBack} from '../../modules';
import {ActionBar} from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import Swiper, {Item} from '../../components/swiper';

import whereami from '../../base/map/whereami';
import ImagePicker from '../../components/gallery';

var moment = require('moment');
import {fetch} from '../../modules';

/**
 * 举报详情
 */

class ReportDone extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: false,
            historyState: historyStack.get(),
            titleText: historyStack.get().type == 'not_found' ? '没找到车' : '处理完成',
            number: 150,
            initialPosition: undefined,
            lastPosition: undefined,
            region: {
                latitude: undefined,
                longitude: undefined,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005,
            },
            value: '',
            imgPath: ''
        };
    }

    componentWillMount() {

    }
    componentDidMount(){

    }

    handleBack(){
        JumpBack();
    }
    /**
     * 备注
     */
    onChange(text){
        this.setState({value: text});
        this.setState({number: 150 - text.length });
    }
    /**
     * 提交
     */
    submitReportInfo(){
        // this.setState({value: this.state.imgPath});
        if(historyStack.get().type == 'processed' ){//如果处理完成必须传照片
            if( this.state.imgPath == ''){
                Toast.fail('必须上传照片', 2);
                return ;
            }else{
                this.setState({loading: true});
            }
        }
        let _token;
        let _this = this;
        function fetchReport(respones) {
            let imgPath = '';
            if (respones){
                imgPath = respones['data']['img_path'];
            }
            fetch(`${apibase}/maintenance/process`, {
                method: 'POST',
                body: {token: _token,
                    'maintenance_id': historyStack.get().id,
                    'img_path': imgPath,
                    'info': _this.state.value,
                    'process_type': historyStack.get().type,
                    'maintenance_type': historyStack.get().maintenanceType
                }
            }).then(respones2=>{
                _this.setState({loading: false});
                if(respones2['code'] === 0){
                    Toast.success('提交成功', 1);
                    setTimeout(function(){
                        JumpTo('repair/entry', false);
                    }, 1);
                }else{
                    Toast.fail(respones2['message'], 1.5);
                }
            });
        }
        return getUserInfo()
        .then(userInfo=> {
            let {token} = userInfo;
            _token = token;
            if(historyStack.get().type == 'processed'){
                fetch(`${apibase}/user/upload-img-with-base64`, {
                    method: 'POST',
                    body: {token, 'img': this.state.imgPath}
                }).then(respones=>{
                    if(respones['code'] === 0){
                    //alert(respones['data']['img_path'] );
                        fetchReport(respones);
                    }else{
                        this.setState({loading: false});
                        Toast.fail('图片上传失败', 1.5);
                    }
                });
            }else{
                fetchReport();
            }
        });
    }

    render() {

        return this.renderReportDetail();
    }

    renderReportDetail() {
        return (
            <base.VerticalLayout className="wrap" style={styles.container} onStartShouldSetResponder={()=>this.getKeyboardState()} onResponderGrant={e=>this.dismissKeyboard && this.dismissKeyboard(e)}>
                <ActionBar title={this.state.titleText} handleBack={this.handleBack.bind(this)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <base.VerticalLayout style={{
                    paddingTop: 30,
                    paddingBottom: 30,
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: 170
                }}>

                    {/* 上传图片 */}
                    <ImagePicker onSelectPhoto={base64Data=>{ this.setState({imgPath: base64Data}); }} style={{backgroundColor: '#fff'}}  desc="请拍摄举报位置周围真实环境"/>

                </base.VerticalLayout>
                <base.VerticalLayout style={{
                    paddingLeft: 10,
                    paddingRight: 10,
                    paddingBottom: 30
                }}>
                    <UselessTextInput style={{
                        height: 120,
                        backgroundColor: '#e5e5e5',
                        padding: 10,
                        borderWidth: 0,
                        fontSize: 14,
                        alignItems: 'flex-start'
                    }}
                        multiline={true}
                        numberOfLines={4}
                        maxLength={150}
                        placeholder="备注"
                        onChangeText={(text) => { this.onChange(text); } }
                    />
                    <base.HorizontalLayout style={{
                        paddingTop: 5,
                        justifyContent: 'flex-end'
                    }}>
                        <Text style={{
                            color: '#999',
                            fontSize: 12
                        }}>还剩{this.state.number}字</Text>
                    </base.HorizontalLayout>
                </base.VerticalLayout>
                <base.VerticalLayout style={{
                    flex: 1,
                    borderStyle: 'solid',
                    borderColor: '#979797',
                    borderBottomWidth: 1,
                    borderTopWidth: 1,
                    borderLeftWidth: 0,
                    borderRightWidth: 0,
                    backgroundColor: '#F6F6F6'
                }}>
                </base.VerticalLayout>
                <base.VerticalLayout style={{
                    paddingLeft: 20,
                    paddingRight: 20,
                    justifyContent: 'center',
                }}>
                    <Button style={{
                        height: 48,
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderRadius: 6,
                        backgroundColor: '#333',
                        flexGrow: 1,
                        marginTop: 16,
                        marginBottom: 16
                    }} onPress={this.submitReportInfo.bind(this)}>
                        <Text style={{fontSize: 18, color: '#fff'}}>提交</Text>
                    </Button>
                </base.VerticalLayout>
                {this.state.loading ? <base.View style={{position: 'absolute', flexDirection: 'column', left: 0, top: 0, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,.5)'}}>
                        <ActivityIndicator size='large' src='loading'/>
                    <base.HorizontalLayout style={{marginTop: 10}}><Text>图片上传中 ...</Text></base.HorizontalLayout>
                </base.View> :null }
           </base.VerticalLayout>
        );
    }

}

export default  pageWrapper(ReportDone);
const styles = {
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff',
        position: 'relative'
    }
};



