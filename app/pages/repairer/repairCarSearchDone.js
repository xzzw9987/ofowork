import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys } from '../../runtime/api';
import { Text, View, List, br} from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';

/**
 * 搜索车辆
 */
class RepairCarSearchDone extends Component {
    handleGoBack() {
        JumpBack();
    }

    render() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="车辆信息" handleBack={this.handleGoBack.bind(this) }>返回</ActionBar>
                <base.Br />
                <base.VerticalLayout style={styles.list}>
                    <base.HorizontalLayout style={Object.assign({}, styles.item, styles.borderTop)}>
                        <Text style={{fontSize: 16, color: '#000'}}>报修状态</Text>
                        <base.HorizontalLayout><Text style={{fontSize: 16, color: '#000'}}>未报修</Text>
                         <base.Image style={styles.icon} src='icon1'></base.Image>
                        </base.HorizontalLayout>
                    </base.HorizontalLayout>
                    <base.HorizontalLayout style={styles.item}>
                        <Text style={{fontSize: 16, color: '#000'}}>车牌号</Text>
                        <Text style={{fontSize: 16, color: '#000'}}>123</Text>
                    </base.HorizontalLayout>
                    <base.HorizontalLayout style={styles.item}>
                        <Text style={{fontSize: 16, color: '#000'}}>解锁码</Text>
                        <Text style={{fontSize: 16, color: '#000'}}>123456</Text>
                    </base.HorizontalLayout>
                </base.VerticalLayout>
            </base.View>);
    }
}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
    /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    item: {
        height: 60,
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#fff',
        paddingLeft: 10,
        paddingRight: 10,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    borderTop: {
        borderStyle: 'solid',
        borderTopColor: '#c6c6c6',
        borderTopWidth: 1,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    icon: {
        width: 18,
        height: 18,
        paddingLeft: 5,
    },
};

export default connect((state) => {
    return {
    };
})(RepairCarSearchDone);
