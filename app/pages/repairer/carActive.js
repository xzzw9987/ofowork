import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as base from '../../base';
import { Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator } from '../../base';
import Modal from '../../components/commonModal';
import CameraView from '../../components/camera';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import * as actions from '../../runtime/action';
import pageWrapper from '../../modules/pageWrapper';
import Loading from '../../modules/loading';

const platePrefix = 'http://ofo.so/plate/';
const ActivePanel = pageWrapper(class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            showCam: false,
        };
        this.active = true;
        this.historyState = historyStack.get();
    }

    async viewWillAppear() {
        this.active = true;
        this.haveRead = false;

        await this.props.dispatch(actions.getZone());
        // this.props.locationInfo.error ?
        //     this.setState({ loading: false, }) :
        //     this.setState({ loading: false, showCam: true, });
        this.setState({ loading: false, showCam: true });
    }

    viewWillDisappear() {
        this.active = false;
        this.setState({
            showCam: false,
        });
    }

    // componentWillUnmount() {
    //     this.isUnmount = true;
    // }

    handleBack() {
        JumpBack();
    }

    renderPanel() {
        return (
            <View style={styles.container}>
                <ActionBar title={'新车激活'}
                    handleBack={() => this.handleBack()}
                    rightAction={() => {
                    }}>
                    <Text style={{ fontSize: 15, color: '#000', }}>返回</Text>
                </ActionBar>
                {/*this.state.showCam ? (<base.HorizontalLayout style={styles.itemBot}>
                    <base.Image style={styles.icon} src='icon'></base.Image>
                    <base.HorizontalLayout style={styles.address}>
                        <base.Text style={{ flexWrap: 'wrap', fontSize: 14, }}>
                            {this.historyState.selected ? this.historyState.selected.zone_name : (this.props.zoneInfo ? this.props.zoneInfo.zone_name : '')}
                        </base.Text>
                    </base.HorizontalLayout>
                    <base.TouchableHighlight
                        underlayColor={'#f3f3f3'}
                        activeOpacity={.1}
                        onPress={() => this.zoneList()}>
                        <base.VerticalLayout style={{ alignItems: 'center', }}>
                            <base.Image style={styles.arrow} src='arrow'></base.Image>
                        </base.VerticalLayout>
                    </base.TouchableHighlight>
                </base.HorizontalLayout>) : null*/}
                {this.state.showCam ? (<CameraView onBarCodeRead={d => this.onBarCodeRead(d)} hint={'扫描获取车牌'} buttons={this.renderButtons()}
                    torch={true} />) : null}
            </View>
        );
    }

    render() {
        if (this.state.loading)
            return <Loading />;
        return this.renderPanel();
    }

    handlePress() {
        // if (this.historyState.selected) {
        //     const { zone_id, zone_name, zone_level, } = this.historyState.selected;
        //     JumpTo('activeInput', true, { zone_id, zone_name, zone_level, });
        // } else if (!this.props.zoneInfo.zone_id) {
        //     Toast.fail('请选择所在区域', 1);
        //     return;
        // } else if (this.props.locationInfo.error) {
        //     Toast.fail('定位糙了');
        // } else {
        //     JumpTo('activeInput', true, {});
        // }
        JumpTo('activeInput', true, {});
    }

    // zoneList() {
    //     JumpTo('chooseZone', true, {
    //         select: (options) => {
    //             this.historyState.selected = options;
    //             if (!this.isUnmount)
    //                 this.forceUpdate();
    //         },
    //     });
    // }

    async onBarCodeRead(d) {
        d = d.trim();
        if (this.haveRead || !this.active) return;
        this.haveRead = true;
        if (!this.valid(d)) {
            Toast.fail('仅支持扫描7位车牌上的二维码', 1, () => this.haveRead = false);
            return;
        }
        // if (!this.props.zoneInfo.zone_id) {
        //     Toast.fail('请选择所在区域', 1, () => this.haveRead = false);
        //     return;
        // }
        this.setState({ carno: d.substring(platePrefix.length, d.length), loading: true, });
        // const { zone_id, zone_name, zone_level, } = this.historyState.selected ? this.historyState.selected : this.props.zoneInfo;
        const { zone_id, zone_name, zone_level } = this.props.zoneInfo;
        // const { latitude, longitude, accuracy } = this.props.locationInfo;
        const { carno } = this.state;

        const m = await this.props.fetch(Object.assign({}, this.state, this.props));
        this.setState({ loading: false });
        if (m.code !== 0) {
            Toast.fail(m.message, 3, () => this.haveRead = false);
        } else {
            Modal({
                title: m.data.lock_type === 1 ? '机械锁车辆' : '智能锁车辆',
                content: m.data.lock_type === 1 ? `车牌号：${carno}\n解锁码：${this.props.carSearchInfo.unlock_code}` : `车牌号：${carno}\n智能锁SN码：${this.props.carSearchInfo.unlock_code}`,
                buttons: [
                    {
                        text: '错误', onPress: () => {
                            m.data.lock_type === 1 ?
                                JumpTo('codeReset', true, { zone_id, zone_name, zone_level, carno, }) :
                                Toast.fail('请联系当地质检或采购', 1, () => this.haveRead = false);
                        },
                    },
                    {
                        text: '正确', onPress: async () => {
                            const d = await this.props.dispatch(
                                m.data.lock_type === 1 ?
                                    actions.activeNewCar({ ...{ carno, zone_id, zone_level }, newCode: '', }) :
                                    actions.activeSmartCar({ ...{ carno, zone_id, zone_level }, newCode: '', })
                            );
                            d.code === 0 ?
                                Toast.success('激活成功', 1, () => this.haveRead = false) :
                                Toast.fail(d.message, 1, () => this.haveRead = false);
                        },
                    }
                ],
            });
        }


    }

    valid(text) {
        return text.startsWith(platePrefix);
    }

    renderButtons() {
        return [
            <View key={0}>
                <TouchableHighlight onPress={() => this.handlePress()}>
                    <base.VerticalLayout style={{ alignItems: 'center', }}>
                        <base.Image src="hand" style={{ width: 64, height: 64, }} />
                        <Text style={{ marginTop: 10, fontSize: 14, backgroundColor: 'rgba(0,0,0,0)', color: '#fff', }}>手动输入</Text>
                    </base.VerticalLayout>
                </TouchableHighlight>
            </View>
        ];
    }
});


const mapStateToProps = (state, ownProps) => {
    return {
        userInfo: state.api.userInfo || {},
        carSearchInfo: state.api.unlockCode,
        // locationInfo: state.api.locationInfo,
        zoneInfo: Object.assign(
            {}, state.api.zoneInfo, {
                zone_id: state.api.zone_ids[0] || '',
                zone_name: state.api.zone_names[0] || '',
                zone_level: state.api.userInfo.repairer_level || state.api.userInfo.operation_level || '',
            }),
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch,
        fetch: props => dispatch(actions.fetchUnlockCode({ ...props, })),
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
        position: 'relative',
    },
    itemBot: {
        position: 'absolute',
        height: 48,
        top: 44,
        left: 0,
        right: 0,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: '#fff',
        borderRadius: 4,
        zIndex: 999,
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',
    },
    icon: {
        width: 11,
        height: 15,
    },
    address: {
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10,
    },
    arrow: {
        width: 10,
        height: 16,
    },
};

export default connect(mapStateToProps, mapDispatchToProps)(ActivePanel);
