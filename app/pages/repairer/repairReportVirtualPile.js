import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, loadToken } from '../../runtime/api';
import { Text, View, Br, Button, ListView, List, TextInput, Toast, TouchableHighlight} from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';

class RepairReportVirtualPile extends Component {
    constructor() {
        super();
        this.state = {
            num: '',
            msg: ''
        };
    }

    handleGoBack() {
        this.props.dispatch({type: 'clear', data: {code: undefined, alert: undefined, reportStatus: undefined}});
        JumpBack();
    }

    handleSelectPosition() {
        JumpTo('repairer/selectVirtualPosition', true);
    }

    handleConfirmReport() {
        if(!this.props.virtualPileInfo ||(
            this.props.virtualPileInfo &&!this.props.virtualPileInfo.pile_id)){
            this.setState({msg: '请选择车辆摆放点'});
            return;
        }
        if(!this.state.num){
            this.setState({msg: '请输入车辆数量'});
            return;
        }
        this.props.dispatch(api(keys.reportiVirtualPile, {token: this.props.token, pile_id: this.props.virtualPileInfo.pile_id, num: this.state.num}));
    }

    componentWillReceiveProps(nextProps) {

        if ( nextProps.reportStatus && nextProps.reportStatus.ret == true) {
            Toast.success('上报成功', 2);
            setTimeout(function(){
                JumpBack();
            }, 2500);
        } else if ( nextProps.reportStatus && nextProps.reportStatus.ret == false){
            this.setState({msg: this.props.alertMessage});
                //Toast.fail(this.props.alertMessage);
        }


    }

    render() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="上报车辆" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.HorizontalLayout style={styles.hint}>
                    <Text style={{fontSize: 17, color: '#000'}}> {this.state.msg ? this.state.msg : null}</Text>
                </base.HorizontalLayout>
                <TouchableHighlight  underlayColor={'#F6F6F6'} onPress={this.handleSelectPosition.bind(this) }>
                    <base.HorizontalLayout style={styles.items}>
                        <base.Text style={styles.textStyle}>车辆摆放点</base.Text>

                        <base.HorizontalLayout style={styles.vbox}>

                            <base.Text style={this.props.virtualPileInfo?styles.textStyle:styles.positionText}>{this.props.virtualPileInfo? this.props.virtualPileInfo.pile_name : '选择位置'}</base.Text>
                            <base.Image style={{width: 13, height: 17, marginLeft: 5}} src='position'></base.Image>
                        </base.HorizontalLayout>
                    </base.HorizontalLayout>
                </TouchableHighlight>


                <base.HorizontalLayout style={styles.items}>
                    <base.Text style={styles.textStyle}>车辆数量</base.Text>
                    <TextInput
                    style={styles.itemTextRight}
                    keyboardType="numeric"
                    placeholder="请输入车辆数量"
                    value={this.state.num}
                    onChangeText={(text) => { this.setState({ num: text }); } }
                    >
                    </TextInput>
                </base.HorizontalLayout>


                <Br/>
                <base.HorizontalLayout style={styles.btnBox}>
                    <Button style={styles.submitBtn}  onPress={this.handleConfirmReport.bind(this) }>上报车辆</Button>
                </base.HorizontalLayout>

            </base.View>
        );
    }
}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
    /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    vbox: {
        alignItems: 'center',
    },
    items: {
        marginTop: 10,
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        borderTopColor: '#c6c6c6',
        borderTopWidth: 1,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        height: 60,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    textStyle: {
        color: '#000',
        fontSize: 16,
    },
    itemRight: {
        color: '#000',
        fontSize: 16,
    },
    itemTextRight: {
        borderWidth: 0,
        textAlign: 'right',
        height: 58,
        fontSize: 15,
        width: 160
    },
    arrow: {
        height: 14,
        top: 23,
    },
    positionText: {
        color: '#7f7f7f',
        fontSize: 15
    },
    btnBox: {
        paddingLeft: 16,
        paddingRight: 16,
        height: 44,
    },
    submitBtn: {
        backgroundColor: '#2C2C2C',
        color: '#fff',
        height: 44,
        borderRadius: 2,
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 15,
        flexGrow: 1,
        flexBasis: 0
    },
    hint: {
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
    },
};

export default connect((state) => {
    return {
        token: state.api.token,
        virtualPileInfo: state.user.virtualPileInfo,
        reportStatus: state.api.reportStatus,
        alertMessage: state.api.alert,
    };
})(RepairReportVirtualPile);
