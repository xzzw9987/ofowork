import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys } from '../../runtime/api';
import { Text, View, Br, Button, HorizontalLayout, ListView, List, Map, Toast, TouchableHighlight } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';

var StaticImage = require('../../base/staticImages');

/**
 * 待维修车辆地图
 */
class RepairCarMap extends Component {

    constructor(){
        super();
        this.state={
            initialPosition: undefined,
            lastPosition: undefined,
            region: {
                latitude: 39.98362,
                longitude: 116.306715,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005,
            },
            annotations: [],
            refresh: false,
        };
    }

    componentWillReceiveProps(nextProps) {

        // 更换选中区域
        if (typeof nextProps.repairerCoordinate !== undefined && nextProps.repairerCoordinate != this.props.repairerCoordinate) {

            // 车辆位置
            let annotations = [];
            nextProps.repairerCoordinate.map((item) => {
                annotations.push({
                    title: item.carno,
                    latitude: parseFloat(item.lat),
                    longitude: parseFloat(item.lng),
                    icon: StaticImage['icon'],
                });
            });
            this.setState({annotations});
        }
    }

    componentWillUnmount() {
        this.setState({refresh: false});
    }

    onLocationChange(e) {
        if(!this.state.lastPosition ) {
            let latitude = e.latitude;
            let longitude = e.longitude;
            // 用户当前位置
            this.setState({lastPosition: {latitude, longitude},
                region: {latitude, longitude, latitudeDelta: 0.005, longitudeDelta: 0.005}});

            this.props.dispatch(api(keys.repairerCoordinate, {token: this.props.token, zone_id: this.props.zone_ids[this.props.zoneIndex || 0], lat: latitude, lng: longitude}));
        }
    }

    handleGoBack() {
        this.setState({refresh: false});
        JumpBack();
    }

    /**
     * 跳转待维修车辆详情
     */
    handleRepairList() {
        console.log('jump to repair cars list page');
        JumpTo('repairer/carList', true);
    }

    render() {

        let zoneIndex = (this.props.zoneIndex || 0);
        let zone_id = '' + ((this.props.zone_ids || [])[zoneIndex] || 0);
        const carsWaitForRepair = this.props.carsWaitForRepair[zone_id] || 0;

        return (
            <base.View style={styles.container}>
                <ActionBar title="未修车辆" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.VerticalLayout className="overflowY" style={styles.list}>

                    <base.VerticalLayout style={styles.top}>
                         <TouchableHighlight  style={styles.inner}>
                                <base.HorizontalLayout style={styles.box}>
                                    <Text style={{fontSize: 16, color: '#000'}}>未维修车辆: {/*{this.props.carsWaitForRepair[""+(this.props.zoneIndex || 0)] || 0}</Text>*/}
                                    {carsWaitForRepair}</Text>
                                    <Button style={styles.listBtn} onPress={ this.handleRepairList.bind(this) }><base.Image style={styles.icon} src='detail'></base.Image></Button>
                                </base.HorizontalLayout>
                        </TouchableHighlight>
                    </base.VerticalLayout>
                    <Map style={{flexGrow: 1, height: 300}}  region={this.state.region} annotations={this.state.annotations} showsUserLocation={true} onLocationChange={this.onLocationChange.bind(this)}/>
                </base.VerticalLayout>
            </base.View>
        );
    }
}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
    /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    color2: {
        color: '#7f7f7f',
        fontSize: 15,
    },
    list: {
        flexGrow: 1
    },
    top: {
        height: 60,
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        backgroundColor: '#fff',
    },
    inner: {
        paddingLeft: 10,
        paddingRight: 10,
        height: 59,
    },
    box: {
        justifyContent: 'space-between',
        height: 59,
        alignItems: 'center',
    },
    icon: {
        width: 19,
        height: 14
    },
    listBtn: {
        height: 59,
        width: 35,
        justifyContent: 'center',
        alignItems: 'center',

    }
};

export default connect((state) => {
    return {
        token: state.api.token,
        userInfo: state.api.userInfo,
        zone_ids: state.api.zone_ids,
        zoneIndex: state.user.zoneIndex,
        carsWaitForRepair: state.api.carsWaitForRepair,
        repairerCoordinate: state.api.repairerCoordinate,
    };
})(RepairCarMap);
