import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys } from '../../runtime/api';
import { Text, View, Br, Button, TextInput, TouchableHighlight } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';

/**
 * 搜索车辆
 */
class RepairCarSearch extends Component {

    constructor(){
        super();

        this.state={
            carno: ''
        };
    }


    componentWillMount() {
        this.props.dispatch({type: 'clear', data: {code: undefined, alert: undefined}});
    }

    componentWillUnmount() {
        //console.log("componentWillUnmount")
       //this.props.dispatch({type: 'clearSearchInfo'});
       //this.props.dispatch({type: 'clearCarDetail'});
    }

    componentWillReceiveProps(nextProps){
        console.log('typeoftypeoftypeoftypeof', typeof nextProps.carWaitForRepairDetail, nextProps.carWaitForRepairDetail);
        if(typeof nextProps.carWaitForRepairDetail !== undefined && nextProps.carWaitForRepairDetail != null  && nextProps.carWaitForRepairDetail != this.props.carWaitForRepairDetail) {
            console.log('RepairCarSearch componentWillReceiveProps', nextProps.carWaitForRepairDetail);
            JumpTo('repairer/carDetail', true);
        }
    }

    handleSearch() {
        console.log('RepairCarSearch handleSearch', this.state.carno);

        this.props.dispatch(api(keys.carSearchNum, {token: this.props.token, bicycle_num: this.state.carno}, 'carSearchInfo'));

    }
    handleSearchDetail(){
        console.log('this.props.carSearchInfo.status', this.props.carSearchInfo.status);
        if(this.props.carSearchInfo.status){
            let zone_id = '' + this.props.zone_ids[this.props.zoneIndex || 0];
            this.props.dispatch(api(keys.carWaitForRepairDetail, {token: this.props.token, zone_id, bicycle_number: this.state.carno}));
        }else{
            let info = this.props.carSearchInfo;
            this.props.dispatch({type: 'carDetail', data: info});
            JumpTo('repairer/carDetail', true);
        }

    }
    deleteSearch(){
        this.setState({ carno: '' });
        this.props.dispatch({type: 'clearSearchInfo'});
        this.props.dispatch({type: 'clearCarDetail'});
    }
    handleGoBack() {
        this.props.dispatch({type: 'clear', data: {code: undefined, alert: undefined}});
        this.props.dispatch({type: 'clearSearchInfo'});
        this.props.dispatch({type: 'clearCarDetail'});
        JumpBack();
    }

    render() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="搜索" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.HorizontalLayout  className="input" style={this.props.alert ? Object.assign({}, styles.searchBox) : styles.searchBox}>
                    <base.HorizontalLayout style={styles.leftBox}>

                        <base.HorizontalLayout style={styles.searchBtn}>
                            <base.Image style={styles.search} src='search2'></base.Image>
                        </base.HorizontalLayout>

                        <TextInput style={styles.serachInput} placeholder='请输入搜索车号'keyboardType="numeric" value={this.state.carno} onChangeText={(text) => { this.setState({ carno: text }); } } onSubmitEditing={this.handleSearch.bind(this)}></TextInput>

                        {this.state.carno ?
                            <TouchableHighlight style={styles.deleteBtn} onPress={this.deleteSearch.bind(this)}>
                                <base.HorizontalLayout>
                                    <base.Image style={styles.deleteImg} src='deleteIcon'></base.Image>
                                </base.HorizontalLayout>
                            </TouchableHighlight> : null}
                    </base.HorizontalLayout>
                    <base.HorizontalLayout style={styles.rightBox}>
                        <Button style={styles.rightBtn} onPress={this.handleSearch.bind(this)}><Text>查询</Text></Button>
                    </base.HorizontalLayout>
                </base.HorizontalLayout>
                <Br/>
                {this.props.alert ? (<base.HorizontalLayout style={styles.hint}>
                    <Text style={styles.font17}>{this.props.alert}</Text>
                </base.HorizontalLayout>) : <Text></Text>}
                {this.props.carSearchInfo ?
                    <TouchableHighlight onPress={this.handleSearchDetail.bind(this)} style={styles.searchDone} underlayColor={'#fff'}>
                        <base.VerticalLayout>
                            <base.HorizontalLayout style={styles.topBox}>

                                <base.Image style={{width: 20, height: 20, marginRight: 5}} src={this.props.carSearchInfo.status === 0 ? 'reported' : 'unreport'} />
                                <Text style={{fontSize: 18}}>{this.props.carSearchInfo.status ? '已报修' : '未报修'}</Text>
                            </base.HorizontalLayout>
                            <base.HorizontalLayout style={styles.bottomBox}>
                                <base.HorizontalLayout style={styles.center}>
                                    <Text>解锁码：</Text>
                                    <Text style={{fontSize: 24, color: '#ff7000'}}>{this.props.carSearchInfo.unlock_code}</Text>
                                </base.HorizontalLayout>
                                <base.HorizontalLayout style={styles.center}>
                                    <Text>车牌：</Text>
                                    <Text style={{fontSize: 24}}>{this.props.carSearchInfo.bicycle}</Text>
                                </base.HorizontalLayout>
                              </base.HorizontalLayout>
                        </base.VerticalLayout>
                </TouchableHighlight> : null}

            </base.View>
        );
    }
}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
    /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    searchBox: {
        height: 65,
        backgroundColor: '#fff',
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        paddingLeft: 10,
        alignItems: 'center',
    },
    leftBox: {
        flexGrow: 1,
        flexBasis: 0,
        position: 'relative',
        justifyContent: 'space-between',
        borderRadius: 4,
        backgroundColor: '#ebebeb',
        height: 44
    },
    serachInput: {
        height: 44,
        fontSize: 16,
        backgroundColor: '#ebebeb',
        flexGrow: 1,
        flexBasis: 0,
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: '#ebebeb',
    },
    rightBox: {
        width: 60,
        height: 60
    },
    rightBtn: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 60,
        height: 60
    },
    error: {
        borderBottomColor: '#f33001',
        borderTopColor: '#f33001',
    },
    searchBtn: {
        height: 44,
        width: 40,
        position: 'relative'
    },
    search: {
        width: 20,
        height: 20,
        position: 'absolute',
        left: 10,
        top: 12,
    },
    deleteBtn: {
        height: 44,
        width: 40,
        position: 'relative',
    },
    deleteImg: {
        width: 20,
        height: 20,
        position: 'absolute',
        left: 10,
        top: 12,
    },
    hint: {
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
    },
    searchDone: {
        backgroundColor: '#fff',
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderTopColor: '#c6c6c6',
        borderBottomWidth: 1,
        borderTopWidth: 1,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    topBox: {
        height: 46,
        paddingLeft: 20,
        alignItems: 'center',
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',

        borderBottomWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    bottomBox: {
        height: 68,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20
    }
};

export default connect((state) => {
    return {
        token: state.api.token,
        code: state.api.code,
        alert: state.api.alert,
        userInfo: state.api.userInfo,
        zone_ids: state.api.zone_ids,
        zone_names: state.api.zone_names,
        zoneIndex: state.user.zoneIndex,
        carWaitForRepairDetail: state.api.carWaitForRepairDetail,
        carSearchInfo: state.api.carSearchInfo
    };
})(RepairCarSearch);
