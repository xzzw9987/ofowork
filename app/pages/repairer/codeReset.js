import React, { Component } from 'react';
import * as base from '../../base';
import { connect } from 'react-redux';
import {
    Text,
    View,
    Br,
    Button,
    Tabs,
    TabPane,
    ScrollView,
    Toast,
    TouchableHighlight,
    ActivityIndicator
} from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import CameraView from '../../components/camera';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack, ResetTo } from '../../modules';
import * as actions from '../../runtime/action';
import platform from '../../modules/platform';
import Keyboard from '../../modules/keyboard';

const CodeReset = pageWrapper(
    class extends Component {
        constructor(...args) {
            super(...args);
            this.state = {
                historyState: historyStack.get(),
                loading: false
            };
        }

        handleBack() {
            JumpBack();
        }

        renderLoading() {
            return (
                <base.View style={styles.container}>
                    <ActionBar
                        title="重新录入解锁码"
                        rightAction={() => this.props.fetch(this.props)}
                    >
                        <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    </ActionBar>
                    <base.View style={{ position: 'relative', flexGrow: 1 }}>
                        <base.View style={styles.inner}>
                            <ActivityIndicator size="large" src="loading" />
                            <base.HorizontalLayout style={{ marginTop: 10 }}>
                                <Text>正在加载 ...</Text>
                            </base.HorizontalLayout>
                        </base.View>
                    </base.View>
                </base.View>
            );
        }

        renderPanel() {
            return (
                <View style={styles.container}>
                    <ActionBar
                        title={'重新录入解锁码'}
                        handleBack={() => this.handleBack()}
                        rightAction={() => {
                            //    JumpTo('chainList', true, {});
                        }}
                    >
                        <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    </ActionBar>

                    <View style={{ flexGrow: 1, flexDirection: 'column' }}>
                        <base.HorizontalLayout
                            style={{
                                ...styles.borderTop,
                                ...styles.borderBottom,
                                paddingLeft: 20,
                                paddingRight: 20,
                                marginTop: 10,
                                alignItems: 'center',
                                backgroundColor: '#fff'
                            }}
                        >
                            <Text style={{ fontSize: 18, marginRight: 10 }}>车牌号</Text>
                            <base.TextInput
                                value={
                                    this.state.historyState.carno
                                        ? this.state.historyState.carno
                                        : ''
                                }
                                onChangeText={text => {
                                    this.setState({ carno: text });
                                }}
                                placeholder="请输入车牌号"
                                style={{ fontSize: 14, flexGrow: 1, height: 64 }}
                            />
                        </base.HorizontalLayout>
                        <base.HorizontalLayout
                            style={{
                                ...styles.borderTop,
                                ...styles.borderBottom,
                                paddingLeft: 20,
                                paddingRight: 20,
                                marginTop: 10,
                                alignItems: 'center',
                                backgroundColor: '#fff'
                            }}
                        >
                            <Text style={{ fontSize: 18, marginRight: 10, color: '#fd7000' }}>
                                解锁码
              </Text>
                            <base.TextInput
                                autoFocus={true}
                                onChangeText={text => {
                                    this.setState({
                                        newCode: text,
                                        carno: this.state.carno || this.state.historyState.carno
                                    });
                                }}
                                placeholder="请输入解锁码"
                                style={{
                                    fontSize: 14,
                                    flexGrow: 1,
                                    height: 64,
                                    color: '#fd7000'
                                }}
                            />
                        </base.HorizontalLayout>

                        <View style={{ height: 150 }} />

                        <View>
                            <TouchableHighlight
                                onPress={() => this.onSubmit()}
                                style={{
                                    backgroundColor: '#2c2c2c',
                                    marginLeft: 20,
                                    marginRight: 20,
                                    flexGrow: 1
                                }}
                            >
                                <base.HorizontalLayout
                                    style={{
                                        height: 44,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}
                                >
                                    <Text style={{ color: '#fff' }}>
                                        确认录入
                  </Text>
                                </base.HorizontalLayout>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>
            );
        }

        render() {
            if (this.state.loading) return this.renderLoading();
            return this.renderPanel();
        }

        async onSubmit() {
            Keyboard.dismiss();
            const j = url => {
                if (
                    platform.OS !== 'web' &&
                    this.props.navigator &&
                    this.props.navigator.getCurrentRoutes
                ) {
                    const navigator = this.props.navigator,
                        route = navigator.getCurrentRoutes().filter(d => d.url === url)[0];
                    if (route) {
                        navigator.popToRoute(route);
                        return true;
                    }
                    return false;
                } else {
                    ResetTo(url, true, {});
                    return true;
                }
            };
            // if (!this.valid(this.state.carno)) {
            //     Toast.fail('请输入正确的车牌号码', 1);
            //     return;
            // }
            if (!this.validCode(this.state.newCode)) {
                Toast.fail('请输入4位解锁码', 1);
                return;
            }
            this.setState({ loading: true });
            const { zone_id, zone_name, zone_level } = this.state.historyState.zone_id
                ? this.state.historyState
                : this.props.zoneInfo;
            //   const { latitude, longitude, accuracy } = this.props.locationInfo;

            const res = await this.props.fetch({
                ...this.state,
                zone_id,
                zone_level,
                // latitude,
                // longitude,
                // accuracy
            });
            this.setState({ loading: false });
            if (res.code === 0) {
                Toast.success('激活成功', 1);
                platform.OS === 'web' && JumpTo('activeInput', true, {});
                setTimeout(() => {
                    if (!j('repairer/carActive')) j('activeInput');
                }, 500);
            } else {
                Toast.fail(res.message, 1);
            }
        }

        valid(text) {
            return /^\d+$/.test(text);
        }

        validCode(code) {
            return /^\d{4}$/.test(code);
        }
    }
);

const mapStateToProps = (state, ownProps) => {
    return {
        userInfo: state.api.userInfo || {},
        token: state.api.token,
        // locationInfo: state.api.locationInfo || {
        //   latitude: '',
        //   longitude: '',
        //   accuracy: ''
        // },
        zoneInfo: {
            ...state.api.zoneInfo,
            zone_id: state.api.zone_ids[0],
            zone_name: state.api.zone_names[0],
            zone_level: state.api.userInfo.repairer_level ||
            state.api.userInfo.operation_level ||
            ''
        }
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch,
        fetch: props => dispatch(actions.activeNewCar({ ...props }))
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    },
    borderTop: {
        borderTopStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#ddd'
    },
    borderBottom: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    borderLeft: {
        borderLeftStyle: 'solid',
        borderLeftWidth: 1,
        borderLeftColor: '#ddd'
    },
    borderRight: {
        borderRightStyle: 'solid',
        borderRightWidth: 1,
        borderRightColor: '#ddd'
    },
    inner: {
        position: 'absolute',
        flexDirection: 'column',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CodeReset);
