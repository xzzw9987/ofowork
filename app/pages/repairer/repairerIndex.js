import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, loadToken, getDateDiff, _loadToken, numberSplit } from '../../runtime/api';
import { Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import styles from '../../css/repairer/indexCss';
import pageWrapper from '../../modules/pageWrapper';
import platform from '../../modules/platform';
import Dimensions from '../../modules/dimensions';
import ListView from '../../components/ListView';
import RefreshControl from '../../components/refreshControl';
import Storage from '../../base/storage';
var { height, width } = Dimensions.get('window');
var moment = require('moment');
import {
    fetchRepairerIndex,
    fetchRepairerList
} from '../../runtime/action';
import { repairerList } from '../../runtime/reducer';
/**
 * 师傅首页
 */

class repairer extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            selectedPaneIndex: 0,
            pageCount: 0,
            selected: [],
            isRefreshing: false,
            schoolId: ''
        };
    }


    componentWillMount() {
        Storage.getItem('ReportList').then(d => {
            if (!d) return;
            d = JSON.parse(d);
            this.setState({ selected: d });
        });
    }

    viewWillAppear() {
        this.props.fetch({ schoolId: this.state.schoolId }).then(() => this.setState({ loading: false }));
    }

    viewWillDisappear() {
        Storage.setItem('ReportList', JSON.stringify(this.state.selected));
    }

    //切换学校详情
    handleSwitchZone(schoolId) {
        this.setState({ schoolId: schoolId });
        this.props.fetch({ schoolId: schoolId }).then(() => this.setState({ loading: false }));
    }

    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderRepairerPage();
    }

    JumpToMapPage() {
        JumpTo('repair/mapIndex', true, { currentId: this.props.schoolId });
    }

    renderLoading() {
        let top = platform.OS == 'ios' ? 20 : 0;
        return (
            <base.View style={{ paddingTop: top, ...styles.container }}>
                <base.View style={{ position: 'relative', flexGrow: 1 }}>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='loading' />
                        <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }
    renderRepairerPage() {
        let top = platform.OS == 'ios' ? 20 : 0;
        return (

            <base.View style={{ paddingTop: top, ...styles.container }} className="wrap">
                <base.HorizontalLayout style={styles.nav}>
                    <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} style={{ height: 45 }}>
                        <base.HorizontalLayout className="overflowX hiddenY">
                            {this.props.schoolNames.map(((item, idx) => {
                                return (
                                    <base.HorizontalLayout key={idx} style={styles.inner}>
                                        <Button clickDuration={0}
                                            style={(this.props.schoolId == item.id) ? Object.assign({}, styles.itemBtn, styles.light) : styles.itemBtn}
                                            key={item.id} onPress={() => {
                                                this.handleSwitchZone(item.id);
                                            }}>
                                            <base.Text
                                                style={(this.props.schoolId == item.id) ? styles.active : { color: '#7f7f7f' }}>{item.name}</base.Text>
                                        </Button>
                                    </base.HorizontalLayout>
                                );
                            }))}
                        </base.HorizontalLayout>
                    </ScrollView>
                </base.HorizontalLayout>
                {/*<base.VerticalLayout style={{paddingLeft: 20, paddingRight: 20}}>
                    <base.VerticalLayout style={styles.topBox}>
                        <base.Text style={styles.h2}>今日订单量</base.Text>
                        <base.HorizontalLayout style={styles.endBox}>
                            <base.Text style={styles.h1}>{this.props.orderInfo.num}</base.Text>
                            <base.Text style={styles.h4}>比昨天此时</base.Text>
                            <base.Text style={{fontSize:14,color:this.props.orderInfo.textColor}}>{this.props.orderInfo.sign}{this.props.orderInfo.increase}单
                            </base.Text>
                        </base.HorizontalLayout>
                    </base.VerticalLayout>
                </base.VerticalLayout>*/}
                <base.VerticalLayout style={styles.topBox}>
                    <base.VerticalLayout style={{ paddingLeft: 20, paddingRight: 20 }}>
                        <base.Text style={styles.h2}>本月报修率</base.Text>
                        <base.HorizontalLayout style={styles.endBox}>
                            <base.Text style={styles.h1}>{this.props.schoolInfo.rate}%</base.Text>
                            <base.HorizontalLayout
                                style={Object.assign({ backgroundColor: this.props.schoolInfo.color }, styles.textBox)}>
                                <base.Text style={styles.titleText}>{this.props.schoolInfo.rateText}</base.Text>
                            </base.HorizontalLayout>
                        </base.HorizontalLayout>
                    </base.VerticalLayout>
                </base.VerticalLayout>
                <base.VerticalLayout style={{ height: 10, backgroundColor: '#F6F6F6' }}></base.VerticalLayout>
                <base.VerticalLayout style={styles.paneBox}>
                    {this.renderPane()}
                    <base.TouchableHighlight style={styles.mapIcon} activeOpacity={.1} underlayColor={'#fff'}
                        onPress={this.JumpToMapPage.bind(this)}>
                        <base.HorizontalLayout style={{ alignItems: 'center' }}>
                            <base.Text>地图模式</base.Text>
                            <base.Image style={{ width: 15, height: 19, marginLeft: 3 }} src='map'></base.Image>
                        </base.HorizontalLayout>
                    </base.TouchableHighlight>
                </base.VerticalLayout>
                <base.VerticalLayout style={styles.listBox}>
                    {this.renderList()}
                </base.VerticalLayout>
            </base.View>
        );
    }
    makeRefreshControl(schoolId) {
        if (this.state.selectedPaneIndex == 1) {
            return null;
        }
        return <RefreshControl
            refreshing={this.state.isRefreshing}
            onRefresh={() => {
                this.setState({ isRefreshing: true });
                this.props.fetch({ schoolId: schoolId }).then(() => { this.setState({ isRefreshing: false }); Toast.success('刷新成功', 1); });
            }}
        />;
    }
    renderDot(item) {
        return this.state.selected.indexOf(item.id) > -1 ? null : (
            <base.VerticalLayout style={styles.yuan}></base.VerticalLayout>
        );
    }
    renderPane() {
        let pane = [
            {
                text: '维修列表', style: {
                    borderTopLeftRadius: 6,
                    borderBottomLeftRadius: 6
                }
            }
        ];
        return (
            <base.HorizontalLayout style={styles.paneInner}>
                {
                    pane.map((pane, index) => {

                        return (
                            <base.HorizontalLayout
                                key={index}
                                style={{ flex: 1, justifyContent: 'center', ...pane.style }}>
                                <base.TouchableHighlight
                                    activeOpacity={.1}
                                    underlayColor={'#fff'}
                                    style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                                    onPress={() => {
                                        this.setState({ selectedPaneIndex: index });
                                    }}>
                                    <base.HorizontalLayout style={{ justifyContent: 'center' }}>
                                        <Text style={{ fontSize: 16 }}>{pane.text}</Text>
                                    </base.HorizontalLayout>
                                </base.TouchableHighlight>
                            </base.HorizontalLayout>
                        );
                    })
                }
            </base.HorizontalLayout>);
    }

    renderList() {
        let dataSource = this.props.dataSource,
            list = dataSource[this.state.selectedPaneIndex]['list'];
        if (!list) {
            return (
                <base.VerticalLayout style={{ justifyContent: 'center', alignItems: 'center', flexGrow: 1, flexBasis: 0 }}>
                    <ActivityIndicator size='large' src='loading' />
                    <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                </base.VerticalLayout>
            );
        }
        if (list && list.length == 0) {
            return (
                <base.VerticalLayout style={{ flexGrow: 1, flexBasis: 0, justifyContent: 'center', alignItems: 'center' }}>
                    <base.Image style={{ width: 70, height: 70, marginBottom: 5 }} src='noData' />
                    <base.Text style={{ fontSize: 16, color: '#666' }}>暂无数据</base.Text>
                </base.VerticalLayout>
            );
        }
        return (
            <base.VerticalLayout style={{ flexGrow: 1, flexBasis: 0 }}>

                <ListView
                    dataSource={list}
                    refreshControl={this.makeRefreshControl(this.props.schoolId)}
                    renderRow={(v, index) => {
                        return (
                            <base.TouchableHighlight key={index} onPress={() => {
                                let arr = this.state.selected;
                                arr.push(v.id);
                                this.setState({ selected: arr });
                                v.onPress && v.onPress(v);
                            }}
                                activeOpacity={.1}
                                underlayColor={'#fff'}
                                style={styles.itemInfo}>

                                <base.HorizontalLayout style={width <= 320 ? { marginLeft: -10, ...styles.item } : styles.item}>
                                    <base.HorizontalLayout style={width <= 320 ? styles.smallBox : styles.imgBox}>
                                        <base.Image style={{ width: 81, height: 81 }} src={v.img_path ? `${v.img_path}?x-oss-process=image/resize,h_80` : 'defaultImage'
                                        } />
                                        {v.is_inner ?
                                            <base.HorizontalLayout style={styles.hint}>
                                                <base.Text style={{ fontSize: 13, color: '#fff' }}>内部
                                            {
                                                        v.maintenance_type == 'repair' ? '报修' : '举报'
                                                    }
                                                </base.Text>
                                            </base.HorizontalLayout> : null
                                        }
                                    </base.HorizontalLayout>
                                    <base.VerticalLayout style={{ flex: 1 }}>
                                        <base.Text style={{ fontSize: 16 }}>车牌 {v.car_no === '-----' ? '无' : v.car_no}</base.Text>
                                        <base.HorizontalLayout style={{ marginTop: 10, marginBottom: 5, flexWrap: 'wrap' }}>
                                            {v.reason.map((r, index) => {
                                                return (
                                                    <base.HorizontalLayout key={index} style={styles.reason}>
                                                        <base.Text style={styles.reasonItem}>{r}</base.Text>
                                                    </base.HorizontalLayout>
                                                );
                                            })}
                                        </base.HorizontalLayout>
                                        <base.Text style={{ fontSize: 14 }}>{v.address}</base.Text>
                                        <base.HorizontalLayout style={{ alignItems: 'center', marginTop: 10 }}>
                                            {v.distance <= 1000 ?
                                                <base.HorizontalLayout style={styles.jin}>
                                                    <base.Text style={{ fontSize: 14 }}>近</base.Text>
                                                </base.HorizontalLayout> : null
                                            }

                                            {
                                                v.distance == null ?
                                                    (<base.Text style={{ fontSize: 14 }}>距离未知</base.Text>) : (<base.Text style={{ fontSize: 14 }}>距离{v.distance}米</base.Text>)
                                            }
                                        </base.HorizontalLayout>
                                    </base.VerticalLayout>
                                    <base.HorizontalLayout style={styles.itemHint}>
                                        <base.Text style={{ color: '#999', fontSize: 14 }}>
                                            {getDateDiff(v.time)}
                                        </base.Text>
                                        {this.renderDot(v)}

                                    </base.HorizontalLayout>
                                </base.HorizontalLayout>
                            </base.TouchableHighlight>
                        );
                    }} className="overflowY" />

            </base.VerticalLayout>
        );

    }
}

const mapStateToProps = state => {
    return {
        schoolId: state.api.schoolId || 0,
        schoolNames: state.api.schoolNames || [],
        schoolInfo: state.api.schoolInfo || {},
        //orderInfo: state.api.orderInfo || {},
        compare: state.api.listToCompare,
        newMessage: list => {
            if (!state.api.listToCompare) return false;
            return state.api.listToCompare[0]['time'] != list[0]['time'];
        },
        dataSource: [
            {
                list: beautifyList(state.api.repairerReportList) || null
            }
            // },
            // {
            //     list: beautifyList2(state.api.repairerRepairList, state.api.schoolId) || null
            // }
        ]
    };
};
const mapDispatchToProps = dispatch => {
    return {
        dispatch,
        fetch: props => dispatch(fetchRepairerIndex(props))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(repairer));
function beautifyList(list) {

    if (!list)
        return null;

    return list.map(v => {
        return {
            ...v,
            onPress() {

                /* JumpTo Detail */
                JumpTo('repair/reportDetail', true, { id: v.id });
            }
        };
    });
}
function beautifyList2(list, currentId) {
    if (!list)
        return null;

    return list.map(v => {
        return {
            ...v,
            onPress() {
                /* JumpTo repairCarDetail */
                JumpTo('repairer/detail', true, { biycleNumber: v.bicycle_num, currentId: currentId });
            }
        };
    });
}



