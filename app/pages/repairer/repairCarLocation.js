import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys } from '../../runtime/api';
import { Text, View, Br, Button, HorizontalLayout, ListView, List, Map, Toast } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';

var StaticImage = require('../../base/staticImages');

/**
 * 待维修车辆地图
 */
class RepairCarLocation extends Component {

    constructor(){
        super();
        this.state={
            initialPosition: undefined,
            lastPosition: undefined,
            region: {
                latitude: 39.98362,
                longitude: 116.306715,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005,
            },
            annotations: [],
            refresh: false,
        };
    }

    componentWillMount() {
        let annotations = [{
            latitude: parseFloat(this.props.carLocation.lat),
            longitude: parseFloat(this.props.carLocation.lng),
            icon: StaticImage['icon'],
            title: '',
            key: ''
        }];

        const region = {latitude: parseFloat(this.props.carLocation.lat), longitude: parseFloat(this.props.carLocation.lng), latitudeDelta: 0.005, longitudeDelta: 0.005};

        this.setState({annotations, refresh: true, region});
    }

    handleGoBack() {
        this.setState({refresh: false});
        JumpBack();
    }

    render() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="车辆报修位置" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.VerticalLayout className="overflowY" style={styles.list}>
                    <Br/>
                    <Map style={{flexGrow: 1, height: 300}}  region={this.state.region} annotations={this.state.annotations} showsUserLocation={false}/>
                </base.VerticalLayout>
            </base.View>
        );
    }
}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
    /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    color2: {
        color: '#7f7f7f',
        fontSize: 15,
    },
    list: {
        flexGrow: 1
    },
    top: {
        height: 60
    },
    inner: {
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 59,
        backgroundColor: '#fff',
        paddingLeft: 10,
        paddingRight: 10,
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    icon: {
        width: 19,
        height: 14
    }
};

export default connect((state) => {
    return {
        token: state.api.token,
        userInfo: state.api.userInfo,
        carLocation: state.user.carLocation,
    };
})(RepairCarLocation);
