import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys } from '../../runtime/api';
import { Text, View, Br, Button, TextInput, List, ScrollView, TouchableHighlight, ActivityIndicator } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';

/**
 * 修车师傅考勤记录
 */
class RepairCheckRecords extends Component {

    componentWillMount() {
        this.props.dispatch(api(keys.repairerCheckRecords, {token: this.props.token, page: 0}));
    }
    handleGoBack() {
        JumpBack();
    }
    render() {
        let checkRecords = this.props.checkRecords;
        if(!checkRecords || !checkRecords.list) {
            return (
                <base.View style={styles.container}>
                    <ActionBar title="考勤记录" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                    <base.View style={{position: 'absolute', flexDirection: 'column', left: 0, top: 44, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center'}}>
                        <ActivityIndicator size='large' src='loading'/>
                        <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            );
        }
        return (
             <base.View style={styles.container}>
                <ActionBar title="考勤记录" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <Br/>

                <TouchableHighlight style={{height: 46}}>
                    <base.HorizontalLayout style={styles.box}>
                        <base.HorizontalLayout style={styles.title}><Text>日期</Text></base.HorizontalLayout>
                        <base.HorizontalLayout style={styles.title}><Text>上班</Text></base.HorizontalLayout>
                        <base.HorizontalLayout style={styles.title}><Text>下班</Text></base.HorizontalLayout>
                    </base.HorizontalLayout>
                </TouchableHighlight>

                <ScrollView className="overflowY" style={styles.list}>

                        {checkRecords.list.map((item, idx) => {

                            return (<TouchableHighlight key={idx}>
                                <base.HorizontalLayout style={{flexGrow: 1, height: 60}}>
                                <base.HorizontalLayout style={styles.item}><Text>{item.date ? item.date : '--'}</Text></base.HorizontalLayout>
                                <base.HorizontalLayout className="center" style={styles.item}><Text>{item.sign_in_time ? item.sign_in_time : '--'}</Text></base.HorizontalLayout>
                                <base.HorizontalLayout className="center" style={styles.item}><Text>{item.sign_out_time ? item.sign_out_time : '--'}</Text></base.HorizontalLayout>
                                </base.HorizontalLayout>
                            </TouchableHighlight>);
                        })}

                </ScrollView>
            </base.View>
        );
    }
}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
    /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    box: {
        borderStyle: 'solid',
        borderTopColor: '#c6c6c6',
        borderTopWidth: 1,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    title: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        backgroundColor: '#fff',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    item: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        backgroundColor: '#fff',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        flexBasis: 0
    },
    list: {
        flexGrow: 1
    }
};

export default connect((state) => {
    return {
        token: state.api.token,
        checkRecords: state.api.checkRecords
    };
})(RepairCheckRecords);
