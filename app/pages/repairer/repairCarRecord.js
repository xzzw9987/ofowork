import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, apibase, getUserInfo } from '../../runtime/api';
import { Text, View, Br, Button, Checkbox, Toast, TextInput, ActivityIndicator, TouchableHighlight, Map } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import whereami from '../../base/map/whereami';
import * as actions from '../../runtime/action';
import ImagePicker from '../../components/gallery';
import { fetch } from '../../modules';

/**
 * 维修车辆
 */

const RepairCarRecord = pageWrapper(class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            carno: '',
            imgPath: '',
            checkedList: [],
            token: '',
            latitude: '',
            longitude: '',
            loading: false,
            checkedType: [],
        };
    }

    viewWillAppear() {
        whereami().then(e => {
            let latitude = e.latitude;
            let longitude = e.longitude;
            // 用户当前位置
            this.setState({
                latitude, longitude
            });
        });
        // 损坏部件接口
        return getUserInfo()
            .then(userInfo => {
                let { token } = userInfo;
                fetch(`${apibase}/repairer/reason-list`, {
                    method: 'POST',
                    body: { token, 'bicycle_no': this.state.carno }
                }).then(respones => {
                    let list = [];
                    let c = [];
                    Object.keys(respones['data']['list']).map(key =>
                        list.push({
                            name: key,
                            checked: false,
                            title: respones['data']['list'][key]
                        }),
                    );
                    if (list.length > 0) {
                        for (let i = 0; i < list.length; i++) {
                            c.push(list[i].checked);
                        }
                    }
                    this.setState({
                        checkedList: list,
                        token,
                        checkedType: c,
                    });
                });
            });
    }

    // 返回
    handleBack() {
        JumpBack();
    }

    // 提交
    async submitInfo() {
        if (this.state.imgPath === '') {
            Toast.fail('请上传照片', 2);
            return;
        }
        if (!/^\d+$/.test(this.state.carno)) {
            Toast.fail('车牌号4到7位', 2);
            return;
        }
        const reason = this.state.checkedList.filter(v => v.checked).map(v => v.name);
        if (reason.length <= 0) {
            Toast.fail('请选择维修原因', 2);
            return;
        }
        this.setState({ loading: true });

        const res = await fetch(`${apibase}/user/upload-img-with-base64`, {
            method: 'POST',
            body: { token: this.state.token, 'img': this.state.imgPath }
        });

        if (res.code === 0) {
            await this.props.dispatch(actions.subRepairerRecord({ carno: this.state.carno, img_path: res.data.img_path, reason: reason.join(',') }));

            if (this.props.subRecordStatus.code === 0) {
                Toast.success('上传成功', 1);
                this.setState({
                    loading: false,
                    carno: '',
                    checkedList: [],
                    imgPath: '',
                });
                this.viewWillAppear();
            } else {
                Toast.fail(this.props.subRecordStatus.message, 1);
                this.setState({ loading: false });
            }
        } else {
            Toast.fail('图片上传失败', 1);
            this.setState({ loading: false });
        }
    }

    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderRepairCarRecord();
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="维修车辆" handleBack={this.handleBack.bind(this)}>
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>
                <base.View style={{ position: 'relative', flexGrow: 1 }}>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='loading' />
                        <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }

    renderRepairCarRecord() {
        return (
            <base.VerticalLayout className="wrap" style={styles.container} onStartShouldSetResponder={() => this.getKeyboardState()} onResponderGrant={e => this.dismissKeyboard && this.dismissKeyboard(e)}>
                <ActionBar title="维修车辆" handleBack={this.handleBack.bind(this)}>
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>

                <base.VerticalLayout style={styles.imgWrap}>
                    {/* 上传图片 */}
                    <ImagePicker options={{ chooseFromLibraryButtonTitle: null }} onSelectPhoto={base64Data => {
                        this.setState({ imgPath: base64Data });
                    }} style={{ backgroundColor: '#fff' }} desc="请拍摄维修车辆照片" />
                </base.VerticalLayout>
                {/* 车牌号 */}
                <base.HorizontalLayout style={{ ...styles.searchBox, ...createBorderDetailStyle(1, 0, 1, 0) }}>
                    <base.VerticalLayout>
                        <base.Text>车牌号</base.Text>
                    </base.VerticalLayout>
                    <base.VerticalLayout style={styles.serachWrap}>
                        <TextInput style={styles.serachInput} placeholder='输入车牌号(如换牌,请输入新车牌号)' keyboardType="numeric" value={this.state.carno} onChangeText={(text) => { this.setState({ carno: text }); }}></TextInput>
                    </base.VerticalLayout>
                </base.HorizontalLayout>

                <base.VerticalLayout>
                    <base.Text style={{ ...styles.chooseText, ...createMarginStyle(15, 15, 0, 15) }}>请选择损坏部件</base.Text>
                    <base.HorizontalLayout style={{ ...styles.checkInfo, ...createPaddingStyle(7, 15, 15, 15) }}>
                        {/* 多选 */}
                        {this.state.checkedList.map((v, idx) => {
                            return (
                                <TouchableHighlight style={{ width: 120, height: 40 }} underlayColor={'#fff'} clickDuration={0} key={idx} onPress={() => {
                                    v.checked = !v.checked;
                                    this.setState({ checked: this.state.checkedList });
                                }}>
                                    <base.HorizontalLayout style={styles.checkBox}>
                                        <Checkbox checked={v.checked}
                                            onChange={() => {
                                                let c = this.state.checkedType;
                                                c[idx] = !c[idx];
                                                v.checked = c[idx];
                                                this.setState({
                                                    checked: this.state.checkedList,
                                                    checkedType: c,
                                                });
                                            }} />
                                        <Text style={{ marginLeft: 5 }} >{v.title}</Text>
                                    </base.HorizontalLayout>
                                </TouchableHighlight>
                            );
                        })}
                    </base.HorizontalLayout>
                </base.VerticalLayout>
                <Button style={{ ...styles.requireBtn, ...createMarginStyle(30, 15, 10, 15) }} onPress={this.submitInfo.bind(this)}>提交</Button>
            </base.VerticalLayout>
        );
    }

});

// 整合样式
const createStyle = (type) => (top, right, bottom, left) => ({
    [`${type}Top`]: top,
    [`${type}Right`]: right,
    [`${type}Bottom`]: bottom,
    [`${type}Left`]: left,
});
const createBordersStyle = (type) => (top, right, bottom, left) => ({
    [`${type}TopWidth`]: top,
    [`${type}RightWidth`]: right,
    [`${type}BottomWidth`]: bottom,
    [`${type}LeftWidth`]: left,
});
const createBorderStyle = createStyle('border');
const createMarginStyle = createStyle('margin');
const createPaddingStyle = createStyle('padding');
const createBorderDetailStyle = createBordersStyle('border');

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff',
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    imgWrap: {
        paddingTop: 30,
        paddingBottom: 30,
        alignItems: 'center',
        justifyContent: 'center',
        height: 160
    },
    serachWrap: {
        marginLeft: 20,
        flex: 1,
    },
    searchBox: {
        height: 65,
        backgroundColor: '#fff',
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderTopColor: 'rgb(221,221,221)',
        alignItems: 'center',
        paddingLeft: 15,
    },
    serachInput: {
        height: 44,
        fontSize: 16,
        flexGrow: 1,
        flexBasis: 0,
    },
    chooseText: {
        color: '#aaa',
    },
    requireBtn: {
        height: 48,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        backgroundColor: '#333',
        fontSize: 15,
        color: '#fff',
    },
    checkInfo: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
    },
    checkBox: {
        flexShrink: 0,
        alignItems: 'center',
        height: 40,
        width: 130,
        marginTop: 3
    },
};

const mapStateToProps = state => {
    return {
        token: state.api.token,
        img_path: state.api.imageUploadStatus,
        subRecordStatus: state.api.subRecordStatus,
    };
};

export default connect(mapStateToProps)(RepairCarRecord);



