import React, {Component} from 'react';
import * as base from '../../base';
import {api, keys, loadToken, getTodayDate, getUserInfo, apibase} from '../../runtime/api';
import {Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator, Map} from '../../base';
import {connect} from 'react-redux';
import {JumpTo, historyStack, JumpBack} from '../../modules';
import {ActionBar} from '../../components/actionbar';
import styles from '../../css/repairer/repairDetailCss';
import pageWrapper from '../../modules/pageWrapper';
import {fetch} from '../../modules';
var StaticImage = require('../../base/staticImages');

var moment = require('moment');
import {
    fetchReportDetail
} from '../../runtime/action';

/**
 * 举报详情
 */

class CarDetail extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            historyState: historyStack.get(),
            region: {
                latitude: 39.98362,
                longitude: 116.306715,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005,
            },
            initialPosition: undefined,
            lastPosition: undefined,
            annotations: [],
        };
    }

    componentWillMount() {
    }

    viewWillAppear() {
        this.props.fetch().then(()=>{

            console.log('position', this.props.carLocation);
            let annotations = [{
                latitude: parseFloat(this.props.carLocation.lat),
                longitude: parseFloat(this.props.carLocation.lng),
                icon: StaticImage['icon'],
                title: '',
                key: ''
            }];
            const region = {latitude: parseFloat(this.props.carLocation.lat), longitude: parseFloat(this.props.carLocation.lng), latitudeDelta: 0.005, longitudeDelta: 0.005};
            this.setState({annotations, refresh: true, region});
            this.setState({loading: false});
        });
    }

    handleBack(){
        this.props.clear();
        JumpBack();
    }
    repairDone(biycleNum) {
        let _this = this;
        return getUserInfo()
        .then(userInfo=> {
            let {token} = userInfo;
            fetch(`${apibase}/repairer/done`, {
                method: 'POST',
                body: {token, 'bicycle_number': biycleNum}
            }).then(respones=>{
                if(respones['code'] === 0){
                    Toast.success('处理成功', 1.5);
                    setTimeout(function() {
                        _this.handleBack();
                    }, 1500);
                }else{
                    Toast.fail(Toast['message'], 1.5);
                }
            });
        });



    }
    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderReportDetail();
    }
    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="报修" handleBack={this.handleBack.bind(this)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <base.View style={{position: 'relative', flexGrow: 1}}>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='loading'/>
                        <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }
    renderReportDetail() {
        return (
            <base.VerticalLayout className="wrap" style={styles.container}>
                <ActionBar title="报修" handleBack={this.handleBack.bind(this)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <ScrollView style={{flex: 1}} className="overflow-y">
                    <base.VerticalLayout style={{height: 350}}>
                        <Map style={{flexGrow: 1, height: 350}}  region={this.state.region} annotations={this.state.annotations} />
                    </base.VerticalLayout>

                    <base.VerticalLayout style={styles.infoBox}>
                        <base.VerticalLayout style={styles.center}>
                            <base.Text style={styles.h1}>车牌号 {this.props.biycleNum}</base.Text>
                            <base.HorizontalLayout style={styles.reasonBox}>
                                {this.props.reportReason.map((r, index)=> {
                                    return (
                                        <base.HorizontalLayout key={index} style={styles.reason}>
                                            <base.Text style={styles.reasonItem}>{r.reason_tag}</base.Text>
                                        </base.HorizontalLayout>
                                    );
                                })}
                            </base.HorizontalLayout>
                            <base.HorizontalLayout style={{alignItems: 'center'}}>
                                <base.Text style={styles.color2}>{getTodayDate(this.props.reportTime)}</base.Text>
                                <base.HorizontalLayout style={styles.lineBox}><base.Text style={styles.color2}>解锁码{this.props.unlockCode}</base.Text></base.HorizontalLayout>
                            </base.HorizontalLayout>
                        </base.VerticalLayout>
                         {this.props.address ? <base.HorizontalLayout style={styles.itemBot}>
                                <base.Image style={styles.icon} src='icon'></base.Image>
                                <base.HorizontalLayout style={styles.address}>
                                    <base.Text style={{flexWrap: 'wrap'}}>{this.props.address}</base.Text>
                                </base.HorizontalLayout>
                            </base.HorizontalLayout>:null}
                        <base.VerticalLayout style={styles.btnBox}>
                            <Button style={styles.btn} onPress={this.repairDone.bind(this, this.props.biycleNum)}>
                                <base.Text style={{fontSize: 18, color: '#fff'}}>处理完成</base.Text>
                            </Button>
                        </base.VerticalLayout>
                    </base.VerticalLayout>

                </ScrollView>


           </base.VerticalLayout>
        );
    }

}

const mapStateToProps = state=> {
    let reportDetail = state.api.reportDetail || {address: {}};
    return {
        biycleNum: reportDetail.bicycle || 0,
        reportReason: reportDetail.reason || [],
        reportTime: reportDetail.time || '',
        unlockCode: reportDetail.unlock_code || '',
        address: reportDetail.address && reportDetail.address.actual || '',
        carLocation: {
            lat: reportDetail.address && reportDetail.address.latitude || 0,
            lng: reportDetail.address && reportDetail.address.longitude || 0,
        }
    };
};
const mapDispatchToProps = dispatch=> {
    return {
        dispatch,
        fetch: props=>dispatch(fetchReportDetail(props)),
        clear: props=>dispatch({type: '@@clear', payload: {reportDetail: {address: {}}}})
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(CarDetail));



