import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as base from '../../base';
import { Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator } from '../../base';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import * as actions from '../../runtime/action';
import pageWrapper from '../../modules/pageWrapper';
class ZoneList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            selectedPaneIndex: 0,
            historyState: historyStack.get(),
        };
        this.pane = [{ text: '校园' }, { text: '城市' }];
    }

    async viewWillAppear() {
        const { zone_id, current_level } = this.props;
        await this.props.fetchList({ zone_id, current_level, });
        this.setState({ loading: false });
    }

    handlePress(item) {
        this.state.historyState.select(item);
        JumpBack();
    }

    handleBack() {
        JumpBack();
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title='选择进车区域'
                    rightAction={() => this.props.fetch(this.props)}>
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>
                <base.View style={{ position: 'relative', flexGrow: 1 }}>
                    <base.View style={styles.inner}>
                        <ActivityIndicator size='large' src='loading' />
                        <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }

    renderPane() {
        return (
            <base.HorizontalLayout style={{ height: 44, ...styles.borderBottom }}>
                {
                    this.pane.map((pane, index) => {
                        let focusStyle = {
                            borderBottomWidth: 4,
                            borderBottomStyle: 'solid',
                            borderBottomColor: '#fff'
                        };
                        if (index === this.state.selectedPaneIndex) {
                            focusStyle = {
                                borderBottomWidth: 4,
                                borderBottomStyle: 'solid',
                                borderBottomColor: base.Color.yellow
                            };
                        }
                        return (
                            <base.HorizontalLayout
                                key={index}
                                style={{ flexGrow: 1, justifyContent: 'center', ...focusStyle }}>
                                <base.TouchableHighlight
                                    activeOpacity={.1}
                                    underlayColor={'#f3f3f3'}
                                    style={{ alignItems: 'center', justifyContent: 'center', flexGrow: 1 }}
                                    onPress={() => {
                                        this.setState({ selectedPaneIndex: index });
                                    }}>
                                    <base.HorizontalLayout style={{ justifyContent: 'center' }}>
                                        <Text style={{ fontSize: 18 }}>{pane.text}</Text>
                                    </base.HorizontalLayout>
                                </base.TouchableHighlight>
                            </base.HorizontalLayout>
                        );
                    })
                }
            </base.HorizontalLayout>);
    }

    renderList() {
        const { societyList, schoolList } = this.props;
        let list;
        list = this.state.selectedPaneIndex ? societyList : schoolList;
        return (
            <base.View style={styles.container}>
                <ActionBar title={'选择进车区域'}
                    handleBack={() => this.handleBack()}
                    rightAction={() => {
                    }}>
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>
                {this.renderPane()}
                <base.VerticalLayout style={{ flexGrow: 1, flexBasis: 0 }}>
                    <ScrollView style={{ flexGrow: 1, flexBasis: 0 }}>
                        {list.map((item, idx) => {
                            return (
                                <base.HorizontalLayout key={idx} style={{
                                    height: 64,
                                    marginTop: 10,
                                    backgroundColor: '#fff',
                                    paddingLeft: 20,
                                    alignItems: 'center', ...styles.borderTop, ...styles.borderBottom
                                }}>
                                    <base.TouchableHighlight
                                        onPress={() => this.handlePress(item)}
                                        activeOpacity={.1}
                                        underlayColor={'#f3f3f3'}
                                        style={{ flexGrow: 1 }}>
                                        <base.HorizontalLayout style={{ flexGrow: 1, alignItems: 'center' }}>
                                            <Text>
                                                {item.zone_name}
                                            </Text>
                                        </base.HorizontalLayout>
                                    </base.TouchableHighlight>
                                </base.HorizontalLayout>
                            );
                        })}
                    </ScrollView>
                </base.VerticalLayout>
            </base.View>
        );
    }

    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderList();
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        zone_id: state.api.zone_ids[0],
        societyList: state.api.societyList,
        schoolList: state.api.schoolList,
        current_level: state.api.userInfo.repairer_level || state.api.userInfo.operation_level,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch,
        fetchList: props => dispatch(actions.getZoneList({ ...props }))
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    inner: {
        position: 'absolute',
        flexDirection: 'column',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    borderTop: {
        borderTopStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#ddd'
    },
    borderBottom: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    borderLeft: {
        borderLeftStyle: 'solid',
        borderLeftWidth: 1,
        borderLeftColor: '#ddd'
    },
    borderRight: {
        borderRightStyle: 'solid',
        borderRightWidth: 1,
        borderRightColor: '#ddd'
    },
    cbox: {
        justifyContent: 'center',
    },
    color2: {
        color: '#7f7f7f',
    },
    p10: {
        paddingRight: 10
    },
    list: {
        flexGrow: 1,
    },
    listBody: {
        flexGrow: 1,
        borderStyle: 'solid',
        borderTopColor: '#c6c6c6',
        borderTopWidth: 1,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    listView: {
        flexGrow: 1,
    },
    top: {
        backgroundColor: '#fff',
        flexShrink: 0
    },
    mingci: {
        paddingLeft: 6,
        paddingRight: 6,
        height: 17,
        backgroundColor: '#696969',
        borderRadius: 2,
        alignItems: 'center',
        marginLeft: 10,
    },
    btnBox: {
        marginTop: 10,
        alignItems: 'center',
    },
    num: {
        color: '#ff8300',
        fontSize: 26,
    },
    mingText: {
        fontSize: 12,
        color: '#fff'
    },
    detailBtn: {
        width: 144,
        height: 35,
        backgroundColor: '#ffd900',
        borderRadius: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    /*item */
    items: {
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        height: 60,
        justifyContent: 'center',
        position: 'relative',
        backgroundColor: '#fff',
        paddingRight: 10,
        paddingLeft: 10,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    item: {
        height: 60,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    arrow: {
        width: 8,
        height: 14,
    },
    flexBox: {
        flexGrow: 1,
        alignItems: 'center',
    },
    flexBoxCenter: {
        flexGrow: 1,
        justifyContent: 'center',
    },
    flexBoxRight: {
        flexGrow: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
};

export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(ZoneList));

