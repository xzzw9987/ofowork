import React, {Component} from 'react';
import * as base from '../../base';
import {api, keys} from '../../runtime/api';
import {Text, View, Br, Button, HorizontalLayout, ListView, List, Map, Toast} from '../../base';
import {connect} from 'react-redux';
import {JumpTo, JumpBack} from '../../modules';
import {ActionBar} from '../../components/actionbar';
import whereaimi from '../../base/map/whereami';
import platform from '../../modules/platform';
import Modal from '../../components/commonModal';
var StaticImage = require('../../base/staticImages');

class RepairSelectVirtualPosition extends Component {
    constructor() {
        super();
        this.state = {
            initialPosition: undefined,
            lastPosition: undefined,
            region: {
                latitude: 39.979024,
                longitude: 116.312589,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005,
            },
            annotations: [],
            currentAnnotationInfo: {}
        };

        this.currentAnnotationInfo = null;
    }

    componentDidMount() {
        if (!this.state.lastPosition) {
            whereaimi().then(e=> {
                let latitude = e.latitude;
                let longitude = e.longitude;
                // 用户当前位置
                this.setState({
                    lastPosition: {latitude, longitude},
                    region: {latitude, longitude, latitudeDelta: 0.005, longitudeDelta: 0.005}
                });

                this.props.dispatch(api(keys.selectVirtualPosition, {
                    token: this.props.token,
                    lat: latitude,
                    lng: longitude
                }));
            });
        }
    }

    handleGoBack() {
        this.props.dispatch({type: 'clear', data: {code: undefined, alert: undefined}});
        JumpBack();
    }

    handleConfirmSelectPosition() {
        let currentAnnotationInfo = this.currentAnnotationInfo;
        if(currentAnnotationInfo){
            this.props.dispatch({
                type: 'virtualPileInfo',
                data: {pile_id: currentAnnotationInfo['pile_id'], pile_name: currentAnnotationInfo['name']}
            });
            JumpBack();
        }else{
            Toast.fail('请选择摆放点', 1.5);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (typeof nextProps.nearVirtualPile !== undefined && nextProps.nearVirtualPile != this.props.nearVirtualPile) {

            // 虚拟车桩
            let annotations = [];
            nextProps.nearVirtualPile.map((item, i) => {
                annotations.push({
                    key: `${i}`,
                    pile_id: item.pile_id,
                    name: item.name,
                    latitude: parseFloat(item.lat),
                    longitude: parseFloat(item.lng),
                    icon: StaticImage['bicycle'],
                    title: item.name,
                    size: {
                        width: 44,
                        height: 51
                    },
                    animation: 'AMAP_ANIMATION_DROP',
                    label: {content: 'position', offset: '（0,0）'}
                });
            });
            this.setState({annotations});
        }
    }


    onLocationChange(e) {
    }

    onAnnotationClicked(key) {
        let current = this.state.annotations[key];
        if(platform.OS != 'web'){
            Modal({
                content: `确认选择：${current['pile_id']}？`,
                buttons: [
                    {text: '取消', onPress: () => {}},
                    {text: '确认', onPress: () => {
                        this.currentAnnotationInfo = current ;
                    }},
                ]
            });
        }else{
            this.currentAnnotationInfo = current;
        }
    }

    render() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="车辆摆放点" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.VerticalLayout style={{flex: 1}}>
                    <Map style={{flex: 1}} region={this.state.region} annotations={this.state.annotations}
                         showsUserLocation={true}
                         onLocationChange={this.onLocationChange.bind(this)}
                        //  onAnnotationClicked={ () => {
                        //        this.onAnnotationClicked.bind(this)
                        // }}
                         onAnnotationClicked={this.onAnnotationClicked.bind(this)}
                    />
                    <base.HorizontalLayout style={styles.footer}>
                        <Button style={styles.submitBtn} className="confirmSelectionBtn"
                                onPress={ this.handleConfirmSelectPosition.bind(this)}>确认选点</Button>
                    </base.HorizontalLayout>
                </base.VerticalLayout>

            </base.View>
        );
    }
}


const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    btn: {
        backgroundColor: '#ffd900',
        borderRadius: 2,
        flexGrow: 1,
        height: 44,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15
    },
    footer: {
        position: 'relative',
        height: 80,
        backgroundColor: '#fff',
        marginTop: -15,
        zIndex: 9999,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 16,
        paddingRight: 16,
    },
    submitBtn: {
        backgroundColor: '#2C2C2C',
        color: '#fff',
        height: 44,
        borderRadius: 2,
        justifyContent: 'center',
        fontSize: 15,
        alignItems: 'center',
        flexGrow: 1,
    }
};
const mapDispatchToProps = dispatch=> {
    return {
        dispatch
    };
};
export default connect((state) => {
    return {
        token: state.api.token,
        nearVirtualPile: state.api.nearVirtualPile,
    };
}, mapDispatchToProps)(RepairSelectVirtualPosition);
