import React, { Component } from 'react';
import createTab from '../../modules/createTab';
import RepairerIndex from './repairerIndex';
import WorkBench from './repairerBench';
import ProfileIndex from '../profile/profileIndexWithoutBack';

export default createTab([
    {
        icon: require('../../public/images/workIndex.png'),
        selectedIcon: require('../../public/images/workIndexActive.png'),
        title: '首页',
        view: <RepairerIndex/>
    },
    {
        icon: require('../../public/images/workBench.png'),
        selectedIcon: require('../../public/images/workBenchActive.png'),
        title: '工作台',
        view: <WorkBench/>
    },
    {
        icon: require('../../public/images/self.png'),
        selectedIcon: require('../../public/images/selfActive.png'),
        title: '我',
        view: <ProfileIndex/>
    }
]);
