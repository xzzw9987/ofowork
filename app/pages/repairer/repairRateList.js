import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, parseNum, loadToken } from '../../runtime/api';
import { Text, View, Br, Button, ScrollView, TouchableHighlight, ActivityIndicator } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';

var moment = require('moment');

/**
 * 每日报修率列表
 */
class RepairRateList extends Component {

    componentWillMount() {
        loadToken.bind(this, () => {
            let zone_id = this.props.zone_ids[this.props.zoneIndex || 0];
            this.props.dispatch(api(keys.carsRepairRate, {token: this.props.token, type: 'history', zone_id: zone_id}, 'repairRateList'));
        })();

    }

    componentWillReceiveProps(nextProps){
        let zone_id = this.props.zone_ids[this.props.zoneIndex || 0];
        //获取报修率列表
        //this.props.dispatch(api(keys.carsRepairRate,{token: this.props.token,type:"history", zone_id:zone_id},"repairRateList"));
    }

    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }

    showWeekCN(dayOfEN) {
        return {
            'Monday': '星期一',
            'Tuesday': '星期二',
            'Wednesday': '星期三',
            'Thursday': '星期四',
            'Friday': '星期五',
            'Saturday': '星期六',
            'Sunday': '星期日',
        }[dayOfEN] || '未知';
    }

    render() {
        let repairRateList = this.props.repairRateList;
        if(!repairRateList) {
            return (
                 <base.View style={styles.container}>
                    <ActionBar title="每日报修率" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                    <base.View style={{position: 'absolute', flexDirection: 'column', left: 0, top: 44, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center'}}>
                        <ActivityIndicator size='large' src='loading'/>
                        <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                 </base.View>
            );
        } else {
            const zone_id = this.props.zone_ids[this.props.zoneIndex || 0];
            const zone_name = this.props.zone_names[this.props.zoneIndex || 0];
            let carsRepairText = '';
            let color = '#2EBF38';
            if(this.props.repairRate < 5){
                carsRepairText = '正常';
                color = '#2EBF38';
            }else if(this.props.repairRate >= 5 && cthis.props.repairRate <10){
                carsRepairText = '较高';
                color = '#FF8300';
            }else if(this.props.repairRate >= 15 ){
                carsRepairText = '很高';
                color = '#E23000';
            }
            return (
                <base.View style={styles.container}>
                    <ActionBar title="每日报修率" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                    <base.VerticalLayout style={styles.list}>
                    <base.VerticalLayout>
                             <base.HorizontalLayout  style={styles.items}>
                                <base.VerticalLayout  className="te" style={styles.leftBox}>
                                    <Text style={{fontSize: 18}} className="te mt5">{zone_name}</Text>
                                    <Text style={styles.color2}>近30日均值</Text>
                                </base.VerticalLayout>
                                <base.VerticalLayout style={styles.vcbox}>
                                    <base.HorizontalLayout style={styles.vcbox}>
                                        <base.HorizontalLayout style={Object.assign({backgroundColor: color}, styles.textBox)}>
                                            <base.Text style={styles.titleText}>{carsRepairText}</base.Text>
                                        </base.HorizontalLayout>
                                        <Text style={{fontSize: 24}}>{this.props.repairRate}%</Text>
                                    </base.HorizontalLayout>
                                </base.VerticalLayout>
                            </base.HorizontalLayout>
                        <Br/>
                        </base.VerticalLayout>
                        <base.VerticalLayout style={styles.scrollBorder}></base.VerticalLayout>
                     <ScrollView className="overflowY" style={styles.listView}>

                            {this.props.repairRateList.map(((item, idx) => {
                                return (
                                    <TouchableHighlight key={idx}  style={styles.items}>
                                        <base.HorizontalLayout style={styles.item}>
                                            <base.HorizontalLayout style={styles.one}>
                                                <Text style={{marginRight: 10}}>{item.date}</Text>
                                                 <Text style={styles.smallText}>{this.showWeekCN(moment(item.date).format('dddd'))}</Text>
                                            </base.HorizontalLayout>
                                            <base.VerticalLayout style={styles.right}>
                                                <Text style={{fontSize: 20}}>{item.rate}%</Text>
                                            </base.VerticalLayout>
                                        </base.HorizontalLayout>
                                    </TouchableHighlight>
                                );
                            }))}

                        </ScrollView>


                    </base.VerticalLayout>
                </base.View>
            );

        }
    }
}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
    /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    color2: {
        color: '#7f7f7f',
        fontSize: 12
    },
    smallText: {
        fontSize: 14,
        color: '#7f7f7f',
    },
    vcbox: {
        alignItems: 'center',
        justifyContent: 'center',
    },
  /*item */
    items: {
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        height: 60,
        backgroundColor: '#fff',
        paddingRight: 10,
        paddingLeft: 10,
        justifyContent: 'space-between',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    itemText: {
        height: 60,
        alignItems: 'center',
    },
    list: {
        flexGrow: 1
    },
    listView: {
        flexGrow: 1,
    },
    scrollBorder: {
        borderStyle: 'solid',
        borderTopColor: '#c6c6c6',
        borderTopWidth: 1,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    item: {
        height: 60,
        alignItems: 'center',
    },
    right: {
        alignItems: 'flex-end',
        flexGrow: 1,
    },
    icon: {
        width: 11,
        height: 15,
        marginRight: 3
    },
    one: {
        flexGrow: 1,
        alignItems: 'center',
    },
    textBox: {
        height: 20,
        width: 46,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 4,
    },
    titleText: {
        color: '#fff',
        fontSize: 14
    },
    leftBox: {
        flexGrow: 1,
        height: 50,
        justifyContent: 'flex-end',
    }
};

export default connect((state) => {
    return {
        token: state.api.token,
        userInfo: state.api.userInfo,
        zone_ids: state.api.zone_ids,
        zoneIndex: state.user.zoneIndex,
        repairRate: state.api.repairRate,
        repairRateList: state.api.repairRateList,
        zone_names: state.api.zone_names,
    };
})(RepairRateList);
