import React, {Component} from 'react';
import * as base from '../../base';
import {api, keys, loadToken, getTodayDate, _loadToken, numberSplit} from '../../runtime/api';
import {
    Text,
    View,
    Br,
    Button,
    Tabs,
    TabPane,
    ScrollView,
    Toast,
    TouchableHighlight,
    ActivityIndicator,
    Map
} from '../../base';
import {connect} from 'react-redux';
import {JumpTo, historyStack, JumpBack} from '../../modules';
import {ActionBar} from '../../components/actionbar';
import styles from '../../css/repairer/reportDetailCss';
import pageWrapper from '../../modules/pageWrapper';
import Swiper, {Item} from '../../components/swiper';
import whereami from '../../base/map/whereami';
import getDistance from '../../base/map/getDistance';
import Vibration from '../../modules/vibration';

var StaticImage = require('../../base/staticImages');

var moment = require('moment');
import {
    fetchReportDetail
} from '../../runtime/action';

/**
 * 举报详情
 */

class ReportDetail extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            scrollable: true,
            historyState: historyStack.get(),
            region: {
                latitude: 39.98362,
                longitude: 116.306715,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005,
            },
            initialPosition: undefined,
            lastPosition: undefined,
            annotations: [],
            distance: ''
        };
    }

    componentWillMount() {
    }

    componentDidMount() {
        whereami().then(e=> {
            let latitude = e.latitude;
            let longitude = e.longitude;
            // 用户当前位置
            this.setState({
                lastPosition: {latitude, longitude},
                region: {latitude, longitude, latitudeDelta: 0.005, longitudeDelta: 0.005}
            });
        });

    }

    viewWillAppear() {
        const p1 = this.props.fetch().then(()=> {

            console.log('position', this.props.carLocation);
            let annotations = [{
                latitude: parseFloat(this.props.carLocation.lat),
                longitude: parseFloat(this.props.carLocation.lng),
                icon: StaticImage['reportIcon'],
                size: {
                    width: 30,
                    height: 38
                },
                title: '',
                key: 'hello~'
            }];

            const region = {
                latitude: parseFloat(this.props.carLocation.lat),
                longitude: parseFloat(this.props.carLocation.lng),
                latitudeDelta: 0.005,
                longitudeDelta: 0.005
            };
            this.setState({annotations, refresh: true, region});
            this.setState({loading: false});
            return {
                latitude: parseFloat(this.props.carLocation.lat),
                longitude: parseFloat(this.props.carLocation.lng)
            };
        });

        this.updateLocation(p1);
        let _this = this;
        this.timer = setInterval(function () {
            _this.updateLocation(p1);
        }, 10000);
    }

    viewWillDisappear() {
        clearInterval(this.timer);
    }

    updateLocation(p1) {
        p1 = p1 || Promise.resolve({
            latitude: this.state.region.latitude,
            longitude: this.state.region.longitude
        });

        const p2 = whereami();
        Promise.all([p1, p2])
            .then(d=>d[1].error ? null : getDistance(d[0], d[1]))
            .then(distance=> {
                if (distance == null) {
                    this.setState({distance});
                    return;
                }
                distance = ~~distance;
                distance < 10 && Vibration.vibrate();
                this.setState({distance});
            });
    }

    handleBack() {
        this.props.clear();
        JumpBack();
    }

    // onLocationChange(e) {
    //     console.log('RepairCarMap onLocationChange', this.state.refresh,e);
    //     if(!this.state.lastPosition ) {
    //         let latitude = e.latitude;
    //         let longitude = e.longitude;
    //         // 用户当前位置
    //         this.setState({lastPosition:{latitude,longitude},
    //             region: {latitude, longitude, latitudeDelta: 0.005, longitudeDelta: 0.005}});
    //     }
    // }
    /**
     * 点击未发现车辆或处理完成
     */
    reportDone(type) {
        console.log('type=====', type);
        console.log('maintenanceType=====', this.props.maintenanceType);
        JumpTo('repair/reportDone', true, {
            type: type,
            id: historyStack.get().id,
            maintenanceType: this.props.maintenanceType
        });
    }

    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderReportDetail();
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="维修" handleBack={this.handleBack.bind(this)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <base.View style={{position: 'relative', flexGrow: 1}}>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='loading'/>
                        <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }

    renderReportDetail() {
        return (
            <base.VerticalLayout className="wrap" style={styles.container}>
                <ActionBar title="维修" handleBack={this.handleBack.bind(this)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <ScrollView scrollEnabled={this.state.scrollable} style={{flex: 1}} className="overflow-y">
                    <base.VerticalLayout style={{height: 300}}>
                        {this.props.reportImg.length > 0 ?
                            <Swiper>
                                {
                                    this.props.reportImg.map((item, index)=> (
                                        <Item key={index} style={{height: 300, justifyContent: 'center'}}>
                                            <base.Image className="contain" resizeMode="contain" style={{height: 300}}
                                                        src={`${item}?x-oss-process=image/resize,h_320`}></base.Image>
                                        </Item>
                                    ))
                                }
                            </Swiper> :
                            <base.HorizontalLayout style={{height: 300, justifyContent: 'center'}}>
                                <base.Image className="contain" resizeMode="contain" style={{height: 300}}
                                            src="defaultImage2"></base.Image>
                            </base.HorizontalLayout>
                        }
                    </base.VerticalLayout>
                    <base.VerticalLayout style={styles.center}>
                        <base.HorizontalLayout style={{alignItems: 'center'}}>
                            <base.Text style={styles.h1}>
                                车牌号 {this.props.biycleNum === '-----' ? '无' : this.props.biycleNum}</base.Text>
                            {this.props.isInner ?
                                <base.HorizontalLayout style={styles.hint}>
                                    <base.Text style={{color: '#fff', fontSize: 12}}>内部
                                        {
                                            this.props.maintenanceType == 'repair' ? '报修' : '举报'
                                        }
                                    </base.Text>
                                </base.HorizontalLayout> : null
                            }

                        </base.HorizontalLayout>
                        <base.HorizontalLayout style={{marginTop: 10, marginBottom: 5, flexWrap: 'wrap'}}>
                            {this.props.reportReason.map((r, index)=> {
                                return (
                                    <base.HorizontalLayout key={index} style={styles.reason}>
                                        <base.Text style={styles.reasonItem}>{r}</base.Text>
                                    </base.HorizontalLayout>
                                );
                            })}
                        </base.HorizontalLayout>
                        {this.props.descr ? <base.HorizontalLayout style={styles.textBox}>
                            <base.Text style={{color: '#666', fontSize: 14}}>备注：{this.props.descr}</base.Text>
                        </base.HorizontalLayout> : null}
                        <base.HorizontalLayout style={styles.codeBox}>
                            <base.Text style={styles.color2}>{getTodayDate(this.props.reportTime)}</base.Text>
                            <base.HorizontalLayout style={styles.lineBox}>
                                <base.Text style={styles.color2}>解锁码{this.props.unlockCode}</base.Text>
                            </base.HorizontalLayout>
                        </base.HorizontalLayout>
                        {
                            this.state.distance !== null ?
                                (<base.HorizontalLayout style={styles.distanceBox}>
                                    <base.Text style={{color: '#333', fontSize: 14}}>当前距离</base.Text>
                                    <base.Text style={{
                                        fontSize: 22,
                                        fontWeight: 'bold',
                                        color: '#FF7000'
                                    }}>{`${this.state.distance}`}</base.Text>
                                    <base.Text style={{color: '#333', fontSize: 14}}>米</base.Text>
                                </base.HorizontalLayout>)
                                : (
                                <base.HorizontalLayout style={styles.distanceBox}>
                                    <base.Text style={{color: '#333', fontSize: 14}}>当前距离未知</base.Text>
                                </base.HorizontalLayout>)
                        }
                    </base.VerticalLayout>
                    {this.props.address ? <base.HorizontalLayout style={styles.itemBot}>
                        <base.Image style={styles.icon} src='icon'></base.Image>
                        <base.HorizontalLayout style={styles.address}>
                            <base.Text style={{flexWrap: 'wrap'}}>{this.props.address}</base.Text>
                        </base.HorizontalLayout>
                    </base.HorizontalLayout> : null}
                    <base.VerticalLayout style={{height: 300}}>
                        <Map style={{flexGrow: 1, height: 300}} region={this.state.region}
                             annotations={this.state.annotations}
                             onMoveShouldSetResponder={()=>true}
                             onResponderMove={()=>this.setState({scrollable: false})}
                             onResponderRelease={()=>this.setState({scrollable: true})}
                        />
                    </base.VerticalLayout>
                </ScrollView>
                <base.VerticalLayout style={styles.footer}>
                    <base.HorizontalLayout style={styles.hintBox}>
                        <base.Text style={{fontSize: 12, color: '#333'}}>请在举报地提交处理结果</base.Text>
                    </base.HorizontalLayout>
                    <base.HorizontalLayout style={styles.btnBox}>
                        <Button onPress={this.reportDone.bind(this, 'not_found')} style={styles.btn}>
                            <base.Text style={{color: '#fff'}}>没找到车</base.Text>
                        </Button>
                        <Button onPress={this.reportDone.bind(this, 'processed')} style={styles.btn}>
                            <base.Text style={{color: '#fff'}}>已找到车</base.Text>
                        </Button>
                    </base.HorizontalLayout>
                </base.VerticalLayout>
            </base.VerticalLayout>
        );
    }

}

const mapStateToProps = state=> {
    let reportDetail = state.api.reportDetail || {img_path: ''};
    return {
        reportImg: reportDetail.img_path && reportDetail.img_path.split(',') || [],
        biycleNum: reportDetail.car_no || 0,
        reportReason: reportDetail.reason || [],
        reportTime: reportDetail.time || '',
        unlockCode: reportDetail.unlock_code || '',
        address: reportDetail.address || '',
        descr: reportDetail.descr || '',
        carLocation: {
            lat: reportDetail.img_lat || reportDetail.lat || 0,
            lng: reportDetail.img_lng || reportDetail.lng || 0,
        },
        isInner: reportDetail.is_inner || false,
        maintenanceType: reportDetail.maintenance_type || 'repair'
    };
};
const mapDispatchToProps = dispatch=> {
    return {
        dispatch,
        fetch: props=>dispatch(fetchReportDetail(props)),
        clear: props=>dispatch({type: '@@clear', payload: {reportDetail: {img_path: ''}}})
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(ReportDetail));



