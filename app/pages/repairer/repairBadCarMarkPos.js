import React, {Component} from 'react';
import * as base from '../../base';
import {api, keys, apibase, getUserInfo} from '../../runtime/api';
import {Text, View, Br, Button, Toast, TextInput, ActivityIndicator, TouchableHighlight, Map, Image} from '../../base';
import {connect} from 'react-redux';
import {JumpTo, historyStack, JumpBack} from '../../modules';
import {ActionBar} from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import whereami from '../../base/map/whereami';
import * as actions from '../../runtime/action';
import ImagePicker from '../../components/gallery';
import {fetch} from '../../modules';
import StaticImage from '../../base/staticImages';
import geoMessage from '../../base/map/geoMessage';

/**
 * 地图选点
 */

const repairBadCarMarkPos = pageWrapper(class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            scrollable: true,
            token: '',
            address: '',
            region: null,
            annotations: [],
        };
    }

    viewWillAppear() {
        whereami().then(e => {
            let latitude = e.latitude;
            let longitude = e.longitude;
            // 用户当前位置
            this.setState({
                region: {
                    latitude,
                    longitude,
                    latitudeDelta: 0.005,
                    longitudeDelta: 0.005,
                },
            });
            geoMessage({latitude, longitude}).then(d => {
                this.setState({address: d.address, loading: false,});
            });
        });
    }

    // 返回
    handleBack() {
        JumpBack();
    }

    // 提交
    async submitInfo() {
        // 当前位置 传经纬度
        await this.props.dispatch(
            actions.subBadCarMarkPos({
                    latitude: this.state.region.latitude,
                    longitude: this.state.region.longitude,
                    address: this.state.address
                }
            ));
        this.handleBack();

    }

    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderrepairBadCarMarkPos();
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="地图选点" handleBack={this.handleBack.bind(this)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <base.View style={{position: 'relative', flexGrow: 1}}>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='loading'/>
                    </base.View>
                </base.View>
            </base.View>
        );
    }

    renderMapCenterIcon() {
        if (!this.state.iconPosition) return null;
        return (<Image className="fit" src="spot" style={{
            width: 26,
            height: 44,
            left: this.state.iconPosition.width / 2 - 15,
            top: this.state.iconPosition.height / 2 - 40,
            position: 'absolute',
            zIndex: 1,
        }}></Image>);
    }

    onRegionChange(e) {
        const copied = {...e};
        delete copied['target'];
        this.setState({
            region: copied
        });
        geoMessage({latitude: e.latitude, longitude: e.longitude}).then(d => {
            this.setState({address: d.address});
        });
    }

    renderMapAdd() {
        if (!this.state.latitude || !this.state.longitude) return (<base.Text style={{
            fontSize: 14,
            color: '#666',
            marginTop: 6
        }}>{this.state.address ? this.state.address : null}</base.Text>);

        return (<base.Text style={{
            fontSize: 14,
            color: '#666',
            marginTop: 6
        }}>{this.state.address ? this.state.address : null}</base.Text>);
    }

    renderMap() {
        return (this.state.loading && this.state.region) ? null :
            (<Map style={{flexGrow: 1}} defaultRegion={this.state.region}
                  onRegionChange={e => this.onRegionChange(e)}
                  annotations={this.state.annotations}
                  onMoveShouldSetResponder={() => true}
                  onResponderMove={() => this.setState({scrollable: false})}
                  onResponderRelease={() => this.setState({scrollable: true})}
            />);
    }

    renderrepairBadCarMarkPos() {
        return (
            <base.VerticalLayout className="wrap" style={styles.container}
                                 onStartShouldSetResponder={() => this.getKeyboardState()}
                                 onResponderGrant={e => this.dismissKeyboard && this.dismissKeyboard(e)}>
                <ActionBar title="地图选点" handleBack={this.handleBack.bind(this)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <base.VerticalLayout style={{flex: 1, position: 'relative'}}
                                     onLayout={e => this.setState({iconPosition: e.nativeEvent.layout})}>
                    { this.renderMap() }
                    { this.renderMapCenterIcon() }
                </base.VerticalLayout>
                <base.HorizontalLayout style={styles.gpsWrap}>
                    <Image className="fit" src="icon" style={styles.mapImg}></Image>
                    <base.VerticalLayout style={{...styles.gpsWrapPos, ...createMarginStyle(0, 10, 0, 10)}}>
                        <base.Text style={{fontSize: 16, color: '#333'}}>位置</base.Text>
                        { this.renderMapAdd() }
                    </base.VerticalLayout>
                    <Button style={styles.requireBtn} onPress={this.submitInfo.bind(this)}>确认选点</Button>
                </base.HorizontalLayout>
            </base.VerticalLayout>
        );
    }
});

// 整合样式
const createStyle = (type) => (top, right, bottom, left) => ({
    [`${type}Top`]: top,
    [`${type}Right`]: right,
    [`${type}Bottom`]: bottom,
    [`${type}Left`]: left,
});
const createBordersStyle = (type) => (top, right, bottom, left) => ({
    [`${type}TopWidth`]: top,
    [`${type}RightWidth`]: right,
    [`${type}BottomWidth`]: bottom,
    [`${type}LeftWidth`]: left,
});
const createBorderStyle = createStyle('border');
const createMarginStyle = createStyle('margin');
const createPaddingStyle = createStyle('padding');
const createBorderDetailStyle = createBordersStyle('border');

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff',
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    requireBtn: {
        display: 'flex',
        borderRadius: 2,
        backgroundColor: '#000',
        fontSize: 15,
        color: '#fff',
        padding: 10,
        width: 90,
        justifyContent: 'center',
        alignItems: 'center',
    },
    mapImg: {
        display: 'flex',
        width: 15,
        height: 20,
        alignItems: 'center',
    },
    gpsWrap: {
        display: 'flex',
        margin: 15,
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 60,
    },
    gpsWrapPos: {
        display: 'flex',
        flex: 1,
    }
};

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(repairBadCarMarkPos);



