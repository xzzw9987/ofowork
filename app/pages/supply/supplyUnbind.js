import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Text,
    View,
    Br,
    Button,
    Tabs,
    TabPane,
    HorizontalLayout,
    ScrollView,
    Toast,
    TouchableHighlight,
    ActivityIndicator
} from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import CameraView from '../../components/camera';
import Modal from '../../components/commonModal';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack, ResetTo } from '../../modules';
import { submitUnbind } from '../../runtime/action/supply';

const platePrefix = 'http://ofo.so/plate/';
const unbindContainer = pageWrapper(
    class extends Component {
        constructor(...args) {
            super(...args);
            this.state = {
                // historyState: historyStack.get(),
                loading: false,
                showCam: false
            };
            this.active = true;
        }

        viewWillAppear() {
            this.active = true;
            this.haveReadBarCode = false;

            this.setState({
                showCam: true
            });
        }

        viewWillDisappear() {
            this.active = false;

            this.setState({
                showCam: false
            });
        }

        handleBack() {
            this.setState({ showCam: false });
            JumpBack();
        }

        renderLoading() {
            return (
                <View style={styles.container}>
                    <ActionBar title="车辆解绑">
                        <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    </ActionBar>
                    <View style={{ position: 'relative', flexGrow: 1 }}>
                        <View style={styles.inner}>
                            <ActivityIndicator size="large" src="loading" />
                            <HorizontalLayout style={{ marginTop: 10 }}>
                                <Text>正在加载 ...</Text>
                            </HorizontalLayout>
                        </View>
                    </View>
                </View>
            );
        }

        renderPanel() {
            return (
                <View style={styles.container}>
                    <ActionBar
                        title="车辆解绑"
                        handleBack={() => this.handleBack()}
                    >
                        <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    </ActionBar>

                    {this.state.showCam
                        ? <CameraView
                            onBarCodeRead={d => this.onBarCodeRead(d)}
                            hint={'请扫描'}
                            torch={true}
                        />
                        : null}
                </View>
            );
        }

        render() {
            if (this.state.loading) return this.renderLoading();
            return this.renderPanel();
        }

        uploadSuccess(d) {
            if (d.code !== 0) return;
            Toast.success('解绑成功', 2);

            const j = url => {
                if (this.props.navigator && this.props.navigator.getCurrentRoutes) {
                    const navigator = this.props.navigator,
                        route = navigator.getCurrentRoutes().filter(d => d.url === url)[0];
                    if (route) {
                        navigator.popToRoute(route);
                        return true;
                    }
                    return false;
                } else {
                    ResetTo(url, true, {});
                    return true;
                }
            };

            setTimeout(() => {
                j('supply/unbind');
            }, 500);
        }

        uploadFail(d) {
            if (d.code === 0) return;
            Toast.fail(d.message, 2, () => this.haveReadBarCode = false);
        }

        onBarCodeRead(d) {
            if (this.haveReadBarCode || !this.active) return;
            this.haveReadBarCode = true;
            d = d.trim();

            Modal({
                content: '是否确定解绑？',
                buttons: [
                    {
                        text: '否',
                        onPress: () => {
                            this.haveReadBarCode = false;
                        }
                    },
                    {
                        text: '确定',
                        onPress: () => {
                            this.handleUnbind(d);
                        }
                    }
                ]
            });
        }

        async handleUnbind(code) {
            let res = null;
            this.setState({ loading: true });
            if (code.startsWith(platePrefix)) {
                code = code.substring(platePrefix.length, code.length);
                res = await submitUnbind({
                    type: /^\d+$/.test(code) ? 'bicycle_no' : 'fender_sn',
                    x_no: code
                });
            } else {
                res = await submitUnbind({
                    type: 'bicycle_id',
                    x_no: code
                });
            }
            this.setState({ loading: false });
            this.uploadSuccess(res);
            this.uploadFail(res);
        }
    }
);

const mapStateToProps = (state, ownProps) => {
    return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    },
    inner: {
        position: 'absolute',
        flexDirection: 'column',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
};

export default connect(mapStateToProps, mapDispatchToProps)(unbindContainer);
