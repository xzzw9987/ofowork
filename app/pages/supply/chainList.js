import React, { Component } from 'react';
import * as base from '../../base';
import { Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator } from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import CameraView from '../../components/camera';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { fetchList, del } from '../../runtime/action/supply';
import platform from '../../modules/platform';
import Modal from '../../components/commonModal';

export default pageWrapper(class extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            historyState: historyStack.get(),
            list: [{ code: '123', pwd: '456', date: '2016-10-01' }],
        };
        this.active = true;
    }

    viewWillAppear() {
        this.active = true;
        this.fetchList(daysAgo(0));
    }

    fetchList(arg) {
        this.setState({ loading: true });
        return fetchList(arg).then(d => {
            let { data } = d;
            this.setState({
                loading: false,
                list: data.list.map(v => ({
                    code: v['bicycle_no'],
                    pwd: v['unlock_code'],
                    date: v['create_time'],
                    type: v['lock_type'],
                    fender: v['fender_sn'],
                    bicycleId: v['bicycle_id']
                }))
            });
        });
    }

    del(id) {
        return del(id).then(d => {
            if (d.code === 0) {
                Toast.success('删除成功', 2);
                return this.fetchList(daysAgo(0));
            }
            Toast.fail(`删除失败,${d.message}`);
        });
    }

    viewWillDisappear() {
        this.active = false;
    }

    handleBack() {
        JumpBack();
    }

    render() {
        return this.state.loading ? this.renderLoading() : this.renderList();
    }

    renderList() {
        const { from } = this.state.historyState;
        const mechList = this.state.list.filter(x => x.type === '1');
        const smartList = this.state.list.filter(x => x.type !== '1');
        return (
            <base.View style={styles.container}>
                <ActionBar title={from === 'mech' ? `机械锁—今日录入 (${mechList.length}条)` : `智能锁—今日录入 (${smartList.length}条)`} handleBack={() => this.handleBack()}>
                    {

                        !this.state.historyState
                            ? (<base.Image style={{ width: 17, height: 17 }} src='setting' />) :
                            <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    }
                </ActionBar>
                <base.View style={{ flexGrow: 1, flexShrink: 0, flexBasis: 0 }}>
                    <ScrollView style={{ flexGrow: 1, flexShrink: 0, flexBasis: 0 }}>
                        {
                            from === 'mech' ?
                                    mechList.map((d, index) => this.renderItem(d, index)) :
                                    smartList.map((d, index) => this.renderItem(d, index))
                        }
                    </ScrollView>
                </base.View>
            </base.View>
        );
    }

    renderItem(d, index) {
        return (
            <base.VerticalLayout key={index} style={{
                ...styles.borderBottom,
                paddingLeft: 10,
                paddingRight: 10,
                backgroundColor: '#fff'
            }}>
                <base.HorizontalLayout style={{ alignItems: 'center' }}>

                    <base.VerticalLayout style={{ flex: 1, alignItems: 'flex-start' }}>
                        <Text style={{ fontSize: 14, marginTop: 10, marginBottom: 6 }}>{`车辆条码: ${d.bicycleId}`}</Text>
                        <Text style={{ fontSize: 14, marginBottom: 6 }}>{`车牌号: ${d.code}`}</Text>
                        {d.type === '1' ? <Text style={{ fontSize: 12, color: '#666', marginBottom: 6 }}>{`密码: ${d.pwd}`}</Text> : <Text style={{ fontSize: 12, color: '#666', marginBottom: 6 }}>{`智能锁SN码: ${d.pwd}\n挡泥板编码: ${d.fender}`}</Text>}

                    </base.VerticalLayout>

                    <base.VerticalLayout style={{ flex: 1, alignItems: 'flex-end', marginRight: 20 }}>
                        <Text style={{ fontSize: 14, color: '#666' }}>{d.date}</Text>
                    </base.VerticalLayout>

                    <TouchableHighlight onPress={e => {

                        const choice = Modal({
                            content: `确认删除：${d.code}？`,
                            buttons: [
                                { text: '取消', onPress: () => { } },
                                {
                                    text: '确认', onPress: () => {
                                        this.del(d.code);
                                    }
                                },
                            ]
                        });
                        if (platform.OS === 'web' && choice) {
                            this.del(d.code);
                        }
                    }}>
                        <View style={{ backgroundColor: '#fff' }}>
                            <base.Image src="deleteIcon" style={{ width: 20, height: 20 }} />
                        </View>
                    </TouchableHighlight>
                </base.HorizontalLayout>
            </base.VerticalLayout>);
    }

    renderLoading() {
        const { from } = this.state.historyState;
        return (
            <base.View style={styles.container}>
                <ActionBar title={from === 'mech' ? '机械锁—今日录入' : '智能锁—今日录入'} handleBack={() => this.handleBack()}>
                    {

                        !this.state.historyState
                            ? (<base.Image style={{ width: 17, height: 17 }} src='setting' />) :
                            <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    }
                </ActionBar>
                <base.View style={{ position: 'relative', flexGrow: 1 }}>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='loading' />
                        <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }

    valid(text) {
        return /^\d+$/.test(text);
    }
});

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    borderTop: {
        borderTopStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#ddd'
    },
    borderBottom: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    borderLeft: {
        borderLeftStyle: 'solid',
        borderLeftWidth: 1,
        borderLeftColor: '#ddd'
    },
    borderRight: {
        borderRightStyle: 'solid',
        borderRightWidth: 1,
        borderRightColor: '#ddd'
    }
};

function daysAgo(num) {
    let d = new Date();
    d.setTime(d.getTime() - num * 24 * 3600 * 1000);
    function b(n) {
        if (n < 10) {
            return `0${n}`;
        }
        return `${n}`;
    }

    return `${d.getFullYear()}-${b(d.getMonth() + 1)}-${b(d.getDate())}`;
}
