import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as base from '../../base';
import {
    Text,
    View,
    Br,
    Button,
    Tabs,
    TabPane,
    ScrollView,
    Toast,
    TouchableHighlight,
    ActivityIndicator
} from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import CameraView from '../../components/camera';
import Modal from '../../components/commonModal';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack, ResetTo } from '../../modules';
import { submitCode } from '../../runtime/action/supply';
import { submitSmart } from '../../runtime/action/supply';
import { checkRepeat } from '../../runtime/action/supply';

const barCodeContainer = pageWrapper(
    class extends Component {
        constructor(...args) {
            super(...args);
            this.state = {
                historyState: historyStack.get(),
                showCam: false
            };
            this.active = true;
        }

        viewWillAppear() {
            this.active = true;
            this.haveReadBarCode = false;

            this.setState({
                showCam: true
            });
        }

        viewWillDisappear() {
            this.active = false;

            this.setState({
                showCam: false
            });
        }

        handleBack() {
            this.setState({ showCam: false });
            JumpBack();
        }

        renderButtons() {
            const { from, carNo, unlock, fenderSn } = this.state.historyState;
            return [
                <View key={0}>
                    <TouchableHighlight
                        onPress={() =>
                            JumpTo('supply/barCodeInput', true, {
                                from,
                                carNo,
                                unlock,
                                fenderSn
                            })}
                    >
                        <base.VerticalLayout style={{ alignItems: 'center' }}>
                            <base.Image src="hand" style={{ width: 64, height: 64 }} />
                            <Text
                                style={{
                                    marginTop: 10,
                                    fontSize: 14,
                                    backgroundColor: 'rgba(0,0,0,0)',
                                    color: '#fff'
                                }}
                            >
                                手动输入
              </Text>
                        </base.VerticalLayout>
                    </TouchableHighlight>
                </View>
            ];
        }

        render() {
            return (
                <View style={styles.container}>
                    <ActionBar
                        title="车辆条码录入"
                        handleBack={() => this.handleBack()}
                        rightItem="跳过"
                        rightAction={() => this.skip()}
                    >
                        <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    </ActionBar>

                    {this.state.showCam
                        ? <CameraView
                            onBarCodeRead={d => this.onBarCodeRead(d)}
                            hint={'扫描获取车辆条码'}
                            buttons={this.renderButtons()}
                            torch={true}
                        />
                        : null}
                </View>
            );
        }

        async skip(d) {
            const { from, carNo, unlock, fenderSn } = this.state.historyState;
            //   { latitude, longitude, accuracy } = this.props.locationInfo;
            let res = null;
            from === 'mech'
                ? (res = await submitCode({
                    no: carNo,
                    code: unlock,
                    // lat: latitude,
                    // lng: longitude,
                    // accuracy,
                    barCode: d || ''
                }))
                : (res = await submitSmart({
                    bicycle_no: carNo,
                    unlock_code: unlock,
                    fender_sn: fenderSn,
                    // lat: latitude,
                    // lng: longitude,
                    // accuracy,
                    barCode: d || ''
                }));

            this.uploadSuccess(res);
            this.uploadFail(res);
        }

        uploadSuccess(d) {
            if (d.code !== 0) return;
            Toast.success('录入成功', 2);

            /**
               * useless code modification.
               */
            this.setState({
                showCam: false
            });
            const j = url => {
                if (this.props.navigator && this.props.navigator.getCurrentRoutes) {
                    const navigator = this.props.navigator,
                        route = navigator.getCurrentRoutes().filter(d => d.url === url)[0];
                    if (route) {
                        navigator.popToRoute(route);
                        return true;
                    }
                    return false;
                } else {
                    ResetTo(url, true, {});
                    return true;
                }
            };

            setTimeout(() => {
                j('chainIndex');
            }, 500);
        }

        uploadFail(d) {
            if (d.code === 0) return;
            Toast.fail(d.message, 2, () => this.haveReadBarCode = false);
        }

        async onBarCodeRead(d) {
            if (this.haveReadBarCode || !this.active) return;
            this.haveReadBarCode = true;
            d = d.trim();

            Modal({
                content: `车辆条码：\n${d}`,
                buttons: [
                    {
                        text: '重新扫描',
                        onPress: () => {
                            this.haveReadBarCode = false;
                        }
                    },
                    {
                        text: '确定',
                        onPress: () => {
                            this.skip(d);
                        }
                    }
                ]
            });
        }
    }
);

const mapStateToProps = (state, ownProps) => {
    return {
        // locationInfo: state.api.locationInfo
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(barCodeContainer);
