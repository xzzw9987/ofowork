import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as base from '../../base';
import {
    Text,
    View,
    Br,
    Button,
    Tabs,
    TabPane,
    ScrollView,
    Toast,
    TouchableHighlight,
    ActivityIndicator
} from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import CameraView from '../../components/camera';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack, ResetTo } from '../../modules';
import { submitCode } from '../../runtime/action/supply';
import { submitSmart } from '../../runtime/action/supply';
import Keyboard from '../../modules/keyboard';

const ChainSubContainer = pageWrapper(
    class extends Component {
        constructor(...args) {
            super(...args);
            this.state = {
                historyState: historyStack.get(),
                code: (historyStack.get().code || '').trim(),
                barCode: ''
            };
            this.active = true;
        }

        viewWillAppear() {
            this.active = true;
        }

        viewWillDisappear() {
            this.active = false;
        }

        handleBack() {
            JumpBack();
        }

        render() {
            return (
                <base.VerticalLayout
                    style={styles.container}
                    onStartShouldSetResponder={() => true}
                    onResponderGrant={() => Keyboard.dismiss()}
                >
                    <ActionBar
                        title={'车辆条码录入'}
                        handleBack={() => this.handleBack()}
                        rightItem="跳过"
                        rightAction={() => this.skip(1)}
                    >
                        <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    </ActionBar>

                    <base.VerticalLayout style={{ flexGrow: 1 }}>

                        <base.HorizontalLayout
                            style={{
                                ...styles.borderBottom,
                                paddingLeft: 20,
                                paddingRight: 20,
                                alignItems: 'center',
                                backgroundColor: '#fff'
                            }}
                        >
                            <Text style={{ fontSize: 18, marginRight: 10 }}>车辆条码</Text>
                            <base.TextInput
                                autoFocus={true}
                                onChangeText={text => {
                                    this.setState({ barCode: text });
                                }}
                                placeholder="请输入车辆条码"
                                style={{ fontSize: 14, flexGrow: 1, height: 64 }}
                                ref={c => this._textInput = c}
                                value={this.state.barCode}
                            />
                        </base.HorizontalLayout>

                        <base.VerticalLayout style={{ height: 84 }} />

                        <base.VerticalLayout>
                            <TouchableHighlight
                                onPress={() => this.onSubmit()}
                                style={{
                                    backgroundColor: '#2c2c2c',
                                    marginLeft: 20,
                                    marginRight: 20
                                }}
                            >
                                <base.HorizontalLayout
                                    style={{
                                        height: 44,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}
                                >
                                    <Text style={{ color: '#fff' }}>
                                        确认录入
                  </Text>
                                </base.HorizontalLayout>
                            </TouchableHighlight>
                        </base.VerticalLayout>
                    </base.VerticalLayout>
                </base.VerticalLayout>
            );
        }

        onSubmit() {
            //   Keyboard.dismiss();
            this.skip();
        }

        async skip(d) {
            if (!d && !this.state.barCode) {
                Toast.fail('请输入正确条码', 1, () => {
                    this._textInput.getFocus();
                });
                return false;
            }
            const { from, carNo, unlock, fenderSn } = this.state.historyState;
            // { latitude, longitude, accuracy } = this.props.locationInfo;
            let res = null;
            from === 'mech'
                ? (res = await submitCode({
                    no: carNo,
                    code: unlock,
                    //   lat: latitude,
                    //   lng: longitude,
                    //   accuracy,
                    barCode: d ? '' : this.state.barCode
                }))
                : (res = await submitSmart({
                    bicycle_no: carNo,
                    unlock_code: unlock,
                    fender_sn: fenderSn,
                    //   lat: latitude,
                    //   lng: longitude,
                    //   accuracy,
                    barCode: d ? '' : this.state.barCode
                }));

            this.uploadSuccess(res);
            this.uploadFail(res);
        }

        uploadSuccess(d) {
            if (d.code !== 0) return;
            Toast.success('录入成功', 2);

            /**
               * useless code modification.
               */
            this.setState({
                showCam: false
            });
            const j = url => {
                if (this.props.navigator && this.props.navigator.getCurrentRoutes) {
                    const navigator = this.props.navigator,
                        route = navigator.getCurrentRoutes().filter(d => d.url === url)[0];
                    if (route) {
                        navigator.popToRoute(route);
                        return true;
                    }
                    return false;
                } else {
                    ResetTo(url, true, {});
                    return true;
                }
            };

            setTimeout(() => {
                // j('chainIndex');
                if (!j('chainInputNew')) j('chainIndex');
            }, 500);
        }

        uploadFail(d) {
            if (d.code === 0) return;
            this.setState({ barCode: '' });
            Toast.fail(d.message, 2, () => {
                this._textInput.getFocus();
            });
        }
    }
);

const mapStateToProps = (state, ownProps) => {
    return {
        locationInfo: state.api.locationInfo
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    },
    borderTop: {
        borderTopStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#ddd'
    },
    borderBottom: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    borderLeft: {
        borderLeftStyle: 'solid',
        borderLeftWidth: 1,
        borderLeftColor: '#ddd'
    },
    borderRight: {
        borderRightStyle: 'solid',
        borderRightWidth: 1,
        borderRightColor: '#ddd'
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ChainSubContainer);
