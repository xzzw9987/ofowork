import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as base from '../../base';
import {
    Text,
    View,
    Br,
    Button,
    Tabs,
    TabPane,
    ScrollView,
    Toast,
    TouchableHighlight,
    ActivityIndicator
} from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import CameraView from '../../components/camera';
import Modal from '../../components/commonModal';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack, ResetTo } from '../../modules';
import { submitSmart } from '../../runtime/action/supply';
import { checkRepeat } from '../../runtime/action/supply';

const platePrefix = 'http://ofo.so/plate/';

const smartLock = pageWrapper(
    class extends Component {
        constructor(...args) {
            super(...args);
            this.state = {
                historyState: historyStack.get(),
                showCam: false,
                sn: ' ',
                title: 'SN码扫描',
                hint: '扫描获取SN码'
            };
            this.active = true;
        }

        viewWillAppear() {
            this.active = true;
            this.haveReadBarCode = false;

            this.setState({
                showCam: true
            });
        }

        viewWillDisappear() {
            this.active = false;

            this.setState({
                showCam: false
            });
        }

        handleBack() {
            JumpBack();
        }

        render() {
            return (
                <View style={styles.container}>
                    <ActionBar
                        title={this.state.title}
                        handleBack={() => this.handleBack()}
                    >
                        {!this.state.historyState
                            ? <base.Image style={{ width: 17, height: 17 }} src="setting" />
                            : <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>}
                    </ActionBar>

                    {this.state.showCam
                        ? <CameraView
                            onBarCodeRead={d => this.onBarCodeRead(d)}
                            hint={this.state.hint}
                            torch={true}
                        />
                        : null}
                </View>
            );
        }

        async onBarCodeRead(d) {
            if (this.haveReadBarCode || !this.active) return;
            this.haveReadBarCode = true;
            d = d.trim();

            if (!this.validSN(d)) {
                Toast.fail('仅支持扫描智能锁上的二维码', 2, () => this.haveReadBarCode = false);
                return;
            }

            const status = await checkRepeat({ type: 'lock_sn', x_no: d });
            if (status.code === 0) {
                Modal({
                    content: `SN码：\n${d}`,
                    buttons: [
                        {
                            text: '重新扫描',
                            onPress: () => {
                                this.haveReadBarCode = false;
                            }
                        },
                        {
                            text: '确定',
                            onPress: () => {
                                this.setState({ showCam: false });
                                JumpTo('supply/barCode', true, {
                                    carNo: this.state.historyState.bicycle_no,
                                    unlock: d,
                                    fenderSn: 'unnecessary',
                                    from: 'smart'
                                });
                            }
                        }
                    ]
                });
            } else {
                Toast.fail(status.message, 2, () => this.haveReadBarCode = false);
            }
        }

        validSN(text) {
            return /[\d][A-Z]{2}\d+/g.test(text);
        }
    }
);

const mapStateToProps = (state, ownProps) => {
    return {
        // locationInfo: state.api.locationInfo
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(smartLock);
