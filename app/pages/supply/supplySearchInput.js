import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Text,
  View,
  Br,
  Button,
  Tabs,
  TabPane,
  ScrollView,
  Toast,
  TouchableHighlight,
  ActivityIndicator,
  TextInput,
  HorizontalLayout
} from '../../base';
import Modal from '../../components/commonModal';
import pageWrapper from '../../modules/pageWrapper';
import CameraView from '../../components/camera';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack, ResetTo } from '../../modules';
import platform from '../../modules/platform';
import * as actions from '../../runtime/action';
import Keyboard from '../../modules/keyboard';

const SupplySnInput = pageWrapper(
  class extends Component {
    constructor(props) {
      super(props);
      this.state = {
        loading: false
      };
    }

    handleBack() {
      JumpBack();
    }

    renderLoading() {
      return (
        <View style={styles.container}>
          <ActionBar title="查询解锁码">
            <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
          </ActionBar>
          <View style={{ position: 'relative', flexGrow: 1 }}>
            <View style={styles.inner}>
              <ActivityIndicator size="large" src="loading" />
              <HorizontalLayout style={{ marginTop: 10 }}>
                <Text>正在加载 ...</Text>
              </HorizontalLayout>
            </View>
          </View>
        </View>
      );
    }

    renderPanel() {
      return (
        <View style={styles.container}>
          <ActionBar title={'查询解锁码'} handleBack={() => this.handleBack()}>
            <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
          </ActionBar>

          <View style={{ flexGrow: 1, flexDirection: 'column' }}>
            <HorizontalLayout
              style={{
                ...styles.borderTop,
                ...styles.borderBottom,
                paddingLeft: 20,
                paddingRight: 20,
                marginTop: 10,
                alignItems: 'center',
                backgroundColor: '#fff'
              }}
            >
              <Text style={{ fontSize: 18, marginRight: 10, color: '#fd7000' }}>
                车牌号
              </Text>
              <TextInput
                autoFocus={true}
                onChangeText={text => {
                  this.setState({ carCode: text });
                }}
                placeholder="请输入车牌号"
                style={{
                  fontSize: 14,
                  flexGrow: 1,
                  height: 64,
                  color: '#fd7000'
                }}
                ref={c => this._textInput = c}
              />
            </HorizontalLayout>

            <View style={{ height: 150 }} />

            <View>
              <TouchableHighlight
                onPress={() => this.onSubmit()}
                style={{
                  backgroundColor: '#2c2c2c',
                  marginLeft: 20,
                  marginRight: 20,
                  flexGrow: 1
                }}
              >
                <HorizontalLayout
                  style={{
                    height: 44,
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  <Text style={{ color: '#fff' }}>
                    查询
                  </Text>
                </HorizontalLayout>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      );
    }

    render() {
      if (this.state.loading) return this.renderLoading();
      return this.renderPanel();
    }

    async onSubmit() {
      Keyboard.dismiss();

      const j = url => {
        if (this.props.navigator && this.props.navigator.getCurrentRoutes) {
          const navigator = this.props.navigator,
            route = navigator.getCurrentRoutes().filter(d => d.url === url)[0];
          if (route) {
            navigator.popToRoute(route);
            return true;
          }
          return false;
        } else {
          ResetTo(url, true, {});
          return true;
        }
      };

      this.setState({ loading: true });
      await this.props.dispatch(
        actions.fetchOperationCarSearch({
          bicycle_num: this.state.carCode
        })
      );

      const { searchCode, searchResult, searchMessage } = this.props;
      this.setState({ loading: false });
      if (searchCode !== 0) {
        Modal({
          title: parseInt(searchResult.lock_type) === 1 ? '机械锁车辆' : '智能锁车辆',
          content: `查询失败：\n${searchMessage}`,
          buttons: [
            {
              text: '确定',
              onPress: () => {
                this.setState({ carCode: '' });
                this._textInput.getFocus();
              }
            }
          ]
        });
      } else {
        if (
          this.props.activeInfo.is_active === false &&
          this.props.recordInfo.is_record === false
        ) {
          this.setState({ carCode: '' });

          Toast.fail('未在组装厂录入该车牌!', 1.5, () => {
            this._textInput.getFocus();
          });
          return;
        }
        Modal({
          title: parseInt(searchResult.lock_type) === 1 ? '机械锁车辆' : '智能锁车辆',
          content: `${searchResult.active_info.is_active ? '已激活' : '未激活'}\n解锁码：${searchResult.unlock_code}`,
          buttons: [
            {
              text: '确定',
              onPress: () => {
                this.props.clear();
                if (!j('supply/codeSearchNew')) j('supply/codeSearch');
              }
            }
          ]
        });
      }
    }
  }
);

const mapStateToProps = (state, ownProps) => {
  const operationCarSearch = state.api.operationCarSearch || {};
  return {
    searchResult: state.api.operationCarSearch,
    searchCode: state.api.operationCarCode,
    searchMessage: state.api.operationCarMessage,
    activeInfo: operationCarSearch.active_info, //激活信息
    recordInfo: operationCarSearch.record_info //组装信息
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch,
    clear: () => {
      dispatch({
        type: '@@clear',
        payload: {
          activeInfo: {},
          recordInfo: {},
          operationCarSearch: {},
          operationCarCode: '',
          operationCarMessage: ''
        }
      });
    }
  };
};

const styles = {
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    /*justifyContent: 'center',*/
    alignItems: 'stretch',
    backgroundColor: '#F6F6F6'
  },
  borderTop: {
    borderTopStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: '#ddd'
  },
  borderBottom: {
    borderBottomStyle: 'solid',
    borderBottomWidth: 1,
    borderBottomColor: '#ddd'
  },
  borderLeft: {
    borderLeftStyle: 'solid',
    borderLeftWidth: 1,
    borderLeftColor: '#ddd'
  },
  borderRight: {
    borderRightStyle: 'solid',
    borderRightWidth: 1,
    borderRightColor: '#ddd'
  },
  inner: {
    position: 'absolute',
    flexDirection: 'column',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(SupplySnInput);
