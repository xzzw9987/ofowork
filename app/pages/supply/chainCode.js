import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as base from '../../base';
import {
    Text,
    View,
    Br,
    Button,
    Tabs,
    TabPane,
    ScrollView,
    Toast,
    TouchableHighlight,
    ActivityIndicator
} from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import CameraView from '../../components/camera';
import Modal from '../../components/commonModal';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack, ResetTo } from '../../modules';
import { submitSmart } from '../../runtime/action/supply';
import { checkRepeat } from '../../runtime/action/supply';

const platePrefix = 'http://ofo.so/plate/';

const smartLock = pageWrapper(
    class extends Component {
        constructor(...args) {
            super(...args);
            this.state = {
                historyState: historyStack.get(),
                showCam: false,
                sn: ' ',
                title: 'SN码扫描',
                hint: '扫描获取SN码'
            };
            this.active = true;
        }

        viewWillAppear() {
            this.active = true;
            this.haveReadBarCode = false;

            const { oldSn } = this.state.historyState;
            if (oldSn) {
                this.setState({ sn: oldSn, title: '扫描挡泥板', hint: '扫描获取挡泥板编码' });
            }

            this.setState({
                showCam: true
            });
        }

        viewWillDisappear() {
            this.active = false;

            this.setState({
                showCam: false
            });
        }

        handleBack() {
            this.setState({ showCam: false });
            JumpBack();
        }

        render() {
            return (
                <View style={styles.container}>
                    <ActionBar
                        title={this.state.title}
                        handleBack={() => this.handleBack()}
                    >
                        {!this.state.historyState
                            ? <base.Image style={{ width: 17, height: 17 }} src="setting" />
                            : <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>}
                    </ActionBar>

                    {this.state.showCam
                        ? <CameraView
                            onBarCodeRead={d => this.onBarCodeRead(d)}
                            hint={this.state.hint}
                            torch={true}
                        />
                        : null}
                </View>
            );
        }

        async onBarCodeRead(d) {
            if (this.haveReadBarCode || !this.active) return;
            this.haveReadBarCode = true;
            d = d.trim();

            // const { latitude, longitude, accuracy } = this.props.locationInfo;

            if (d.startsWith(platePrefix)) {
                if (!this.state.sn) {
                    Toast.fail('请先扫描SN编码', 2, () => this.haveReadBarCode = false);
                    return;
                }
                const fenderSn = d.substring(platePrefix.length, d.length);

                if (!this.validBack(fenderSn)) {
                    Toast.fail('二维码格式不正确', 2, () => this.haveReadBarCode = false);
                    return;
                }

                Modal({
                    content: `挡泥板编码：\n${fenderSn}`,
                    buttons: [
                        {
                            text: '重新扫描',
                            onPress: () => {
                                this.haveReadBarCode = false;
                            }
                        },
                        {
                            text: '确定',
                            onPress: () => {
                                this.setState({ showCam: false });
                                JumpTo('supply/barCode', true, {
                                    carNo: this.state.historyState.bicycle_no,
                                    unlock: this.state.sn,
                                    fenderSn,
                                    from: 'smart'
                                });
                            }
                        }
                    ]
                });
            } else {
                if (!this.validSN(d)) {
                    Toast.fail('仅支持扫描智能锁上的二维码', 2, () => this.haveReadBarCode = false);
                    return;
                }

                const status = await checkRepeat({ type: 'lock_sn', x_no: d });
                if (status.code === 0) {
                    Modal({
                        content: `SN码：\n${d}`,
                        buttons: [
                            {
                                text: '重新扫描',
                                onPress: () => {
                                    this.haveReadBarCode = false;
                                }
                            },
                            {
                                text: '确定',
                                onPress: () => {
                                    Toast.success('扫描成功', 1, () => {
                                        this.setState({ sn: d, title: '扫描挡泥板', hint: '扫描获取挡泥板编码' });
                                        this.haveReadBarCode = false;
                                    });
                                }
                            }
                        ]
                    });
                } else {
                    Toast.fail(status.message, 2, () => this.haveReadBarCode = false);
                }
            }
        }

        validSN(text) {
            return /[\d][A-Z]{2}\d+/g.test(text);
        }

        validBack(text) {
            return /[\d][A-Z]\d+/g.test(text);
        }
    }
);

const mapStateToProps = (state, ownProps) => {
    return {
        // locationInfo: state.api.locationInfo
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(smartLock);
