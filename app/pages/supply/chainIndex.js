import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as base from '../../base';
import {
    Text,
    View,
    Br,
    Button,
    Tabs,
    TabPane,
    ScrollView,
    Toast,
    TouchableHighlight,
    ActivityIndicator,
} from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import CameraView from '../../components/camera';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { checkRepeat, fetchOldSn } from '../../runtime/action/supply';
import * as actions from '../../runtime/action';

const platePrefix = 'http://ofo.so/plate/';
const ChainContainer = pageWrapper(
    class extends Component {
        constructor(...args) {
            super(...args);
            this.state = {
                historyState: historyStack.get(),
                loading: true,
                showCam: false,
            };
            this.active = true;
        }

        async viewWillAppear() {
            this.active = true;
            this.haveReadBarCode = false;
            await this.props.dispatch(actions.getLocation());
            this.props.locationInfo.error
                ? this.setState({ loading: false })
                : this.setState({ loading: false, showCam: true });
            // this.setState({ loading: false, showCam: true });
        }

        viewWillDisappear() {
            this.active = false;

            this.setState({
                showCam: false,
            });
        }

        handleBack() {
            JumpBack();
        }

        renderLoading() {
            const { from } = this.state.historyState;
            return (
                <base.View style={styles.container}>
                    <ActionBar title={from === 'mech' ? '机械锁车牌录入' : '智能锁车牌录入'}>
                        <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    </ActionBar>
                    <base.View style={{ position: 'relative', flexGrow: 1 }}>
                        <base.View style={styles.inner}>
                            <ActivityIndicator size="large" src="loading" />
                            <base.HorizontalLayout style={{ marginTop: 10 }}>
                                <Text>正在加载 ...</Text>
                            </base.HorizontalLayout>
                        </base.View>
                    </base.View>
                </base.View>
            );
        }

        renderPanel() {
            const { from } = this.state.historyState;
            return (
                <View style={styles.container}>
                    <ActionBar
                        title={from === 'mech' ? '机械锁车牌录入' : '智能锁车牌录入'}
                        handleBack={() => this.handleBack()}
                        rightItem="记录"
                        rightAction={() => {
                            JumpTo('chainList', true, { from });
                        }}
                    >
                        <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    </ActionBar>

                    {this.state.showCam
                        ? <CameraView
                            onBarCodeRead={d => this.onBarCodeRead(d)}
                            hint={'扫描获取车牌'}
                            buttons={this.renderButtons()}
                            torch={true}
                        />
                        : null}
                </View>
            );
        }

        async onBarCodeRead(d) {
            const { from } = this.state.historyState;
            d = d.trim();
            if (this.haveReadBarCode || !this.active) return;
            this.haveReadBarCode = true;

            const carNo = d.substring(platePrefix.length, d.length);
            // this.setState({ loading: true });
            const res = await checkRepeat({ type: 'bicycle_no', x_no: carNo });
            // this.setState({ loading: false }, async () => {
            //     if (res.code === 0) {
            //         from === 'mech' && JumpTo('chainSubmit', true, { code: carNo });
            //         // this.setState({ loading: true });
            //         const oldSn = await fetchOldSn({ carNo });
            //         this.setState({ loading: false, showCam: false });
            //         if (oldSn.code === 0) {
            //             from === 'smart' && JumpTo('chainCode', true, { bicycle_no: carNo, oldSn: oldSn.data.lock_sn });
            //             from === 'upgrade' && JumpTo('supply/barCode', true, {
            //                 carNo,
            //                 unlock: oldSn.data.lock_sn,
            //                 fenderSn: 'unnecessary',
            //                 from: 'smart'
            //             });
            //         } else {
            //             from === 'smart' && JumpTo('chainCode', true, { bicycle_no: carNo });
            //             from === 'upgrade' && JumpTo('chainCodeUpgrade', true, { bicycle_no: carNo });
            //         }
            //     } else {
            //         Toast.fail(res.message, 2, () => this.haveReadBarCode = false);
            //     }
            // });
            if (res.code === 0) {
                from === 'mech' && JumpTo('chainSubmit', true, { code: carNo });
                // this.setState({ loading: true });
                const oldSn = await fetchOldSn({ carNo });
                this.setState({ showCam: false }, () => {
                    if (oldSn.code === 0) {
                        from === 'smart' && JumpTo('chainCode', true, { bicycle_no: carNo, oldSn: oldSn.data.lock_sn });
                        from === 'upgrade' && JumpTo('supply/barCode', true, {
                            carNo,
                            unlock: oldSn.data.lock_sn,
                            fenderSn: 'unnecessary',
                            from: 'smart'
                        });
                    } else {
                        from === 'smart' && JumpTo('chainCode', true, { bicycle_no: carNo });
                        from === 'upgrade' && JumpTo('chainCodeUpgrade', true, { bicycle_no: carNo });
                    }
                });

            } else {
                Toast.fail(res.message, 2, () => this.haveReadBarCode = false);
            }
        }

        valid(text) {
            return (
                text.startsWith(platePrefix) &&
                /^\d{6,11}$/.test(text.substring(platePrefix.length, text.length))
            );
        }

        renderButtons() {
            const { from } = this.state.historyState;
            return [
                <View key={0}>
                    <TouchableHighlight
                        onPress={() => JumpTo('chainInput', true, { from })}
                    >
                        <base.VerticalLayout style={{ alignItems: 'center' }}>
                            <base.Image src="hand" style={{ width: 64, height: 64 }} />
                            <Text
                                style={{
                                    marginTop: 10,
                                    fontSize: 14,
                                    backgroundColor: 'rgba(0,0,0,0)',
                                    color: '#fff',
                                }}
                            >
                                手动输入
                            </Text>
                        </base.VerticalLayout>
                    </TouchableHighlight>
                </View>,
            ];
        }

        render() {
            if (this.state.loading) return this.renderLoading();
            return this.renderPanel();
        }
    }
);

const mapStateToProps = (state, ownProps) => {
    return {
        locationInfo: state.api.locationInfo,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch,
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    inner: {
        position: 'absolute',
        flexDirection: 'column',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
};

export default connect(mapStateToProps, mapDispatchToProps)(ChainContainer);
