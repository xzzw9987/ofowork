import React, { Component } from 'react';
import * as base from '../../base';
import { connect } from 'react-redux';
import {
    Text,
    View,
    Br,
    Button,
    Tabs,
    TabPane,
    ScrollView,
    Toast,
    TouchableHighlight,
    ActivityIndicator
} from '../../base';
import Modal from '../../components/commonModal';
import pageWrapper from '../../modules/pageWrapper';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack, ResetTo } from '../../modules';
import * as actions from '../../runtime/action';
import Keyboard from '../../modules/keyboard';

const ActiveInput = pageWrapper(
    class extends Component {
        constructor(...args) {
            super(...args);
            this.state = {
                historyState: historyStack.get(),
                loading: false
            };
            this.active = true;
        }

        viewWillAppear() {
            this.active = true;
            this._textInput.getFocus();
        }

        viewWillDisappear() {
            this.active = false;
        }

        handleBack() {
            JumpBack();
        }

        renderLoading() {
            return (
                <base.View style={styles.container}>
                    <ActionBar
                        title="新车激活"
                        rightAction={() => this.props.fetch(this.props)}
                    >
                        <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    </ActionBar>
                    <base.View style={{ position: 'relative', flexGrow: 1 }}>
                        <base.View style={styles.inner}>
                            <ActivityIndicator size="large" src="loading" />
                            <base.HorizontalLayout style={{ marginTop: 10 }}>
                                <Text>正在加载 ...</Text>
                            </base.HorizontalLayout>
                        </base.View>
                    </base.View>
                </base.View>
            );
        }

        renderPanel() {
            return (
                <View style={styles.container}>
                    <ActionBar title={'新车激活'} handleBack={() => this.handleBack()}>
                        <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                    </ActionBar>

                    <View style={{ flexGrow: 1, flexDirection: 'column' }}>
                        <base.HorizontalLayout
                            style={{
                                ...styles.borderTop,
                                ...styles.borderBottom,
                                paddingLeft: 20,
                                paddingRight: 20,
                                marginTop: 10,
                                alignItems: 'center',
                                backgroundColor: '#fff'
                            }}
                        >
                            <Text style={{ fontSize: 18, marginRight: 10 }}>车牌号</Text>
                            <base.TextInput
                                autoFocus={true}
                                onChangeText={text => {
                                    this.setState({ carno: text });
                                }}
                                placeholder="请输入车牌号"
                                style={{ fontSize: 14, flexGrow: 1, height: 64 }}
                                ref={c => this._textInput = c}
                            />
                        </base.HorizontalLayout>

                        <View style={{ height: 150 }} />

                        <View>
                            <TouchableHighlight
                                onPress={() => this.onSubmit()}
                                style={{
                                    backgroundColor: '#2c2c2c',
                                    marginLeft: 20,
                                    marginRight: 20,
                                    flexGrow: 1
                                }}
                            >
                                <base.HorizontalLayout
                                    style={{
                                        height: 44,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}
                                >
                                    <Text style={{ color: '#fff' }}>
                                        激活
                  </Text>
                                </base.HorizontalLayout>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>
            );
        }

        render() {
            if (this.state.loading) return this.renderLoading();
            return this.renderPanel();
        }

        async onSubmit() {
            Keyboard.dismiss();
            // if (!this.valid(this.state.carno)) {
            //     Toast.fail('请输入正确的车牌号码', 2);
            //     return;
            // }
            this.setState({ loading: true });
            const { zone_id, zone_name, zone_level } = this.props.zoneInfo;
            const { carno } = this.state;

            const res = await this.props.fetch(
                Object.assign({}, this.state, this.props)
            );
            this.setState({ loading: false });
            if (res.code !== 0) {
                this.setState({ carno: '' });
                Toast.fail(res.message, 2, () => {
                    this._textInput.getFocus();
                });
            } else {
                Modal({
                    title: res.data.lock_type === 1 ? '机械锁车辆' : '智能锁车辆',
                    content: res.data.lock_type === 1
                        ? `车牌号：${carno}\n解锁码：${this.props.carSearchInfo.unlock_code}`
                        : `车牌号：${carno}\n智能锁SN码：${this.props.carSearchInfo.unlock_code}`,
                    buttons: [
                        {
                            text: '错误',
                            onPress: () => {
                                res.data.lock_type === 1
                                    ? JumpTo('codeReset', true, {
                                        zone_id,
                                        zone_name,
                                        zone_level,
                                        carno
                                    })
                                    : Toast.fail('请联系当地质检或采购', 1);
                            }
                        },
                        {
                            text: '正确',
                            onPress: async () => {
                                this.setState({ loading: true });
                                const d = await this.props.dispatch(
                                    res.data.lock_type === 1
                                        ? actions.activeNewCar({
                                            ...{
                                                carno,
                                                zone_id,
                                                zone_level
                                            },
                                            newCode: ''
                                        })
                                        : actions.activeSmartCar({
                                            ...{
                                                carno,
                                                zone_id,
                                                zone_level
                                            },
                                            newCode: ''
                                        })
                                );
                                this.setState({ loading: false });
                                d.code === 0
                                    ? Toast.success('激活成功', 1, () => {
                                        this.setState({ carno: '' });
                                        this._textInput.getFocus();
                                    })
                                    : Toast.fail(d.message, 1);
                            }
                        }
                    ]
                });
            }
        }

        valid(text) {
            return /^\d+$/.test(text);
        }
    }
);

const mapStateToProps = (state, ownProps) => {
    return {
        carSearchInfo: state.api.unlockCode,
        zoneInfo: {
            ...state.api.zoneInfo,
            zone_id: state.api.zone_ids[0],
            zone_name: state.api.zone_names[0],
            zone_level: state.api.userInfo.repairer_level ||
            state.api.userInfo.operation_level ||
            ''
        }
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch,
        fetch: props => dispatch(actions.fetchUnlockCode({ ...props }))
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    },
    borderTop: {
        borderTopStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#ddd'
    },
    borderBottom: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    borderLeft: {
        borderLeftStyle: 'solid',
        borderLeftWidth: 1,
        borderLeftColor: '#ddd'
    },
    borderRight: {
        borderRightStyle: 'solid',
        borderRightWidth: 1,
        borderRightColor: '#ddd'
    },
    inner: {
        position: 'absolute',
        flexDirection: 'column',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ActiveInput);
