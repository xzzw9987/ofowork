import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as base from '../../base';
import {
    Text,
    View,
    Br,
    Button,
    Tabs,
    TabPane,
    ScrollView,
    Toast,
    TouchableHighlight,
    ActivityIndicator
} from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import CameraView from '../../components/camera';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { checkRepeat } from '../../runtime/action/supply';
import Keyboard from '../../modules/keyboard';

const ChainInputContainer = pageWrapper(
    class extends Component {
        constructor(...args) {
            super(...args);
            this.state = {
                historyState: historyStack.get(),
                code: ''
            };
            this.active = true;
        }

        viewWillAppear() {
            this.active = true;
            this.setState({ code: '' });
            this._textInput.getFocus();
        }

        viewWillDisappear() {
            this.active = false;
        }

        handleBack() {
            JumpBack();
        }

        render() {
            return (
                <base.VerticalLayout style={styles.container}>
                    <ActionBar title="SN码录入" handleBack={() => this.handleBack()}>
                        {!this.state.historyState
                            ? <base.Image style={{ width: 17, height: 17 }} src="setting" />
                            : <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>}
                    </ActionBar>

                    <base.VerticalLayout style={{ flexGrow: 1 }}>
                        <base.HorizontalLayout
                            style={{
                                ...styles.borderTop,
                                ...styles.borderBottom,
                                paddingLeft: 20,
                                paddingRight: 20,
                                marginTop: 10,
                                alignItems: 'center',
                                backgroundColor: '#fff'
                            }}
                        >
                            <Text style={{ fontSize: 18, marginRight: 10 }}>SN</Text>
                            <base.TextInput
                                value={this.state.code}
                                autoFocus={true}
                                onChangeText={text => {
                                    this.setState({ code: text });
                                }}
                                placeholder="请输入SN码"
                                style={{ fontSize: 14, flexGrow: 1, height: 64 }}
                                ref={c => this._textInput = c}
                            />
                        </base.HorizontalLayout>

                        <base.VerticalLayout style={{ height: 150 }} />

                        <base.VerticalLayout>
                            <TouchableHighlight
                                onPress={() => this.onSubmit()}
                                style={{
                                    backgroundColor: '#2c2c2c',
                                    marginLeft: 20,
                                    marginRight: 20
                                }}
                            >
                                <base.HorizontalLayout
                                    style={{
                                        height: 44,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}
                                >
                                    <Text style={{ color: '#fff' }}>
                                        下一步
                  </Text>
                                </base.HorizontalLayout>
                            </TouchableHighlight>
                        </base.VerticalLayout>
                    </base.VerticalLayout>
                </base.VerticalLayout>
            );
        }

        async onSubmit() {
            const { carNo, from } = this.state.historyState;
            Keyboard.dismiss();
            const res = await checkRepeat({
                type: 'lock_sn',
                x_no: this.state.code
            });
            if (res.code === 0) {
                from !== 'upgrade' &&
                    JumpTo('supply/fenderCodeInput', true, {
                        carNo,
                        unlock: this.state.code,
                        from: 'smart'
                    });

                JumpTo('supply/barCodeInput', true, {
                    carNo,
                    unlock: this.state.code,
                    fenderSn: 'unnecessary',
                    from: 'upgrade'
                });


            } else {
                this.setState({ code: '' });
                Toast.fail(res.message, 2, () => {
                    this._textInput.getFocus();
                });
            }
        }
    }
);

const mapStateToProps = (state, ownProps) => {
    return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    },
    borderTop: {
        borderTopStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#ddd'
    },
    borderBottom: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    borderLeft: {
        borderLeftStyle: 'solid',
        borderLeftWidth: 1,
        borderLeftColor: '#ddd'
    },
    borderRight: {
        borderRightStyle: 'solid',
        borderRightWidth: 1,
        borderRightColor: '#ddd'
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(
    ChainInputContainer
);
