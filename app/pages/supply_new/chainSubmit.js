import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as base from '../../base';
import {
    Text,
    View,
    Br,
    Button,
    Tabs,
    TabPane,
    ScrollView,
    Toast,
    TouchableHighlight,
    ActivityIndicator
} from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import CameraView from '../../components/camera';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack, ResetTo } from '../../modules';
import { submitCode } from '../../runtime/action/supply';
import Keyboard from '../../modules/keyboard';

const ChainSubContainer = pageWrapper(
    class extends Component {
        constructor(...args) {
            super(...args);
            this.state = {
                historyState: historyStack.get(),
                code: (historyStack.get().code || '').trim(),
                pwd: ''
            };
            this.active = true;
        }

        viewWillAppear() {
            this.active = true;
            this._textInput.getFocus();
        }

        viewWillDisappear() {
            this.active = false;
        }

        handleBack() {
            JumpBack();
        }

        render() {
            return (
                <base.VerticalLayout style={styles.container}>
                    <ActionBar title={'解锁码录入'} handleBack={() => this.handleBack()}>
                        {!this.state.historyState
                            ? <base.Image style={{ width: 17, height: 17 }} src="setting" />
                            : <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>}
                    </ActionBar>

                    <base.VerticalLayout style={{ flexGrow: 1 }}>
                        <base.HorizontalLayout
                            style={{
                                ...styles.borderTop,
                                ...styles.borderBottom,
                                paddingLeft: 20,
                                paddingRight: 20,
                                marginTop: 10,
                                alignItems: 'center',
                                backgroundColor: '#fff'
                            }}
                        >
                            <Text style={{ fontSize: 18, marginRight: 10 }}>车牌号</Text>
                            <base.HorizontalLayout
                                style={{
                                    justifyContent: 'flex-start',
                                    alignItems: 'center',
                                    flexGrow: 1,
                                    height: 64
                                }}
                            >
                                <Text style={{ fontSize: 14 }}>{this.state.code}</Text>
                            </base.HorizontalLayout>
                        </base.HorizontalLayout>

                        <base.HorizontalLayout
                            style={{
                                ...styles.borderBottom,
                                paddingLeft: 20,
                                paddingRight: 20,
                                alignItems: 'center',
                                backgroundColor: '#fff'
                            }}
                        >
                            <Text style={{ fontSize: 18, marginRight: 10 }}>解锁码</Text>
                            <base.TextInput
                                autoFocus={true}
                                keyboardType="numeric"
                                onChangeText={text => {
                                    this.setState({ pwd: text });
                                }}
                                placeholder="请输入解锁密码"
                                style={{ fontSize: 14, flexGrow: 1, height: 64 }}
                                ref={c => this._textInput = c}
                                value={this.state.pwd}
                            />
                        </base.HorizontalLayout>

                        <base.VerticalLayout style={{ height: 84 }} />

                        <base.VerticalLayout>
                            <TouchableHighlight
                                onPress={() => this.onSubmit()}
                                style={{
                                    backgroundColor: '#2c2c2c',
                                    marginLeft: 20,
                                    marginRight: 20
                                }}
                            >
                                <base.HorizontalLayout
                                    style={{
                                        height: 44,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}
                                >
                                    <Text style={{ color: '#fff' }}>
                                        下一步
                  </Text>
                                </base.HorizontalLayout>
                            </TouchableHighlight>
                        </base.VerticalLayout>
                    </base.VerticalLayout>
                </base.VerticalLayout>
            );
        }

        onSubmit() {
            Keyboard.dismiss();
            if (!this.validCode(this.state.pwd)) {
                this.setState({ pwd: '' });
                Toast.fail('请输入4位解锁码', 2, () => {
                    this._textInput.getFocus();
                });
                return;
            }
            //   if (!this.validCar(this.state.code)) {
            //       Toast.fail('请输入正确的车牌号', 2);
            //       return;
            //   }
            JumpTo('supply/barCodeInput', true, {
                carNo: this.state.code,
                unlock: this.state.pwd,
                from: 'mech'
            });
        }

        validCar(code) {
            return /^\d{6,11}$/.test(code);
        }
        validCode(pwd) {
            return /^\d{4}$/.test(pwd);
        }
    }
);

const mapStateToProps = (state, ownProps) => {
    return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6'
    },
    borderTop: {
        borderTopStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#ddd'
    },
    borderBottom: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    borderLeft: {
        borderLeftStyle: 'solid',
        borderLeftWidth: 1,
        borderLeftColor: '#ddd'
    },
    borderRight: {
        borderRightStyle: 'solid',
        borderRightWidth: 1,
        borderRightColor: '#ddd'
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ChainSubContainer);
