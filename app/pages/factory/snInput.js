import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Text,
  View,
  Br,
  Button,
  Tabs,
  TabPane,
  ScrollView,
  Toast,
  TouchableHighlight,
  ActivityIndicator,
  TextInput,
  HorizontalLayout
} from '../../base';
import Modal from '../../components/commonModal';
import pageWrapper from '../../modules/pageWrapper';
import CameraView from '../../components/camera';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack, ResetTo } from '../../modules';
import platform from '../../modules/platform';
import * as actions from '../../runtime/action';
import Keyboard from '../../modules/keyboard';

const FacSnInput = pageWrapper(
  class extends Component {
    constructor(props) {
      super(props);
      this.state = {
        loading: false
      };
    }

    handleBack() {
      JumpBack();
    }

    renderLoading() {
      return (
        <View style={styles.container}>
          <ActionBar title="查询解锁码">
            <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
          </ActionBar>
          <View style={{ position: 'relative', flexGrow: 1 }}>
            <View style={styles.inner}>
              <ActivityIndicator size="large" src="loading" />
              <HorizontalLayout style={{ marginTop: 10 }}>
                <Text>正在加载 ...</Text>
              </HorizontalLayout>
            </View>
          </View>
        </View>
      );
    }

    renderPanel() {
      return (
        <View style={styles.container}>
          <ActionBar title={'查询解锁码'} handleBack={() => this.handleBack()}>
            <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
          </ActionBar>

          <View style={{ flexGrow: 1, flexDirection: 'column' }}>
            <HorizontalLayout
              style={{
                ...styles.borderTop,
                ...styles.borderBottom,
                paddingLeft: 20,
                paddingRight: 20,
                marginTop: 10,
                alignItems: 'center',
                backgroundColor: '#fff'
              }}
            >
              <Text style={{ fontSize: 18, marginRight: 10, color: '#fd7000' }}>
                锁SN码
              </Text>
              <TextInput
                autoFocus={true}
                onChangeText={text => {
                  this.setState({ snCode: text });
                }}
                placeholder="请输入12到14位锁SN码"
                style={{
                  fontSize: 14,
                  flexGrow: 1,
                  height: 64,
                  color: '#fd7000'
                }}
                ref={c => this._textInput = c}
              />
            </HorizontalLayout>

            <View style={{ height: 150 }} />

            <View>
              <TouchableHighlight
                onPress={() => this.onSubmit()}
                style={{
                  backgroundColor: '#2c2c2c',
                  marginLeft: 20,
                  marginRight: 20,
                  flexGrow: 1
                }}
              >
                <HorizontalLayout
                  style={{
                    height: 44,
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  <Text style={{ color: '#fff' }}>
                    查询
                  </Text>
                </HorizontalLayout>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      );
    }

    render() {
      if (this.state.loading) return this.renderLoading();
      return this.renderPanel();
    }

    async onSubmit() {
      Keyboard.dismiss();

      const j = url => {
        if (this.props.navigator && this.props.navigator.getCurrentRoutes) {
          const navigator = this.props.navigator,
            route = navigator.getCurrentRoutes().filter(d => d.url === url)[0];
          if (route) {
            navigator.popToRoute(route);
            return true;
          }
          return false;
        } else {
          ResetTo(url, true, {});
          return true;
        }
      };

      // if (!this.valid(this.state.snCode)) {
      //     Toast.fail('请输入正确的SN码', 2);
      //     return;
      // }
      this.setState({ loading: true });
      const { snCode } = this.state;

      const res = await this.props.dispatch(
        actions.factoryCodeSearch({
          snCode
        })
      );
      this.setState({ loading: false });
      if (res.code !== 0) {
        this._textInput.getFocus();
        this.setState({ snCode: '' });
        Toast.fail(res.message, 1);
      } else {
        Modal({
          content: `解锁码：${res.data.unlock_code}`,
          buttons: [
            {
              text: '确定',
              onPress: () => {
                setTimeout(() => {
                  if (!j('factory/codeSearchNew')) j('factory/codeSearch');
                }, 500);
              }
            }
          ]
        });
      }
    }

    // valid(text) {
    //     return /[\d][A-Z]{2}\d+/g.test(text);
    // }
  }
);

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch
  };
};

const styles = {
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    /*justifyContent: 'center',*/
    alignItems: 'stretch',
    backgroundColor: '#F6F6F6'
  },
  borderTop: {
    borderTopStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: '#ddd'
  },
  borderBottom: {
    borderBottomStyle: 'solid',
    borderBottomWidth: 1,
    borderBottomColor: '#ddd'
  },
  borderLeft: {
    borderLeftStyle: 'solid',
    borderLeftWidth: 1,
    borderLeftColor: '#ddd'
  },
  borderRight: {
    borderRightStyle: 'solid',
    borderRightWidth: 1,
    borderRightColor: '#ddd'
  },
  inner: {
    position: 'absolute',
    flexDirection: 'column',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(FacSnInput);
