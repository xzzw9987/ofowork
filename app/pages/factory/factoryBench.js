import React, { Component } from 'react';
import { Text, View, Br, Button, ListView, List, Toast, ScrollView, TouchableHighlight, ActivityIndicator, HorizontalLayout, VerticalLayout, Image } from '../../base';
import { ActionBar } from '../../components/actionbar';
import WorkBench from '../../components/workBench';

export default (props) => (
    <View style={styles.container}>
        <ActionBar title="工作台"></ActionBar>
        <VerticalLayout style={styles.jump}>
            <WorkBench buttonNum={3} showTitle={true} isVertical={true} {...props} />
        </VerticalLayout>
    </View>
);

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff',
    },
    jump: {
        borderTopStyle: 'solid',
        borderTopColor: '#d8d8d8',
        borderTopWidth: 1,
        position: 'relative',
        flexGrow: 1,
    },
    bannerContainer: {
        alignItems: 'center'
    },
    banner: {
        flexGrow: 1,
        flexBasis: 0,
    }
};
