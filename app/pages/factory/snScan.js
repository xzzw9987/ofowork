import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as base from '../../base';
import { Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator, VerticalLayout, Image } from '../../base';
import Modal from '../../components/commonModal';
import CameraView from '../../components/camera';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import * as actions from '../../runtime/action';
import pageWrapper from '../../modules/pageWrapper';
import Loading from '../../modules/loading';

const SnSearchPanel = pageWrapper(class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            showCam: false,
        };
        this.active = true;
    }

    viewWillAppear() {
        this.active = true;
        this.haveRead = false;
        this.setState({
            showCam: true,
        });
    }

    viewWillDisappear() {
        this.active = false;
        this.setState({
            showCam: false,
        });
    }

    handleBack() {
        JumpBack();
    }

    renderPanel() {
        return (
            <View style={styles.container}>
                <ActionBar title={'查询解锁码'} handleBack={() => this.handleBack()}>
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>
                {this.state.showCam ? (<CameraView onBarCodeRead={d => this.onBarCodeRead(d)} hint={'扫描锁SN二维码'} buttons={this.renderButtons()}
                    torch={true} />) : null}
            </View>
        );
    }

    handlePress() {
        JumpTo('factory/snInput', true, {});
    }

    async onBarCodeRead(d) {
        d = d.trim();
        if (this.haveRead || !this.active) return;
        this.haveRead = true;
        // if (!this.valid(d)) {
        //     Toast.fail('请扫描车锁SN码', 1, () => this.haveRead = false);
        //     return;
        // }
        this.setState({ loading: true });
        const res = await this.props.dispatch(actions.factoryCodeSearch({
            snCode: d,
        }));
        this.setState({ loading: false });
        if (res.code === 0) {
            Modal({
                content: `解锁码：${res.data.unlock_code}`,
                buttons: [{
                    text: '确定', onPress: () => {
                        this.haveRead = false;
                    },
                }]
            });
        } else {
            Modal({
                content: `查询失败：\n${res.message}`,
                buttons: [{
                    text: '确定', onPress: () => this.haveRead = false
                }]
            });
        }

    }

    // valid(text) {
    //     return /[\d][A-Z]{2}\d+/g.test(text);
    // }

    renderButtons() {
        return [
            <View key={0}>
                <TouchableHighlight onPress={() => this.handlePress()}>
                    <VerticalLayout style={{ alignItems: 'center', }}>
                        <Image src="hand" style={{ width: 64, height: 64, }} />
                        <Text style={{ marginTop: 10, fontSize: 14, backgroundColor: 'rgba(0,0,0,0)', color: '#fff', }}>手动输入</Text>
                    </VerticalLayout>
                </TouchableHighlight>
            </View>
        ];
    }

    render() {
        if (this.state.loading)
            return <Loading />;
        return this.renderPanel();
    }
});


const mapStateToProps = (state, ownProps) => {
    return {
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch,
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
        position: 'relative',
    },
    itemBot: {
        position: 'absolute',
        height: 48,
        top: 44,
        left: 0,
        right: 0,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: '#fff',
        borderRadius: 4,
        zIndex: 999,
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',
    },
    icon: {
        width: 11,
        height: 15,
    },
    address: {
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10,
    },
    arrow: {
        width: 10,
        height: 16,
    },
};

export default connect(mapStateToProps, mapDispatchToProps)(SnSearchPanel);
