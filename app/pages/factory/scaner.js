import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as base from '../../base';
import { Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator } from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import CameraView from '../../components/camera';
import Modal from '../../components/commonModal';
import { ActionBar } from '../../components/actionbar';
import { JumpTo, JumpBack, ResetTo, historyStack } from '../../modules';
import * as actions from '../../runtime/action';

// const alert = Modal.alert;

const platePrefix = 'http://ofo.so/plate/';
const FacScaner = pageWrapper(class extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            showCam: false,
            loading: false,
            historyState: historyStack.get()
        };
        this.active = true;
    }

    viewWillAppear() {
        this.active = true;
        this.haveReadBarCode = false;
        this.setState({
            showCam: true,
        });

    }

    viewWillDisappear() {
        this.active = false;

        this.setState({
            showCam: false,
        });
    }

    handleBack() {
        JumpBack();
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="扫描二维码">
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>
                <base.View style={{ position: 'relative', flexGrow: 1 }}>
                    <base.View style={styles.inner}>
                        <ActivityIndicator size='large' src='loading' />
                        <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }

    renderPanel() {
        const { fromWhere } = this.state.historyState;
        return (
            <View style={styles.container}>
                <ActionBar title="扫描二维码" handleBack={() => this.handleBack()}>
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>

                {this.state.showCam ? (<CameraView onBarCodeRead={d => this.onBarCodeRead(d)} hint={fromWhere === 'brand' ? '扫描获取车辆信息' : '扫描后泥板二维码'}
                    buttons={fromWhere === 'brand' ? this.renderButtons() : null}
                    torch={true} />) : null}
            </View>
        );
    }

    async onBarCodeRead(d) {
        const { fromWhere } = this.state.historyState;
        d = d.trim();
        if (this.haveReadBarCode || !this.active) return;
        this.haveReadBarCode = true;
        // if (!this.valid(d)) {
        //     Toast.fail('请输入正确的车牌号码', 2, () => this.haveReadBarCode = false);
        //     return;
        // }
        const carno = d.substring(platePrefix.length, d.length);

        this.setState({
            loading: true,
        });

        const res = await this.props.dispatch(actions.fetchStatus({
            carno,
            type: fromWhere === 'brand' ? 'bicycle_no' : 'fender_sn'
        }));
        this.setState({
            loading: false,
        });
        if (res.code === 0) {
            this.haveReadBarCode = false;
            Toast.success('成功');
        } else {
            // alert('车辆状态', `${res.message}`, [
            //     { text: '确定', onPress: () => this.haveReadBarCode = false, style: 'default' },
            // ]);
            Modal({
                title: '车辆状态：',
                content: res.message,
                buttons: [{
                    text: '确定', onPress: () => this.haveReadBarCode = false
                }]
            });
        }
    }

    // valid(text) {
    //     return text.startsWith(platePrefix) && /^\d{6,11}$/.test(text.substring(platePrefix.length, text.length));
    // }

    handlePress() {
        JumpTo('factory/manually', true, {});
    }

    renderButtons() {
        return [
            <View key={0}>
                <TouchableHighlight onPress={() => this.handlePress()}>
                    <base.VerticalLayout style={{ alignItems: 'center' }}>
                        <base.Image src="hand" style={{ width: 64, height: 64 }} />
                        <Text style={{ marginTop: 10, fontSize: 14, backgroundColor: 'rgba(0,0,0,0)', color: '#fff' }}>手动输入</Text>
                    </base.VerticalLayout>
                </TouchableHighlight>
            </View>
        ];
    }

    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderPanel();
    }
});

const mapStateToProps = (state, ownProps) => {
    return {
        bicycleStatus: state.api.bicycleStatus,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch,
    };
};

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    inner: {
        position: 'absolute',
        flexDirection: 'column',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
};

export default connect(mapStateToProps, mapDispatchToProps)(FacScaner);
