import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, loadToken, apibase, _loadToken, getUserInfo } from '../../runtime/api';
import { Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator, Map } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import { fetch } from '../../modules';
import platform from '../../modules/platform';
var StaticImage = require('../../base/staticImages');
import whereami from '../../base/map/whereami';

/**
 * 700bikeMap
 */

class mapIndex extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            selectedPaneIndex: 0,
            historyState: historyStack.get(),
            initialPosition: undefined,
            lastPosition: undefined,
            region: {
                latitude: 39.979024,
                longitude: 116.312589,
                latitudeDelta: 0.05,
                longitudeDelta: 0.05,
            },
            annotations: [],
            currentAnnotationInfo: {},

        };

        this.currentAnnotationInfo = null;
    }
    componentWillMount() {
        let annotations = [];
        let promiseList = [];
        let i = 0;

        return getUserInfo()
            .then(userInfo => {
                return whereami().then(d => ({
                    userInfo,
                    latitude: d.latitude,
                    longitude: d.longitude
                }));
            })
            .then((d) => {
                let { userInfo, latitude, longitude } = d;
                let { token } = userInfo;
                let allLat = 0,
                    allLong = 0;
                let distance = this.state.historyState.distance,
                    filter = this.state.historyState.filter;
                promiseList.push(fetch(`${apibase}/cooperation-700-bike/get-list`, {
                    method: 'POST',
                    body: { token, 'lat': latitude, 'lng': longitude, 'distance': distance, filter: filter, tpye: 'get_map' }
                }).then(respones => {
                    if (respones['code'] != 0 || respones['data']['list'].length == 0) {
                        this.setState({ region: { latitude: latitude, longitude: longitude, latitudeDelta: 0.05, longitudeDelta: 0.05 } });
                        return [];
                    } else {
                        respones['data']['list'].map((item) => {
                            annotations.push({
                                key: `${i++}`,
                                info: item,
                                latitude: parseFloat(item.lat),
                                longitude: parseFloat(item.lng),
                                icon: StaticImage['sevenicon'],
                                size: {
                                    width: 30,
                                    height: 38
                                },
                            });
                            allLat += parseFloat(item.lat);
                            allLong += parseFloat(item.lng);
                        });
                        allLat /= annotations.length;
                        allLong /= annotations.length;
                        let delta = 0.05;
                        if (distance === 0) {
                            delta = 1;
                        }
                        this.setState({ region: { latitude: allLat, longitude: allLong, latitudeDelta: delta, longitudeDelta: delta } });
                        this.setState({ annotations });

                    }
                    // return Promise.all(promiseList);
                }));

            });
    }

    onLocationChange(e) {
    }

    render() {
        // if (this.state.loading)
        //     return this.renderLoading();
        return this.renderMapPage();
    }
    handleBack() {
        JumpBack();
    }
    /**
     * 点击小图标
     */
    onAnnotationClicked(key) {
        let currentAnnotationInfo = this.state.annotations[key].info;
        JumpTo('700Bike/detail', true, { info: currentAnnotationInfo });
    }
    renderLoading() {
        let top = platform.OS == 'ios' ? 20 : 0;
        return (
            <base.View style={{ paddingTop: top, ...styles.container }}>
                <base.View style={{ position: 'relative', flexGrow: 1 }}>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='loading' />
                        <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }
    renderMapPage() {
        let top = platform.OS == 'ios' ? 20 : 0;
        return (
            <base.View style={styles.container} className="wrap">

                <ActionBar title="地图模式" handleBack={this.handleBack.bind(this)}>
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>
                <base.VerticalLayout style={{ flex: 1 }}>
                    <Map style={{ flex: 1 }} region={this.state.region}
                        annotations={this.state.annotations}
                        onLocationChange={this.onLocationChange.bind(this)}
                        onAnnotationClicked={this.onAnnotationClicked.bind(this)}
                        showsUserLocation={true}
                    />

                </base.VerticalLayout>

            </base.View>
        );
    }


}
export default pageWrapper(mapIndex);
const styles = {
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff'
    },
    topNav: {
        height: 44,
        backgroundColor: '#fff',
        position: 'relative',
        borderBottomColor: '#ddd',
        borderBottomWidth: 1,
        borderBottomStyle: 'solid',
        flexShrink: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    vcbox: {
        alignItems: 'center',
        height: 44,
        position: 'absolute',
        left: 0,
        justifyContent: 'center',
    },
    backBtn: {
        paddingLeft: 10,
        paddingRight: 15,
        height: 44,
        alignItems: 'center',
        justifyContent: 'center',
    },
    arrow: {
        width: 10,
        height: 16
    },
    paneInner: {
        width: 250,
        height: 30,
        borderRadius: 6,
        overflow: 'hidden',
        borderStyle: 'solid',
        borderColor: '#ffd900',
        borderBottomWidth: 1,
        borderTopWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        backgroundColor: '#ffd900'
    },
};


