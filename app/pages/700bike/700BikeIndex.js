import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, loadToken, getDateDiff, apibase, getUserInfo, timeStamp } from '../../runtime/api';
import { Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator, TextInput } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import styles from '../../css/700Bike/indexCss';
import pageWrapper from '../../modules/pageWrapper';
import platform from '../../modules/platform';
import ListView from '../../components/ListView';
import RefreshControl from '../../components/refreshControl';
import Storage from '../../base/storage';
import whereami from '../../base/map/whereami';
var moment = require('moment');
import { fetch } from '../../modules';
import {
    fetchBikeList
} from '../../runtime/action';
/**
 * 师傅首页
 */

class bikeIndex extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            historyState: historyStack.get(),
            pageCount: 0,
            distance: 1000,
            filter: '',
            carnum: '',
            isRefreshing: false,
        };
    }

    viewWillAppear() {
        this.props.fetch({ distance: this.state.distance, filter: this.state.filter }).then(() => this.setState({ loading: false }));
    }


    render() {
        return this.renderPage();
    }

    JumpToMapPage() {
        JumpTo('700Bike/Map', true, { distance: this.state.distance, filter: this.state.filter });
    }
    handleBack() {
        JumpBack();
    }
    /**
     * 搜索
     */
    handleSearch() {
        return whereami().then(d => {
            getUserInfo().then((userInfo) => {
                let { token } = userInfo;
                let { latitude, longitude } = d;
                let carno = this.state.carno;
                console.log('index location', latitude, longitude);
                fetch(`${apibase}/cooperation-700-bike/search`, {
                    method: 'POST',
                    body: { token, 'bicycle_num': carno, 'lat': latitude, 'lng': longitude }
                }).then(respones => {
                    console.log('respones', respones);
                    if (respones['code'] === 0) {
                        //dispatch({type: "setBikeDetail", data: respones['data']});
                        JumpTo('700Bike/detail', true, { info: respones['data'] });
                    } else {
                        Toast.fail(respones['message'], 1.5);
                    }
                });

            });
        });
    }
    /**
     * 选择条件后重新获取列表
     */
    fetchList(name, value) {

        let _this = this;
        if (!this.state.loading) {
            if (name == 'distance' && value != this.state.distance) {
                this.setState({ pageCount: 0 }, () => {
                    setDistance();
                });
            } else if (name == 'filter') {
                if (value != this.state.filter) {
                    this.setState({ pageCount: 0 }, () => {
                        setDistance();
                    });
                } else {
                    value = '';
                    this.setState({ pageCount: 0 }, () => {
                        setDistance();
                    });
                }
            }
            function setDistance() {
                _this.refs['listview'] && _this.refs['listview'].scrollTo({ x: 0, y: 0 });
                _this.setState({ [name]: value }, () => {
                    _this.props.fetch({ distance: _this.state.distance, filter: _this.state.filter, page: _this.state.pageCount }).then(() => _this.setState({ loading: false }));
                });
            }
        }

    }
    renderPage() {
        let top = platform.OS == 'ios' ? 20 : 0;
        let filter = [{ id: 'low_power', text: '低电量' }, { id: 'unused', text: '闲置车' }];
        let distance = [{ text: '1000米', id: 1000 }, { text: '2000米', id: 2000 }, { text: '5000米', id: 5000 }, { text: '全部', id: 0 }];
        return (

            <base.View style={{ paddingTop: top, ...styles.container }} className="wrap">
                <base.HorizontalLayout style={styles.topNav}>
                    <base.HorizontalLayout style={styles.vcbox} >
                        <Button style={styles.backBtn} clickDuration={0} onPress={this.handleBack.bind(this)}>
                            <base.Image style={styles.arrow} src='back'></base.Image>
                        </Button>
                        <base.HorizontalLayout className="input" style={styles.searchBox}>
                            <base.HorizontalLayout style={styles.leftBox}>

                                <TextInput style={styles.serachInput} placeholder='请输入搜索车号' keyboardType="numeric" value={this.state.carno} onChangeText={(text) => { this.setState({ carno: text }); }} onSubmitEditing={this.handleSearch.bind(this)}></TextInput>
                                {this.state.carno ?
                                    <base.TouchableHighlight onPress={this.handleSearch.bind(this)} activeOpacity={.1} underlayColor={'#fff'}>
                                        <base.HorizontalLayout style={styles.searchBtn}>
                                            <base.Image style={styles.search} src='search2'></base.Image>
                                        </base.HorizontalLayout>
                                    </base.TouchableHighlight> : null}
                            </base.HorizontalLayout>

                        </base.HorizontalLayout>
                        {/*<TextInput style={styles.serachInput} placeholder='请输入搜索车号'keyboardType="numeric" value={this.state.carnum} onChangeText={(text) => { this.setState({ carnum: text }) } } onSubmitEditing={this.handleSearch.bind(this)}></TextInput>*/}
                        <base.TouchableHighlight style={styles.mapIcon} activeOpacity={.1} underlayColor={'#fff'}
                            onPress={this.JumpToMapPage.bind(this)}>
                            <base.HorizontalLayout style={{ alignItems: 'center' }}>
                                <base.Image style={{ width: 15, height: 19, marginRight: 2 }} src='map'></base.Image>
                                <Text>地图模式</Text>
                            </base.HorizontalLayout>
                        </base.TouchableHighlight>
                    </base.HorizontalLayout>
                </base.HorizontalLayout>
                <base.HorizontalLayout style={styles.innerBox}>
                    {
                        filter.map((pane, index) => {
                            return (
                                <base.HorizontalLayout style={styles.li} key={index}>
                                    <Button clickDuration={0} style={pane.id === this.state.filter ? { backgroundColor: '#ffd900', ...styles.filterBtn } : { backgroundColor: '#ededed', ...styles.filterBtn }} onPress={() => { this.fetchList('filter', pane.id); }}>
                                        <base.Text style={pane.id === this.state.filter ? { color: '#000' } : { color: '#999' }}>{pane.text}</base.Text>
                                    </Button>
                                </base.HorizontalLayout>
                            );
                        })
                    }
                </base.HorizontalLayout>
                <base.HorizontalLayout style={styles.distanceBox}>
                    {
                        distance.map((pane, index) => {
                            return (
                                <base.TouchableHighlight style={styles.distance} clickDuration={0} key={index} onPress={() => { this.fetchList('distance', pane.id); }} activeOpacity={.1} underlayColor={'#fff'}>
                                    <base.HorizontalLayout style={{
                                        flexBasis: 0,
                                        flexGrow: 1
                                    }}>
                                        <base.HorizontalLayout style={{ alignItems: 'center', flexBasis: 0, flexGrow: 1, justifyContent: 'center' }}>
                                            <base.Text style={pane.id === this.state.distance ? { color: '#000' } : { color: '#999' }}>{pane.text}</base.Text>
                                        </base.HorizontalLayout>
                                        {index < 3 ?
                                            <base.HorizontalLayout style={styles.line}></base.HorizontalLayout>
                                            : null
                                        }

                                    </base.HorizontalLayout>
                                </base.TouchableHighlight>

                            );
                        })
                    }
                </base.HorizontalLayout>

                <base.VerticalLayout style={styles.listBox}>
                    {this.renderList()}
                </base.VerticalLayout>
            </base.View>
        );
    }
    renderList() {
        let list = this.props.bikeList;
        if (!list) {
            return (
                <base.VerticalLayout style={{ justifyContent: 'center', alignItems: 'center', flexGrow: 1, flexBasis: 0 }}>
                    <ActivityIndicator size='large' src='loading' />
                    <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                </base.VerticalLayout>
            );
        }
        if (list && list.length == 0) {
            return (
                <base.VerticalLayout style={{ flexGrow: 1, flexBasis: 0, justifyContent: 'center', alignItems: 'center' }}>
                    <base.Image style={{ width: 70, height: 70, marginBottom: 5 }} src='noData' />
                    <base.Text style={{ fontSize: 16, color: '#666' }}>暂无数据</base.Text>
                </base.VerticalLayout>
            );
        }
        return (
            <base.VerticalLayout style={{ flexGrow: 1, flexBasis: 0 }}>
                <ListView
                    ref="listview"
                    dataSource={list}
                    renderRow={(v, index) => {
                        return (
                            <base.TouchableHighlight style={styles.item} key={index} onPress={() => { v.onPress && v.onPress(v); }} activeOpacity={.1} underlayColor={'#fff'}>
                                <base.HorizontalLayout style={styles.itemInner}>
                                    <base.HorizontalLayout style={v.battery > 20 ? { backgroundColor: '#2EBF38', ...styles.battery } : { backgroundColor: '#ff7000', ...styles.battery }}>
                                        <base.HorizontalLayout style={{ alignItems: 'flex-end' }}>
                                            <base.Text style={{ fontSize: 18, color: '#fff' }}>{v.battery}</base.Text>
                                            <base.Text style={{ fontSize: 12, color: '#fff' }}>%</base.Text>
                                        </base.HorizontalLayout>
                                    </base.HorizontalLayout>
                                    <base.VerticalLayout style={{ flexGrow: 1, paddingLeft: 10 }}>
                                        <base.Text style={styles.carno}>车牌{v.carno}</base.Text>
                                        <base.HorizontalLayout style={{ alignItems: 'center' }}>
                                            <base.Text style={{ color: '#999', marginRight: 5 }}>距离</base.Text>
                                            <base.Text style={{ color: '#ff7000', fontSize: 18 }}>{v.distance}m</base.Text>
                                        </base.HorizontalLayout>
                                    </base.VerticalLayout>
                                    <base.HorizontalLayout style={{ height: 20, alignItems: 'center' }}>
                                        <base.Text style={{ fontSize: 14, color: '#666' }}>闲置</base.Text>
                                        <base.Text style={{ fontSize: 14, color: '#ff7000' }}>{v.time}小时</base.Text>
                                    </base.HorizontalLayout>


                                </base.HorizontalLayout>
                            </base.TouchableHighlight>
                        );
                    }}
                    className="overflowY"
                    onEndReached={() => { if (this.state.pageCount > 49) return; this.setState({ pageCount: ++this.state.pageCount }); this.props.fetch({ distance: this.state.distance, filter: this.state.filter, concat: true, 'page': this.state.pageCount }).then(() => { this.setState({ isRefreshing: false }); }); }} />


            </base.VerticalLayout>
        );

    }
}

const mapStateToProps = state => {
    return {
        bikeList: beautifyList(state.api.bikeList) || null
    };
};
const mapDispatchToProps = dispatch => {
    return {
        dispatch,
        fetch: props => dispatch(fetchBikeList(props))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(bikeIndex));

function beautifyList(list) {
    if (!list)
        return null;

    return list.map(v => {
        return {
            ...v,
            onPress() {

                /* JumpTo Detail */
                //JumpTo('700Bike/detail', true, {id: v.id});
                JumpTo('700Bike/detail', true, { info: v });
            }
        };
    });
}


