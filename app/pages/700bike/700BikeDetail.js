import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, loadToken, getTodayDate, getUserInfo, apibase, timeStamp } from '../../runtime/api';
import {
    Text,
    View,
    Br,
    Button,
    Tabs,
    TabPane,
    ScrollView,
    Toast,
    TouchableHighlight,
    ActivityIndicator,
    Map
} from '../../base';
import { connect } from 'react-redux';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import styles from '../../css/700Bike/detailCss';
import pageWrapper from '../../modules/pageWrapper';
import { fetch } from '../../modules';
import whereami from '../../base/map/whereami';
var StaticImage = require('../../base/staticImages');

var moment = require('moment');
import {
    fetchBikeDetail
} from '../../runtime/action';
import externalApp from '../../modules/externalApp';
/**
 * 700bike detail
 */

class BikeDetail extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            historyState: historyStack.get(),
            region: {
                latitude: 39.98362,
                longitude: 116.306715,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005,
            },
            initialPosition: undefined,
            lastPosition: undefined,
            annotations: [],
        };
    }

    viewWillAppear() {
        if (this.state.historyState) {
            this.props.fetch().then(() => {
                this.setAnnotations();
                this.setState({ loading: false });
            });
        } else {
            this.setAnnotations();
            this.setState({ loading: false });
        }


    }

    setAnnotations() {
        let annotations = [{
            latitude: parseFloat(this.props.carLocation.lat),
            longitude: parseFloat(this.props.carLocation.lng),
            icon: StaticImage['icon'],
            title: '',
            key: ''
        }];
        const region = {
            latitude: parseFloat(this.props.carLocation.lat),
            longitude: parseFloat(this.props.carLocation.lng),
            latitudeDelta: 0.005,
            longitudeDelta: 0.005
        };
        this.setState({ annotations, refresh: true, region });
    }

    viewWillDisappear() {
        this.props.clear();
    }

    handleBack() {
        JumpBack();
    }

    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderBikeDetail();
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="车辆详情" handleBack={this.handleBack.bind(this)}>
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>
                <base.View style={{ position: 'relative', flexGrow: 1 }}>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='loading' />
                        <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }

    goThere() {

        whereami().then(d => {

            let conf = {
                sourceApplication: 'ofo work',
                sid: 'BGVIS1',
                did: 'BGVIS2',
                dlat: parseFloat(this.state.region.latitude),
                dlon: parseFloat(this.state.region.longitude),
                t: 'car',
                dev: 0
            };


            if (!d.error) {
                /* no error */
                conf = Object.assign({}, conf, {
                    slat: parseFloat(d.latitude),
                    slon: parseFloat(d.longitude)
                });
            }
            externalApp('amap', 'path', conf);
        });
    }

    renderBikeDetail() {
        return (
            <base.VerticalLayout className="wrap" style={styles.container}>
                <ActionBar title="车辆详情" handleBack={this.handleBack.bind(this)}>
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>
                <ScrollView style={{ flex: 1 }} className="overflow-y">
                    <base.VerticalLayout style={{ height: 350 }}>
                        <Map style={{ flexGrow: 1, height: 350 }} region={this.state.region}
                            annotations={this.state.annotations} />
                    </base.VerticalLayout>

                    <base.VerticalLayout style={styles.infoBox}>
                        <base.HorizontalLayout style={styles.item}>
                            <base.HorizontalLayout
                                style={this.props.battery > 20 ? { backgroundColor: '#2EBF38', ...styles.battery } : { backgroundColor: '#ff7000', ...styles.battery }}>
                                <base.HorizontalLayout style={{ alignItems: 'flex-end' }}>
                                    <base.Text style={{ fontSize: 18, color: '#fff' }}>{this.props.battery}</base.Text>
                                    <base.Text style={{ fontSize: 12, color: '#fff' }}>%</base.Text>
                                </base.HorizontalLayout>
                            </base.HorizontalLayout>
                            <base.VerticalLayout style={{ flexGrow: 1, paddingLeft: 10 }}>
                                <base.Text style={styles.carno}>车牌{this.props.biycleNum}</base.Text>
                                <base.HorizontalLayout style={{ alignItems: 'center' }}>
                                    <base.Text style={{ color: '#999', marginRight: 5 }}>距离</base.Text>
                                    <base.Text style={{ color: '#ff7000', fontSize: 18 }}>{this.props.distance}m
                                    </base.Text>
                                </base.HorizontalLayout>
                            </base.VerticalLayout>
                            <base.HorizontalLayout style={{ height: 20, alignItems: 'center' }}>
                                <base.Text style={{ fontSize: 14, color: '#666' }}>闲置</base.Text>
                                <base.Text style={{ fontSize: 14, color: '#ff7000' }}>{this.props.time}小时</base.Text>
                            </base.HorizontalLayout>
                        </base.HorizontalLayout>
                        {this.props.address ? <base.HorizontalLayout style={styles.itemBot}>
                            <base.HorizontalLayout style={{ width: 11 }}>
                                <base.Image style={styles.icon} src='icon'></base.Image>
                            </base.HorizontalLayout>
                            <base.HorizontalLayout style={styles.address}>
                                <base.Text style={{ flexWrap: 'wrap', paddingLeft: 10 }}>{this.props.address}</base.Text>
                            </base.HorizontalLayout>
                        </base.HorizontalLayout> : null}
                        <base.VerticalLayout style={{
                            paddingLeft: 20,
                            paddingRight: 20,
                            justifyContent: 'center',
                        }}>
                            <Button style={{
                                height: 48,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 6,
                                backgroundColor: '#333',
                                flexGrow: 1,
                                marginTop: 16,
                                marginBottom: 16
                            }} onPress={e => this.goThere(e)}>
                                <Text style={{ fontSize: 18, color: '#fff' }}>去这里</Text>
                            </Button>
                        </base.VerticalLayout>
                    </base.VerticalLayout>

                </ScrollView>


            </base.VerticalLayout>
        );
    }

}

const mapStateToProps = state => {
    let bikeDetail = state.api.bikeDetail || { address: {} };
    return {
        biycleNum: bikeDetail.carno || 0,
        time: bikeDetail.time || '',
        battery: bikeDetail.battery || '',
        distance: bikeDetail.distance || '',
        carLocation: {
            lat: bikeDetail.lat || '',
            lng: bikeDetail.lng || '',
        },
        address: bikeDetail.address || '',
    };
};
const mapDispatchToProps = dispatch => {
    return {
        dispatch,
        fetch: props => dispatch(fetchBikeDetail(props)),
        clear: props => dispatch({ type: '@@clear', payload: { bikeDetail: { address: {} } } })
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(BikeDetail));



