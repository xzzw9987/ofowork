import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, apibase, getUserInfo } from '../../runtime/api';
import { Text, View, Br, Button, TextInput, TouchableHighlight } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack} from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import Keyboard from '../../modules/keyboard';
var moment = require('moment');

/**
 * 运营审核坏车
 */
class OperationHx extends Component {
    constructor(){
        super();
        this.state={
            phone: '',
            msg: '',
        };
    }

    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }

    /**
     * 手机号删除
     */
    deleteSearch(){
        this.setState({phone: '', msg: ''});
    }

    /**
     * 查询
     */
    handleSearch() {
        Keyboard.dismiss();
        if (this.state.phone === '') {
            this.setState({msg: '请输入正确的手机号！'});
            return;
        }
        JumpTo('hx/workRecord', true, { phone: this.state.phone });
    }

    valid(text) {
        return /^\d{11}$/.test(text);
    }

    render() {
        return (
            <base.View style={styles.container} onStartShouldSetResponder={() => true} onResponderGrant={() => Keyboard.dismiss()}>
                <ActionBar title="审核坏车" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>

                <base.HorizontalLayout  className="input" style={styles.searchBox}>
                    <base.HorizontalLayout style={styles.leftBox}>
                        <base.HorizontalLayout style={styles.searchBtn}>
                            <base.Image style={styles.search} src='search2' />
                        </base.HorizontalLayout>
                        <TextInput style={styles.searchInput} placeholder='请输入被审核人手机号' keyboardType="numeric"
                                   value={this.state.phone} maxLength={11}
                                   onChangeText={(text)=>{this.setState({phone: text, msg: ''});}} />
                        {this.state.phone ?
                            <TouchableHighlight style={styles.deleteBtn} onPress={this.deleteSearch.bind(this)}>
                                <base.HorizontalLayout>
                                    <base.Image style={styles.deleteImg} src='deleteIcon' />
                                </base.HorizontalLayout>
                            </TouchableHighlight> : null}
                    </base.HorizontalLayout>
                    <base.HorizontalLayout style={styles.rightBox}>
                        <Button style={styles.rightBtn} onPress={this.handleSearch.bind(this)}>查询</Button>
                    </base.HorizontalLayout>
                </base.HorizontalLayout>
                <Br/>

                {this.state.msg !== '' ?
                    <base.HorizontalLayout style={{height: 60, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontSize: 17}}>请输入正确的手机号！</Text>
                    </base.HorizontalLayout> : null}
            </base.View>
        );
    }
}
export default pageWrapper(OperationHx);

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    searchBox: {
        height: 65,
        backgroundColor: '#fff',
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        paddingLeft: 10,
        alignItems: 'center',
        marginBottom: 10,
    },
    leftBox: {
        flexGrow: 1,
        flexBasis: 0,
        position: 'relative',
        justifyContent: 'space-between',
        borderRadius: 4,
        backgroundColor: '#ebebeb',
        height: 44,
        paddingRight: 10
    },
    searchBtn: {
        height: 44,
        width: 40,
        position: 'relative'
    },
    search: {
        width: 20,
        height: 20,
        position: 'absolute',
        left: 10,
        top: 12,
    },
    searchInput: {
        height: 44,
        fontSize: 16,
        backgroundColor: '#ebebeb',
        flexGrow: 1,
        flexBasis: 0,
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: '#ebebeb',
    },
    deleteBtn: {
        height: 44,
        width: 40,
        position: 'relative',
    },
    deleteImg: {
        width: 20,
        height: 20,
        position: 'absolute',
        left: 20,
        top: 12,
    },
    rightBox: {
        width: 60,
        height: 60,
    },
    rightBtn: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 60,
        height: 60,
        fontSize: 18,
        color: '#ff8300',
    },
};

