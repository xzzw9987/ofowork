import React, {Component} from 'react';
import * as base from '../../base';
import {api, keys, loadToken, apibase, _loadToken, getUserInfo} from '../../runtime/api';
import {Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator, Map, TextInput, UselessTextInput} from '../../base';
import {connect} from 'react-redux';
import {JumpTo, historyStack, JumpBack} from '../../modules';
import {ActionBar} from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import Swiper, {Item} from '../../components/swiper';

import whereami from '../../base/map/whereami';
import ImagePicker from '../../components/gallery';

var moment = require('moment');
import {fetch} from '../../modules';

/**
 * 举报详情
 */

class ProcessDone extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: false,
            historyState: historyStack.get(),
            number: 140,
            initialPosition: undefined,
            lastPosition: undefined,
            latitude: '',
            longitude: '',
            value: '',
            imgPath: ''
        };
    }

    componentWillMount() {
        whereami().then(e=>{
            let latitude = e.latitude;
            let longitude = e.longitude;
            // 用户当前位置
            this.setState({latitude, longitude});
        });
    }
    componentDidMount(){

    }

    handleBack(){
        JumpBack();
    }
    /**
     * 备注
     */
    onChange(text){
        this.setState({value: text});
        this.setState({number: 140 - text.length });
    }
    /**
     * 提交
     */
    submitReportInfo(){
        // this.setState({value: this.state.imgPath});
        if( this.state.imgPath == ''){
            Toast.fail('必须上传照片', 2);
            return ;
        }else{
            this.setState({loading: true});
        }
        let _token;
        let _this = this;
        function fetchReport(respones) {
            let imgPath = '';
            if (respones){
                imgPath = respones['data']['img_path'];
            }
            fetch(`${apibase}/hx/record`, {
                method: 'POST',
                body: {token: _token,
                    'hx_list_id': historyStack.get().id,
                    'img_path': imgPath,
                    'info': _this.state.value,
                    'lat': _this.state.latitude,
                    'lng': _this.state.longitude,
                    'bicycle_no': historyStack.get().bicycle_no,
                }
            }).then(respones2=>{
                _this.setState({loading: false});
                console.log('提交信息接口返回＝＝＝＝＝＝', respones2);
                if(respones2['code'] === 0){
                    Toast.success('提交成功', 1);
                    setTimeout(function(){
                        JumpBack();
                    }, 1);
                }else{
                    Toast.fail(respones2['message'], 1.5);
                }
            });
        }
        return getUserInfo()
        .then(userInfo=> {
            let {token} = userInfo;
            _token = token;
            fetch(`${apibase}/user/upload-img-with-base64`, {
                method: 'POST',
                body: {token, 'img': this.state.imgPath}
            }).then(respones=>{
                console.log('上传图片接口返回＝＝＝＝＝＝', respones);
                if(respones['code'] === 0){
                    //alert(respones['data']['img_path'] );
                    fetchReport(respones);
                }else{
                    this.setState({loading: false});
                    Toast.fail('图片上传失败', 1.5);
                }
            });
        });
    }

    render() {
        return this.renderReportDetail();
    }

    renderReportDetail() {
        return (
            <base.VerticalLayout className="wrap" style={styles.container} onStartShouldSetResponder={()=>this.getKeyboardState()} onResponderGrant={e=>this.dismissKeyboard && this.dismissKeyboard(e)}>
                <ActionBar title="处理完成" handleBack={this.handleBack.bind(this)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <base.VerticalLayout style={{
                    paddingTop: 30,
                    paddingBottom: 30,
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: 170
                }}>

                    {/* 上传图片 */}
                    <ImagePicker options={{ chooseFromLibraryButtonTitle: null }} onSelectPhoto={base64Data=>{ this.setState({imgPath: base64Data}); }} style={{backgroundColor: '#fff'}}  desc="请拍摄已处理车辆，含车牌号和周围真实环境"/>

                </base.VerticalLayout>
                <base.VerticalLayout style={{
                    paddingLeft: 10,
                    paddingRight: 10,
                    paddingBottom: 30
                }}>
                    <UselessTextInput style={{
                        height: 120,
                        backgroundColor: '#e5e5e5',
                        padding: 10,
                        borderWidth: 0,
                        fontSize: 14,
                        alignItems: 'flex-start'
                    }}
                        multiline={true}
                        numberOfLines={4}
                        maxLength={140}
                        placeholder="备注"
                        onChangeText={(text) => { this.onChange(text); } }
                    />
                    <base.HorizontalLayout style={{
                        paddingTop: 5,
                        justifyContent: 'flex-end'
                    }}>
                        <Text style={{
                            color: '#999',
                            fontSize: 12
                        }}>还剩{this.state.number}字</Text>
                    </base.HorizontalLayout>
                </base.VerticalLayout>
                <base.VerticalLayout style={{
                    flex: 1,
                    borderStyle: 'solid',
                    borderColor: '#979797',
                    borderBottomWidth: 1,
                    borderTopWidth: 1,
                    borderLeftWidth: 0,
                    borderRightWidth: 0,
                    backgroundColor: '#F6F6F6'
                }}>
                </base.VerticalLayout>
                <base.VerticalLayout style={{
                    paddingLeft: 20,
                    paddingRight: 20,
                    justifyContent: 'center',
                }}>
                    <Button style={{
                        height: 40,
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderRadius: 6,
                        backgroundColor: '#333',
                        flexGrow: 1,
                        marginTop: 10,
                        marginBottom: 10
                    }} onPress={this.submitReportInfo.bind(this)}>
                        <Text style={{fontSize: 18, color: '#fff'}}>提交</Text>
                    </Button>
                    <base.HorizontalLayout style={{
                        justifyContent: 'center',
                        paddingBottom: 10
                    }}>
                        <Text>提交后可在工作记录中查看记录</Text>
                    </base.HorizontalLayout>
                </base.VerticalLayout>
                {this.state.loading ? <base.View style={{position: 'absolute', flexDirection: 'column', left: 0, top: 0, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,.5)'}}>
                        <ActivityIndicator size='large' src='loading'/>
                    <base.HorizontalLayout style={{marginTop: 10}}><Text>图片上传中 ...</Text></base.HorizontalLayout>
                </base.View> :null }
           </base.VerticalLayout>
        );
    }

}

export default pageWrapper(ProcessDone);
const styles = {
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff',
        position: 'relative'
    }
};



