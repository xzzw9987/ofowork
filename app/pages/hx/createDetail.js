import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, parseNum, apibase, showWeekCN, getUserInfo } from '../../runtime/api';
import {
    Text, View, Br, Button, ScrollView, TouchableHighlight, ActivityIndicator, UselessTextInput,
    Toast, Checkbox, TextInput
} from '../../base';
import pageWrapper from '../../modules/pageWrapper';
import { connect } from 'react-redux';
import { JumpTo, JumpBack, historyStack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import { fetch } from '../../modules';
import ImagePicker from '../../components/gallery';
import whereami from '../../base/map/whereami';

/**
 * 创建记录
 */
class CreateDetail extends Component {

    constructor(...args) {
        super(...args);
        this.state = {
            carno: historyStack.get().carno,
            imgPath: '',
            checkedList: [],
            token: '',
            info: '',
            latitude: '',
            longitude: '',
            loading: false
        };
    }

    componentDidMount() {
        whereami().then(e => {
            let latitude = e.latitude;
            let longitude = e.longitude;
            // 用户当前位置
            this.setState({ latitude, longitude });
        });
        return getUserInfo()
            .then(userInfo => {
                let { token } = userInfo;
                fetch(`${apibase}/hx/reason-list`, {
                    method: 'POST',
                    body: { token, 'bicycle_no': this.state.carno }
                }).then(respones => {
                    let list = [];
                    Object.keys(respones['data']['list']).map(key =>
                        list.push({
                            name: key,
                            checked: false,
                            title: respones['data']['list'][key]
                        })
                    );
                    this.setState({
                        checkedList: list,
                        token
                    });
                });
            });
    }
    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }

    /**
     * 创建
     */
    createDetail() {

    }

    /**
     * 提交
     */
    submitInfo() {
        if (this.state.imgPath === '') {
            Toast.fail('必须上传照片', 2);
            return;
        }

        let reason = this.state.checkedList.filter(v => v.checked).map(v => v.name);
        if (reason.length <= 0) {
            Toast.fail('请选择维修原因', 2);
            return;
        }
        //let reason = this.state.checkedList.filter(v => v.checked).map(v => v.name).join(',');
        this.setState({ loading: true });
        fetch(`${apibase}/user/upload-img-with-base64`, {
            method: 'POST',
            body: { token: this.state.token, 'img': this.state.imgPath }
        }).then(respones => {
            if (respones['code'] === 0) {
                fetchRecord(respones['data']['img_path']);
            } else {
                Toast.fail('图片上传失败', 1.5);
                this.setState({ loading: false });
            }
        });
        let _this = this;
        function fetchRecord(imgPath) {
            fetch(`${apibase}/hx/record`, {
                method: 'POST',
                body: {
                    'token': _this.state.token,
                    'bicycle_no': _this.state.carno,
                    'img_path': imgPath,
                    'info': _this.state.info,
                    'reason': reason,
                    'lat': _this.state.latitude,
                    'lng': _this.state.longitude
                }
            }).then(respones2 => {
                _this.setState({ loading: false });
                if (respones2['code'] === 0) {
                    Toast.success('提交成功', 1);
                    setTimeout(function () {
                        JumpBack();
                    }, 1);
                } else {
                    Toast.fail(respones2['message'], 1.5);
                }
            });
        }
    }

    render() {
        const title = this.state.carno === '' ? '车牌号：无' : '车牌号：' + this.state.carno;
        return (
            <base.View style={styles.container}>
                <ActionBar title={title} handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <ScrollView style={styles.wrap} className="overflow-y">
                    <base.VerticalLayout style={styles.wrap}>
                        <base.VerticalLayout style={{
                            paddingTop: 30,
                            paddingBottom: 30,
                            alignItems: 'center',
                            justifyContent: 'center',
                            height: 160
                        }}>

                            {/* 上传图片 */}
                            <ImagePicker options={{ chooseFromLibraryButtonTitle: null }} onSelectPhoto={base64Data => {
                                this.setState({ imgPath: base64Data });
                            }} style={{ backgroundColor: '#fff' }} desc="请拍摄已处理车辆，含车牌号和周围真实环境" />

                        </base.VerticalLayout>
                        <base.HorizontalLayout style={styles.checkInfo}>

                            {this.state.checkedList.map((v, idx) => {
                                return (
                                    <TouchableHighlight style={{ width: 120, height: 40 }} underlayColor={'#fff'} clickDuration={0} key={idx} onPress={() => {
                                        this.state.checkedList.forEach(v => v.checked = false);
                                        v.checked = true;
                                        this.setState({ checked: this.state.checkedList });
                                    }}>
                                        <base.HorizontalLayout style={styles.checkBox}>
                                            <Checkbox checked={v.checked}
                                                onChange={() => {
                                                    this.state.checkedList.forEach(v => v.checked = false);
                                                    v.checked = true;
                                                    this.setState({ checked: this.state.checkedList });
                                                }} />
                                            <Text style={{ marginLeft: 5 }} >{v.title}</Text>
                                        </base.HorizontalLayout>
                                    </TouchableHighlight>
                                );
                            })}

                        </base.HorizontalLayout>

                        <base.VerticalLayout style={{
                            paddingLeft: 20,
                            paddingRight: 20,
                            paddingBottom: 30
                        }}>
                            <TextInput style={{
                                height: 70,
                                backgroundColor: '#e5e5e5',
                                padding: 10,
                                borderWidth: 0,
                                fontSize: 14,
                                alignItems: 'flex-start'
                            }} keyboardType="numeric"
                                multiline={true}
                                numberOfLines={4}
                                maxLength={150}
                                placeholder="备注:"
                                onChangeText={(text) => {
                                    this.setState({ info: text });
                                }}
                            />
                        </base.VerticalLayout>
                        <base.VerticalLayout style={{
                            paddingLeft: 20,
                            paddingRight: 20,
                            justifyContent: 'center'
                        }}>
                            <Button style={{
                                height: 48,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 6,
                                backgroundColor: '#333',
                                flexGrow: 1,
                                marginTop: 16,
                                marginBottom: 16
                            }} onPress={this.submitInfo.bind(this)}>
                                <Text style={{ fontSize: 18, color: '#fff' }}>提交</Text>
                            </Button>
                        </base.VerticalLayout>
                        <base.HorizontalLayout style={{ justifyContent: 'center' }}>
                            <Text style={{ fontSize: 16, paddingBottom: 20 }}>提交后可在工作记录中查看记录</Text>
                        </base.HorizontalLayout>
                    </base.VerticalLayout>
                </ScrollView>
                {this.state.loading ? <base.View style={{ position: 'absolute', flexDirection: 'column', left: 0, top: 0, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,.5)' }}>
                    <ActivityIndicator size='large' src='images/loading.gif' />
                    <base.HorizontalLayout style={{ marginTop: 10 }}><Text>图片上传中 ...</Text></base.HorizontalLayout>
                </base.View> : null}
            </base.View>
        );
    }


}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    wrap: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    checkInfo: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        borderStyle: 'solid',
        borderColor: '#d6d6d6',
        borderBottomWidth: 0,
        borderTopWidth: 1,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        paddingTop: 7,
        paddingBottom: 10,
        paddingLeft: 40,
        paddingRight: 20
    },
    checkBox: {
        flexShrink: 0,
        alignItems: 'center',
        height: 40,
        width: 130,
        marginTop: 3

    }


};
export default pageWrapper(CreateDetail);
