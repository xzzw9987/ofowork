import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys } from '../../runtime/api';
import { Text, View, Br, Button, TextInput, TouchableHighlight, ScrollView, ActivityIndicator } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack, historyStack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';

var moment = require('moment');
import {
    fetchWorkRecord
} from '../../runtime/action';

/**
 * 工作记录
 */
class WorkRecord extends Component {

    constructor() {
        super();
        this.state = {
            loading: true,
            phone: historyStack.get().phone,
        };
    }

    viewWillAppear() {
        this.props.fetch({ phone: this.state.phone }).then(() => { this.setState({ loading: false }); });
    }

    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }

    render() {
        let list = this.props.list;
        if (this.state.loading)
            return this.renderLoading();
        return this.renderList(list);
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="工作记录" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.View style={{ position: 'absolute', flexDirection: 'column', left: 0, top: 44, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator size='large' src='loading' />
                    <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                </base.View>
            </base.View>
        );
    }

    renderList(list) {
        return (
            <base.View style={styles.container}>
                <ActionBar title="工作记录" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.VerticalLayout style={styles.container}>

                    {this.props.phone && this.props.phone != '' ?
                        <base.VerticalLayout>
                            <base.HorizontalLayout style={styles.topBox}>
                                <base.HorizontalLayout style={styles.leftBox}>
                                    <Text>姓名：</Text>
                                    <Text>{this.props.name}</Text>
                                </base.HorizontalLayout>

                                <base.HorizontalLayout style={styles.center}>
                                    <Text>手机号：</Text>
                                    <Text>{this.props.phone}</Text>
                                </base.HorizontalLayout>
                            </base.HorizontalLayout>
                            <base.VerticalLayout style={styles.scrollBorder} />
                        </base.VerticalLayout> : null}

                    {list.length > 0 ?
                        <ScrollView className="overflowY" style={styles.listView}>
                            {list.map((item, idx) => {
                                return (
                                    <TouchableHighlight key={idx} style={styles.listView} onPress={() => { JumpTo('hx/workRecord/detail', true, { date: item.date, phone: this.state.phone }); }}>
                                        <base.HorizontalLayout style={styles.list}>
                                            <base.HorizontalLayout style={styles.one}>
                                                <base.Text>{item.date}</base.Text>
                                            </base.HorizontalLayout>
                                            <base.HorizontalLayout style={styles.two}>
                                                <base.Text>找到：{item.cnt}辆</base.Text>
                                            </base.HorizontalLayout>
                                            <base.HorizontalLayout style={styles.three}>
                                                <base.Text>审核过：{item.cnt_passed}辆</base.Text>
                                            </base.HorizontalLayout>
                                        </base.HorizontalLayout>
                                    </TouchableHighlight>
                                );
                            })}
                        </ScrollView>
                        :
                        <base.View style={{ position: 'absolute', flexDirection: 'column', left: 0, top: 88, right: 0, bottom: 44, alignItems: 'center', justifyContent: 'center' }}>
                            <base.Image style={{ height: 60, width: 60, }} src='noData' />
                            <base.HorizontalLayout style={{ marginTop: 10 }}><Text>暂无数据</Text></base.HorizontalLayout>
                        </base.View>}
                </base.VerticalLayout>
            </base.View>
        );
    }
}


const mapStateToProps = state => {
    let workRecord = state.api.workRecord || {};
    return {
        list: workRecord.list || [],
        name: workRecord.name || '无',
        phone: workRecord.phone || historyStack.get().phone,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        dispatch,
        fetch: props => dispatch(fetchWorkRecord(props)),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(WorkRecord));


const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    topBox: {
        alignItems: 'center',
        borderStyle: 'solid',
        borderBottomColor: '#c6c6c6',
        borderBottomWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        justifyContent: 'center',
        backgroundColor: '#fff',
        paddingTop: 15,
        paddingBottom: 15,
    },
    leftBox: {
        borderRightStyle: 'solid',
        borderRightColor: '#D6D6D6',
        borderRightWidth: 1,
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    center: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    listView: {
        flexGrow: 1,
        flexBasis: 0,
    },
    scrollBorder: {
        borderStyle: 'solid',
        borderTopColor: '#c6c6c6',
        borderTopWidth: 1,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        marginTop: 20,
    },
    list: {
        borderBottomWidth: 1,
        borderBottomColor: '#c6c6c6',
        borderBottomStyle: 'solid',
        justifyContent: 'space-around',
        height: 54,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10
    },
    one: {
        flexGrow: .8,
        flexBasis: 0
    },
    two: {
        flexGrow: 1,
        justifyContent: 'flex-start',
        flexBasis: 0
    },
    three: {
        flexGrow: 1,
        alignItems: 'flex-end',
        flexBasis: 0
    }
};
