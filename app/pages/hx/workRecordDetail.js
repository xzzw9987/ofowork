import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, _loadToken, apibase } from '../../runtime/api';
import { Text, View, Br, Button, List, Toast, ScrollView, TouchableHighlight, ActivityIndicator } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack, historyStack, fetch } from '../../modules';
import pageWrapper from '../../modules/pageWrapper';
import { ActionBar } from '../../components/actionbar';
import RefreshControl from '../../components/refreshControl';
import ListView from '../../components/ListView';

var moment = require('moment');
import {
    fetchWorkRecordDetail
} from '../../runtime/action';

/**
 * 工作记录详情
 */
class WorkRecordDetail extends Component {

    constructor() {
        super();
        this.state = {
            loading: true,
            type: 'ofo',
            pageCount: 0,
            phone: historyStack.get().phone,
            date: historyStack.get().date,
            detail: [],
        };
    }

    viewWillAppear() {
        this.props.fetch({ 'type': this.state.type, 'date': this.state.date, 'phone': this.state.phone })
            .then(() => this.setState({ loading: false, detail: this.props.workRecordDetail }));
    }

    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }

    /**
     * tab切换
     */
    handleSwitchZone(type) {
        this.props.clear();
        this.setState({ loading: true });
        this.setState({ type: type, pageCount: 0 });
        if (this.state.type !== type) {
            this.props.fetch({ 'type': type, 'date': this.state.date, 'phone': this.state.phone }).then(() => {
                this.setState({ loading: false, detail: this.props.workRecordDetail });
                setTimeout(() => this.refs['listView'] && this.refs['listView'].scrollTo({ x: 0, y: 0, animated: false }), 50);
            });
        }
    }

    /**
     * 下拉刷新
     */
    makeRefreshControl() {
        return <RefreshControl
            refreshing={this.state.isRefreshing}
            onRefresh={() => {
                this.setState({ isRefreshing: true });
                this.props.fetch({ 'type': this.state.type, 'date': this.state.date, 'phone': this.state.phone })
                    .then(() => { this.setState({ isRefreshing: false, pageCount: 0, detail: this.props.workRecordDetail }); Toast.success('刷新成功', 1); });
            }}
        />;
    }

    /**
     * 上拉加载
     */
    makePullLoading() {
        if (this.state.pageCount > 8) return;
        this.setState({ pageCount: ++this.state.pageCount });
        this.props.fetch({ 'type': this.state.type, 'date': this.state.date, 'phone': this.state.phone, concat: true, 'page': this.state.pageCount }).then(() => {
            this.setState({ isRefreshing: false, detail: this.props.workRecordDetail });
        });
    }

    /**
     * 审核操作
     */
    handleSwitchPass(item) {
        let type = '';
        if (item.status === '0') {
            type = 'pass';
        }
        fetch(`${apibase}/hx/handle`, {
            method: 'POST',
            body: { 'id': item.id, 'token': this.props.token, 'type': type }
        }).then((d) => {
            if (d.code === 0) {
                if (item.status === '1') {
                    item.status = '0';
                    Toast.success('撤回通过成功', 1);
                }
                else {
                    item.status = '1';
                    Toast.success('审核通过', 1);
                }
                this.setState({});
            }
            else {
                Toast.fail(d.message, 1);
            }
        });
    }

    /**
     * 运营审核2天之内
     */
    handlePassOrNot() {
        let date = this.state.date;
        let oldDay = parseInt(date.split('-')[2]);
        let oldMonth = parseInt(date.split('-')[1]);
        let nowDay = new Date().getDate();
        let nowMonth = new Date().getMonth() + 1;
        return (nowMonth === oldMonth && (nowDay - oldDay) < 2);
    }

    render() {
        let workRecordDetail = this.state.detail || [];
        if (this.state.loading)
            return this.renderLoading();
        return this.renderPageList(workRecordDetail);
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="工作记录" style={{ borderBottomColor: '#7d7d7d' }} />
                <base.VerticalLayout style={styles.listView}>
                    <base.HorizontalLayout style={styles.nav}>
                        <base.VerticalLayout style={styles.inner}>
                            <Button clickDuration={0} style={(this.state.type === 'ofo') ? styles.item1 : styles.item2} onPress={() => { this.handleSwitchZone('ofo'); }}>
                                <base.Text style={(this.state.type === 'ofo') ? styles.text1 : styles.text2}>ofo推荐</base.Text>
                            </Button>
                        </base.VerticalLayout>
                        <base.VerticalLayout style={styles.inner}>
                            <Button clickDuration={0} style={(this.state.type === 'self') ? styles.item1 : styles.item2} onPress={() => { this.handleSwitchZone('self'); }}>
                                <base.Text style={(this.state.type === 'self') ? styles.text1 : styles.text2}>自主完成</base.Text>
                            </Button>
                        </base.VerticalLayout>
                    </base.HorizontalLayout>
                </base.VerticalLayout>
                <base.View style={{ position: 'absolute', flexDirection: 'column', left: 0, top: 44, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator size='large' src='loading' />
                    <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                </base.View>
            </base.View>
        );
    }

    renderPageList(workRecordDetail) {
        return (
            <base.View style={styles.container}>
                <ActionBar title="工作记录" style={{ borderBottomColor: '#7d7d7d' }} handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.VerticalLayout style={styles.listView}>
                    <base.HorizontalLayout style={styles.nav}>
                        <base.VerticalLayout style={styles.inner}>
                            <Button clickDuration={0} style={(this.state.type === 'ofo') ? styles.item1 : styles.item2} onPress={() => { this.handleSwitchZone('ofo'); }}>
                                <base.Text style={(this.state.type === 'ofo') ? styles.text1 : styles.text2}>ofo推荐</base.Text>
                            </Button>
                        </base.VerticalLayout>
                        <base.VerticalLayout style={styles.inner}>
                            <Button clickDuration={0} style={(this.state.type === 'self') ? styles.item1 : styles.item2} onPress={() => { this.handleSwitchZone('self'); }}>
                                <base.Text style={(this.state.type === 'self') ? styles.text1 : styles.text2}>自主完成</base.Text>
                            </Button>
                        </base.VerticalLayout>
                    </base.HorizontalLayout>
                    {workRecordDetail.length > 0 ?
                        <ListView ref="listView" onEndReached={() => { this.makePullLoading(); }}
                            dataSource={workRecordDetail}
                            renderRow={(item, idx) => {
                                return (
                                    <base.HorizontalLayout key={item.id} style={{ ...styles.list, position: 'relative' }}>
                                        <base.HorizontalLayout>
                                            <base.Image style={{ width: 80, height: 80, marginRight: 10, }} src={item.img_path ? `${item.img_path}?x-oss-process=image/resize,h_80` : 'defaultImage'} />
                                        </base.HorizontalLayout>

                                        <base.HorizontalLayout style={{ flex: 1 }}>
                                            <base.VerticalLayout style={{ flex: 1 }}>
                                                <base.HorizontalLayout style={styles.firstHorizontal}>
                                                    <base.Text style={styles.firstHorizontalText1}>车牌{item.bicycle_no ? item.bicycle_no : '无'}</base.Text>
                                                    <base.Text style={styles.time}>{item.time}</base.Text>
                                                </base.HorizontalLayout>

                                                {item.reason.length > 0 ?
                                                    <base.HorizontalLayout>
                                                        {item.reason.map(function (reason, index) {
                                                            return (
                                                                <base.HorizontalLayout style={styles.reasons} key={index}>
                                                                    <base.Text style={styles.reasonItem}>{reason}</base.Text>
                                                                </base.HorizontalLayout>
                                                            );
                                                        })}
                                                    </base.HorizontalLayout> : null
                                                }

                                                <base.HorizontalLayout>
                                                    <base.Text style={styles.address}>工单编号：{item.log_id ? item.log_id : '无'}</base.Text>
                                                </base.HorizontalLayout>

                                                <base.HorizontalLayout style={{ ...styles.resultTab, ...styles.firstHorizontal }}>
                                                    <base.Text style={item.status === '1' ? styles.process : styles.pending}>{item.status === '1' ? '已审核' : '待审核'}</base.Text>
                                                </base.HorizontalLayout>

                                            </base.VerticalLayout>
                                        </base.HorizontalLayout>

                                        {this.state.phone && this.state.phone !== '' && this.handlePassOrNot() ?
                                            <Button onPress={() => { this.handleSwitchPass(item); }} style={styles.pass}>{item.status === '1' ? '撤回通过' : '通过'}</Button> : null}
                                    </base.HorizontalLayout>
                                );
                            }}
                            refreshControl={this.makeRefreshControl()} className="overflowY" style={styles.listView} />
                        :
                        <base.View style={{ position: 'absolute', flexDirection: 'column', left: 0, top: 88, right: 0, bottom: 44, alignItems: 'center', justifyContent: 'center' }}>
                            <base.Image style={{ height: 60, width: 60, }} src='noData' />
                            <base.HorizontalLayout style={{ marginTop: 10 }}><Text>暂无数据</Text></base.HorizontalLayout>
                        </base.View>
                    }
                </base.VerticalLayout>

            </base.View>
        );
    }
}


const mapStateToProps = state => {
    return {
        loading: state.api.loading,
        token: state.api.token,
        workRecordDetail: state.api.workRecordDetail,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        dispatch,
        fetch: props => dispatch(fetchWorkRecordDetail(props)),
        clear: () => {
            dispatch({
                type: '@@clear', payload: {
                    workRecordDetail: [],
                }
            });
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(WorkRecordDetail));

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    nav: {
        backgroundColor: '#fff',
        borderStyle: 'solid',
        borderBottomColor: '#7d7d7d',
        borderBottomWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
    },
    inner: {
        alignItems: 'center',
        justifyContent: 'center',
        flexGrow: 1,
        height: 44,
    },
    item1: {
        paddingLeft: 20,
        paddingRight: 20,
        borderBottomColor: '#FAD500',
        borderBottomWidth: 4,
        borderStyle: 'solid',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        height: 40,
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    item2: {
        height: 44,
        paddingBottom: 4,
        flexGrow: 1,
        paddingLeft: 20,
        paddingRight: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text1: {
        alignSelf: 'center',
        flexGrow: 1,

    },
    text2: {
        alignSelf: 'center',
        flexGrow: 1,
        color: '#999',
    },
    listView: {
        flexGrow: 1,
        flexBasis: 0,
        backgroundColor: '#fff',
    },
    list: {
        borderBottomColor: '#d6d6d6',
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        marginLeft: 10,
        paddingTop: 20,
        paddingRight: 15,
        paddingBottom: 8,
    },
    firstHorizontal: {
        justifyContent: 'space-between',
        position: 'relative',
        marginBottom: 8,
    },
    firstHorizontalText1: {
        alignSelf: 'flex-start',
        fontSize: 18,
    },
    reasons: {
        paddingLeft: 4,
        paddingRight: 4,
        paddingTop: 2,
        paddingBottom: 2,
        marginBottom: 8,
        marginRight: 10,
        borderColor: '#ff7000',
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 2,
    },
    reasonItem: {
        color: '#ff7000',
        fontSize: 12,
    },
    address: {
        fontSize: 14,
        flexWrap: 'wrap',
        marginBottom: 8,
    },
    time: {
        color: '#999',
        fontSize: 14,
        position: 'absolute',
        right: 0,
        top: 3,
    },
    reminder1: {
        width: 8,
        height: 8,
        borderRadius: 8,
        backgroundColor: '#ff7000',
        position: 'absolute',
        top: -1,
        right: -12,
    },
    resultTab: {
        borderRadius: 2,
    },
    pending: {
        fontSize: 14,
        backgroundColor: '#ffd900',
        paddingLeft: 3,
        paddingRight: 3,
    },
    process: {
        fontSize: 14,
        backgroundColor: '#2EBF38',
        paddingLeft: 3,
        paddingRight: 3,
    },
    name: {
        fontSize: 14,
        marginLeft: 5,
    },
    pass: {
        position: 'absolute',
        bottom: 10,
        right: 15,
        backgroundColor: '#2C2C2C',
        color: '#fff',
        borderRadius: 2,
        justifyContent: 'center',
        fontSize: 15,
        alignItems: 'center',
        flexGrow: 1,
        padding: 5,
        width: 75
    },
};
