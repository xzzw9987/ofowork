import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, loadToken, apibase, _loadToken, getUserInfo } from '../../runtime/api';
import { Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator, Map } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import { fetch } from '../../modules';
import platform from '../../modules/platform';
var StaticImage = require('../../base/staticImages');
import whereami from '../../base/map/whereami';

/**
 * 师傅首页
 */

class HxHotSpot extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            region: {
                latitude: 39.979024,
                longitude: 116.312589,
                latitudeDelta: 0.05,
                longitudeDelta: 0.05,
            },
            hotSpots: [],
            type: 'last_hour'
        };
    }

    viewWillAppear() {
        this.getCurrentPosition();
        this.handleGetData();
        this.setState({ loading: false });
    }

    // 用户当前位置
    getCurrentPosition() {
        whereami().then(e => {
            let latitude = e.latitude;
            let longitude = e.longitude;
            this.setState({
                region: {
                    latitude: latitude,
                    longitude: longitude,
                    latitudeDelta: 0.05,
                    longitudeDelta: 0.05
                }
            });
        });
    }

    //获取热点
    handleGetData() {
        let promiseList = [];
        let reportHotSpots = [];
        return getUserInfo()
            .then(userInfo => {
                let { token } = userInfo;
                let type = this.state.type;
                promiseList.push(fetch(`${apibase}/hot-spot/broken`, {
                    method: 'POST',
                    body: { token, 'type': type }
                }).then(respones => {
                    if (respones['code'] != 0 || respones['data']['list'].length == 0) {
                        this.getCurrentPosition();
                        return [];
                    } else {
                        respones['data']['list'].map((item, index) => {
                            reportHotSpots.push({
                                key: `${index}`,
                                latitude: parseFloat(item.lat),
                                longitude: parseFloat(item.lng),
                                radius: 25,
                                count: parseFloat(item.cnt),
                            });
                        });
                        return reportHotSpots;
                    }
                }));
                Promise.all(promiseList).then(respones => {
                    this.setState({ hotSpots: respones[0] });
                });
            });
    }

    //当前历史切换
    handleSwitchType(type) {
        this.setState({ type: type });
        this.getCurrentPosition();
        this.handleGetData();
    }

    //返回
    handleBack() {
        JumpBack();
    }

    render() {
        let top = platform.OS === 'ios' ? 20 : 0;
        return (
            <base.View style={{ paddingTop: top, ...styles.container }} className="wrap">
                <base.HorizontalLayout style={styles.topNav}>
                    <base.HorizontalLayout style={styles.vcBox} >
                        <Button style={styles.backBtn} onPress={this.handleBack.bind(this)}>
                            <base.Image style={styles.arrow} src='back' />
                        </Button>
                    </base.HorizontalLayout>
                    {this.renderPane()}
                </base.HorizontalLayout>
                <base.VerticalLayout style={{ flex: 1 }}>
                    {this.renderMap()}
                </base.VerticalLayout>

            </base.View>
        );
    }

    renderMap() {
        return this.state.loading ? null : (<Map style={{ flex: 1 }} showsUserLocation={true}
            region={this.state.region}
            hotSpots={this.state.hotSpots} />);
    }

    renderPane() {
        return (
            <base.HorizontalLayout style={styles.paneInner}>
                <base.HorizontalLayout style={{ flex: 1, justifyContent: 'center', borderTopLeftRadius: 6, borderBottomLeftRadius: 6, backgroundColor: (this.state.type === 'last_hour') ? '#ffd900' : '#fff' }}>
                    <Button clickDuration={0} style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                        onPress={() => { this.handleSwitchType('last_hour'); }}>
                        <base.Text>当前</base.Text>
                    </Button>
                </base.HorizontalLayout>
                <base.HorizontalLayout style={{ flex: 1, justifyContent: 'center', borderTopRightRadius: 6, borderBottomRightRadius: 6, backgroundColor: (this.state.type === 'history') ? '#ffd900' : '#fff' }}>
                    <Button clickDuration={0} style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                        onPress={() => { this.handleSwitchType('history'); }}>
                        <base.Text>历史</base.Text>
                    </Button>
                </base.HorizontalLayout>
            </base.HorizontalLayout>);
    }
}
export default pageWrapper(HxHotSpot);
const styles = {
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff'
    },
    paneInner: {
        width: 200,
        height: 30,
        borderRadius: 6,
        overflow: 'hidden',
        borderStyle: 'solid',
        borderColor: '#ffd900',
        borderWidth: 1,
    },
    item1: {
        backgroundColor: '#ffd900',
        width: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    item2: {
        backgroundColor: '#fff',
        width: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    topNav: {
        height: 44,
        backgroundColor: '#fff',
        position: 'relative',
        borderBottomColor: '#ddd',
        borderBottomWidth: 1,
        borderBottomStyle: 'solid',
        flexShrink: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    vcBox: {
        alignItems: 'center',
        height: 44,
        position: 'absolute',
        left: 0,
        justifyContent: 'center',
    },
    backBtn: {
        paddingLeft: 10,
        paddingRight: 15,
        height: 44,
        alignItems: 'center',
        justifyContent: 'center',
    },
    arrow: {
        width: 10,
        height: 16
    },
};
