import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, loadToken, apibase, _loadToken, getUserInfo } from '../../runtime/api';
import {
    Text,
    View,
    Br,
    Button,
    Tabs,
    TabPane,
    ScrollView,
    Toast,
    TouchableHighlight,
    ActivityIndicator,
    Map
} from '../../base';
import { connect } from 'react-redux';
import { JumpTo, historyStack, JumpBack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import pageWrapper from '../../modules/pageWrapper';
import { fetch } from '../../modules';
var StaticImage = require('../../base/staticImages');
import whereami from '../../base/map/whereami';

/**
 * 师傅首页
 */

class HxMapIndex extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            selectedPaneIndex: 0,
            historyState: historyStack.get(),
            initialPosition: undefined,
            lastPosition: undefined,
            region: {
                latitude: 39.979024,
                longitude: 116.312589,
                latitudeDelta: 0.05,
                longitudeDelta: 0.05,
            },
            annotations: [],
            currentAnnotationInfo: {}
        };

        this.currentAnnotationInfo = null;
    }

    viewWillAppear() {
        this.setState({ loading: false });
        let annotations = [];
        let reportAnnotations = [];
        let promiseList = [];
        let i = 0;
        const ami = () => {
            whereami().then(e => {
                let latitude = e.latitude;
                let longitude = e.longitude;
                // 用户当前位置
                this.setState({
                    region: {
                        latitude: latitude,
                        longitude: longitude,
                        latitudeDelta: 0.05,
                        longitudeDelta: 0.05
                    }
                });
            });
        };

        return getUserInfo()
            .then(userInfo => {
                let { token } = userInfo;
                let reportLat = 0;
                let reportLong = 0;
                let area_code = userInfo['zone_ids'];
                //维修地图
                promiseList.push(fetch(`${apibase}/hx/list`, {
                    method: 'POST',
                    body: { token, area_code }
                }).then(respones => {
                    if (respones['code'] != 0 || respones['data']['list'].length == 0) {
                        ami();
                        return [];
                    } else {
                        respones['data']['list'].map((item) => {
                            let icon = StaticImage['repairIcon'];
                            let size = { width: 30, height: 38 };
                            reportAnnotations.push({
                                key: `${i++}`,
                                id: item.id,
                                latitude: parseFloat(item.lat),
                                longitude: parseFloat(item.lng),
                                icon: icon,
                                size: size,
                            });
                            reportLat += parseFloat(item.lat);
                            reportLong += parseFloat(item.lng);
                        });
                        reportLat /= reportAnnotations.length;
                        reportLong /= reportAnnotations.length;
                        ami();
                        // this.setState({region: {latitude: reportLat, longitude: reportLong, latitudeDelta: 0.05, longitudeDelta: 0.05}});
                        return reportAnnotations;
                    }
                }));
                Promise.all(promiseList).then(respones => {
                    let annotations = respones[0];
                    this.setState({ annotations });
                });

            });
    }

    onLocationChange(e) {
    }

    render() {
        // if (this.state.loading)
        //     return this.renderLoading();
        return this.renderMapPage();
    }

    handleBack() {
        JumpBack();
    }

    /**
     * 点击小图标
     */
    onAnnotationClicked(key) {
        let currentAnnotationInfo = this.state.annotations[key];
        JumpTo('hx/detail', true, { id: currentAnnotationInfo.id });


    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="找车地图" handleBack={this.handleBack.bind(this)}>
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>
                <base.View style={{ position: 'relative', flexGrow: 1 }}>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='loading' />
                        <base.HorizontalLayout style={{ marginTop: 10 }}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }

    renderMapPage() {
        return (
            <base.View style={styles.container} className="wrap">

                <ActionBar title="找车地图" handleBack={this.handleBack.bind(this)}>
                    <Text style={{ fontSize: 15, color: '#000' }}>返回</Text>
                </ActionBar>
                <base.VerticalLayout style={{ flex: 1 }}>
                    {this.renderMap()}
                </base.VerticalLayout>

            </base.View>
        );
    }

    renderMap() {
        return this.state.loading ? null : (<Map style={{ flex: 1 }} region={this.state.region} showsUserLocation={true}
            annotations={this.state.annotations}
            onLocationChange={this.onLocationChange.bind(this)}
            onAnnotationClicked={this.onAnnotationClicked.bind(this)} />);
    }

    renderPane() {
        let pane = [
            {
                text: '维修地图', style: {
                    backgroundColor: '#fff',
                    borderTopLeftRadius: 6,
                    borderBottomLeftRadius: 6
                }
            }
        ];
        return (
            <base.HorizontalLayout style={styles.paneInner}>
                {
                    pane.map((pane, index) => {
                        let focusStyle = {
                            backgroundColor: '#fff',

                        };
                        if (index === this.state.selectedPaneIndex) {
                            focusStyle = {
                                backgroundColor: '#ffd900'
                            };
                        }
                        return (
                            <base.HorizontalLayout
                                key={index}
                                style={{ flex: 1, justifyContent: 'center', ...pane.style, ...focusStyle }}>
                                <base.TouchableHighlight
                                    activeOpacity={.1}
                                    underlayColor={'#fff'}
                                    style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                                    <base.HorizontalLayout style={{ justifyContent: 'center' }}>
                                        <Text>{pane.text}</Text>
                                    </base.HorizontalLayout>
                                </base.TouchableHighlight>
                            </base.HorizontalLayout>
                        );
                    })
                }
            </base.HorizontalLayout>);
    }

}
export default pageWrapper(HxMapIndex);
const styles = {
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff'
    },
    topNav: {
        height: 44,
        backgroundColor: '#fff',
        position: 'relative',
        borderBottomColor: '#ddd',
        borderBottomWidth: 1,
        borderBottomStyle: 'solid',
        flexShrink: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    vcbox: {
        alignItems: 'center',
        height: 44,
        position: 'absolute',
        left: 0,
        justifyContent: 'center',
    },
    backBtn: {
        paddingLeft: 10,
        paddingRight: 15,
        height: 44,
        alignItems: 'center',
        justifyContent: 'center',
    },
    arrow: {
        width: 10,
        height: 16
    },
    paneInner: {
        width: 250,
        height: 30,
        borderRadius: 6,
        overflow: 'hidden',
        borderStyle: 'solid',
        borderColor: '#ffd900',
        borderBottomWidth: 1,
        borderTopWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        backgroundColor: '#ffd900'
    },
};

