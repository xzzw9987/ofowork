import React, { Component } from 'react';
import { Text, View, Br, Button, ListView, List, Toast, ScrollView, TouchableHighlight, ActivityIndicator, HorizontalLayout, VerticalLayout, Image } from '../../base';
import { ActionBar } from '../../components/actionbar';
import WorkBench from '../../components/workBench';

export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <View style={styles.container}>
                <ActionBar title="工作台"></ActionBar>
                <VerticalLayout style={styles.jump}>
                    <HorizontalLayout onLayout={e => this.setState({ bannerHeight: e.nativeEvent.layout.width / 1.736 })} style={styles.bannerContainer}>
                        <Image className="fit" src="hxBanner2" style={{ ...styles.banner, height: this.state.bannerHeight || null }}></Image>
                    </HorizontalLayout>
                    <WorkBench buttonNum={2} showTitle={false} {...this.props}/>
                </VerticalLayout>
            </View>
        );
    }
}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#fff',
    },
    jump: {
        borderTopStyle: 'solid',
        borderTopColor: '#d8d8d8',
        borderTopWidth: 1,
        position: 'relative',
        flexGrow: 1,
    },
    bannerContainer: {
        alignItems: 'center'
    },
    banner: {
        flexGrow: 1,
        flexBasis: 0,
    }
};
