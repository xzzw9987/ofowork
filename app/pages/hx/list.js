import React, {Component} from 'react';
import * as base from '../../base';
import {api, keys, loadToken, getDateDiff, _loadToken, numberSplit} from '../../runtime/api';
import {Text, View, Br, Button, Tabs, TabPane, ScrollView, Toast, TouchableHighlight, ActivityIndicator} from '../../base';
import {connect} from 'react-redux';
import {JumpTo, historyStack, JumpBack} from '../../modules';
import styles from '../../css/repairer/indexCss';
import pageWrapper from '../../modules/pageWrapper';
import Dimensions from '../../modules/dimensions';
import ListView from '../../components/ListView';
import RefreshControl from '../../components/refreshControl';
import Storage from '../../base/storage';
import {ActionBar} from '../../components/actionbar';
var {height, width} = Dimensions.get('window');
var moment = require('moment');
import {
    fetchHxList
} from '../../runtime/action';
import {hxList} from '../../runtime/reducer';
/**
 * 推荐车辆
 */

class HxList extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            loading: true,
            selected: [],
            isRefreshing: false,
        };
    }


    componentWillMount() {
    }

    viewWillAppear() {
        this.props.fetch().then(()=>this.setState({loading: false}));
    }

    viewWillDisappear(){
        Storage.setItem('HxList', JSON.stringify(this.state.selected));
    }
    handleBack() {
        JumpBack();
    }
    render() {
        if (this.state.loading)
            return this.renderLoading();
        return this.renderRepairerPage();
    }

    renderLoading() {
        return (
            <base.View style={styles.container}>
                <ActionBar title="推荐车辆" handleBack={this.handleBack.bind(this)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                <base.View style={{position: 'relative', flexGrow: 1}}>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='loading'/>
                        <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            </base.View>
        );
    }
    renderRepairerPage() {
        return (

            <base.View style={styles.container} className="wrap">
                <ActionBar title="推荐车辆" handleBack={this.handleBack.bind(this)}>
                    <Text style={{fontSize: 15, color: '#000'}}>返回</Text>
                </ActionBar>
                {this.renderList()}
            </base.View>
        );
    }
    makeRefreshControl(schoolId){
        return <RefreshControl
            refreshing={this.state.isRefreshing}
            onRefresh={()=>{
                this.setState({isRefreshing: true});
                this.props.fetch().then(()=>{this.setState({isRefreshing: false});Toast.success('刷新成功', 1); });
            }}
        />;
    }
    renderDot(item){
        return this.state.selected.indexOf(item.id) > -1 ? null: (
             <base.VerticalLayout style={styles.yuan}></base.VerticalLayout>
        );
    }

    renderList() {
        let list = this.props.list;
        if( !list ){
            return (
                <base.View style={{position: 'relative', flexGrow: 1}}>
                    <base.View style={{
                        position: 'absolute',
                        flexDirection: 'column',
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size='large' src='loading'/>
                        <base.HorizontalLayout style={{marginTop: 10}}><Text>正在加载 ...</Text></base.HorizontalLayout>
                    </base.View>
                </base.View>
            );
        }
        if (list && list.length === 0){
            return (
                <base.VerticalLayout style={{flexGrow: 1, flexBasis: 0, justifyContent: 'center', alignItems: 'center'}}>
                    <base.Image style={{width: 70, height: 70, marginBottom: 5}} src='noData'/>
                    <base.Text style={{fontSize: 16, color: '#666'}}>暂无数据</base.Text>
                </base.VerticalLayout>
            );
        }
        return (
            <base.VerticalLayout style={{flexGrow: 1, flexBasis: 0}}>

                <ListView
                    dataSource={list}
                    refreshControl={this.makeRefreshControl(this.props.schoolId)}
                    renderRow={(v, index)=> {
                        return (
                            <base.TouchableHighlight key={index} onPress={()=>{
                                let arr = this.state.selected;
                                arr.push(v.id);
                                this.setState({selected: arr});
                                v.onPress && v.onPress(v);}}
                                                        activeOpacity={.1}
                                                        underlayColor={'#fff'}
                                                        style={styles.itemInfo}>

                                <base.HorizontalLayout style={width <= 320 ? {marginLeft: -10, ...styles.item} :styles.item}>
                                    <base.HorizontalLayout style={width <= 320 ? styles.smallBox :styles.imgBox}>
                                        <base.Image style={{width: 81, height: 81}} src={v.img_path ? `${v.img_path}?x-oss-process=image/resize,h_80`:'defaultImage'
                                    }/>
                                    </base.HorizontalLayout>
                                    <base.VerticalLayout style={{flex: 1}}>
                                        <base.Text style={{fontSize: 16}}>车牌 {v.bicycle_no === '-----' ? '无' : v.bicycle_no}</base.Text>
                                        <base.HorizontalLayout style={{marginTop: 10, marginBottom: 5, flexWrap: 'wrap'}}>
                                            {v.reason.map((r, index)=> {
                                                return (
                                                    <base.HorizontalLayout key={index} style={styles.reason}>
                                                        <base.Text style={styles.reasonItem}>{r}</base.Text>
                                                    </base.HorizontalLayout>
                                                );
                                            })}
                                        </base.HorizontalLayout>
                                        <base.Text style={{fontSize: 14}}>{v.address}</base.Text>
                                        <base.HorizontalLayout style={{alignItems: 'center', marginTop: 10}}>
                                            {v.distance <= 1000 ?
                                                <base.HorizontalLayout style={styles.jin}>
                                                    <base.Text style={{fontSize: 14}}>近</base.Text>
                                                </base.HorizontalLayout> : null
                                            }

                                            {
                                                v.distance == null ?
                                                    (<base.Text style={{fontSize: 14}}>距离未知</base.Text>) :(<base.Text style={{fontSize: 14}}>距离{v.distance}米</base.Text>)
                                            }
                                        </base.HorizontalLayout>
                                    </base.VerticalLayout>
                                    <base.HorizontalLayout style={styles.itemHint}>
                                        <base.Text style={{color: '#999', fontSize: 14}}>
                                            {getDateDiff(v.time)}
                                        </base.Text>
                                            { this.renderDot(v) }

                                    </base.HorizontalLayout>
                                </base.HorizontalLayout>
                            </base.TouchableHighlight>
                        );
                    }} className="overflowY"/>

            </base.VerticalLayout>
        );

    }
}

const mapStateToProps = state=> {
    return {
        list: beautifyList(state.api.hxList) || null
    };
};
const mapDispatchToProps = dispatch=> {
    return {
        dispatch,
        fetch: props=>dispatch(fetchHxList(props))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(pageWrapper(HxList));
function beautifyList(list) {

    if (!list)
        return null;

    return list.map(v=> {
        return {
            ...v,
            onPress(){

                /* JumpTo Detail */
                JumpTo('hx/detail', true, {id: v.id});
            }
        };
    });
}



