import React, { Component } from 'react';
import * as base from '../../base';
import { api, keys, parseNum, apibase, showWeekCN, getUserInfo } from '../../runtime/api';
import { Text, View, Br, Button, ScrollView, TouchableHighlight, ActivityIndicator, Toast } from '../../base';
import { connect } from 'react-redux';
import { JumpTo, JumpBack, historyStack } from '../../modules';
import { ActionBar } from '../../components/actionbar';
import {fetch} from '../../modules';

/**
 * 创建记录
 */
export default class CreateRecord extends Component {

    constructor(...args){
        super(...args);
        this.state = {
            carno: ''
        };
    }
    componentWillMount() {


    }

    /**
     * 返回
     */
    handleGoBack() {
        JumpBack();
    }
    /**
     * 创建
     */
    createDetail(type){
        if(type == 'ok'){
            if( this.state.carno == '' || !this.valid(this.state.carno)){
                Toast.fail('请输入正确的车牌号', 1);
                return;
            }
        }
        JumpTo('hx/createDetail', true, {
            carno: type == 'ok' ? this.state.carno : ''
        });
        this.setState({carno: ''});
    }

    valid(text) {
        return /^\d{4,11}$/.test(text);
    }

    render(){
        return (
            <base.View style={styles.container}>
                <ActionBar title="创建记录" handleBack={this.handleGoBack.bind(this)}>返回</ActionBar>
                <base.VerticalLayout style={styles.wrap}>
                    <base.HorizontalLayout style={styles.inputBox}>
                        <base.Text>车牌号</base.Text>
                        <base.TextInput className='height58' keyboardType="numeric" style={styles.textInput}  placeholder='请输入车牌号' value={this.state.carno} onChangeText={(text) => { this.setState({ carno: text }); } } />
                    </base.HorizontalLayout>
                    <base.HorizontalLayout style={styles.btnBox}>
                        <Button onPress={this.createDetail.bind(this, 'ok')} style={styles.btn}>
                            <base.Text style={{color: '#fff'}}>确定</base.Text>
                        </Button>
                        <Button onPress={this.createDetail.bind(this, 'not')} style={styles.btn}>
                            <base.Text style={{color: '#fff'}}>无车牌号</base.Text>
                        </Button>
                    </base.HorizontalLayout>

                </base.VerticalLayout>
            </base.View>
        );
    }


}

const styles = {
    container: {
        flexGrow: 1,
        flexDirection: 'column',
    /*justifyContent: 'center',*/
        alignItems: 'stretch',
        backgroundColor: '#F6F6F6',
    },
    wrap: {
        flexGrow: 1
    },
    inputBox: {
        marginTop: 30,
        marginBottom: 50,
        height: 60,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 25,
        paddingRight: 25,
        borderStyle: 'solid',
        borderColor: '#d6d6d6',
        borderBottomWidth: 1,
        borderTopWidth: 1,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        backgroundColor: '#fff'
    },
    textInput: {
        marginLeft: 20,
        height: 60,
        flexGrow: 1,
        alignItems: 'center',
    },
    btnBox: {
        height: 50,
        paddingLeft: 25,
        paddingRight: 25,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    btn: {
        width: 145,
        height: 36,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 6,
        backgroundColor: '#333'
    },
};
