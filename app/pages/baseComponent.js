import * as base from '../base';
import { api, keys } from '../runtime/api';
import { connect } from 'react-redux';
import { JumpTo } from '../modules';

import Storage from '../base/storage';

export default class BaseComponent {

    saveToken(value) {
        Storage.setItem('token', value).then(() => {
            console.log('token saved', value);
        });
    }

    getToken() {
        Storage.getItem('token').then((data) => {
            if (data) {
                console.log('get token', data);
                return data;
            } else {
                Storage.setItem('token', 'value').then(() => {
                    console.log('item saved');
                });
            }
        });
    }

}

