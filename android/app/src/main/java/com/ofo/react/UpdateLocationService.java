package com.ofo.react;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;

import com.ofo.LogToFile;
import com.ofo.MainActivity;
import com.ofo.R;

/**
 * Created by xinzhongzhu on 17/6/26.
 */

public class UpdateLocationService extends Service {
    private UpdateLocationBackground mBackground;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("start_command", "_");
        super.onStartCommand(intent, flags, startId);
        String token = intent.getStringExtra("token");
        String wsURL = intent.getStringExtra("wsURL");
        Log.d("start_service", "_");
        LogToFile.init(this);
        mBackground = new UpdateLocationBackground(this, token, wsURL);

        Intent i =  new Intent(this, MainActivity.class);
        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentIntent(PendingIntent.getActivity(this, 0, i, 0))
            .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher))
                .setContentTitle("您被跟踪了!")
                .setContentText("ofo work正在监视您的位置")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setWhen(System.currentTimeMillis());
        Notification notification = builder.build();
        notification.defaults = Notification.DEFAULT_SOUND;
        startForeground(9090, notification);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void restart() {
        PendingIntent service = PendingIntent.getService(
                this,
                9090,
                new Intent(this, UpdateLocationService.class),
                PendingIntent.FLAG_ONE_SHOT
        );
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, 1000, service);
        Log.d("Remove_Service  restart", "_");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.d("Remove_Service", "_");
        restart();
    }

//    @Override
//    public void onTrimMemory(int level) {
//        super.onTrimMemory(level);
//        restart();
//    }
}
