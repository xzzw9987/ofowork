package com.ofo.react;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.util.Log;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.ofo.WebSocketClient;

import java.net.URI;

/**
 * Created by xinzhongzhu on 17/4/22.
 */

public class ReactDeviceManager extends ReactContextBaseJavaModule {
    private ReactApplicationContext context = null;
    private UpdateLocationBackground u;

    public ReactDeviceManager(ReactApplicationContext reactContext) {
        super(reactContext);
        context = reactContext;
    }

    @Override
    public String getName() {
        return "DeviceManager";
    }

    @ReactMethod
    public void getDeviceID(final Callback callback) {

        String address = Settings.Secure.getString(
                context.getContentResolver(),
                Settings.Secure.ANDROID_ID
        );
        callback.invoke(address);
    }

    @ReactMethod
    public void startUploadLocation(String token, final String wsURL) {
//        u = new UpdateLocationBackground(context, token, wsURL);
//        return ;
        if(!isMyServiceRunning(UpdateLocationService.class)) {
            Intent intent = new Intent(context, UpdateLocationService.class);
            intent.putExtra("token", token);
            intent.putExtra("wsURL", wsURL);
            Log.d("will_start", "_");
            context.startService(intent);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo serviceInfo : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(serviceInfo.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
