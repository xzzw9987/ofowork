package com.ofo.react;
import android.content.Intent;
import android.support.annotation.Nullable;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.Callback;

import android.util.Log;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.shell.MainReactPackage;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import android.app.Activity;

public class OfoReactModules extends ReactContextBaseJavaModule {

    protected ArrayList<Activity> activities = new ArrayList<Activity>();

    public OfoReactModules(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "OFOAndroid";
    }

    @ReactMethod
    public void fetch(String url, @Nullable ReadableMap options, Callback resolve, Callback reject) {

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);
        try {
            httppost.setHeader("Content-Type","application/x-www-form-urlencoded;charset=UTF-8");
            httppost.setEntity(new StringEntity(options.getString("body")));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            reject.invoke("ops,it fail!,UnsupportedEncodingException");
        }
        HttpResponse response = null;
        try {
            response = httpclient.execute(httppost);
        } catch (IOException e) {
            e.printStackTrace();
            reject.invoke("ops,it fail!,IOException");
        }
        HttpEntity httpEntity = response.getEntity();
        try
        {
            InputStream inputStream = httpEntity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    inputStream));
            String result = "";
            String line = "";
            while (null != (line = reader.readLine()))
            {
                result += line;

            }
            resolve.invoke(result);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            reject.invoke("ops,it fail!");
        }
        return;
    }
}