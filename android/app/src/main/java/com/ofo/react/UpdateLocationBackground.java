package com.ofo.react;

import android.content.Context;
import android.util.Log;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.ofo.LogToFile;
import com.ofo.WebSocketClient;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Created by xinzhongzhu on 17/6/6.
 */

public class UpdateLocationBackground {
    private String mToken = "";
    private String mWsURL = "";
    private AMapLocation lastLocation = null;
    private int mSequence = 0;
    private Context mContext;
    private WebSocketClient mSocket;
    private AMapLocationClient mAmapLocationClient;
    private AMapLocationListener mLocationListener;
    private Boolean isConnected = false;
    private List<HashMap<String, Object>> locCoords = null;

    public UpdateLocationBackground(Context context, String token, String url) {
        mToken = token;
        mWsURL = url;
        mContext = context;
        locCoords = new ArrayList<>();
        makeSequence();
        makeSocket();
        makeLocationClient();
    }

    private void makeSequence() {
        String str = readFromFile(mContext);
        if (str != null) mSequence = Integer.parseInt(str);
    }

    private void send(HashMap<String, Object> dict) {
        if (!isConnected) {
            locCoords.add(dict);
            mSocket.connect();
        } else {
            JSONObject obj = new JSONObject(dict);
            LogToFile.i("LOCATION_INFO", obj.toString());
            mSocket.send(obj.toString());

            // Save file
            int seq = (int) dict.get("sequence_id");
            writeToFile(String.valueOf(seq), mContext);
        }
    }

    private void makeLocationClient() {
        mAmapLocationClient = new AMapLocationClient(mContext);
        Log.d("make_location_client", "_");
        mLocationListener = new AMapLocationListener() {
            @Override
            public void onLocationChanged(AMapLocation loc) {
                if (loc != null) {
                    HashMap<String, Object> dict = new HashMap<String, Object>();

                    float distance = 0;
                    if (lastLocation != null) {
                        distance = AMapUtils.calculateLineDistance(
                                new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()),
                                new LatLng(loc.getLatitude(), loc.getLongitude())
                        );
                    }
                    lastLocation = loc;
                    dict.put("token", mToken);
                    dict.put("platform", "android");
                    dict.put("lat", loc.getLatitude());
                    dict.put("lng", loc.getLongitude());
                    dict.put("accuracy", loc.getAccuracy());
                    dict.put("time", (int)(System.currentTimeMillis() / 1000));
                    dict.put("distance", distance);
                    dict.put("sequence_id", ++mSequence);
                    Log.d("latitude", loc.getLatitude() + "");
                    send(dict);
                }
            }
        };

        mAmapLocationClient.setLocationListener(mLocationListener);
        mAmapLocationClient.setLocationOption(getDefaultOption());
        mAmapLocationClient.startLocation();

    }

    private AMapLocationClientOption getDefaultOption() {
        AMapLocationClientOption mOption = new AMapLocationClientOption();
        mOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);//可选，设置定位模式，可选的模式有高精度、仅设备、仅网络。默认为高精度模式
        mOption.setGpsFirst(true);//可选，设置是否gps优先，只在高精度模式下有效。默认关闭
        mOption.setHttpTimeOut(30000);//可选，设置网络请求超时时间。默认为30秒。在仅设备模式下无效
        mOption.setInterval(2000);//可选，设置定位间隔。默认为2秒
        mOption.setNeedAddress(false);//可选，设置是否返回逆地理地址信息。默认是true
        mOption.setLocationCacheEnable(false);
        AMapLocationClientOption.setLocationProtocol(AMapLocationClientOption.AMapLocationProtocol.HTTP);//可选， 设置网络请求的协议。可选HTTP或者HTTPS。默认为HTTP
        mOption.setSensorEnable(true);//可选，设置是否使用传感器。默认是false
        return mOption;
    }

    private void makeSocket() {
        mSocket = new WebSocketClient(URI.create(mWsURL), new WebSocketClient.Listener() {
            @Override
            public void onConnect() {
                Log.d("Connect to ", mWsURL);
                isConnected = true;

                for(HashMap<String, Object> dict: locCoords) {
                    send(dict);
                }
            }

            @Override
            public void onMessage(String message) {
                Log.d("Message ", message);
            }

            @Override
            public void onMessage(byte[] data) {

            }

            @Override
            public void onDisconnect(int code, String reason) {
                isConnected = false;
            }

            @Override
            public void onError(Exception error) {

            }
        }, null);

        mSocket.connect();
    }

    private void writeToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("config.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private String readFromFile(Context context) {
        String ret = null;
        try {
            InputStream inputStream = context.openFileInput("config.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

}
