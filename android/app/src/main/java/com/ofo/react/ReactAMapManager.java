package com.ofo.react;

import android.support.annotation.Nullable;


import com.amap.api.maps.AMap;
import com.amap.api.maps.MapView;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

public class ReactAMapManager extends SimpleViewManager<ReactAMapView> {

    public static final String REACT_CLASS = "ReactAMapView";


    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @ReactProp(name = "region")
    public void setRegion(final ReactAMapView v, @Nullable final ReadableMap region) {
        if (region != null) {
            v.setRegion(region);
        }
    }

    @ReactProp(name = "defaultRegion")
    public void setDefaultRegion(final ReactAMapView v, @Nullable final ReadableMap region) {
        if (region != null) {
            v.setDefaultRegion(region);
        }
    }


    @ReactProp(name = "showsUserLocation", defaultBoolean = false)
    public void setShowsUserLocation(MapView v, boolean showsUserLocation) {
        AMap map = v.getMap();
        map.setMyLocationEnabled(showsUserLocation);
    }

    @ReactProp(name = "zoomEnabled", defaultBoolean = true)
    public void setZoomEnabled(MapView v, boolean zoomEnabled) {
        AMap map = v.getMap();
        map.getUiSettings().setZoomControlsEnabled(zoomEnabled);
        map.getUiSettings().setZoomGesturesEnabled(zoomEnabled);
    }

    @ReactProp(name = "showsUserLocation", defaultBoolean = false)
    public void setShowsUserLocation(ReactAMapView v, boolean showsUserLocation) {
        v.setShowsUserLocation(showsUserLocation);
    }

    @ReactProp(name = "zoomEnabled", defaultBoolean = true)
    public void setZoomEnabled(ReactAMapView v, boolean zoomEnabled) {
        v.setZoomEnabled(zoomEnabled);
    }

    @ReactProp(name = "clusterMode", defaultBoolean = false)
    public void setClusterMode(ReactAMapView v, boolean clusterMode) {
        v.setClusterMode(clusterMode);
    }

    @ReactProp(name = "annotations")
    public void setAnnotations(final ReactAMapView v, @Nullable final ReadableArray annotations) {
        if (annotations != null) {
            v.setAnnotations(annotations);
        }
    }

    @ReactProp(name = "hotSpots")
    public void setHotSpots(final ReactAMapView v, @Nullable final ReadableArray hotSpots) {
        if (hotSpots != null) {
            v.setHotSpots(hotSpots);
        }
    }

    @ReactProp(name = "lines")
    public void setLines(final ReactAMapView v, @Nullable final ReadableArray lines) {
        if (lines != null) {
            v.setLines(lines);
        }
    }

    @Override
    public ReactAMapView createViewInstance(final ThemedReactContext context) {
        ReactAMapView mapView = new ReactAMapView(context);
        mapView.onCreate(null);
        return mapView;
    }
}
