/**
 * Created by xinzhongzhu on 17/4/21.
 */

package com.ofo.react;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptor;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.Gradient;
import com.amap.api.maps.model.HeatmapTileProvider;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.MyLocationStyle;
import com.amap.api.maps.model.Polyline;
import com.amap.api.maps.model.PolylineOptions;
import com.amap.api.maps.model.Text;
import com.amap.api.maps.model.TextOptions;
import com.amap.api.maps.model.TileOverlayOptions;
import com.amap.api.maps.model.WeightedLatLng;
import com.facebook.common.executors.UiThreadImmediateExecutorService;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.common.Priority;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableMapKeySetIterator;
import com.facebook.react.bridge.ReadableType;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.events.RCTEventEmitter;
import com.ofo.MainApplication;
import com.ofo.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ReactAMapView extends MapView implements AMap.OnMyLocationChangeListener, AMap.OnCameraChangeListener {
    private ReactContext mReactContext = null;
    private HeatmapTileProvider.Builder heatMapBuilder = null;
    private Boolean hasTriggerLocationChange = false;
    private Boolean hasSetDefaultRegion = false;
    private Boolean mClusterMode = false;
    private Boolean mHasSetClusterMode = false;
    private ReadableArray mAnnotations = null;
    private ReadableArray mHostSpots = null;
    private ReadableArray mLines = null;
    private Polyline mPolyline = null;
    private float mZoomLevel = -1;
    private float imgScale = 0.8f;

    public ReactAMapView(ReactContext context) {
        super(MainApplication.getAppContent());
        mReactContext = context;
        MyLocationStyle myLocationStyle = new MyLocationStyle();
        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_SHOW);
        myLocationStyle.myLocationIcon(BitmapDescriptorFactory.
                fromResource(R.drawable.gps_point));
        this.getMap().setMyLocationStyle(myLocationStyle);
        this.getMap().getUiSettings().setMyLocationButtonEnabled(true);// 设置默认定位按钮是否显示
        this.getMap().setMyLocationEnabled(true);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
        this.getMap().setOnMyLocationChangeListener(this);
        this.getMap().setOnCameraChangeListener(this);
    }


    @Override
    public void onMyLocationChange(Location location) {
        if (location != null && !hasTriggerLocationChange) {
            hasTriggerLocationChange = true;
            final WritableMap event = Arguments.createMap();
            event.putString("type", "locationChange");
            event.putDouble("latitude", location.getLatitude());
            event.putDouble("longitude", location.getLongitude());
            event.putDouble("accuracy", location.getAccuracy());
            mReactContext.getJSModule(RCTEventEmitter.class).receiveEvent(this.getId(), "topChange", event);
        }
    }

    @Override
    public void onCameraChange(CameraPosition camPos) {

    }

    @Override
    public void onCameraChangeFinish(CameraPosition camPos) {
        final WritableMap event = Arguments.createMap();
        AMap aMap = this.getMap();
        event.putString("type", "cameraChange");
        event.putDouble("latitude", camPos.target.latitude);
        event.putDouble("longitude", camPos.target.longitude);
        event.putDouble("latitudeDelta",
                Math.abs(.5 * (
                        aMap.getProjection().getVisibleRegion().farLeft.latitude -
                                aMap.getProjection().getVisibleRegion().nearLeft.latitude
                )));
        event.putDouble("longitudeDelta",
                Math.abs(.5 * (
                        aMap.getProjection().getVisibleRegion().farLeft.longitude -
                                aMap.getProjection().getVisibleRegion().farRight.longitude
                )));
        mReactContext.getJSModule(RCTEventEmitter.class).receiveEvent(this.getId(), "topChange", event);
        mZoomLevel = camPos.zoom;
        if (mClusterMode) {
            setAnnotations(mAnnotations);
            Log.d("farLeft",
                    Double.toString(aMap.getProjection().getVisibleRegion().farLeft.latitude)
                            + ',' +
                            Double.toString(aMap.getProjection().getVisibleRegion().farLeft.longitude));

            Log.d("nearRight",
                    Double.toString(aMap.getProjection().getVisibleRegion().nearRight.latitude)
                            + ',' +
                            Double.toString(aMap.getProjection().getVisibleRegion().nearRight.longitude));
        }
    }

    public void setRegion(@Nullable final ReadableMap region) {
        final ReactAMapView _this = this;
        final Runnable actualUpdate = new Runnable() {
            @Override
            public void run() {
                final AMap map = _this.getMap();
                final double latitude = region.getDouble("latitude");
                final double longitude = region.getDouble("longitude");
                final double latitudeDelta = region.getDouble("latitudeDelta");
                final double longitudeDelta = region.getDouble("longitudeDelta");
                map.animateCamera(CameraUpdateFactory.newLatLngBounds(new LatLngBounds(
                        new LatLng(latitude - latitudeDelta, longitude - longitudeDelta),
                        new LatLng(latitude + latitudeDelta, longitude + longitudeDelta)), 10)
                );
            }
        };
        if (region != null) {
            if (this.getWidth() <= 0 && this.getHeight() <= 0) {
                this.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        actualUpdate.run();
                        _this.removeOnLayoutChangeListener(this);
                    }
                });
            } else {
                actualUpdate.run();
            }
        }
    }

    public void setDefaultRegion(@Nullable final ReadableMap region) {
        if (!hasSetDefaultRegion) {
            hasSetDefaultRegion = true;
            setRegion(region);
        }
    }

    public void setShowsUserLocation(boolean showsUserLocation) {
        AMap map = this.getMap();
        map.setMyLocationEnabled(showsUserLocation);
    }

    public void setZoomEnabled(boolean zoomEnabled) {
        AMap map = this.getMap();
        map.getUiSettings().setZoomControlsEnabled(zoomEnabled);
        map.getUiSettings().setZoomGesturesEnabled(zoomEnabled);
    }

    public void setClusterMode(boolean clusterMode) {
        AMap map = this.getMap();
        if (mHasSetClusterMode && mClusterMode != clusterMode) {
            mClusterMode = clusterMode;
            this.setAnnotations(mAnnotations);
        }
        mHasSetClusterMode = true;
        mClusterMode = clusterMode;
    }

    public ReadableArray processAnnotations(final ReadableArray annotations) {
        final int size = annotations.size();
        if (size == 0 || !mClusterMode || mZoomLevel == -1) {
            return annotations;
        }
        int gridCount = this.gridCount(mZoomLevel);
        Log.d("gridCount", Integer.toString(gridCount));
        if (gridCount == 9999) {
            return clipAnnotationsInView(annotations);
        }
        double minimumLongitude = 99999;
        double maximumLongitude = -99999;
        double minimumLatitude = 99999;
        double maximumLatitude = -99999;

        List<ReadableMap> copy = convertReadableArrayToList(annotations);
        List<ReadableMap> ret = new ArrayList<ReadableMap>();

        for (int i = 0; i < annotations.size(); i++) {
            double latitude = annotations.getMap(i).getDouble("latitude");
            double longitude = annotations.getMap(i).getDouble("longitude");

            if (latitude > maximumLatitude) maximumLatitude = latitude;
            if (latitude < minimumLatitude) minimumLatitude = latitude;
            if (longitude > maximumLongitude) maximumLongitude = longitude;
            if (longitude < minimumLongitude) minimumLongitude = longitude;
        }

        double bias = 0.0005;
        maximumLatitude += bias;
        minimumLatitude -= bias;
        maximumLongitude += bias;
        minimumLongitude -= bias;

        double longitudeDelta = (maximumLongitude - minimumLongitude) / gridCount;
        double latitudeDelta = (maximumLatitude - minimumLatitude) / gridCount;

        for (int i = 0; i < gridCount; i++) {
            for (int j = 0; j < gridCount; j++) {
                double targetLatitude = minimumLatitude + (i + .5f) * latitudeDelta;
                double targetLongitude = minimumLongitude + (j + .5f) * longitudeDelta;

                List<ReadableMap> t = new ArrayList<ReadableMap>();
                for (int k = 0; k < copy.size(); k++) {
                    double latitude = copy.get(k).getDouble("latitude");
                    double longitude = copy.get(k).getDouble("longitude");

                    boolean inGrid = Math.abs(latitude - targetLatitude) <= .5 * latitudeDelta
                            && Math.abs(longitude - targetLongitude) <= .5 * longitudeDelta;

                    if (inGrid) {
                        t.add(copy.get(k));
                        copy.remove(k);
                        k--;
                    }
                }

                if (t.size() > 0) {
                    double latitude = 0;
                    double longitude = 0;
                    for (int k = 0; k < t.size(); k++) {
                        ReadableMap map = t.get(k);
                        latitude += map.getDouble("latitude") / t.size();
                        longitude += map.getDouble("longitude") / t.size();
                    }
                    final double _latitude = latitude;
                    final double _longitude = longitude;
                    final String _size = Integer.toString(t.size());
                    ret.add(new ReadableMap() {
                        @Override
                        public boolean hasKey(String name) {
                            return name.equals("latitude")
                                    || name.equals("longitude")
                                    || name.equals("image")
                                    || name.equals("key")
                                    || name.equals("number")
                                    || name.equals("title");
                        }

                        @Override
                        public boolean isNull(String name) {
                            return false;
                        }

                        @Override
                        public boolean getBoolean(String name) {
                            return false;
                        }

                        @Override
                        public double getDouble(String name) {
                            switch (name) {
                                case "latitude":
                                    return _latitude;
                                case "longitude":
                                    return _longitude;
                                default:
                                    return 0;
                            }
                        }

                        @Override
                        public int getInt(String name) {
                            return 0;
                        }

                        @Override
                        public String getString(String name) {
                            switch (name) {
                                case "key":
                                    return null;
                                case "image":
                                    return "cluster_icon";
                                case "number":
                                    return _size;
                                default:
                                    return "";
                            }
                        }

                        @Override
                        public ReadableArray getArray(String name) {
                            return null;
                        }

                        @Override
                        public ReadableMap getMap(String name) {
                            return null;
                        }

                        @Override
                        public ReadableType getType(String name) {
                            return null;
                        }

                        @Override
                        public ReadableMapKeySetIterator keySetIterator() {
                            return null;
                        }
                    });
                }
            }
        }
        return convertListToReadableArray(ret);
    }

    public ReadableArray clipAnnotationsInView(ReadableArray array) {
        AMap aMap = this.getMap();
        List<ReadableMap> list = new ArrayList<ReadableMap>();
        for (int i = 0; i < array.size(); i++) {
            if (inBound(
                    new LatLng(array.getMap(i).getDouble("latitude"),
                            array.getMap(i).getDouble("longitude")),
                    aMap.getProjection().getVisibleRegion().farLeft,
                    aMap.getProjection().getVisibleRegion().nearRight
            )) {
                list.add(array.getMap(i));
            }
        }
        return convertListToReadableArray(list);
    }

    public boolean inBound(LatLng targetCoord, LatLng farLeft, LatLng nearRight) {
        return targetCoord.latitude >= nearRight.latitude
                && targetCoord.latitude <= farLeft.latitude
                && targetCoord.longitude >= farLeft.longitude
                && targetCoord.longitude <= nearRight.longitude;

    }

    public ReadableArray convertListToReadableArray(final List<ReadableMap> list) {
        return new ReadableArray() {
            @Override
            public int size() {
                return list.size();
            }

            @Override
            public boolean isNull(int index) {
                return false;
            }

            @Override
            public boolean getBoolean(int index) {
                return false;
            }

            @Override
            public double getDouble(int index) {
                return 0;
            }

            @Override
            public int getInt(int index) {
                return 0;
            }

            @Override
            public String getString(int index) {
                return null;
            }

            @Override
            public ReadableArray getArray(int index) {
                return null;
            }

            @Override
            public ReadableMap getMap(int index) {
                return list.get(index);
            }

            @Override
            public ReadableType getType(int index) {
                return null;
            }
        };
    }

    public List<ReadableMap> convertReadableArrayToList(final ReadableArray array) {
        List<ReadableMap> list = new ArrayList<ReadableMap>();
        int size = array.size();
        for (int i = 0; i < size; i++) {
            list.add(array.getMap(i));
        }
        return list;
    }

    public int gridCount(float zoomLevel) {
        if (zoomLevel > 17) {
            return 9999;
        }
        if (zoomLevel < 10) {
            return 1;
        }
        return (int) (.8571f * zoomLevel - 7.56f);
    }

    public void setAnnotations(@Nullable final ReadableArray annotations) {
        if (annotations != null) {
            mAnnotations = annotations;
            draw();
        }
    }

    private void drawAnnotations(@Nullable final ReadableArray annotations) {
        if (annotations != null) {
            final AMap map = this.getMap();
            final int thisID = this.getId();
            ReadableArray annos = processAnnotations(annotations);
            final int size = annos.size();
            for (int i = 0; i < size; i++) {
                ReadableMap annotation = annos.getMap(i);
                double lat = annotation.getDouble("latitude");
                double lng = annotation.getDouble("longitude");
                final MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(lat, lng));
                if (annotation.hasKey("title")) {
                    markerOptions.title(annotation.getString("title"));
//                    markerOptions = markerOptions.title(annotation.getString("title"));
                }
                if (annotation.hasKey("key")) {
                    markerOptions.snippet(annotation.getString("key"));
//                    markerOptions = markerOptions.snippet(annotation.getString("key"));
                }
                if (annotation.hasKey("image")) {
                    String uri = annotation.getString("image");
                    if (uri.startsWith("http://") || uri.startsWith("https://") ||
                            uri.startsWith("file://")) {
                        Log.d("imageName", annotation.getString("image"));
                        ImageRequest imageRequest = ImageRequestBuilder
                                .newBuilderWithSource(Uri.parse(uri))
                                .setRequestPriority(Priority.HIGH)
                                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                                .build();

                        final DataSource<CloseableReference<CloseableImage>> dataSource
                                = Fresco.getImagePipeline().fetchDecodedImage(imageRequest, this);

                        dataSource.subscribe(
                                new BaseBitmapDataSubscriber() {
                                    @Override
                                    public void onNewResultImpl(@Nullable Bitmap bitmap) {
                                        // Pass bitmap to system, which makes a copy of the bitmap.
                                        // No need to do any cleanup.
                                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(resizeBitmap(bitmap, imgScale)));
                                        markerOptions.anchor(0.5f, 0.5f);
                                        map.addMarker(markerOptions);
                                        dataSource.close();
                                    }

                                    @Override
                                    public void onFailureImpl(DataSource dataSource) {
                                        // No cleanup required here.
                                        dataSource.close();
                                    }
                                }, UiThreadImmediateExecutorService.getInstance());
                    } else {
                        markerOptions.icon(getBitmapDescriptorByName(uri));
                        markerOptions.anchor(0.5f, 0.5f);
                        map.addMarker(markerOptions)
                            .setZIndex(9);
                    }
                }

                if (annotation.hasKey("number")) {
                    map.addText(new TextOptions().position(new LatLng(lat, lng)).text(annotation.getString("number")).fontSize(40).backgroundColor(Color.TRANSPARENT).align(Text.ALIGN_CENTER_HORIZONTAL, Text.ALIGN_CENTER_VERTICAL))
                        .setZIndex(10);
                }
            }
            map.setOnMarkerClickListener(new AMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    if (marker.getSnippet() != null) {
                        final WritableMap event = Arguments.createMap();
                        event.putString("key", marker.getSnippet());
                        mReactContext.getJSModule(RCTEventEmitter.class).receiveEvent(thisID, "topSelect", event);
                        draw();
                    }
                    return true;
                }
            });
        }
    }

    public void setHotSpots(@Nullable final ReadableArray hotSpots) {
        if (hotSpots != null) {
            mHostSpots = hotSpots;
            draw();
        }
    }

    private void drawHotSpots(@Nullable final ReadableArray hotSpots) {
        if (hotSpots != null) {
            int size = hotSpots.size();
            WeightedLatLng[] latlngs = new WeightedLatLng[size];

            AMap map = this.getMap();
            for (int i = 0; i < size; i++) {
                ReadableMap hotSpot = hotSpots.getMap(i);
                latlngs[i] = new WeightedLatLng(
                        new LatLng(hotSpot.getDouble("latitude"), hotSpot.getDouble("longitude")),
                        hotSpot.getDouble("count"));
            }
            if (heatMapBuilder == null) {
                heatMapBuilder = new HeatmapTileProvider.Builder();
            }

            int[] colorMap = {Color.BLUE, Color.GREEN, Color.RED};
            float[] startPoints = {0.2f, 0.5f, 0.9f};

            heatMapBuilder.weightedData(Arrays.asList(latlngs)).gradient(new Gradient(colorMap, startPoints));
            heatMapBuilder.radius(25);
            HeatmapTileProvider heatmapTileProvider = heatMapBuilder.build();


            TileOverlayOptions tileOverlayOptions = new TileOverlayOptions();
            tileOverlayOptions.tileProvider(heatmapTileProvider);
            map.addTileOverlay(tileOverlayOptions);
        }
    }

    public void setLines(@Nullable final ReadableArray lines) {
        if (lines != null) {
            mLines = lines;
            draw();
        }
    }

    private void drawLines(@Nullable final ReadableArray lines) {
        if (lines != null) {
            List<LatLng> latLngs = new ArrayList<LatLng>();
            int size = lines.size();
//            if (mPolyline != null) mPolyline.remove();
            for (int i = 0; i < lines.size(); i++) {
                latLngs.add(new LatLng(
                        lines.getMap(i).getDouble("latitude"),
                        lines.getMap(i).getDouble("longitude")));
            }
            this.getMap()
                    .addPolyline(
                            new PolylineOptions().addAll(latLngs)
                                    .width(25)
                                    .color(Color.argb(255, 1, 255, 1))
                    );
        }
    }

    private void draw() {
        this.getMap().clear(true);
        drawAnnotations(mAnnotations);
        drawHotSpots(mHostSpots);
        drawLines(mLines);
    }

    private int getDrawableResourceByName(String name) {
        return mReactContext.getResources().getIdentifier(
                name,
                "drawable",
                mReactContext.getPackageName());
    }

    private BitmapDescriptor getBitmapDescriptorByName(String name) {
        return BitmapDescriptorFactory.fromBitmap(
                resizeBitmap(
                        BitmapFactory.decodeResource(mReactContext.getResources(),
                                getDrawableResourceByName(name))
                        , imgScale));
    }

    private Bitmap resizeBitmap(Bitmap bitmap, float scale) {
        return Bitmap.createScaledBitmap(bitmap, Math.round(scale * bitmap.getWidth()), Math.round(scale * bitmap.getHeight()), false);
    }

}
