package com.ofo;

import android.os.Bundle;
import android.util.Log;

import com.facebook.react.ReactActivity;

public class MainActivity extends BaseOFOReactActivity {

    public static Bundle mSavedInstanceState = null;
    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "work";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Log.d("onCreate", savedInstanceState.toString());
        mSavedInstanceState = savedInstanceState ;
    }

    public static Bundle getSavedInstanceState(){
        return mSavedInstanceState;
    }
}
