package com.ofo;

import android.app.Application;
import android.os.Build;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.react.ReactApplication;
import com.imagepicker.ImagePickerPackage;
import com.joshblour.reactnativepermissions.ReactNativePermissionsPackage;
import com.ofo.react.ReactDevicePackage;
import com.ofo.react.ReactLocationPackage;
import com.lwansbrough.RCTCamera.RCTCameraPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
//import com.microsoft.codepush.react.CodePush;
import com.github.alinz.reactnativewebviewbridge.WebViewBridgePackage;
import com.ofo.react.ReactAMapPackage;

import java.util.Arrays;
import java.util.List;

import com.pgyersdk.crash.PgyCrashManager;

public class MainApplication extends Application implements ReactApplication {
    private static MainApplication instance;

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        protected boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

       /* @Override
        protected String getJSBundleFile() {
            return CodePush.getJSBundleFile();
        }*/

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                    new MainReactPackage(),
                    new ReactAMapPackage(),
                    new ImagePickerPackage(),
                    new ReactNativePermissionsPackage(),
                    new RCTCameraPackage(),
                    new WebViewBridgePackage(),
                    new ReactLocationPackage(),
                    new ReactDevicePackage()/*,
                    new CodePush(BuildConfig.CODEPUSH_KEY, MainApplication.this, BuildConfig.DEBUG)*/
            );
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        PgyCrashManager.register(this);
        Fresco.initialize(this);
        instance = this;
    }

    public static MainApplication getAppContent() {
        return instance;
    }
}
