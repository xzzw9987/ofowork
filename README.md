ofo work app
The Most Awesome Project in ofo
===

## Install
1. npm install react-native -g
2. cd YourProject && npm install

## Run
- Web
npm run web

- IOS
./debug-ios.sh

- Android
./debug-android.sh

## Release
- Web
npm run webpack-production

- IOS
host="https://pyramid-api.ofo.so" ./archive-ios.sh

- Android
host="https://pyramid-api.ofo.so" ./archive-android.sh

## Structure
```
|-pages
    |-hx 合作
    |-operation 各路运营
    |-profile 个人中心
    |-repairer 修车师傅
    |-supply 供应链
|-runtime
    |-action pls assemble actions in the index.js file
    |-reducer reducers,  we wont use the reducers.js outside anymore
```

## Agreements
- if possible,pls use redux to handle ajax response which changes the state
- use import/export instead of commonJS ways for furthur update
- we recommand you to use eslint, and check the [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript/blob/master/README.md)


## Maintainers
- [FuKai](jasonfu1990.com)
- [ZouMengTing](zoumengting@ofo.so)
- [YangTian](yangtian@ofo.so)
- [XinZhongZhu](xinzhongzhu@ofo.so)

## React Dev Tools will replace file  
Path: `oforeact/node_modules/react-native/local-cli/server/middleware/getDevToolsMiddleware.js`  
