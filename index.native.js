import React, { Component } from 'react';
import { jumpConf, JumpBack } from './app/modules';
import { AppRegistry, Navigator, View, BackAndroid } from 'react-native';
import { store, wrap } from './app/runtime';
import timeoutQueue from './app/modules/timeoutQueue';
import './app/modules/toastWhenNetInfoChange';
import androidCallback from './app/modules/androidBackCallback';

var Pages = require('./app/pages');
var {
  OFO,
  Login,
  Splash,
  ProfileIndex,
  ProfileModifyPassword,
  ProfileModifyPasswordDone,
  RepairCarList,
  RepairCarDetail,
  RepairCarMap,
  RepairCarSearch,
  RepairCheckIn,
  RepairCheckRecords,
  RepairCarRecord,
  RepairBadCarMark,
  RepairBadCarMarkPos,
  RepairCarLocation,
  RepairCarSearchDone,
  OperationDashBoard,
  OperationOrderHistory,
  OperationOrderDetail,
  OperationOrderDetailCity,
  OperationOrderDetailSchool,
  OperationOrderDetailCountry,
  OperationOrderDetailRegion,
  OperationDashBoardCity,
  OperationDashBoardRegion,
  OperationDashBoardCountry,
  RepairUnlockCode,
  RepairRateList,
  OperationDashBoardDistrict,
  OperationDashBoardSchool,
  OperationDashBoardSociety,
  OperationOrderDetailSociety,
  ChainIndex,
  ChainInput,
  ChainInputNew,
  ChainSubmit,
  ChainSubmitNew,
  ChainList,
  RepairerIndex,
  ActivePanel,
  RepairReportDone,
  RepairReportDetail,
  OperationReportDetail,
  RepairerMapIndex,
  MapTest,
  RepairEntry,
  OperationReportManagement,
  ActiveInput,
  CodeReset,
  ChooseZone,
  RepairDetail,
  RepairSelectVirtualPosition,
  RepairReportVirtualPile,
  BikeIndex,
  BikeMapIndex,
  BikeDetail,
  ChainCode,
  CarSchelduling,
  TransportSub,
  FacScan,
  FacInput,
  OperationCarSearch,
  OperationCarInformation,
  OperationChangeUnlockCode,
  OperationRepairRate,
  OperationNewUsers,
  OperationEntry,
  HxEntry,
  OperationHx,
  CreateRecord,
  CreateDetail,
  WorkRecord,
  WorkRecordDetail,
  HxList,
  HxMapIndex,
  HxProcessDone,
  HxDetail,
  HxHotSpot,
  OperationChangeBrand,
  OperationBadCarMap,
  OperationBadCarDetail,
  OperationBadCarDone,
  OperationBadCarData,
  OperationBadCarMessage,
  OperationUnbindBrand,
  LowElectricCar,
  LowElectricCarDetail,
  FactoryEntry,
  FactorySnScan,
  FactorySnInput,
  CarSearchCode,
  SupplyEntry,
  SupplyCodeSearch,
  SupplySearchInput,
  SupplyBarCode,
  SupplyBarInput,
  CompetitorInformation,
  CompetitorInformationMap,
  CompetitorInformationDetail,
  MovePathMap,
  MovePathDate,
  MovePathFence,
  MovePathPerson,
  ChainSnNew,
  SupplyFenderInput,
  SupplyActive,
  RepairRateIndex,
  RepairRateSelectCity,
  RepairRateSelectChild,
  RepairRateSixty,
  ScanCodeLock,
  ScanCodeLockInput,
  ChainCodeUpgrade,
  ScmEntry,
  ScmInform,
  ScmScan,
  ScmScanList,
  ScmConfirm,
  SupplyUnbind
} = Pages;
process.env.NODE_ENV === 'production' && (console.log = () => { });
// var DebugPage = require('./app/pages/profileIndex').ProfileIndex;

var rootStore = store({});

/*
 * Add CodePush, wrap Login
 **/

const urlHash = {};
function addToUrlList(url, scene) {
  urlHash[url] = { url, scene };
}

addToUrlList(
  'ofo',
  wrap(<OFO store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'login',
  wrap(<Login store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'splash',
  wrap(<Splash store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'profile/index',
  wrap(<ProfileIndex store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'profile/modifyPassword',
  wrap(
    <ProfileModifyPassword store={rootStore} dispatch={rootStore.dispatch} />
  )
);
addToUrlList(
  'profile/modifyPasswordDone',
  wrap(
    <ProfileModifyPasswordDone
      store={rootStore}
      dispatch={rootStore.dispatch}
    />
  )
);
addToUrlList(
  'repairer/carList',
  wrap(<RepairCarList store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'repairer/carDetail',
  wrap(<RepairCarDetail store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'repairer/carMap',
  wrap(<RepairCarMap store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'repairer/carSearch',
  wrap(<RepairCarSearch store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'repairer/carSearchDone',
  wrap(<RepairCarSearchDone store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'repairer/checkIn',
  wrap(<RepairCheckIn store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'repairer/carLocation',
  wrap(<RepairCarLocation store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'repairer/checkRecords',
  wrap(<RepairCheckRecords store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'repairer/repairCarRecord',
  wrap(<RepairCarRecord store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'repairer/repairBadCarMark',
  wrap(<RepairBadCarMark store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'repairer/repairBadCarMarkPos',
  wrap(<RepairBadCarMarkPos store={rootStore} dispatch={rootStore.dispatch} />)
);

addToUrlList(
  'operation/dashBoard',
  wrap(<OperationDashBoard store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/orderHistory',
  wrap(
    <OperationOrderHistory store={rootStore} dispatch={rootStore.dispatch} />
  )
);
addToUrlList(
  'operation/orderDetail',
  wrap(<OperationOrderDetail store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/orderDetailCity',
  wrap(
    <OperationOrderDetailCity store={rootStore} dispatch={rootStore.dispatch} />
  )
);
addToUrlList(
  'operation/orderDetailCountry',
  wrap(
    <OperationOrderDetailCountry
      store={rootStore}
      dispatch={rootStore.dispatch}
    />
  )
);
addToUrlList(
  'operation/orderDetailRegion',
  wrap(
    <OperationOrderDetailRegion
      store={rootStore}
      dispatch={rootStore.dispatch}
    />
  )
);
addToUrlList(
  'operation/dashBoard/city',
  wrap(
    <OperationDashBoardCity store={rootStore} dispatch={rootStore.dispatch} />
  )
);
addToUrlList(
  'operation/dashBoard/region',
  wrap(
    <OperationDashBoardRegion store={rootStore} dispatch={rootStore.dispatch} />
  )
);
addToUrlList(
  'operation/dashBoard/country',
  wrap(
    <OperationDashBoardCountry
      store={rootStore}
      dispatch={rootStore.dispatch}
    />
  )
);
addToUrlList(
  'operation/orderDetailSchool',
  wrap(
    <OperationOrderDetailSchool
      store={rootStore}
      dispatch={rootStore.dispatch}
    />
  )
);
addToUrlList(
  'repairer/rateList',
  wrap(<RepairRateList store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'repairer/unlockCode',
  wrap(<RepairUnlockCode store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/dashBoard/district',
  wrap(
    <OperationDashBoardDistrict
      store={rootStore}
      dispatch={rootStore.dispatch}
    />
  )
);
addToUrlList(
  'operation/dashBoard/school',
  wrap(
    <OperationDashBoardSchool store={rootStore} dispatch={rootStore.dispatch} />
  )
);
addToUrlList(
  'operation/dashBoard/society',
  wrap(
    <OperationDashBoardSociety
      store={rootStore}
      dispatch={rootStore.dispatch}
    />
  )
);
addToUrlList(
  'operation/orderDetailSociety',
  wrap(
    <OperationOrderDetailSociety
      store={rootStore}
      dispatch={rootStore.dispatch}
    />
  )
);
addToUrlList(
  'repairerIndex',
  wrap(<RepairerIndex store={rootStore} dispatch={rootStore.dispatch} />)
);

addToUrlList(
  'chainIndex',
  wrap(<ChainIndex store={rootStore} dispatch={rootStore.dispatch} />)
);

addToUrlList(
  'chainInput',
  wrap(<ChainInput store={rootStore} dispatch={rootStore.dispatch} />)
);

addToUrlList(
  'chainInputNew',
  wrap(<ChainInputNew store={rootStore} dispatch={rootStore.dispatch} />)
);

addToUrlList(
  'chainSubmit',
  wrap(<ChainSubmit store={rootStore} dispatch={rootStore.dispatch} />)
);

addToUrlList(
  'chainSubmitNew',
  wrap(<ChainSubmitNew store={rootStore} dispatch={rootStore.dispatch} />)
);

addToUrlList(
  'chainList',
  wrap(<ChainList store={rootStore} dispatch={rootStore.dispatch} />)
);

addToUrlList(
  'repairer/carActive',
  wrap(<ActivePanel store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'repair/reportDetail',
  wrap(<RepairReportDetail store={rootStore} dispatch={rootStore.dispatch} />)
);

addToUrlList(
  'repair/reportDone',
  wrap(<RepairReportDone store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/reportDetail',
  wrap(
    <OperationReportDetail store={rootStore} dispatch={rootStore.dispatch} />
  )
);

addToUrlList(
  'repair/mapIndex',
  wrap(<RepairerMapIndex store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'mapTest',
  wrap(<MapTest store={rootStore} dispatch={rootStore.dispatch} />)
);

addToUrlList(
  'repair/entry',
  wrap(<RepairEntry store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/reportManagement',
  wrap(
    <OperationReportManagement
      store={rootStore}
      dispatch={rootStore.dispatch}
    />
  )
);

addToUrlList(
  'activeInput',
  wrap(<ActiveInput store={rootStore} dispatch={rootStore.dispatch} />)
);

addToUrlList(
  'codeReset',
  wrap(<CodeReset store={rootStore} dispatch={rootStore.dispatch} />)
);

addToUrlList(
  'chooseZone',
  wrap(<ChooseZone store={rootStore} dispatch={rootStore.dispatch} />)
);

addToUrlList(
  'repairer/detail',
  wrap(<RepairDetail store={rootStore} dispatch={rootStore.dispatch} />)
);

addToUrlList(
  'repairer/selectVirtualPosition',
  wrap(
    <RepairSelectVirtualPosition
      store={rootStore}
      dispatch={rootStore.dispatch}
    />
  )
);

addToUrlList(
  'repairer/reportVirtualPile',
  wrap(
    <RepairReportVirtualPile store={rootStore} dispatch={rootStore.dispatch} />
  )
);
addToUrlList(
  '700BikeIndex',
  wrap(<BikeIndex store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  '700Bike/Map',
  wrap(<BikeMapIndex store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  '700Bike/detail',
  wrap(<BikeDetail store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'chainCode',
  wrap(<ChainCode store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'repairer/carSchelduling',
  wrap(<CarSchelduling store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'repairer/transportSubmit',
  wrap(<TransportSub store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'factory/scan',
  wrap(<FacScan store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'factory/manually',
  wrap(<FacInput store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/carSearch',
  wrap(<CarSearchCode store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/carInformation',
  wrap(
    <OperationCarInformation store={rootStore} dispatch={rootStore.dispatch} />
  )
);
addToUrlList(
  'operation/changeUnlockCode',
  wrap(
    <OperationChangeUnlockCode
      store={rootStore}
      dispatch={rootStore.dispatch}
    />
  )
);
addToUrlList(
  'operation/repairRate',
  wrap(<OperationRepairRate store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/newUsers',
  wrap(<OperationNewUsers store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/entry',
  wrap(<OperationEntry store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/hx',
  wrap(<OperationHx store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'hx/entry',
  wrap(<HxEntry store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'hx/createRecord',
  wrap(<CreateRecord store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'hx/createDetail',
  wrap(<CreateDetail store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'hx/workRecord',
  wrap(<WorkRecord store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'hx/workRecord/detail',
  wrap(<WorkRecordDetail store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'hx/list',
  wrap(<HxList store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'hx/map',
  wrap(<HxMapIndex store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'hx/processDone',
  wrap(<HxProcessDone store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'hx/detail',
  wrap(<HxDetail store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'hx/hotSpot',
  wrap(<HxHotSpot store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/changeBrand',
  wrap(<OperationChangeBrand store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/badCarMap',
  wrap(<OperationBadCarMap store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/badCarDetail',
  wrap(
    <OperationBadCarDetail store={rootStore} dispatch={rootStore.dispatch} />
  )
);
addToUrlList(
  'operation/unbindBrand',
  wrap(<OperationUnbindBrand store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/badCarDone',
  wrap(<OperationBadCarDone store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/badCarData',
  wrap(<OperationBadCarData store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/badCarMessage',
  wrap(
    <OperationBadCarMessage store={rootStore} dispatch={rootStore.dispatch} />
  )
);
addToUrlList(
  'operation/lowElectricCar/map',
  wrap(<LowElectricCar store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/lowElectricCar/detail',
  wrap(<LowElectricCarDetail store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'factory/entry',
  wrap(<FactoryEntry store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'factory/codeSearch',
  wrap(<FactorySnScan store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'factory/codeSearchNew',
  wrap(<FactorySnInput store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'factory/snInput',
  wrap(<FactorySnInput store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/carSearch/code',
  wrap(<OperationCarSearch store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'supply/entry',
  wrap(<SupplyEntry store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'supply/codeSearch',
  wrap(<SupplyCodeSearch store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'supply/codeSearchNew',
  wrap(<SupplySearchInput store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'supply/searchInput',
  wrap(<SupplySearchInput store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'supply/barCode',
  wrap(<SupplyBarCode store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'supply/barCodeInput',
  wrap(<SupplyBarInput store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/collectCompetitor/information',
  wrap(
    <CompetitorInformation store={rootStore} dispatch={rootStore.dispatch} />
  )
);
addToUrlList(
  'competitorInformation/map',
  wrap(
    <CompetitorInformationMap store={rootStore} dispatch={rootStore.dispatch} />
  )
);
addToUrlList(
  'competitorInformation/detail',
  wrap(
    <CompetitorInformationDetail
      store={rootStore}
      dispatch={rootStore.dispatch}
    />
  )
);
addToUrlList(
  'movePath/map',
  wrap(<MovePathMap store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'movePath/date',
  wrap(<MovePathDate store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'movePath/fence',
  wrap(<MovePathFence store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'movePath/person',
  wrap(<MovePathPerson store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'chainSnNew',
  wrap(<ChainSnNew store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'supply/fenderCodeInput',
  wrap(<SupplyFenderInput store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'repairer/carActiveNew',
  wrap(<SupplyActive store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'operation/repairRateNew',
  wrap(<RepairRateIndex store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'repairRateSelectCity',
  wrap(<RepairRateSelectCity store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'repairRateSelectChild',
  wrap(
    <RepairRateSelectChild store={rootStore} dispatch={rootStore.dispatch} />
  )
);
addToUrlList(
  'repairRateSixty',
  wrap(<RepairRateSixty store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'chainCodeUpgrade',
  wrap(<ChainCodeUpgrade store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'scanCode/lock',
  wrap(<ScanCodeLock store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'scanCode/lockInput',
  wrap(<ScanCodeLockInput store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'scm/entry',
  wrap(<ScmEntry store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'scm/inform',
  wrap(<ScmInform store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'scm/scan',
  wrap(<ScmScan store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'scm/scanList',
  wrap(<ScmScanList store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'scm/confirm',
  wrap(<ScmConfirm store={rootStore} dispatch={rootStore.dispatch} />)
);
addToUrlList(
  'supply/unbind',
  wrap(<SupplyUnbind store={rootStore} dispatch={rootStore.dispatch} />)
);


// work
// addToUrlList('work', wrap(<OperationOrderDetailSociety store={rootStore} dispatch={rootStore.dispatch} />));
export default class extends Component {
  render() {
    return (
      <View style={{ flexGrow: 1, backgroundColor: '#fff' }}>
        <Navigator
          initialRoute={{ url: 'login' }}
          renderScene={(route, navigator) => {
            let C = urlHash[route.url] || null;
            if (C == null) return null;
            C = C.scene;
            return <C navigator={navigator} />;
          }}
          ref="nav"
        />
      </View>
    );
  }

  componentDidMount() {
    const nav = this.refs.nav;
    Object.assign(jumpConf, {
      JumpTo(url, allowBack = true, state = null) {
        allowBack ? nav.push({ url, state }) : nav.replace({ url, state });
      },
      JumpBack() {
        nav.pop();
      },
      ResetTo(url, state = null) {
        nav.immediatelyResetRouteStack([{ url, state }]);
      },
      getRoute() {
        const routes = nav.getCurrentRoutes();
        return routes[routes.length - 1];
      }
    });
    let isAnimating = false;
    nav.navigationContext.addListener('willfocus', () => {
      isAnimating = true;
      timeoutQueue.pause();
    });
    nav.navigationContext.addListener('didfocus', () => {
      isAnimating = false;
      timeoutQueue.resume();
    });

    BackAndroid.addEventListener('hardwareBackPress', () => {
      if (nav.getCurrentRoutes().length > 1) {
        !isAnimating
          ? androidCallback.callback ? androidCallback.callback() : JumpBack()
          : void 0;
        return true;
      }
      return false;
    });
  }
}
