#!/bin/sh

DEVICE_NAME="iPhone-6, 10.2"
rm -rf __temp__ios__debug/
cd ios

xcodebuild \
-workspace ofo.xcworkspace \
-configuration Debug \
-scheme ofo \
-destination 'platform=iOS Simulator,name=iPhone 6,OS=10.2' \
-derivedDataPath '../__temp__ios__debug/' \
build

cd ..
./node_modules/.bin/ios-sim launch ./__temp__ios__debug/Build/Products/Release-iphonesimulator/ofo.app --devicetypeid "$DEVICE_NAME"
