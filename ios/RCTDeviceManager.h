//
//  RCTDeviceManager.h
//  ofo
//
//  Created by xinzhongzhu on 17/4/22.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RCTBridgeModule.h"

@interface RCTDeviceManager : NSObject<RCTBridgeModule>

@end
