#import <AMapFoundationKit/AMapFoundationKit.h>
#import <MAMapKit/MAMapKit.h>
#import "RCTBridge.h"
#import "RCTViewManager.h"
#import "RCTImageSource.h"
#import "RCTImageLoader.h"
#import "RCTAMap.h"

@interface RCTAMapManager : RCTViewManager
@end

@implementation RCTConvert(CoreLocation)


RCT_CONVERTER(CLLocationDegrees, CLLocationDegrees, doubleValue);
RCT_CONVERTER(CLLocationDistance, CLLocationDistance, doubleValue);

+ (CLLocationCoordinate2D)CLLocationCoordinate2D:(id)json
{
  json = [self NSDictionary:json];
  return (CLLocationCoordinate2D){
    [self CLLocationDegrees:json[@"latitude"]],
    [self CLLocationDegrees:json[@"longitude"]]
  };
}

@end

@implementation RCTConvert(MapKit)

+ (MACoordinateSpan)MKCoordinateSpan:(id)json
{
  json = [self NSDictionary:json];
  return (MACoordinateSpan){
    [self CLLocationDegrees:json[@"latitudeDelta"]],
    [self CLLocationDegrees:json[@"longitudeDelta"]]
  };
}

+ (MACoordinateRegion)MACoordinateRegion:(id)json
{
  return (MACoordinateRegion){
    [self CLLocationCoordinate2D:json],
    [self MKCoordinateSpan:json]
  };
}


+ (NSArray *)MANnnotations:(id)json
{
  return (NSArray *){
    [self NSArray:json]
  };
}

@end

@implementation RCTAMapManager
{
  NSArray *annotations;
  MACoordinateRegion coordinateRegion;
}

RCT_EXPORT_MODULE()
RCT_EXPORT_VIEW_PROPERTY(zoomEnabled, BOOL)
RCT_EXPORT_VIEW_PROPERTY(showsUserLocation, BOOL)
RCT_EXPORT_VIEW_PROPERTY(userTrackingMode, BOOL)
RCT_EXPORT_VIEW_PROPERTY(clusterMode, BOOL)
RCT_EXPORT_VIEW_PROPERTY(onLocationChange, RCTBubblingEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onAnnotationClicked, RCTBubblingEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onRegionChange, RCTBubblingEventBlock)


RCT_CUSTOM_VIEW_PROPERTY(region, MACoordinateRegion, RCTAMap)
{
  if(json == nil)return;
  [view setRegion:[RCTConvert MACoordinateRegion:json]];
}

RCT_CUSTOM_VIEW_PROPERTY(defaultRegion, MACoordinateRegion, RCTAMap)
{
  if(json == nil)return;
  [view setDefaultRegion:[RCTConvert MACoordinateRegion:json]];
}

static NSMutableDictionary* imageDict ;
RCT_CUSTOM_VIEW_PROPERTY(annotations, NSArray, RCTAMap)
{
  if(json == nil)return;
  annotations = [RCTConvert MANnnotations:json];
  if(!imageDict) imageDict = [[NSMutableDictionary alloc]init];
  NSMutableArray* array = [[NSMutableArray alloc]init];
  
  void (^done)(id<MAAnnotation> annotation) = ^(id<MAAnnotation>annotation){
    [array addObject:annotation];
    if([array count] == [annotations count]) {
      [view setAnnotations:array];
    }
  };
  
  if(annotations.count == 0) {
    [view setAnnotations:[[NSArray alloc]init]];
  }
  
  for (int i = 0 ; i < annotations.count ;i++)
  {
    NSLog(@"%@", annotations[i]);
    NSMutableDictionary *dicStart = [[NSMutableDictionary alloc]initWithDictionary:annotations[i]];
    CLLocationCoordinate2D  locationCoordinateStart;
    locationCoordinateStart.latitude = [[dicStart objectForKey:@"latitude"] floatValue];
    locationCoordinateStart.longitude = [[dicStart objectForKey:@"longitude"] floatValue];
    
#ifdef DEBUG
    NSURL *url = [NSURL URLWithString:dicStart[@"image"][@"uri"]];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage *image = [[UIImage alloc] initWithData:data];
    dicStart[@"image"] = image;
    done(dicStart);
    
#else
    UIImage* image ;
    if(imageDict[dicStart[@"image"][@"uri"]]) {
      image = imageDict[dicStart[@"image"][@"uri"]];
      dicStart[@"image"] = image;
      done(dicStart);
    }
    else {
      [self.bridge.imageLoader loadImageWithURLRequest:[RCTConvert NSURLRequest:dicStart[@"image"][@"uri"]]
                                                  size:CGSizeMake([dicStart[@"image"][@"width"] floatValue], [dicStart[@"image"][@"height"] floatValue])
                                                 scale:1
                                               clipped:YES
                                            resizeMode:RCTResizeModeCenter
                                         progressBlock:nil
                                      partialLoadBlock:nil
                                       completionBlock:^(NSError *error, UIImage *image) {
                                         if (error) {
                                           // TODO(lmr): do something with the error?
                                           NSLog(@"%@", error);
                                         }
                                         dispatch_async(dispatch_get_main_queue(), ^{
                                           [imageDict setObject:image forKey:dicStart[@"image"][@"uri"]];
                                           dicStart[@"image"] = image;
                                           done(dicStart);
                                         });
                                       }];
    }
#endif
  }
}

RCT_CUSTOM_VIEW_PROPERTY(lines, NSArray, RCTAMap)
{
  if(json == nil)return;
  [view setLines:[RCTConvert NSArray:json]];
}

RCT_CUSTOM_VIEW_PROPERTY(hotSpots, NSArray, RCTAMap)
{
  if(json == nil)return;
  [view setHotSpots:[RCTConvert NSArray:json]];
}

- (UIView *)view
{
  RCTAMap* view = [[RCTAMap alloc]init];
  view.bridge = self.bridge;
  return view;
}

-(void)mapView:(MAMapView *)mapView regionWillChangeAnimated:(BOOL)animated {
}


- (void)mapView:(MAMapView *)mapView didFailToLocateUserWithError:(NSError *)error
{
  NSLog(@"%@",error);
}

//- (void)mapView:(MAMapView *)mapView mapDidMoveByUser:(BOOL)wasUserAction
//{
//  firstShowMapFlag = 1;
//
//}

//- (void)mapView:(MAMapView *)mapView didSingleTappedAtCoordinate:(CLLocationCoordinate2D)coordinate
//{
//  firstShowMapFlag = 0;
//}

@end
