//
//  RCTLocationManager.h
//  ofo
//
//  Created by xinzhongzhu on 16/12/22.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RCTBridgeModule.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import <MAMapKit/MAMapKit.h>

@interface RCTLocationManager : NSObject<RCTBridgeModule, AMapSearchDelegate>

@end
