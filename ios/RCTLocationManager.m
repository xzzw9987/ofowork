//
//  RCTLocationManager.m
//  ofo
//
//  Created by xinzhongzhu on 16/12/22.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "RCTLocationManager.h"
#import "UpdateLocationBackground.h"

#import <CoreLocation/CLError.h>
#import <CoreLocation/CLLocationManager.h>
#import <CoreLocation/CLLocationManagerDelegate.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import "RCTAssert.h"
#import "RCTBridge.h"
#import "RCTConvert.h"
#import "RCTEventDispatcher.h"
#import "RCTLog.h"

@interface RCTLocationManager()
@property (copy, nonatomic) RCTResponseSenderBlock geoMessageCallback;
@property (strong, nonatomic) AMapSearchAPI* search;
@property (strong, nonatomic) AMapLocationManager* manager ;
@property (strong, nonatomic) UpdateLocationBackground* u;
@end

@implementation RCTConvert(CoreLocation)
+ (CLLocationCoordinate2D)CLLocationCoordinate2D:(id)json
{
  json = [self NSDictionary:json];
  return CLLocationCoordinate2DMake([json[@"latitude"] doubleValue],
                                    [json[@"longitude"] doubleValue]);
}

@end

@implementation RCTLocationManager

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(whereami:(RCTResponseSenderBlock)callback)
{
  
  AMapLocationManager* locationManager = [[AMapLocationManager alloc]init];
  
  /* we must strong hold this variable : manager */
  /* otherwise, the auth will always be denied */
  self.manager = locationManager ;
  
  [locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
  locationManager.locationTimeout = 4;
  locationManager.reGeocodeTimeout = 3;
  
  [locationManager requestLocationWithReGeocode:NO completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
    
    if(error) {
      NSLog(@"error_error");
      NSLog(@"%@", error);
      callback(@[@{@"error": @YES, @"code": [error localizedDescription]}]);
      return;
    }
    else {
      callback(@[
                 @{
                   @"error": @NO,
                   @"accuracy": @(location.horizontalAccuracy),
                   @"latitude":[[NSNumber alloc]initWithDouble:location.coordinate.latitude],
                   @"longitude":[[NSNumber alloc]initWithDouble:location.coordinate.longitude]
                   }
                 ]);
      return ;
    }
  }];
}

RCT_EXPORT_METHOD(geoMessage:(nonnull NSNumber*) latitude
                  longitude:(nonnull NSNumber*) longitude
                  callback:(RCTResponseSenderBlock)callback)
{
  self.geoMessageCallback = callback;
  self.search = [[AMapSearchAPI alloc] init];
  self.search.delegate = self;
  AMapReGeocodeSearchRequest* regeo = [[AMapReGeocodeSearchRequest alloc] init] ;
  regeo.location = [AMapGeoPoint locationWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
  regeo.requireExtension = YES;
  
  [self.search AMapReGoecodeSearch:regeo];
  
}

RCT_EXPORT_METHOD(distance:(NSDictionary*)fromPoint
                  toPoint:(NSDictionary*)toPoint
                  callback:(RCTResponseSenderBlock)callback)
{
  callback(@[
             @{
               @"distance": @(MAMetersBetweenMapPoints(
                                                       MAMapPointForCoordinate([RCTConvert CLLocationCoordinate2D:fromPoint]),
                                                       
                                                       MAMapPointForCoordinate([RCTConvert CLLocationCoordinate2D:toPoint])))
               }
             ]);
}

-(void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response {
  
  if(response.regeocode != nil && self.geoMessageCallback) {
    self.geoMessageCallback(@[
                              @{
                                @"error": @NO,
                                @"address": response.regeocode.formattedAddress
                                }
                              ]);
  }
  else if(self.geoMessageCallback) {
    self.geoMessageCallback(@[
                              @{
                                @"error": @YES,
                                @"address": @""
                                }
                              ]);
    
  }
  
}

-(dispatch_queue_t)methodQueue {
  return dispatch_get_main_queue();
}

@end
