//
//  RCTViewController.h
//  ofo
//
//  Created by xinzhongzhu on 16/11/18.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RCTViewController : UIViewController
+(instancetype)createWithUrl:(NSString*)url;
@end
