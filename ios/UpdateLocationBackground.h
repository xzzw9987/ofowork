//
//  UpdateLocationBackground.h
//  ofo
//
//  Created by xinzhongzhu on 17/6/5.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapLocationKit/AMapLocationKit.h>

@interface UpdateLocationBackground : NSObject<AMapLocationManagerDelegate>
-(instancetype)initWithToken:(NSString*)token withURL:(NSString*)url;
@end
