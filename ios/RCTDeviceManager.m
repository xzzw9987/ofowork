//
//  RCTDeviceManager.m
//  ofo
//
//  Created by xinzhongzhu on 17/4/22.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import "RCTDeviceManager.h"
#import <UIKit/UIKit.h>
#import "SAMKeychain.h"
#import "SAMKeychainQuery.h"
#import "UpdateLocationBackground.h"

@interface RCTDeviceManager()
@property (strong, nonatomic) UpdateLocationBackground* u;
@end

@implementation RCTDeviceManager
RCT_EXPORT_MODULE();
RCT_EXPORT_METHOD(getDeviceID:(RCTResponseSenderBlock)callback)
{
  
  NSString *appName=[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
  NSString *uuid = [SAMKeychain passwordForService:appName account:@"incoding"];
  if (uuid == nil)
  {
    uuid  = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [SAMKeychain setPassword:uuid forService:appName account:@"incoding"];
  }
  
  callback(@[
            @{
             @"deviceID": uuid
            }
          ]);
}

RCT_EXPORT_METHOD(startUploadLocation: (nonnull NSString*) token
                  withURL:(nonnull NSString*)url) {
  self.u = [[UpdateLocationBackground alloc]initWithToken:token withURL:url];
}

@end
