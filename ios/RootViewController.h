//
//  RootViewController.h
//  ofo
//
//  Created by xinzhongzhu on 16/11/18.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UINavigationController
+(instancetype)getInstance;
-(void)jumpTo:(NSString*)url;
@end
