//
//  RCTAMap.h
//  ofo
//
//  Created by xinzhongzhu on 17/1/1.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RCTViewManager.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <MAMapKit/MAMapKit.h>
#import "RCTBridge.h"

@interface RCTAMap : UIView<MAMapViewDelegate>
@property (nonatomic ,assign) BOOL showsUserLocation;
@property (nonatomic, assign) BOOL userTrackingMode;
@property (nonatomic, assign) BOOL zoomEnabled;
@property (nonatomic, assign) BOOL clusterMode;
@property (nonatomic, copy) RCTBubblingEventBlock onLocationChange;
@property (nonatomic, copy) RCTBubblingEventBlock onAnnotationClicked;
@property (nonatomic, copy) RCTBubblingEventBlock onRegionChange;
@property (nonatomic, weak) RCTBridge* bridge;

-(void)setRegion:(MACoordinateRegion)region;
-(void)setDefaultRegion:(MACoordinateRegion)region;
-(void)setAnnotations:(NSArray*)annotations;
-(void)addAnnotation:(id<MAAnnotation>)annotation;
-(void)setHotSpots:(NSArray*)hotSpots;
-(void)setLines:(NSArray*)lines;
@end
