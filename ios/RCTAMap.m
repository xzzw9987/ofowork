//
//  RCTAMap.m
//  ofo
//
//  Created by xinzhongzhu on 17/1/1.
//  Copyright © 2017年 Facebook. All rights reserved.
//
#define PI 3.1415926
#define degToRad(deg) deg * PI / 180
#import "RCTAMap.h"

static NSString* preservePin = @"__preservePin#@!__";

@interface AnnotaionPoint : MAPointAnnotation
@property (nonatomic, strong) UIImage* image;
@property (nonatomic, strong) NSString* key;
@property (nonatomic, strong) NSString* label;
@end

@implementation AnnotaionPoint
@end

@interface AnnotaionView: MAAnnotationView
@property (nonatomic, strong) NSString* key ;
@property (nonatomic, weak) RCTAMap* mapView ;
@end

@implementation AnnotaionView
-(void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];
  if(self.mapView.onAnnotationClicked && selected && self.key != preservePin) {
    self.mapView.onAnnotationClicked(@{@"key": self.key});
  }
}
@end



@implementation RCTAMap {
  MAMapView* mapView;
  MAAnnotationView* userLocationAnnotation;
  id<MAOverlay> mHeatMapOverlay;
  NSMutableArray<id<MAOverlay>>* mPolylineOverlay;
  BOOL firstShowMapFlag;
  BOOL setDefaultRegion;
  
  BOOL mCLusterMode;
  BOOL mHasSetClusterMode;
  NSArray* mAnnotations;
  
}


-(instancetype)init {
  if(self = [super init]) {
    firstShowMapFlag = 1;
    setDefaultRegion = false;
    
    mPolylineOverlay = [[NSMutableArray alloc]init];
  }
  return self ;
}

-(void)updateMap{
  if(mapView) {
    
    // if(_userTrackingMode)
    // mapView.userTrackingMode = MAUserTrackingModeFollow;
    
  }
  else {
    if (self.bounds.size.height > 0 && self.bounds.size.width > 0) {
      [self createMap];
    }
  }
}

-(void)createMap{
  mapView = [[MAMapView alloc]initWithFrame:self.bounds];
  mapView.showsUserLocation = YES;
  mapView.userTrackingMode = MAUserTrackingModeFollowWithHeading;
  mapView.zoomEnabled = YES;
  mapView.rotateEnabled = NO;
  mapView.rotateCameraEnabled = NO;
  mapView.delegate = self;
  
  
  [self updateMap];
  [self addSubview:mapView];
}


-(void)layoutSubviews {
  [self updateMap];
  [mapView setFrame:self.bounds];
}

-(void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation {
  if (updatingLocation) {
    if (firstShowMapFlag) {
      firstShowMapFlag = 0;
      // [mapView setCenterCoordinate:userLocation.coordinate animated:YES];
      if (_onLocationChange) {
        _onLocationChange(@{@"latitude": [NSString stringWithFormat:@"%f",userLocation.coordinate.latitude],
                            @"longitude": [NSString stringWithFormat:@"%f",userLocation.coordinate.longitude]});
      }
      else {
        return;
      }
    }
  }
  else {
    if(userLocationAnnotation) {
      userLocationAnnotation.transform = CGAffineTransformMakeRotation(degToRad(userLocation.heading.trueHeading));
    }
  }
}

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id <MAAnnotation>)annotation {
  NSString *reuseIndetifier;
  
  if([annotation isKindOfClass:[MAUserLocation class]]) {
    MAUserLocation* userLocation = annotation ;
    reuseIndetifier = @"userLocationReuseIndetifier";
    MAAnnotationView* annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
    if(annotationView == nil) {
      annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIndetifier];
      annotationView.image = [UIImage imageNamed:@"userlocation"];
    }
    userLocationAnnotation = annotationView ;
    return annotationView;
  }
  
  
  if ([annotation isKindOfClass:[MAPointAnnotation class]])
  {
    AnnotaionPoint* point = annotation;
    
    if (point.key == preservePin){
      NSString *pointReuseIndentifier = @"pinReuseIndentifier";
      AnnotaionView* pinAnnotationView = (AnnotaionView*)[mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndentifier];
      
      if(pinAnnotationView == nil)
        pinAnnotationView = [[AnnotaionView alloc] initWithAnnotation:annotation reuseIdentifier:pointReuseIndentifier];
      UIImage* pinImage = [UIImage imageNamed:@"mapClusterIcon"];
      pinAnnotationView.image = pinImage ;
      pinAnnotationView.key = point.key;
      pinAnnotationView.mapView = self;
      pinAnnotationView.centerOffset = CGPointMake(0, -.5 * [pinImage size].height);
      
      
      if(point.label) {
        for(int i = 0 ; i < [pinAnnotationView.subviews count] ; i++) {
          if([pinAnnotationView.subviews[i] isKindOfClass:[UILabel class]]) {
            [pinAnnotationView.subviews[i] removeFromSuperview];
            i--;
          }
        }
        float width = pinImage.size.width;
        float height = pinImage.size.height;
        UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, height)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor blackColor];
        label.text = point.label;
        [pinAnnotationView addSubview:label];
      }
      
      return pinAnnotationView;
    }
    
    
    reuseIndetifier = @"annotationReuseIndetifier";
    AnnotaionView *annotationView = (AnnotaionView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
    if (annotationView == nil)
      annotationView = [[AnnotaionView alloc] initWithAnnotation:annotation
                                                 reuseIdentifier:reuseIndetifier];
    annotationView.image = point.image;
    annotationView.key = point.key;
    annotationView.mapView = self;
    //设置中心点偏移，使得标注底部中间点成为经纬度对应点
    annotationView.centerOffset = CGPointMake(0, -.5 * [point.image size].height);
    
    if(point.label) {
      for(int i = 0 ; i < [annotationView.subviews count] ; i++) {
        if([annotationView.subviews[i] isKindOfClass:[UILabel class]]) {
          [annotationView.subviews[i] removeFromSuperview];
          i--;
        }
      }
      
      float width = point.image.size.width;
      float height = point.image.size.height;
      UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, height)];
      label.backgroundColor = [UIColor clearColor];
      label.textAlignment = NSTextAlignmentCenter;
      label.textColor = [UIColor blackColor];
      label.text = point.label;
      label.font = [UIFont systemFontOfSize:10];
      [annotationView addSubview:label];
    }
    
    return annotationView;
  }
  return nil;
}

-(void)setRegion:(MACoordinateRegion)region {
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    [mapView setRegion:region animated:NO];
  });
}

-(void)setDefaultRegion:(MACoordinateRegion)region {
  if(!setDefaultRegion) {
    setDefaultRegion = true;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
      [mapView setRegion:region animated:NO];
    });
  }
}

-(void)removeAnnotations {
  NSArray* annotations = mapView.annotations ;
  NSMutableArray* willRemoveAnnotations = [[NSMutableArray alloc]init];
  for(int i = 0 ; i < annotations.count ; i ++) {
    id<MAAnnotation> annotation = annotations[i];
    if(![annotation isKindOfClass:[MAUserLocation class]]) {
      [willRemoveAnnotations addObject:annotation];
    }
  }
  [mapView removeAnnotations:willRemoveAnnotations];
}

-(void)addAnnotation:(id<MAAnnotation>)annotation {
  if(!annotation)return;
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    
    AnnotaionPoint *pointAnnotation = [[AnnotaionPoint alloc]init];
    pointAnnotation.coordinate = CLLocationCoordinate2DMake([annotation[@"latitude"] floatValue], [annotation[@"longitude"] floatValue]);
    pointAnnotation.image = annotation[@"image"];
    pointAnnotation.key = annotation[@"key"];
    pointAnnotation.label = annotation[@"number"];
    [mapView addAnnotation:pointAnnotation];
    
  });
}

-(NSArray*)processAnnotations:(NSArray*)annotations withZoomLevel:(float)zoomLevel {
  if(annotations.count == 0 || !mCLusterMode)
    return annotations;
  
  double minimumLongitude = 99999;
  double maximumLongitude = -99999;
  double minimumLatitude = 99999;
  double maximumLatitude = -99999;
  
  int gridCount = [self gridCount:zoomLevel];
  
  if(gridCount == 9999) {
    return [self clipAnnotationsInView:annotations];
  }
  NSMutableArray* copy = [[NSMutableArray alloc]initWithArray:annotations];
  NSMutableArray* ret = [[NSMutableArray alloc]init];
  
  for(int i = 0 ; i < annotations.count ; i ++){
    
    double latitude = [annotations[i][@"latitude"] doubleValue];
    double longitude = [annotations[i][@"longitude"] doubleValue];
    
    if(latitude > maximumLatitude) maximumLatitude = latitude;
    if(latitude < minimumLatitude) minimumLatitude = latitude;
    if(longitude > maximumLongitude) maximumLongitude = longitude;
    if(longitude < minimumLongitude) minimumLongitude = longitude;
  }
  
  double bias = 0.0005;
  maximumLatitude += bias;
  minimumLatitude -= bias;
  maximumLongitude += bias;
  minimumLongitude -= bias;
  
  
  double longitudeDelta = (maximumLongitude - minimumLongitude) / gridCount;
  double latitudeDelta = (maximumLatitude - minimumLatitude) / gridCount;
  
  for(int i = 0 ; i < gridCount; i ++) {
    for(int j = 0 ; j < gridCount ; j++) {
      double targetLatitude = minimumLatitude + (i + .5f) * latitudeDelta;
      double targetLongitude = minimumLongitude + (j + .5f) * longitudeDelta;
      
      NSMutableArray* t = [[NSMutableArray alloc]init];
      
      for(int k = 0 ; k < copy.count; k++) {
        
        double latitude = [copy[k][@"latitude"] doubleValue];
        double longitude = [copy[k][@"longitude"] doubleValue];
        
        BOOL inGrid =fabs(latitude - targetLatitude) <= .5 * latitudeDelta
        && fabs(longitude - targetLongitude) <= .5 * longitudeDelta;
        
        if(inGrid) {
          // add object
          [t addObject:copy[k]];
          [copy removeObjectAtIndex:k];
          k--;
        }
      }
      
      if(t.count > 0) {
        float latitude = 0;
        float longitude = 0;
        for(int k = 0 ; k < t.count ; k++) {
          latitude += [t[k][@"latitude"] floatValue] / t.count;
          longitude += [t[k][@"longitude"] floatValue] / t.count;
        }
        [ret addObject:@{
                         @"key": preservePin,
                         @"latitude": @(latitude),
                         @"longitude": @(longitude),
                         @"number": @(t.count).stringValue}];
      }
    }
  }
  return ret;
}

-(void)setAnnotations:(NSArray*)annotations {
  if(!annotations)return ;
  mAnnotations = annotations;
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    NSMutableArray* arr = [[NSMutableArray alloc]init];
    NSArray* annos = [self processAnnotations:annotations withZoomLevel:mapView.zoomLevel];
    [self removeAnnotations];
    
    
    // NSLog(@"%lu, annoscount", (unsigned long)annos.count);
    
    for(int i = 0 ; i < annos.count ; i++) {
      id<MAAnnotation> anno = annos[i];
      AnnotaionPoint *pointAnnotation = [[AnnotaionPoint alloc] init];
      pointAnnotation.coordinate = CLLocationCoordinate2DMake([anno[@"latitude"] floatValue], [anno[@"longitude"] floatValue]);
      pointAnnotation.image = anno[@"image"];
      pointAnnotation.key = anno[@"key"];
      pointAnnotation.label = anno[@"number"];
      [arr addObject:pointAnnotation];
    }
    [mapView addAnnotations:arr];
  });
}

-(NSArray*)clipAnnotationsInView:(NSArray*)annotations {
  NSMutableArray* ret = [[NSMutableArray alloc]init];
  for(int i = 0 ; i < annotations.count ; i ++) {
    if([self inBound:CLLocationCoordinate2DMake([annotations[i][@"latitude"] floatValue],
                                                [annotations[i][@"longitude"] floatValue])
              center: mapView.region.center
                span:mapView.region.span])
      [ret addObject:annotations[i]];
  }
  return ret;
}

-(BOOL)inBound:(CLLocationCoordinate2D)targetCoord
        center:(CLLocationCoordinate2D)center
          span:(MACoordinateSpan)span{
  
  float maximiumLatitude = center.latitude + span.latitudeDelta;
  float minimiumLatitude = center.latitude - span.latitudeDelta;
  float maximiumLongitude = center.longitude +  span.longitudeDelta;
  float minimiumLongitude = center.longitude - span.longitudeDelta;
  
  return targetCoord.latitude >= minimiumLatitude
  && targetCoord.latitude <= maximiumLatitude
  && targetCoord.longitude >= minimiumLongitude
  && targetCoord.longitude <= maximiumLongitude;
}

-(void)setHotSpots:(NSArray*)hotSpots {
  if(!hotSpots)return;
  
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    if(mHeatMapOverlay) [mapView removeOverlay:mHeatMapOverlay] ;
    NSMutableArray* data = [NSMutableArray array];
    
    for(NSDictionary *dic in hotSpots) {
      MAHeatMapNode* node = [MAHeatMapNode new];
      CLLocationCoordinate2D coordinate;
      coordinate.latitude = [dic[@"latitude"] doubleValue];
      coordinate.longitude = [dic[@"longitude"] doubleValue];
      node.coordinate = coordinate;
      
      node.intensity = [dic[@"count"] doubleValue];
      
      [data addObject:node];
    }
    
    MAHeatMapTileOverlay* overlay = [[MAHeatMapTileOverlay alloc]init];
    overlay.radius = 25;
    overlay.data = data;
    overlay.gradient = [[MAHeatMapGradient alloc] initWithColor:@[[UIColor blueColor],[UIColor greenColor], [UIColor redColor]] andWithStartPoints:@[@(0.2),@(0.5),@(0.9)]];
    [mapView addOverlay:overlay];
    
    mHeatMapOverlay = overlay;
    
  });
}

-(BOOL)clusterMode {
  return mCLusterMode;
}

-(void)setClusterMode:(BOOL)clusterMode {
  if(mHasSetClusterMode && clusterMode != mCLusterMode) {
    // something changed .
    mCLusterMode = clusterMode;
    [self setAnnotations:mAnnotations];
  }
  mCLusterMode = clusterMode;
  mHasSetClusterMode = YES;
}

-(void)setLines:(NSArray *)lines {
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    int count = (int)[lines count];
    NSLog(@"linecount is %d", count);
    if(mPolylineOverlay) {
      for(int i = 0 ; i < [mPolylineOverlay count]; i++) {
        id<MAOverlay> overlay = mPolylineOverlay[i];
        [mapView removeOverlay:overlay];
        [mPolylineOverlay removeObjectAtIndex:i];
        i--;
      }
    }
    
    CLLocationCoordinate2D coords[count];
    for(int i = 0 ; i < count ; i ++) {
      NSDictionary* dict = lines[i];
      CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([dict[@"latitude"] floatValue], [dict[@"longitude"] floatValue]);
      coords[i].latitude = coord.latitude;
      coords[i].longitude = coord.longitude;
    }
    
    id<MAOverlay> overlay = [MAPolyline polylineWithCoordinates:coords count:count];
    [mapView addOverlay:overlay];
    [mPolylineOverlay addObject:overlay];
    
    
    //if(count > 0) {
      //CLLocationCoordinate2D endPoints[2];
      //endPoints[0] = coords[0];
      //endPoints[1] = coords[count - 1];
      
      //for(int i = 0 ; i < 2 ; i ++) {
        //MACircle *circle = [MACircle circleWithCenterCoordinate:endPoints[i] radius:7.0f];
        //[mapView addOverlay:circle];
        //[mPolylineOverlay addObject:circle];
      //}
    //}
  });
}



//-(void)mapView:(MAMapView *)mapView mapDidZoomByUser:(BOOL)wasUserAction {
//  [self setAnnotations:mAnnotations];
//}

-(void)mapView:(MAMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
  if(_onRegionChange) {
    _onRegionChange(@{
                      @"latitude": @(mapView.region.center.latitude),
                      @"longitude": @(mapView.region.center.longitude),
                      @"latitudeDelta": @(mapView.region.span.latitudeDelta),
                      @"longitudeDelta": @(mapView.region.span.longitudeDelta)
                      });
  }
  if(mCLusterMode)
    [self setAnnotations:mAnnotations];
}

-(int)gridCount:(float)zoomLevel {
  if(zoomLevel > 17) {
    return 9999;
  }
  if(zoomLevel < 10) {
    return 1;
  }
  return (int)(.8571f * zoomLevel - 7.56f);
}

-(MAOverlayRenderer*)mapView:(MAMapView *)mapView rendererForOverlay:(id<MAOverlay>)overlay {
  if([overlay isKindOfClass:[MATileOverlay class]]) {
    return [[MATileOverlayRenderer alloc] initWithTileOverlay:overlay];
  }
  
  if([overlay isKindOfClass:[MAPolyline class]]) {
    MAPolylineRenderer* polylineRenderer = [[MAPolylineRenderer alloc] initWithPolyline:overlay];
    polylineRenderer.lineWidth    = 10.0f;
    polylineRenderer.fillColor    = [UIColor colorWithRed:0 green:1 blue:0 alpha:1];polylineRenderer.strokeColor  = [UIColor colorWithRed:0 green:1 blue:0 alpha:1];
    return polylineRenderer;
  }
  
  if([overlay isKindOfClass:[MACircle class]]) {
    MACircleRenderer *circleRenderer = [[MACircleRenderer alloc] initWithCircle:overlay];
    circleRenderer.lineWidth    = 0.0f;
    circleRenderer.fillColor    = [UIColor colorWithRed:0 green:1 blue:0 alpha:1];
    return circleRenderer;
  }
  
  return nil;
}

@end
