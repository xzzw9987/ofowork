//
//  RCTViewController.m
//  ofo
//
//  Created by xinzhongzhu on 16/11/18.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "RCTViewController.h"
#import "RCTBundleURLProvider.h"
#import "RCTRootView.h"
#import "LaunchBridge.h"

@interface RCTViewController (){
}
@property NSString* url;
@end

@implementation RCTViewController

-(void)loadView {
  RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:[LaunchBridge getInstance] moduleName:self.url initialProperties:nil];
  
  rootView.backgroundColor = [UIColor whiteColor];
  self.view = rootView;
}

-(instancetype)init:(NSString*)url {
  self = [super init];
  self.url = url ;
  return self ;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

+(instancetype)createWithUrl:(NSString *)url {
  return [[RCTViewController alloc]init:url];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
