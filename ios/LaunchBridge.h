//
//  LaunchBridge.h
//  ofo
//
//  Created by xinzhongzhu on 16/11/18.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "RCTBridge.h"

@interface LaunchBridge : NSObject<RCTBridgeDelegate>
+(RCTBridge*)getInstance;
@end

