//
//  OfoReactManager.h
//  ofo
//
//  Created by 王强 on 16/10/7.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCTBridgeModule.h"

@interface OfoReactManager : NSObject <RCTBridgeModule>

@end
