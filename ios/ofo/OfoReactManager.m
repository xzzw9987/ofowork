//
//  OfoReactManager.m
//  ofo
//
//  Created by 王强 on 16/10/7.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "OfoReactManager.h"
#import "RootViewController.h"
@implementation OfoReactManager

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(JumpTo:(NSString *)page allowBack:(BOOL) allowBack)
{
  dispatch_async(dispatch_get_main_queue(), ^{
    [[RootViewController getInstance]jumpTo:page];
  });
}

RCT_EXPORT_METHOD(JumpBack) {
  dispatch_async(dispatch_get_main_queue(), ^{
    [[RootViewController getInstance]popViewControllerAnimated:YES];
  });
}
@end
