//
//  RootViewController.m
//  ofo
//
//  Created by xinzhongzhu on 16/11/18.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "RootViewController.h"
#import "RCTBundleURLProvider.h"
#import "RCTRootView.h"
#import "RCTViewController.h"

RootViewController *root;
@interface RootViewController () {
  NSURL *jsCodeLocation;
}

@end

@implementation RootViewController

-(instancetype)init {
  self = [super initWithRootViewController:[RCTViewController createWithUrl:@"login"]];
  self.navigationBarHidden = YES;
    
  return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (void)viewDidAppear:(BOOL)animated {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)jumpTo:(NSString*) url {
  [self pushViewController:[RCTViewController createWithUrl:url] animated:YES];
}


+(instancetype)getInstance {
  if(!root)
    root = [[RootViewController alloc]init];
  return root ;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
