//
//  UpdateLocationBackground.m
//  ofo
//
//  Created by xinzhongzhu on 17/6/5.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import "UpdateLocationBackground.h"
#import <CoreLocation/CLError.h>
#import <CoreLocation/CLLocationManager.h>
#import <CoreLocation/CLLocationManagerDelegate.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import <MAMapKit/MAMapKit.h>
#import "SRWebSocket.h"

@interface SocketDelegate : NSObject<SRWebSocketDelegate>
@end

@implementation SocketDelegate

-(void)webSocketDidOpen:(SRWebSocket *)webSocket {
  NSLog(@"Initial Websocket");
}

-(void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
  NSString* string = message ;
  NSLog(@"Receive Message, %@", string);
}

-(void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
  NSLog(@"Error, %@", error);
}

@end


@implementation UpdateLocationBackground {
  NSString* mToken;
  NSString* wsURL;
  id<SRWebSocketDelegate> mSocketDelegate;
  id<AMapLocationManagerDelegate> mLocationManagerDelegate;
  
  SRWebSocket* mSocket;
  AMapLocationManager* mLocationManager;
  
  CLLocation* lastLocation;
  
  NSString* mFilePath;
  int mSequence;
}

-(instancetype)initWithToken:(NSString*)token
                     withURL:(NSString*)url
{
  if(self = [super init]) {
    mToken = token ;
    wsURL = url;
    [self makeInitialSeq];
    [self makeWebSocket];
    [self makeLocationManager];
  }
  return self ;
}

-(void)makeInitialSeq {
  
  NSString* sequenceString;
  NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
  sequenceString = [prefs stringForKey:@"sequence"];
  if(sequenceString == nil) {
    sequenceString = @"0";
  }
  mSequence = [sequenceString intValue];
  NSLog(@"%d,sequenceid", mSequence);
}

-(void)makeWebSocket {
  NSString* updateLocationURL = wsURL;
  NSURL* url = [[NSURL alloc] initWithString:updateLocationURL];
  NSURLRequest* request = [NSURLRequest requestWithURL:url];
  mSocket = [[SRWebSocket alloc]initWithURLRequest:request];
  mSocketDelegate = [[SocketDelegate alloc]init];
  mSocket.delegate = mSocketDelegate ;
  [mSocket open];
}

-(void)makeLocationManager {
  mLocationManager = [[AMapLocationManager alloc]init];
  mLocationManager.delegate = self;
  
  mLocationManager.distanceFilter = 10;
  
  [mLocationManager setPausesLocationUpdatesAutomatically:NO];
  if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9) {
    mLocationManager.allowsBackgroundLocationUpdates = YES;
  }
  
  [mLocationManager startUpdatingLocation];
}

-(void)amapLocationManager:(AMapLocationManager *)manager didUpdateLocation:(CLLocation *)location reGeocode:(AMapLocationReGeocode *)reGeocode {
  if(!mSocket) return;
  NSMutableDictionary* dict = [[NSMutableDictionary alloc]init];
  float distance = 0;
  if(lastLocation) {
    distance = MAMetersBetweenMapPoints(MAMapPointForCoordinate(lastLocation.coordinate), MAMapPointForCoordinate(location.coordinate));
  }
  
  lastLocation = location;
  
  NSDate* date = [NSDate date];
  int secsFrom1970 = (int)date.timeIntervalSince1970;
  dict[@"lat"] = @(location.coordinate.latitude);
  dict[@"lng"] = @(location.coordinate.longitude);
  dict[@"accuracy"] = @(location.horizontalAccuracy);
  dict[@"time"] = @(secsFrom1970);
  dict[@"distance"] = @(distance);
  dict[@"sequence_id"] = @(mSequence++);
  dict[@"token"] = mToken;
  dict[@"platform"] = @"ios";
  
  @try {
    NSError* error;
    [mSocket send:
     [NSJSONSerialization
      dataWithJSONObject:dict
      options:0
      error:&error]
     ];
    
    // Save file
    NSString* strToSave = [NSString stringWithFormat:@"%d", mSequence];
    NSLog(@"save,%@", strToSave);
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:strToSave forKey:@"sequence"];
  }
  @catch (NSException *exception) {
  }
}


@end
