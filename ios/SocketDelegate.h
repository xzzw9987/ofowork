//
//  SocketDelegate.h
//  ofo
//
//  Created by xinzhongzhu on 17/6/5.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SRWebSocket.h"

@interface SocketDelegate : NSObject<SRWebSocketDelegate>

@end
