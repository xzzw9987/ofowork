//
//  LaunchBridge.m
//  ofo
//
//  Created by xinzhongzhu on 16/11/18.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "LaunchBridge.h"
#import "RCTBundleURLProvider.h"
#import "CodePush.h"

RCTBridge* bridge ;

@implementation LaunchBridge

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge {
#ifdef DEBUG
  return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index.ios" fallbackResource:nil];
#else
  return [CodePush bundleURL];
#endif
}

+(RCTBridge*)getInstance {
  if(!bridge)
    bridge = [[RCTBridge alloc] initWithDelegate:[[LaunchBridge alloc]init] launchOptions:nil];
  return bridge;
}


@end
