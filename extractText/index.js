#!/usr/bin/env node

const fs = require('fs'),
    babel = require('babel-core'),
    path = require('path'),
    json = {};

let babelConf = {comments: false};

boot();

function parseProcessArgs() {
    const args = process.argv.slice(2);
    return args.reduce((result, arg) => {
        arg = arg.split('=');
        result[arg[0]] = arg[1] || null;
        return result;
    }, {});
}

function file(filePath) {
    console.log(`Process File: ${filePath}`);
    if (!(path.extname(filePath) === '.js' || path.extname(filePath) === '.jsx')) return;
    let content = fs.readFileSync(filePath).toString();
    content = babel.transform(content, babelConf);
    content = content.code;
    const chWords = content.match(/[\u4e00-\u9fa5]+/g);
    if (chWords) {
        chWords.forEach(word => {
            json[word] = '';
        })
    }
}

function walk(src) {
    try {
        const stat = fs.statSync(src);
        if (stat.isDirectory()) {
            fs.readdirSync(src)
                .forEach(p => walk(path.join(src, p)));
        }
        if (stat.isFile()) {
            file(src);
        }
    }
    catch (e) {

    }
}

function boot() {
    const {src = '', dst = '', rc = `${process.cwd()}/.babelrc`} = parseProcessArgs();
    console.log(`Parse from ${path.resolve('./', src)}\nOutput to ${path.resolve('./', dst)}`);

    if (!src || !dst) return;
    babelConf = Object.assign(babelConf, JSON.parse(fs.readFileSync(rc).toString()));
    walk(src);

    fs.writeFileSync(dst, `export default ${JSON.stringify(json)}`);
}
