#!/bin/sh

PRODUCT_DIR_NAME="./ios_distribution/ofo $(date +%F) $(date +%H)-$(date +%M)-$(date +%S)"

host="$host" ./genVersion.js --hostFile "app/runtime/apiBase.js" --versionFile "app/modules/version.js" --versionContent "$(date +%F) $(date +%H)-$(date +%M)-$(date +%S)"

fastlane gym \
--clean \
--workspace "./ios/ofo.xcworkspace" \
--scheme ofo \
--output_directory "$PRODUCT_DIR_NAME" \
--export_options "./exportOptions.plist" \
--build_path "./__IOS_ARCHIVE"

./deploy.js \
--filePath "$PRODUCT_DIR_NAME/ofo.ipa" \
--uKey "3956662e82c62ba25a28c11aecdfaa34" \
--apiKey "a4864c1b0c1a67c9ba9426ccad529f54" \
--platform "IOS"


