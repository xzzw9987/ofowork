import React from 'react';
import { render } from 'react-dom';
import { Router, Route, hashHistory } from 'react-router';
import memoryHistory from './app/runtime/memoryHistory';
import * as Pages from './app/pages';

const {
  Login,
  Splash,
  ProfileIndex,
  ProfileModifyPassword,
  ProfileModifyPasswordDone,
  RepairCarList,
  RepairCarDetail,
  RepairCarMap,
  RepairCarRecord,
  RepairBadCarMark,
  RepairBadCarMarkPos,
  RepairCarSearch,
  RepairCheckIn,
  RepairCheckRecords,
  RepairCarLocation,
  RepairCarSearchDone,
  OperationDashBoard,
  OperationOrderHistory,
  OperationOrderDetail,
  OperationOrderDetailCity,
  OperationOrderDetailSchool,
  OperationOrderDetailCountry,
  OperationOrderDetailRegion,
  OperationDashBoardCity,
  OperationDashBoardRegion,
  OperationDashBoardCountry,
  OperationReportManagement,
  RepairRateList,
  RepairUnlockCode,
  OperationDashBoardDistrict,
  OperationDashBoardSchool,
  OperationDashBoardSociety,
  OperationOrderDetailSociety,
  RepairReportVirtualPile,
  RepairSelectVirtualPosition,
  RepairerIndex,
  RepairReportDetail,
  ActivePanel,
  OperationReportDetail,
  RepairReportDone,
  ActiveInput,
  RepairerMapIndex,
  CodeReset,
  RepairEntry,
  ChainIndex,
  ChainSubmit,
  ChainSubmitNew,
  ChainList,
  ChainInput,
  ChainInputNew,
  ChooseZone,
  RepairDetail,
  BikeIndex,
  BikeMapIndex,
  BikeDetail,
  ChainCode,
  OperationEntry,
  CarSchelduling,
  TransportSub,
  FacScan,
  FacInput,
  OperationCarSearch,
  OperationCarInformation,
  OperationChangeUnlockCode,
  OperationRepairRate,
  OperationNewUsers,
  OperationHx,
  HxEntry,
  WorkRecord,
  CreateRecord,
  CreateDetail,
  WorkRecordDetail,
  HxList,
  HxMapIndex,
  HxProcessDone,
  HxDetail,
  HxHotSpot,
  OperationChangeBrand,
  OperationBadCarMap,
  OperationBadCarDetail,
  OperationUnbindBrand,
  OperationBadCarDone,
  OperationBadCarData,
  OperationBadCarMessage,
  LowElectricCar,
  LowElectricCarDetail,
  FactoryEntry,
  FactorySnScan,
  FactorySnInput,
  CarSearchCode,
  SupplyEntry,
  SupplyCodeSearch,
  SupplySearchInput,
  SupplyBarCode,
  SupplyBarInput,
  CompetitorInformation,
  CompetitorInformationMap,
  CompetitorInformationDetail,
  MovePathMap,
  MovePathDate,
  MovePathFence,
  MovePathPerson,
  ChainSnNew,
  SupplyFenderInput,
  RepairRateIndex,
  RepairRateSelectCity,
  RepairRateSelectChild,
  RepairRateSixty,
  ScanCodeLock,
  ScanCodeLockInput,
  ChainCodeUpgrade,
  ScmEntry,
  ScmInform,
  ScmScan,
  ScmScanList,
  ScmConfirm,
  SupplyUnbind
} = Pages;

const routes = (
  <Router history={memoryHistory}>
    <Route path="/" component={Login} />
    <Route path="/login" component={Login} />
    <Route path="/profile/index" component={ProfileIndex} />
    <Route path="/profile/modifyPassword" component={ProfileModifyPassword} />
    <Route
      path="/profile/modifyPasswordDone"
      component={ProfileModifyPasswordDone}
    />
    <Route path="/repairer/carList" component={RepairCarList} />
    <Route path="/repairer/carDetail" component={RepairCarDetail} />
    <Route path="/repairer/carMap" component={RepairCarMap} />
    <Route path="/repairer/carSearch" component={RepairCarSearch} />
    <Route path="/repairer/checkIn" component={RepairCheckIn} />
    <Route path="/repairer/checkRecords" component={RepairCheckRecords} />
    <Route path="/repairer/repairCarRecord" component={RepairCarRecord} />
    {/*维修车辆*/}
    <Route path="/repairer/repairBadCarMark" component={RepairBadCarMark} />
    {/*坏车标记*/}
    <Route
      path="/repairer/repairBadCarMarkPos"
      component={RepairBadCarMarkPos}
    />
    {/*地图选点*/}
    <Route path="/repairer/carLocation" component={RepairCarLocation} />
    <Route path="/repairer/carSearchDone" component={RepairCarSearchDone} />
    <Route path="/operation/dashBoard" component={OperationDashBoard} />
    <Route path="/operation/orderHistory" component={OperationOrderHistory} />
    <Route path="/operation/orderDetail" component={OperationOrderDetail} />
    <Route
      path="/operation/orderDetailCountry"
      component={OperationOrderDetailCountry}
    />
    <Route
      path="/operation/orderDetailCity"
      component={OperationOrderDetailCity}
    />
    <Route
      path="/operation/orderDetailSchool"
      component={OperationOrderDetailSchool}
    />
    <Route
      path="/operation/orderDetailRegion"
      component={OperationOrderDetailRegion}
    />
    <Route
      path="/operation/dashBoard/city"
      component={OperationDashBoardCity}
    />
    <Route
      path="/operation/dashBoard/region"
      component={OperationDashBoardRegion}
    />
    <Route
      path="/operation/dashBoard/country"
      component={OperationDashBoardCountry}
    />
    <Route path="/repairer/rateList" component={RepairRateList} />{/*每日维修率 */}
    <Route path="/repairer/unlockCode" component={RepairUnlockCode} />
    {/*修改解锁码 */}
    <Route
      path="/operation/dashBoard/district"
      component={OperationDashBoardDistrict}
    />
    <Route
      path="/operation/dashBoard/school"
      component={OperationDashBoardSchool}
    />
    <Route
      path="/operation/dashBoard/society"
      component={OperationDashBoardSociety}
    />
    <Route
      path="/operation/orderDetailSociety"
      component={OperationOrderDetailSociety}
    />
    <Route
      path="/repairer/reportVirtualPile"
      component={RepairReportVirtualPile}
    />
    <Route
      path="/repairer/selectVirtualPosition"
      component={RepairSelectVirtualPosition}
    />
    <Route path="/repairerIndex" component={RepairerIndex} />
    <Route
      path="/operation/reportManagement"
      component={OperationReportManagement}
    />
    <Route path="/operation/reportDetail" component={OperationReportDetail} />
    <Route path="/repair/reportDetail" component={RepairReportDetail} />
    <Route path="/repairer/carActive" component={ActivePanel} />
    <Route path="/repair/mapIndex" component={RepairerMapIndex} />
    <Route path="/repair/reportDone" component={RepairReportDone} />
    <Route path="/repair/entry" component={RepairEntry} />
    <Route path="/activeInput" component={ActiveInput} />
    <Route path="/codeReset" component={CodeReset} />
    <Route path="/chainIndex" component={ChainIndex} />
    <Route path="/chainInput" component={ChainInput} />
    <Route path="/chainInputNew" component={ChainInputNew} />
    <Route path="/chainSubmit" component={ChainSubmit} />
    <Route path="/chainSubmitNew" component={ChainSubmitNew} />
    <Route path="/chainList" component={ChainList} />
    <Route path="/chooseZone" component={ChooseZone} />
    <Route path="/repairer/detail" component={RepairDetail} />
    <Route path="/700BikeIndex" component={BikeIndex} />
    <Route path="/700Bike/Map" component={BikeMapIndex} />
    <Route path="/700Bike/detail" component={BikeDetail} />
    <Route path="/chainCode" component={ChainCode} />
    <Route path="/operation/entry" component={OperationEntry} />
    <Route path="/repairer/carSchelduling" component={CarSchelduling} />
    <Route path="/repairer/transportSubmit" component={TransportSub} />
    <Route path="/factory/scan" component={FacScan} />
    <Route path="/factory/manually" component={FacInput} />
    <Route path="/operation/carSearch" component={CarSearchCode} />
    <Route
      path="/operation/carInformation"
      component={OperationCarInformation}
    />
    <Route
      path="/operation/changeUnlockCode"
      component={OperationChangeUnlockCode}
    />
    <Route path="/operation/repairRate" component={OperationRepairRate} />
    <Route path="/operation/newUsers" component={OperationNewUsers} />
    <Route path="/operation/hx" component={OperationHx} />
    <Route path="/hx/entry" component={HxEntry} />
    <Route path="/hx/workRecord" component={WorkRecord} />
    <Route path="/hx/createRecord" component={CreateRecord} />
    <Route path="/hx/createDetail" component={CreateDetail} />
    <Route path="/hx/workRecord/detail" component={WorkRecordDetail} />
    <Route path="/hx/list" component={HxList} />
    <Route path="/hx/map" component={HxMapIndex} />
    <Route path="/hx/processDone" component={HxProcessDone} />
    <Route path="/hx/detail" component={HxDetail} />
    <Route path="/hx/hotSpot" component={HxHotSpot} />
    <Route path="/operation/changeBrand" component={OperationChangeBrand} />
    <Route path="/operation/badCarMap" component={OperationBadCarMap} />
    <Route path="/operation/badCarDetail" component={OperationBadCarDetail} />
    <Route path="/operation/unbindBrand" component={OperationUnbindBrand} />
    <Route path="/operation/badCarDone" component={OperationBadCarDone} />
    <Route path="/operation/badCarData" component={OperationBadCarData} />
    <Route path="/operation/badCarMessage" component={OperationBadCarMessage} />
    <Route path="/operation/lowElectricCar/map" component={LowElectricCar} />
    <Route
      path="/operation/lowElectricCar/detail"
      component={LowElectricCarDetail}
    />
    <Route path="/factory/entry" component={FactoryEntry} />
    <Route path="/factory/codeSearch" component={FactorySnScan} />
    <Route path="/factory/snInput" component={FactorySnInput} />
    <Route path="/operation/carSearch/code" component={OperationCarSearch} />
    <Route path="/supply/entry" component={SupplyEntry} />
    <Route path="/supply/codeSearch" component={SupplyCodeSearch} />
    <Route path="/supply/searchInput" component={SupplySearchInput} />
    <Route path="/supply/barCode" component={SupplyBarCode} />
    <Route path="/supply/barCodeInput" component={SupplyBarInput} />
    <Route
      path="/operation/collectCompetitor/information"
      component={CompetitorInformation}
    />
    <Route
      path="/competitorInformation/map"
      component={CompetitorInformationMap}
    />
    <Route
      path="/competitorInformation/detail"
      component={CompetitorInformationDetail}
    />
    <Route path="/movePath/map" component={MovePathMap} />
    <Route path="/movePath/date" component={MovePathDate} />
    <Route path="/movePath/fence" component={MovePathFence} />
    <Route path="/movePath/person" component={MovePathPerson} />
    <Route path="/chainSnNew" component={ChainSnNew} />
    <Route path="/supply/fenderCodeInput" component={SupplyFenderInput} />
    <Route path="/operation/repairRateNew" component={RepairRateIndex} />
    <Route path="/repairRateSelectCity" component={RepairRateSelectCity} />
    <Route path="/repairRateSelectChild" component={RepairRateSelectChild} />
    <Route path="/repairRateSixty" component={RepairRateSixty} />
    <Route path="/scanCode/lock" component={ScanCodeLock} />
    <Route path="/scanCode/lockInput" component={ScanCodeLockInput} />
    <Route path="/chainCodeUpgrade" component={ChainCodeUpgrade} />
    <Route path="/scm/entry" component={ScmEntry} />
    <Route path="/scm/inform" component={ScmInform} />
    <Route path="/scm/scan" component={ScmScan} />
    <Route path="/scm/scanList" component={ScmScanList} />
    <Route path="/scm/confirm" component={ScmConfirm} />
    <Route path="/supply/unbind" component={SupplyUnbind} />
  </Router>
);

export default routes;
