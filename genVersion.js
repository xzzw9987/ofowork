#!/usr/bin/env node

const fs = require('fs'),
    path = require('path');

const argv = process.argv.slice(2),
    conf = walk(argv);
console.log(conf);

fs.createWriteStream(
    path.resolve(process.cwd(), conf['versionFile'][0])
).end(`export default '${conf['versionContent'][0]}';`);

fs.createWriteStream(
    path.resolve(process.cwd(), conf['hostFile'][0])
).end(`export default '${process.env['host'] || 'https://test-pyramid-api.ofo.so'}';`);

function walk(argv) {
    const result = {};
    let current = [];
    argv.forEach(v=> {
        if (v.indexOf('--') > -1) {
            current = result[v.substring(2)] = [];
        }
        else current.push(v);
    });
    return result;
}