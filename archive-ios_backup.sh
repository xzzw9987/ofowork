#!/bin/sh

PRODUCT_DIR_NAME="./ios_distribution/ofo $(date +%F) $(date +%H)-$(date +%M)-$(date +%S)"

host="$host" ./genVersion.js --hostFile "app/runtime/apiBase.js" --versionFile "app/modules/version.js" --versionContent "$(date +%F) $(date +%H)-$(date +%M)-$(date +%S)"

rm -rf __temp__archive.xcarchive
rm -rf __temp__ios__debug
cd ios

xcodebuild \
-workspace ofo.xcworkspace \
-configuration Release \
-scheme ofo \
-destination 'generic/platform=iOS' \
-derivedDataPath '../__temp__ios__debug/' \
-archivePath '../__temp__archive.xcarchive' \
archive

cd ..

xcodebuild \
-exportArchive \
-archivePath './__temp__archive.xcarchive' \
-exportPath "$PRODUCT_DIR_NAME/ofo.ipa" \
-exportOptionsPlist './exportOptions.plist'


rm -rf __temp__archive.xcarchive
rm -rf __temp__ios__debug
