#!/usr/bin/env node

const fs = require('fs'),
    path = require('path'),
    FormData = require('form-data'),
    argv = process.argv.slice(2),
    conf = walk(argv),
    https = require('https'),
    form = new FormData,
    url = require('url');

console.log(conf['uKey'][0], conf['apiKey'][0], conf['platform'][0]);
const fileStream = fs.createReadStream(conf.filePath[0]);
form.append('uKey', conf['uKey'][0]);
form.append('_api_key', conf['apiKey'][0]);
form.append('file', fileStream);
const request = https.request(
    Object.assign({},
        url.parse('https://qiniu-storage.pgyer.com/apiv1/app/upload'),
        {
            method: 'POST',
            headers: form.getHeaders()
        }), res => {
        res.setEncoding('utf8');
        res.on('data', d => {

            const json = JSON.parse(d);
            https.request(Object.assign({},
                url.parse('https://oapi.dingtalk.com/robot/send?access_token=b308f48b0cb3a64a7a1de039b07fa886c4761a32d2c4bca9b604f24b7e625f91'),
                {
                    headers: {'content-type': 'application/json'},
                    method: 'POST'
                }
            ), () => {
            }).end(JSON.stringify(
                {
                    "msgtype": "link",
                    "link": {
                        "text": `快访问\nhttps://www.pgyer.com/app/view/${json['data']['appKey']}\n看看你的打包成果吧!`,
                        "title": `${conf.platform[0]} Work打包成功!`,
                        "messageUrl": `https://www.pgyer.com/app/view/${json['data']['appKey']}`
                    }
                }
            ));

        });
    });

form.pipe(request);


function walk(argv) {
    const result = {};
    let current = [];
    argv.forEach(v => {
        if (v.indexOf('--') > -1) {
            current = result[v.substring(2)] = [];
        }
        else current.push(v);
    });
    return result;
}
