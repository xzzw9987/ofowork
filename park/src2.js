(function() {

    var hostname = 'https://test-pyramid-api.ofo.so/';
    var container = document.querySelector('.container');
    var mask = document.querySelector('.tip');

    if (localStorage.getItem('token')) {
        container.classList.remove('logining');
        mask.classList.remove('show-loading');
        showMap();
    }
    document.querySelector('.submit').addEventListener('click', function() {
        mask.classList.add('show-loading');
        var phone = document.querySelector('#username').value;
        var password = document.querySelector('#password').value;
        var logParam = {phone: phone, password: password};
        return request('user/login', logParam)
            .then(function(d) {
                if (d.code === 0) {
                    document.querySelector('.login').style.display = 'none';
                    container.classList.remove('logining');
                    mask.classList.remove('show-loading');
                    localStorage.setItem('token', d.data.token);
                    showMap();
                } else {
                    mask.classList.remove('show-loading');
                    window.alert(d.message);
                }
            });
    });

    // document.querySelector('.time-search').addEventListener('click', function() {
    //     var time = document.querySelector('#time').value;
    //     console.log(time);
    // });

    function request(path, requestParams) {
        return fetch(hostname + path, {
            method: 'POST',
            body: (function (params) {
                var form = new FormData;
                for (var key in params) {
                    form.append(key, params[key]);
                }
                return form;
            })(requestParams),
            credentials: true
        })
            .then(function(response){return response.json();});
    }
    function showMap() {

        var map = new AMap.Map(document.querySelector('.map-container')),
            token = localStorage.getItem('token'),
            requestParams = {};

        document.querySelector('.time-search').addEventListener('click', timeSearch);

        map.on('moveend', singleton(function (e) {
            hidePanel();
            timeSearch();

        }, 1000));

        function timeSearch() {
            var lat = map.getCenter().lat,
                lng = map.getCenter().lng,
                time = document.querySelector('#time').value || 0.5;

            Object.assign(requestParams, {lat: lat, lng: lng, token: token, time: time});
            makeAnnotations(requestParams, function(e) {
                onClickAnnotation(e);
            });
        }

        var lastCar = null;

        function singleton(callback, duration) {
            var isBusy = false;
            return function () {
                if (isBusy) return;
                isBusy = true;
                callback.apply(this, arguments);
                setTimeout(function () {
                    isBusy = false;
                }, duration);
            };
        }

        document.querySelector('.refresh').addEventListener('click', singleton(function () {
            this.classList.add('rotate');
            setTimeout((function () {
                this.classList.remove('rotate');
            }).bind(this), 500);
            timeSearch();
        }, 500));

        document.querySelector('.js-close').addEventListener('click', hidePanel);

        document.querySelector('.panel').addEventListener('click', handlePanel);

        function handlePanel(e) {
            var newParams;

            e.target.classList.contains('js-not-found') && destiny({type: 0, token: token});
            e.target.classList.contains('js-found') && destiny({type: 1, token: token});

            function destiny(obj) {
                newParams = Object.assign({}, lastCar, obj);
                delete newParams.position;
                return request('stop/check-stop', newParams)
                    .then(function(d) {
                        d.code === 0 &&
                        hidePanel();
                        toast('操作成功!');
                    });
            }
        }

        function showPanel(config) {
            var cls = document.querySelector('.panel').classList;
            cls.add('show-panel');
            cls.remove('hide-panel');

            document.querySelector('.js-bicycle-number').textContent = config.carCode;
            document.querySelector('.js-bicycle-time').textContent = config.time;
        }

        function hidePanel() {
            var cls = document.querySelector('.panel').classList;
            cls.add('hide-panel');
            cls.remove('show-panel');
            lastCar = null;
        }

        function onClickAnnotation(e) {
            lastCar = e;
            showPanel({
                carCode: e['car_no'],
                time: e['time']
            });
        }

        function makeAnnotations(position, onClick) {

            fetchAnnotations(position).then(function (payload) {

                map.remove(map.getAllOverlays('marker'));

                payload.forEach(function (pos) {
                    wrapAnnotation(pos);
                });


                function wrapAnnotation(pos) {
                    var icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAzCAYAAADsBOpPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6Qjg3OERERkRBMzMyMTFFNkE4N0FGODVDN0FCRjE4MDUiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6Qjg3OERERkVBMzMyMTFFNkE4N0FGODVDN0FCRjE4MDUiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpCODc4RERGQkEzMzIxMUU2QTg3QUY4NUM3QUJGMTgwNSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpCODc4RERGQ0EzMzIxMUU2QTg3QUY4NUM3QUJGMTgwNSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pmvj/ZIAAA0DSURBVHjaxFoJcFRFGv7mnoTcCQkJCSHhDKgLCQFRAUEOo4KcCgoi7oqrHHIs61axtVZRpaKrgiKLW4XHegArLiqHJCBELhGCiFyJyJ1EQo5JMslMMjOZefu9fm8uCBACaBf/TOYd3V///fV/NRpJwuWtmuKg6CgXKSZKk/rtpjgpbShmxKMKQ2BEJu93h4TOvBoFDe9K4sl6Sjm0KOL3CV7bQfme7zngUfuUJZaioRjE08oYYZRU9XpA0+N6mkYVLSbCjkfQgBwB2yGu+ZtfCdGUFALI8r0PXCDIz/nMav7ei+tsmhZr2C1+P8HB5nOwO3ygZBAeFaQnCKz/vjbg2zshZfIb2N9rSMBucV13bQ23DLAZ2aTBaxzoXh8ISV1OSV0nY3tOrAeXNZnAwpWHPGREUyn7OkYaFQMu9V19ABB5kiFYwTHmI5xrVtoawBYVjMyp83iO38uDltulaqtNXzJ2PL9HEGx39mZsfh0ldub8hYzOBWrXAbbdCiiD2o9TrGIhIjAVdSgQ/V8X4FpKnZjpMlgxU2haowKVNRJFgHHz2OlwtKrZdgKVb3Al1yv9GlWtSmIS4/j3OrGpWwy4gfILPmQHU0VnkkoRcxyQ9A4Q+ShuSrMS8K+zON55ZZ/4N+xkavhTdGgOsOWSTjxCu29Ty7PEksmtUdbq3UCH1eRfCm5qc1cCxZOp7TxF0xofh0cgDVsutxI/BfwyCU0+w1m+G6TZ2NEE+wVuaSuZBlR8CN+4WhIwnpBN3IYB1kcj1QS8VIFuBFjkWwYZbPQwoOOWZsdoamqCrb4eRpMRISGhNw66+BFye61slRQtu3GIlqO3sCoqYC0i+ekVFz4RD3o3WChJlLrpiv1//91e5AwdjgXz/oLi88U3DjjlMyD8NvgckRa9iGeO4HKqIhrprArQiUf54BqfI5ClK/livuPKCikuxppPPsVjU6YgPDwcEZERNw7adQ74OZ0a9Si4dFRdW5JDgxpFw16T5cAr/pcoCc9fFaxQSEoKNFotCo8fvzlg5WagGtstUvyAVijOQBb/jf4AOCdfkq2EDaMIOs23Q83kY/yrLVOIy4mGBnuz9+rJ70MHfyRdzuPnwiIsfull5G/ffu1O4xaSjvEKaCUomoVQMjtCBpwqCP0nQQGoHi5mBh80XXtjl1qg0bXBAw891Lx/IOAd+fmYMHos+mVlYUtuHlxOZ8s0HTdfUR4EXUMZA05AR5nDlYyoKmkfXCSHN0boepLmpdM1+8zfuhZ19VqMGjPuqs8tefPfaHJZseCFBS2nRhPt84lEUqLJG39sJMKRejqIwXQMOmH/5MlHZLUIrNz6ddkPg1b241cH/IeeUci+c/z1cVlPrxo2mHHNVm88PpAaN2v54y6fS5RpETqspS4K5pAYvmNDk+XzKz4lucoxsOdehEeGX/8GDL1PwaU4kgji66NlKNk1wLtww/W6Sg+yj65SEgnXYXgM3eipV6BJDmLc1mbf8NhpGj2MpFzVASZItlCVaoR1lSZj8ToNp8hleuoJMt03CxHXpjf7btmJ93HyxHEUl5QhKqYDsjLKEX/7AJlD0Ib2hqvqUxjin70ccMMx6OO56w1niXsL9uwsxNEjR+klPWgbF4mcB4YjskMm50H7augR/LJMTZ0cV/vcXJpeTWNUR82buraXDbr64/eQ9/UaxCRmIyzqHtjONKKp4QjcB9Zh8IhwRCZNROOZV32xkj8Mrhaxo8achNOHl2Pjus3ILeiNyOjOCA0x4uL+MqxZ/zEevX8lJj3Vh4CXBHego2nThnD17EqMYUG0Xk0nVcBktzY4JljxznLkf7sLEx6bi7SUeMQlJMMonUf5mRKsXJ+Mgp8+wAv/iEZ4bDocJS/CGMvQUx8DDTeN23aYWtfjxIEleHbeTxg7ZgQWL+KqmHsjKlyDujo7tn27D2/850s4NKV4chatk8hjvXiIUiMHFnaFATq00dNkSP48zK1kB2r7dtt2bN22DX+c/jQezMlBg7UadmcYYqNOI6n9Y5idmo7Fr1Tgi1WfY+rM+5lerofblAi92SQsvl5zGlK9AW8uK0DmgNGYMHks4ttWwlrJkSOToeugRY+ePRAZFYb3VqxERvdl6DdsaUAQLONx++NkLST+E6m4d0uTL7UK9zwe7NnzHbKy+2LQPXfBUnEB5VW10Hi4WRx1BN4VSbE6jBo5BN8dLMaFk06YUmdCT2/kMY+HZH4Q1TVV2PC/IzhzkUlhr0TYbToUHjqLkrO7YKmxcQw3HLZ6DOrfG2nd7sBX65n74UzABuCmlOyBQXy9Xt2uyT637Cqh3ctAZUU5rDYbhg4bBr3egHqXGzZ7Ewp/WI5uGdkwRjYhJsaArp1T8U1ICqyWs0jsPB95a2dh9ZfToDO1w13903H8qBaJCY3QcuUWvsAJGUwwGj1ol3gU06c/gzZhoUhOTkXf7NuxJzcf9b/uRliSuvFdjAA9rkCGVmoJ9JQvE5bZ4CxUY4QmroYbWu5SW30dwiMiYSadik7aUFoZTRdphbPRDo+bYPShcGqVQVascqJzRh/U1pTC6YxC9x6ZMBgk0d/g+4bg+blzERvbHufOnYHdbofL4YTb7UKoWcfho9GkDbAUjkJ/wqvgOyVzuDCoxmDfw1hiNuLaxiMsLExEYgPv7g9bXR3fc2PStJfhaLBCosbMRhMqKytQXUlTlzAQcn7objyLXTtOw6DXo7qqVFieEz+fwOgxYzDp8UkoL7uAZ/48HW4G/2YG/ga9EbW1NSgqPIy4dhmIatcnILfcG1znCMERPS/s8psROWLZIrhsYmddOndC7tatuPPOfritWxekdOhIbdvojk2ICI+BtcGF3Lxv0COjO5KSolBQsB+LXnoFW/Py0DGtIx6ZOAmOxkbsL/gBtdY61FosiI9PgJMRXiO16+YKRsfF48DBg9i3dx9mzLjEjts2KZiUkkA5ifsjpGMIodRRJCG8JNWuleRWX1cnzZ8zT5r9/Bxp/4EfJGpCslSUS42NDdKxY0elxa++Jo0bPVY6fvSYeN5qtUrNtU8++kjq36+/tHrNZ5LFUiXVUDxuj+RwNEo7du2Uxo2bID01ZarEyflfqs+XpEPEclSVIqySTsvR2i9iBh+TH5PVzIMusC/zuH1KCMmsYtnSt1FRbUFcXBwSkxJRU10rsg0tOTBz1kz0yux9zbDg3eX/wmf/XYuUtFT06dNH0K2oqAgF+/YjmX2++dZS0jDAaZ3PoaPI9SelBozk50aNVCQA383AYrcgt7ey02UbXcoQ3/ubN20SLlVDTja5PWjPQUaNfpg2NKrFscy+vd/j642bcO78OTi42RISEpDJyT4x7clLQpYfGVpmKnRQXPJFpv1JsqHTSMd9tbIDlCxffBLKXd/l1C3J6GVe2+0NiI6Jbv6B04RhPejXrp6JqAFvyX/7Kz8W3IuzyBcptjfFT5rD3G4JftNW9TLpsDCwElRFhSYJsgrAVd4dKaqWX3I2D/sqQLJdTl/FgGzSbwPWRs6ezFHsrs4bnzM7MGGdt/ygoVXwl7YNiOENOfEP9zkSeQXSv2b0n3Nrwco29+QAJXYw+CqaXzFaHx1YX5PjeAgJFZGghQ+PD4qPBaceAGpX3TqwdRuDwSqVzDLSc6LPy6mibeZIYAuvzvVFSAb1+pnHGcX/9eaDLV9EhYwMBivXTzthADqIFCeoBQOWfDWspQT5D981nartC/+kJriD6zbfOND67cCpe4DSF5X+Db4DHzsiMYikPHkJOrUYeOoSDXtPdpTfs9jJ20E1Wm/wFEEbHf0Uv8dcFvRfOSN1KDXhmg9Isc3KpjIGlHmNKCHwBzmBw6KehusHLK/BUP5+nx2mBB2qeJ+Ri9yhQxmYZIqwFIYU9YwDAWccNPaNh5U4paEsmGr+unSuODIIYcwg56bprQEMtWOPOEl7ndp9MmgQb2nAHbBJxSZRZyYX9DzwV3B0AaGidywNN7oRC2XvLcYyq8l5WvOAtddcRo8YqIomZhqfHsQBvhDgvGVZnbqsJrU3MQmPIt7SgVEVbcA+caOWPH2d7/UUBfQWNm3LuKcC12AnP8dykCwOtpLXGwM2qv8sLlCLuOS+gfG3EbPZXyfGBwv4dxmklu9V7XXvbklo8yAHe5qDdmQPLzXbiybo5FRe/uMENwpt0YPXlomKjFtVBG4lYATx9iJB/J0TyCa/TzV7CqqswA7ezyboDT6X28qmvWF7qtjpAwTen9/WINAKh2t4bzjF7qvu4/cELEd17SgZqEA8pojfgaW4eJqqznQH3f2lht8PsPc4qspXI1xPLW8IiEXyCHq9qP1VX2JJfnPAbtVmyk7Oqh6iO8SB4AzfoY4Jz4l78vlEmfqO7Qqn/i0tG7fupAfKf8qIVO2wFLTJivErtSwbsDicvmxFNOrBd0nrQOtbTYWwq9V18SKBacSErtQutA7w/wUYANtZHPGPumppAAAAAElFTkSuQmCC';
                    var iconOn = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAzCAYAAADsBOpPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpDN0FBRkY4MkQwMDcxMUU2QjFFNzkwRTI1QTZBQTU2OSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpDN0FBRkY4M0QwMDcxMUU2QjFFNzkwRTI1QTZBQTU2OSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkJDQTczQkZGRDAwMTExRTZCMUU3OTBFMjVBNkFBNTY5IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkJDQTczQzAwRDAwMTExRTZCMUU3OTBFMjVBNkFBNTY5Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+4veC4QAACThJREFUeNrUmQlUU2cWx//v5WWRLSGEAGFHQBYXZFHEutV9q0t1XKYddTpjpy7TUetYj+e0c1qr2EWdodR2xk5PjzMuI9Na6oaKoA4iYFWQTaQgi4AsARISyD5foqUCIQkSnDPvnISQfO9+v/d/997vfvdRWCPFIA7DM55HPeuEzHMG7X0+NdTAgwUdNDhjT9BZwwswyksKVwc9WLQBrUoWChsEOFsWZTdwZjCwEe7VeGdOOaaMCoPQNQQ0PQUMq+cYrY4YMGjR3v4jrt4tQuIFH+TVhViai7Ls/JaDzizsvJA7+HgFg5DASX0ArR3GC6iqvYG3T8iQUhw34KC0BGwW9sbWHMREzhkwqDnworJ0RO2LHhA0bStsnOQ+2pMUdoE1+SKxERk6HR2f6Ux3zFbBaFsGrhiZi+vvj4OLs49dYJ+GdnQQ4bu3p+EPCVdtgqatDTBG/j+2zDYLqtWSYGprQ2enctDgH722COuir1sdS1sbcHpnQr+q3riejbkzZmH71rdQU10zaOi/bpwPD8dWiyrTltSt/7AOFMXudxL/wAAseXkpdu7aBT6fbxcXubeXsega/Sq8d246RG6RFn3W19cXFE2jpLgYLnwXu/i1o6Mfvl6ZbtUl+qi7delCmwJMo1H368MdHR24c+s2cZdq3CspReIHe5Bx+bJVlVfPXNavymYVPrDwElm1uFZhax9KQbEcMW/BArO/KwjwlYwMLF+8FONjYnDhfBo0arVNSh9/Nb3fpbmPumumx9ik7v3idESE+YHFMj/Yw9MTb27bCj3lCK1Ghu07ttvsywvipwJHbKglQt1q4ew8yibD40NywabbyaeXLY4bEylAXPyyAfkylyfCrOAruFA+uodb9HGJTRNLbVwcdOANEwJ6BbTSlP6LEU0jJkdmw5nvPOCMsXZ8s/U8PC7YkuEu8moxhhOJtgLo2SPA+B6CtjWV8MvMnqFX5pM3ORn/U37VkFcbeRlh5BahowLdrbuEt8jD7MkNZX9HeVkxamobIBD6ISa8EeJRk8gvLqAdxkLT8k+wxW/0Be4sAiPeBbAfEO4LyLpagsK7hWSV1MNdxMfcebPA9yMFkIZIyo7oGQNu3taBnRyEfQYdO/Il0s4eh9ArDk6CF6Co7IK28y50N7/BtNnO4EtWoqtyH3ovMQatUVUOKJ4EFQXJOP3NOZzPGwu+azAchnHwKLcBx1OPYMWcw1j161gCfKDH+Q7D3E2uZxGYzR7W4/9DnyYjI/Malq/egkBfMUQePuAYqtFYWYvDqaQYz/8KO95xhbNbEFS174LjtoJYFYJiRNApCojqDMpuHsAbW/OxdMlsJL5H7gpvLATOFORyJdIzc/DJ16egoh5i7eZyMmPwz/5Kc8h7p2VgvV7b/Tkz/TIupqfjtfW/xfy5c9Epa4VS7QQ3QQUk3qvxe/8gJO5twrdHU7Bm0xyylqdCx/UCw+OaylmGqoChg439SXmInrQYy19ZCrF7M2TNLDjyfcDyoxERGQG+wAlfHjqM8LAkjJ95sLsUNvRS12zQqdTyJ+B6ZGVdR0zcOEx5IQHSpno0trSD0pNgUckJeCgkbiy8tPBFXL9Vg/pyNbj+m8CQFVrPWwYDbz5a21rw/b/vovIRD3FRXlAqWCi58wC1D65B2qYgc+igUnRgyoSxCBwxGt+lFpGZK59aReXWfbilrZHUECPQ3NQImUKBGTNngmHY6NDooFBqUfJDMkaEx4HD10IoZCM02B+XhvlCJn0Ar+BtSDu5GcdOrQOL64mECUEoLqTh5dEFmuzrdu0gF8TmgsPRw9OrEOvXvw5HJwf4+PhjXNwoZJ3PQEfdf+AkCTKxtLXXk/cAywrfq2t6cnVaGHQ60CyKLLFyOLvwweMBpeUKPGx2BWWQQd2lhF5HYBgHqOnHkxw6qkZweCypkx9CrRYgLCKaxIXBZG/a9Bfx5pYtcCPRX1VVCaVSCY1KDZ1OAwceC1q4Qkv/nCkq6uusK3zqNhfzEgCRuxhOTk6mSmzyxAlQyOXk6nRYtW4PVJ0y006Yx+GiubkJrc0k1XlMJt+RmO56gGtXKsBmGLS2PCTuSKHsXhkWL1mCVb9chcaGerz+u/XQkeKfx+WQcRyyo25DaUkBRJ7hEHjGdu/5LhaprAN/dSsBXxg04BJjIcHDcf7iRcTHj8fIESHw9QsgaivIcswl2yUhZJ0anE+7hIjwMEgkAuTl5eK9D/biYloaAkit/IuVq6Dq6kJu3g9ol8nRLpVCLPaAmlR4XURdHbmDriIxbt66hZzsHGzc2DOP7748rU8ByELUjj/1/nZhaBa8xcMRFBSE3Bs5uJ1fAE9vX7i4uJiUcSR/yysqcez4CRTeuYMNGzdA7OFh+t0/IAATJ01C5MjH9QhDlNZpNUhOSoab2BPePt7QEfdw4QvBYrORlZ2NTz7aDy93d2z743awmMcaVlRnIylT1KfE+Gmbb+jdIMlPHGNaz2trapB08C9oapVCJBLBS+KFttZ21JDvaeIDmzZvQlT0WKu1wefJn+FfJ07CN9AfsbGxJncrLS1FXk4ufIjN/X8+SNzQvdsdFu3LMNcxoswCm4Lv3WKEBk7s/v/cmTOmJZUip2h1eniTSV5avIjkUIHNBU1O9g2cPX0GVdVVUJFg8yB3JZpc7K/Wre25NXtUAMkOX7O9iqcbKT2gGVqHzr+J7Lqt7871xK+Vyk64Cl3NNljC3irCj60Ss42Vfvd0Wj0LH6ekmAzY++CS/Ngf7PH0FHOwZvNwn9bQznPTcbPo3JBAm4Mtq7yCV49Ot9i2stqXmLA/nhjKHFJoo+26hpuI3D3aao+NtqUBF7l7DHLvDo3SRpvF9zPgv2u4TeNpW7uGEw/G49NvT9oV2mjrSNpJjEmMsrmDOeB263DXOqRuaEJo0NRnziBG0IqqLLxymG2puU3ZraFtPKb4F2PP4nbERE4l23wHq/BGSL1ehfzSTLx/msH396ItN9qfsQNv0/ON38RkYUa4CuE+QoiF7uByHJ/U1kpIZU0oqpbi2n0WkrIn23IDBvXIYKieHg0Y1Oa09pQx6n8Na+tTJHOGDc8b9FmB7QE+qDtF20Eh6nnB2gN4ICB2iQH6OQWO3QKWfg7RbtfsYm/gIT+GApgaKnX/LxX+rwADAFRwZt9A0z/PAAAAAElFTkSuQmCC';
                    var marker = new AMap.Marker(
                        Object.assign(
                            {},
                            pos,
                            {
                                map: map,
                                offset: new AMap.Pixel(-22, -51),
                                icon: icon
                            })
                    );

                    marker.on('click', function () {
                        map.getAllOverlays('marker').forEach(function (marker) {
                            marker.setIcon(icon);
                        });
                        marker.setIcon(iconOn);
                        onClick && onClick(pos);
                    });
                    return marker;
                }
            });
        }

        function fetchAnnotations() {
            return request('stop/near-by2', requestParams)
                .then(function (d) {
                    return d.data.list.map(function (v) {
                        return Object.assign({
                            position: new AMap.LngLat(v.lng, v.lat)
                        }, v);
                    });
                });
        }

        map.plugin('AMap.Geolocation', function () {
            var geolocation = new AMap.Geolocation({
                enableHighAccuracy: true,//是否使用高精度定位，默认:true
                timeout: 10000,          //超过10秒后停止定位，默认：无穷大
                maximumAge: 0,           //定位结果缓存0毫秒，默认：0
                convert: true,           //自动偏移坐标，偏移后的坐标为高德坐标，默认：true
                showButton: true,        //显示定位按钮，默认：true
                buttonPosition: 'LB',    //定位按钮停靠位置，默认：'LB'，左下角
                buttonOffset: new AMap.Pixel(10, 20),//定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
                showMarker: true,        //定位成功后在定位到的位置显示点标记，默认：true
                showCircle: true,        //定位成功后用圆圈表示定位精度范围，默认：true
                panToLocation: true,     //定位成功后将定位到的位置作为地图中心点，默认：true
                zoomToAccuracy: true      //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
            });
            map.addControl(geolocation);
            geolocation.getCurrentPosition();
        });


        function toast(text, duration) {
            var div = document.createElement('div');

            Object.assign(div.style, {
                background: '#000',
                opacity: .4,
                padding: '30px 0',
                color: '#fff',
                fontSize: '12px',
                lineHeight: 2,
                position: 'fixed',
                top: '50%',
                margin: '-12px auto 0',
                left: 0,
                right: 0,
                width: '80px',
                textAlign: 'center',
                borderRadius: '10px'
            });

            div.textContent = text;
            document.body.appendChild(div);

            setTimeout(function () {
                div.parentNode.removeChild(div);
            }, duration || 1000);
        }


    }

})(window);
