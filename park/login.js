(function() {
    
    var hostname = 'https://test-pyramid-api.ofo.so/user/login';
    
    document.addEventListener('DOMContentLoaded', function() {
        var container = document.querySelector('.container');
        var mask = document.querySelector('.tip');

        document.querySelector('.submit').addEventListener('click', function() {
            mask.classList.add('show-loading');
            var phone = document.querySelector('#username').value;
            var password = document.querySelector('#password').value;
            var request = {phone: phone, password: password};
            return fetch(hostname, {
                method: 'POST',
                body: (function (params) {
                    var form = new FormData;
                    for (var key in params) {
                        form.append(key, params[key]);
                    }
                    return form;
                })(request),
                credentials: true
            })
                .then(function(res){return res.json();})
                .then(function(d) {
                    if (d.code === 0) {
                        document.querySelector('.login').style.display = 'none';
                        container.classList.remove('logining');
                        mask.classList.remove('show-loading');
                        window.map();
                    } else {
                        mask.classList.remove('show-loading');
                        window.alert(d.message);
                    }
                });
        });
    });

})(window);
