#!/bin/sh

echo "=====>Start archiving ios bundle..."
host="$host" ./archive-ios.sh
echo "=====>Finished archiving ios bundle"

echo "=====>Start archiving android bundle..."
host="$host" ./archive-android.sh
echo "=====>Finished archiving android bundle..."