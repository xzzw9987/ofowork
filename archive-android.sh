#!/bin/sh

NOW="$(date +%F) $(date +%H)-$(date +%M)-$(date +%S)"
host="$host" ./genVersion.js --hostFile "app/runtime/apiBase.js"  --versionFile "app/modules/version.js" --versionContent "$NOW"
cd android
./gradlew assembleRelease
cd ..
mkdir android_distribution/"$NOW"
cp android/app/build/outputs/apk/app-release.apk android_distribution/"$NOW"

./deploy.js \
--filePath "android_distribution/$NOW/app-release.apk" \
--uKey "3956662e82c62ba25a28c11aecdfaa34" \
--apiKey "a4864c1b0c1a67c9ba9426ccad529f54" \
--platform "Android"
