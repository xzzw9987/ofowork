var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');
var fs = require('fs');

/*eslint-env node*/
module.exports = {
    // Debug-ONLY settings
    devtool: 'eval',
    watch: true,
    entry: ['babel-polyfill', 'react-hot-loader/patch', 'webpack-dev-server/client?http://localhost:8080', 'webpack/hot/only-dev-server', __dirname + '/../../index.web.js'],
    output: {
        path: __dirname + '/../public/',
        filename: '[id].js',
        chunkFilename: 'chunk_[id].js',
        publicPath: '/web/public/'
    },
    resolve: {
        modules: [
            path.resolve(__dirname, '../../node_modules'), 'node_modules'
        ],
        extensions: ['.web.js', '.js', '.json', '.less']
    },
    module: {
        rules: [
            // handle js/jsx files
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                options: JSON.parse(fs.readFileSync(__dirname + '/.babelrc', 'utf8')),
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.less$/,
                loader: 'style-loader!css-loader!less-loader'
            },
            // images, copy directly. Consider embed them with url-loader?
            {
                test: /\.jpg|\.png|\.gif|\.svg|\.ico$/,
                loader: 'file-loader'
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('development')
            }
        }),
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, '../index.html')
        })
    ],
};
