var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');
var fs = require('fs');

/*eslint-env node*/
module.exports = {
    // Debug-ONLY settings
    debug: true,
    devtool: "eval",
    watch: true,
    entry: ['babel-polyfill', 'webpack-hot-middleware/client', __dirname + '/../../index.web.js'],
    output: {
        path: __dirname + '/../public/',
        filename: '[id].js',
        chunkFilename: 'chunk_[id].js',
        publicPath: '/web/public/'
    },
    resolve: {
        modulesDirectories: ['node_modules', path.join(__dirname, '../node_modules')],
        extensions: ['', '.web.js', '.js', '.json', '.less'],
    },
    module: {
        loaders: [
            // handle js/jsx files
            {
                test: /\.jsx?$/,
                loader: `react-hot!babel?${fs.readFileSync(__dirname + '/.babelrc', 'utf8')}`,
                exclude: /(node_modules|bower_components)/
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader!postcss-loader'
            },
            {
                test: /\.less$/,
                loader: 'style-loader!css-loader!postcss-loader!less-loader'
            },
            // images, copy directly. Consider embed them with url-loader?
            {
                test: /\.jpg|\.png|\.gif|\.svg|\.ico$/,
                loader: 'file-loader'
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('development')
            }
        }),
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, '../index.html')
        })
    ],
};
